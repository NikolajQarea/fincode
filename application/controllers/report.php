<?php

class Report extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('report_model');
		$this->load->model('form_model');
		$this->load->model('client_model');
		$this->load->model('user_model');
		$this->load->library('Utilities');
		require dirname(dirname(__FILE__)) . "/helpers/pdf/html2fpdf.php";
	}
	// ADD
	function add()
	{
		if ($this->session->userdata(ROLE_WRITE) || $this->session->userdata(ROLE_READ_ONLY))
		{
			$this->session->set_flashdata(REPORT_DATE, date(DATE_FORMAT));
			$this->session->set_flashdata(CLIENT_VIEW_CLIENT_NAME, PLEASE_SELECT_ID);
			$this->session->set_flashdata(TAX_FORM_ID, PLEASE_SELECT_ID);
			$this->session->set_flashdata(TAX_YEAR_ID, PLEASE_SELECT_ID);
			$this->session->set_flashdata(TAX_QUARTER_ID, PLEASE_SELECT_ID);
			$this->session->set_flashdata(TAX_MONTH_ID, PLEASE_SELECT_ID);
			$this->session->set_flashdata(CLIENT_VIEW_PARTNER_ID, PLEASE_SELECT_ID);
			$this->session->set_flashdata(CLIENT_VIEW_STAFF_ID, PLEASE_SELECT_ID);
			$this->session->set_flashdata(CLIENT_STATUS_ID, PLEASE_SELECT_ID);
			$this->session->set_flashdata(TAX_RETURN_STATUS_ID, PLEASE_SELECT_ID);
			$report['ClientNames'] = $this->utilities->get_drop_down_html('report', CLIENT_VIEW_CLIENT_NAME, $this->utilities->get_client_names(), CLIENT_VIEW_CLIENT_ID, CLIENT_VIEW_CLIENT_NAME, PLEASE_SELECT_ID, $isFilter = false, $isMulti = false, $size = 1);
			$report['TaxForms'] = $this->utilities->get_drop_down_html('report', TAX_FORM_ID, $this->utilities->get_tax_forms(), TAX_FORM_ID, TAX_FORM, PLEASE_SELECT_ID, $isFilter = false, $isMulti = false, $size = 1);
			$report['TaxMonths'] = $this->utilities->get_drop_down_html('report', TAX_MONTH_ID , $this->utilities->get_tax_months(), TAX_MONTH_ID , TAX_MONTH, PLEASE_SELECT_ID, $isFilter = false, $isMulti = false, $size = 1);
			$report['TaxQuarters'] = $this->utilities->get_drop_down_html('report', TAX_QUARTER_ID , $this->utilities->get_tax_quarters(), TAX_QUARTER_ID , TAX_QUARTER, PLEASE_SELECT_ID, $isFilter = false, $isMulti = false, $size = 1);
			$report['TaxYears'] = $this->utilities->get_drop_down_html('report', TAX_YEAR_ID, $this->utilities->get_tax_years(), TAX_YEAR_ID, TAX_YEAR, PLEASE_SELECT_ID, $isFilter = false, $isMulti = false, $size = 1);
			$report['Partners'] = $this->utilities->get_drop_down_html('report', CLIENT_VIEW_PARTNER_ID, $this->utilities->get_partners(), STAFF_ID, STAFF_INITIALS, PLEASE_SELECT_ID, $isFilter = false, $isMulti = false, $size = 1);
			$report['Staff'] = $this->utilities->get_drop_down_html('report', CLIENT_VIEW_STAFF_ID, $this->utilities->get_staff(), STAFF_ID, STAFF_INITIALS, PLEASE_SELECT_ID, $isFilter = false, $isMulti = false, $size = 1);
			$report['ClientStatus'] = $this->utilities->get_drop_down_html('report', CLIENT_STATUS_ID, $this->utilities->get_client_status(), CLIENT_STATUS_ID, CLIENT_STATUS, PLEASE_SELECT_ID, $isFilter = false, $isMulti = false, $size = 1);
			$report['ReturnStatus'] = $this->utilities->get_drop_down_html('report', TAX_RETURN_STATUS_ID, $this->utilities->get_tax_return_status(), TAX_RETURN_STATUS_ID, TAX_RETURN_STATUS, PLEASE_SELECT_ID, $isFilter = false, $isMulti = false, $size = 1);
			$this->load->view(HEADER);
			$this->load->view(MENU);
			$this->load->view('report/add', $report);
			$this->load->view(FOOTER);
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	function generate_report($order = CLIENT_VIEW_CLIENT_NAME, $as = SORT_ORDER)
	{
		$report = $this->get_report($order, $as);
		//$report = array_merge($data, $report);
		$this->load->view(HEADER);
		$this->load->view(MENU);
		$this->load->view('report/view', $report);
		$this->load->view(FOOTER);
	}
	function get_report($order = CLIENT_VIEW_CLIENT_NAME, $as = SORT_ORDER)
	{
		if ($this->session->userdata(ROLE_WRITE) || $this->session->userdata(ROLE_READ_ONLY))
		{
			if (isset($_POST['clear']))
			{
				$this->session->unset_userdata('report_filter');
				return $this->add();
			}
			else
			{
				if (isset($_POST['report']))
				{
					$arr = $_POST['report'];
					$post['order'] = $order;
					$post['as'] = $as;
					$this->session->set_userdata('report_filter', $arr);
				}
			}
			$post = ($this->session->userdata('report_filter')) ? $this->session->userdata('report_filter') : '';
			$newreport = array();
			$newreport['order'] = $order;
			$newreport['as'] = $as;
			// Validate Report Date
			if(isset($post[REPORT_DATE]))
			{
				$newreport[REPORT_DATE] = stripslashes(trim($post[REPORT_DATE]));
				$this->session->set_flashdata(REPORT_DATE, $newreport[REPORT_DATE]);
			}
			else
			{
				$this->session->set_flashdata(REPORT_DATE, date(DATE_FORMAT));
			}
			// Validate Client
			if(isset($post[CLIENT_VIEW_CLIENT_NAME]))
			{
				$this->session->set_flashdata(CLIENT_VIEW_CLIENT_NAME, $post[CLIENT_VIEW_CLIENT_NAME]);
				$newreport[CLIENT_VIEW_CLIENT_NAME] = $post[CLIENT_VIEW_CLIENT_NAME] != PLEASE_SELECT_ID ? $post[CLIENT_VIEW_CLIENT_NAME] : '';
			}
			else
			{
				$this->session->set_flashdata(CLIENT_VIEW_CLIENT_NAME, PLEASE_SELECT_ID);
			}
			// Validate Tax Form
			if(isset($post[TAX_FORM_ID]))
			{
				$this->session->set_flashdata(TAX_FORM_ID, $post[TAX_FORM_ID]);
				$newreport[TAX_FORM_ID] =  $post[TAX_FORM_ID] != PLEASE_SELECT_ID ? $post[TAX_FORM_ID] : '';
			}
			else
			{
				$this->session->set_flashdata(TAX_FORM_ID, PLEASE_SELECT_ID);
			}
			// Validate Tax Year
			if(isset($post[TAX_YEAR_ID]))
			{
				$this->session->set_flashdata(TAX_YEAR_ID, $post[TAX_YEAR_ID]);
				$newreport[TAX_YEAR_ID] =  $post[TAX_YEAR_ID] != PLEASE_SELECT_ID ? $post[TAX_YEAR_ID] : '';
			}
			else
			{
				$this->session->set_flashdata(TAX_YEAR_ID, PLEASE_SELECT_ID);
			}
			// Validate Tax Quarter
			if(isset($post[TAX_QUARTER_ID]))
			{
				$this->session->set_flashdata(TAX_QUARTER_ID, $post[TAX_QUARTER_ID]);
				$newreport[TAX_QUARTER_ID] =  $post[TAX_QUARTER_ID] != PLEASE_SELECT_ID ? $post[TAX_QUARTER_ID] : '';
			}
			else
			{
				$this->session->set_flashdata(TAX_QUARTER_ID, PLEASE_SELECT_ID);
			}
			// Validate Tax Month
			if(isset($post[TAX_MONTH_ID]))
			{
				$this->session->set_flashdata(TAX_MONTH_ID, $post[TAX_MONTH_ID]);
				$newreport[TAX_MONTH_ID] =  $post[TAX_MONTH_ID] != PLEASE_SELECT_ID ? $post[TAX_MONTH_ID] : '';
			}
			else
			{
				$this->session->set_flashdata(TAX_MONTH_ID, PLEASE_SELECT_ID);
			}
			// Validate Client Partner
			if(isset($post[CLIENT_VIEW_PARTNER_ID]))
			{
				$this->session->set_flashdata(CLIENT_VIEW_PARTNER_ID, $post[CLIENT_VIEW_PARTNER_ID]);
				$newreport[CLIENT_VIEW_PARTNER_ID] =  $post[CLIENT_VIEW_PARTNER_ID] != PLEASE_SELECT_ID ? $post[CLIENT_VIEW_PARTNER_ID] : '';
			}
			else
			{
				$this->session->set_flashdata(CLIENT_VIEW_PARTNER_ID, PLEASE_SELECT_ID);
			}
			// Validate Prepared By
			if(isset($post[CLIENT_VIEW_STAFF_ID]))
			{
				$this->session->set_flashdata(CLIENT_VIEW_STAFF_ID, $post[CLIENT_VIEW_STAFF_ID]);
				$newreport[CLIENT_VIEW_STAFF_ID] =  $post[CLIENT_VIEW_STAFF_ID] != PLEASE_SELECT_ID ? $post[CLIENT_VIEW_STAFF_ID] : '';
			}
			else
			{
				$this->session->set_flashdata(CLIENT_VIEW_STAFF_ID, PLEASE_SELECT_ID);
			}
			// Validate Client Status
			if(isset($post[CLIENT_STATUS_ID]))
			{
				$this->session->set_flashdata(CLIENT_STATUS_ID, $post[CLIENT_STATUS_ID]);
				$newreport[CLIENT_STATUS_ID] =  $post[CLIENT_STATUS_ID] != PLEASE_SELECT_ID ? $post[CLIENT_STATUS_ID] : '';
			}
			else
			{
				$this->session->set_flashdata(CLIENT_STATUS_ID, PLEASE_SELECT_ID);
			}
			// Validate Tax Return Status
			if(isset($post[TAX_RETURN_STATUS_ID]))
			{
				$this->session->set_flashdata(TAX_RETURN_STATUS_ID, $post[TAX_RETURN_STATUS_ID]);
				$newreport[TAX_RETURN_STATUS_ID] =  $post[TAX_RETURN_STATUS_ID] != PLEASE_SELECT_ID ? $post[TAX_RETURN_STATUS_ID] : '';
			}
			else
			{
				$this->session->set_flashdata(TAX_RETURN_STATUS_ID, PLEASE_SELECT_ID);
			}
			// Validate Exclude Sent to Client Filter
			if(isset($post['Exclude_Sent']))
			{
				$newreport['Exclude_Sent'] = 1;
				$this->session->set_flashdata('Exclude_Sent', 1);
			}
			else
			{
				if(isset($newreport['Exclude_Sent']))
				{
					unset($newreport['Exclude_Sent']);
				}
				$this->session->set_flashdata('Exclude_Sent', 0);
			}
			$this->session->set_userdata('report_filter', $newreport);
			$reports = $this->report_model->get_report($newreport);
			foreach ($reports as $key => &$rep)
			{
				$dueDate = $this->select_due_date($rep[TAX_FORM_ID], $newreport[REPORT_DATE]);
				if ($dueDate == NULL)
				{
					unset($reports[$key]);
				}
				else
				{
					$rep['Due_Date'] = $dueDate;
				}
			}
			$report['order'] = $order;
			$report['as'] = $as;
			$report['ClientNames'] = $this->utilities->get_drop_down_html('report', CLIENT_VIEW_CLIENT_NAME, $this->utilities->get_client_names(), CLIENT_VIEW_CLIENT_ID, CLIENT_VIEW_CLIENT_NAME, isset($newreport[CLIENT_VIEW_CLIENT_NAME]) ? $newreport[CLIENT_VIEW_CLIENT_NAME] : PLEASE_SELECT_ID, $isFilter = false, $isMulti = false, $size = 1);
			$report['TaxForms'] = $this->utilities->get_drop_down_html('report', TAX_FORM_ID, $this->utilities->get_tax_forms(), TAX_FORM_ID, TAX_FORM, isset($newreport[TAX_FORM_ID]) ? $newreport[TAX_FORM_ID] : PLEASE_SELECT_ID, $isFilter = false, $isMulti = false, $size = 1);
			$report['TaxMonths'] = $this->utilities->get_drop_down_html('report', TAX_MONTH_ID , $this->utilities->get_tax_months(), TAX_MONTH_ID , TAX_MONTH, isset($newreport[TAX_MONTH_ID]) ? $newreport[TAX_MONTH_ID] : PLEASE_SELECT_ID, $isFilter = false, $isMulti = false, $size = 1);
			$report['TaxQuarters'] = $this->utilities->get_drop_down_html('report', TAX_QUARTER_ID , $this->utilities->get_tax_quarters(), TAX_QUARTER_ID , TAX_QUARTER, isset($newreport[TAX_QUARTER_ID]) ? $newreport[TAX_QUARTER_ID] : PLEASE_SELECT_ID, $isFilter = false, $isMulti = false, $size = 1);
			$report['TaxYears'] = $this->utilities->get_drop_down_html('report', TAX_YEAR_ID, $this->utilities->get_tax_years(), TAX_YEAR_ID, TAX_YEAR, isset($newreport[TAX_YEAR_ID]) ? $newreport[TAX_YEAR_ID] : PLEASE_SELECT_ID, $isFilter = false, $isMulti = false, $size = 1);
			$report['Partners'] = $this->utilities->get_drop_down_html('report', CLIENT_VIEW_PARTNER_ID, $this->utilities->get_partners(), STAFF_ID, STAFF_INITIALS, isset($newreport[CLIENT_VIEW_PARTNER_ID]) ? $newreport[CLIENT_VIEW_PARTNER_ID] : PLEASE_SELECT_ID, $isFilter = false, $isMulti = false, $size = 1);
			$report['Staff'] = $this->utilities->get_drop_down_html('report', CLIENT_VIEW_STAFF_ID, $this->utilities->get_staff(), STAFF_ID, STAFF_INITIALS, isset($newreport[CLIENT_VIEW_STAFF_ID]) ? $newreport[CLIENT_VIEW_STAFF_ID] : PLEASE_SELECT_ID, $isFilter = false, $isMulti = false, $size = 1);
			$report['ClientStatus'] = $this->utilities->get_drop_down_html('report', CLIENT_STATUS_ID, $this->utilities->get_client_status(), CLIENT_STATUS_ID, CLIENT_STATUS, isset($newreport[CLIENT_STATUS_ID]) ? $newreport[CLIENT_STATUS_ID] : PLEASE_SELECT_ID, $isFilter = false, $isMulti = false, $size = 1);
			$report['ReturnStatus'] = $this->utilities->get_drop_down_html('report', TAX_RETURN_STATUS_ID, $this->utilities->get_tax_return_status(), TAX_RETURN_STATUS_ID, TAX_RETURN_STATUS, isset($newreport[TAX_RETURN_STATUS_ID]) ? $newreport[TAX_RETURN_STATUS_ID] : PLEASE_SELECT_ID, $isFilter = false, $isMulti = false, $size = 1);
			$report['reports'] = $reports;
			return $report;
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	function select_due_date($tax_form_id, $report_date = NULL)
	{
		if ($this->session->userdata(ROLE_WRITE) || $this->session->userdata(ROLE_READ_ONLY))
		{
			if ($report_date == NULL)
			{
				$report_date = date('m/d/Y');
			}
			$report_time = strtotime(substr($report_date, 0, 5));
			if ($tax_form_id == 16)
			{
				$tax_form_dates = $this->report_model->get_single_date($tax_form_id);
				if (empty($form_dates))
				{
					return NULL;
				}
				$array_dates = array();
				$array_due_dates = array();
				foreach ($tax_form_dates as $date)
				{
					$array_dates[] = strtotime($date['date']) - $report_time;
					$array_due_dates[] = strtotime($date['date']);
				}
				if (!empty($array_dates))
				{
					$min = max($array_dates);
					$min_id = -1;
					for ($i = 0; $i < count($array_dates); $i++)
					{
						if ($array_dates[$i] >= 0 && $array_dates[$i] <= $min)
						{
							$min = $array_dates[$i];
							$min_id = $i;
						}
					}
					if ($min_id == -1)
					{
						return date('m/j', min($array_due_dates)) . '/' . ((int) date('y') + 1);
					}
					return $form_dates[$min_id][TAX_FORM_INITIAL_DUE_DATE_DATE] . (int) date('y');
				}
			}
			$form_desc = $this->report_model->get_tax_form_details($tax_form_id);
			$array_types = array();
			if (isset($form_desc[TAX_FORM_INITIAL_DUE_DATE]))
			{
				$array_types[TAX_FORM_INITIAL_DUE_DATE_TYPE_INITIAL_DATE] = TAX_FORM_INITIAL_DUE_DATE_TYPE_INITIAL_DATE;
			}
			if (isset($form_desc[TAX_FORM_EXTENTION_1]))
			{
				$array_types[TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_1] = TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_1;
			}
			if (isset($form_desc[TAX_FORM_EXTENTION_2]))
			{
				$array_types[TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_2] = TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_2;
			}
			$form_dates = $this->report_model->get_form_dates($tax_form_id, $array_types);
			if (empty($form_dates))
			{
				return NULL;
			}
			$array_dates = array();
			$array_due_dates = array();
			foreach ($form_dates as $date)
			{
				$array_dates[] = strtotime($date[TAX_FORM_INITIAL_DUE_DATE_DATE]) - $report_time;
				$array_due_dates[] = strtotime($date[TAX_FORM_INITIAL_DUE_DATE_DATE]);
			}
			if (!empty($array_dates))
			{
				$min = max($array_dates);
				$min_id = -1;
				for ($i = 0; $i < count($array_dates); $i++)
				{
					if ($array_dates[$i] >= 0 && $array_dates[$i] <= $min)
					{
						$min = $array_dates[$i];
						$min_id = $i;
					}
				}
				if (date('y', strtotime($report_date)) < ((int) date('y')))
				{
					return date('m/j', min($array_due_dates)) . '/' . ((int) date('y'));
				}
				else if (date('y', strtotime($report_date)) > ((int) date('y')))
				{
					return date('m/j', max($array_due_dates)) . '/' . ((int) date('y'));
				}
				if ($min_id == -1)
				{
					return date('m/j', max($array_due_dates)) . '/' . ((int) date('y'));
				}
				$result = date('m/d/y', strtotime($form_dates[$min_id][TAX_FORM_INITIAL_DUE_DATE_DATE] . '/' . (int) date('y')));
				return $result;
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	function convertToExcel($order = CLIENT_VIEW_CLIENT_NAME, $as = SORT_ORDER)
	{
		if ($this->session->userdata(ROLE_WRITE) || $this->session->userdata(ROLE_READ_ONLY))
		{
		
			$reports = $this->get_report($order, $as);
			$data = $this->session->userdata('report_filter');
			$data[CLIENT_VIEW_PARTNER_INITIALS] = $this->utilities->get_staff_initials($data[CLIENT_VIEW_PARTNER_ID]);
			$data[CLIENT_VIEW_STAFF_INITIALS] = $this->utilities->get_staff_initials($data[CLIENT_VIEW_STAFF_ID]);
			$data[TAX_FORM] = $this->utilities->get_tax_form_name($data[TAX_FORM_ID]);

			$output = "Tax Control Report\nTax Form\t"
				. $data[TAX_FORM] 
				. "\nTax Year\t" 
				. $data[TAX_YEAR_ID] 
				. "\nTax Quarter\t" 
				. $data[TAX_QUARTER_ID] 
				. "\nTax Year Begins\t" 
				. $data[TAX_MONTH_ID] 
				. "\nPartner\t" 
				. $data[CLIENT_VIEW_PARTNER_INITIALS] 
				. "\nStaff\t" 
				. $data[CLIENT_VIEW_STAFF_INITIALS] 
				. "\nReport Date\t" 
				. $data[REPORT_DATE] 
				. "\n\n";
			$output .= "Client Name\tTax Form\tTax Year\tTax Quarter\tTax Month\tStates\tPartners\tDue Date\tStaff\tStatus\n";
			foreach ($reports['reports'] as $report)
			{
				if(count(explode(',', $report[TAX_RETURN_VIEW_CLIENT_STATES])) > 1)
				{
					if ($report[TAX_RETURN_VIEW_TAX_YEAR] == 1970)
					{
						$Tax_Year = NOT_AVAILABLE;
					}
					else
					{
						$Tax_Year = $report[TAX_RETURN_VIEW_TAX_YEAR];
					}
					$states = explode(',', $report[TAX_RETURN_VIEW_CLIENT_STATES]);
					$output .= stripslashes($report[TAX_RETURN_VIEW_CLIENT_NAME]) 
						. "\t" . $report[TAX_RETURN_VIEW_TAX_FORM] 
						. "\t" . $Tax_Year 
						. "\t" . $report[TAX_RETURN_VIEW_TAX_QUARTER] 
						. "\t" . $report[TAX_RETURN_VIEW_TAX_MONTH] 
						. "\t" . $states[0] 
						. "\t" . $report[TAX_RETURN_VIEW_PARTNER_INITIALS] 
						. "\t" . date(DATE_FORMAT, strtotime($report['Due_Date'])) 
						. "\t" . $report[TAX_RETURN_VIEW_STAFF_INITIALS] 
						. "\t" . $report[TAX_RETURN_VIEW_TAX_RETURN_STATUS] 
						. "\t" . "\n";
					for ($i = 1; $i < count($states); $i++)
					{
						$output .= "\t\t\t\t\t" . $states[$i] . "\t\t\t\t\n";
					}
				}
				else
				{
					$output .= $report[TAX_RETURN_VIEW_CLIENT_NAME] 
					. "\t" . $report[TAX_RETURN_VIEW_TAX_FORM]
					. "\t" . $report[TAX_RETURN_VIEW_TAX_YEAR] 
					. "\t" . $report[TAX_RETURN_VIEW_TAX_QUARTER_ID] 
					. "\t" . $report[TAX_RETURN_VIEW_TAX_MONTH_ID] 
					. "\t" . $report[TAX_RETURN_VIEW_CLIENT_STATES] 
					. "\t" . $report[TAX_RETURN_VIEW_PARTNER_INITIALS] 
					. "\t" . date(DATE_FORMAT, strtotime($report['Due_Date'])) 
					. "\t" . $report[TAX_RETURN_VIEW_STAFF_INITIALS] 
					. "\t" . $report[TAX_RETURN_VIEW_TAX_RETURN_STATUS] 
					. "\t" . "\n";
				}
			}
			$output .= "\nReport Date\t" . date('H:i:s m/d/Y');
			header("Content-type: application/vnd.ms-excel");
			header('Content-disposition: attachment; filename="GGG_AdHoc_Report_Printed_For_' . $this->session->userdata(STAFF_LOGIN) . '_On_' . date("Y-m-d") . '.xls"');
			print $output;
			exit;
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	function convertToPdf($order = CLIENT_VIEW_CLIENT_NAME, $as = SORT_ORDER)
	{
		if ($this->session->userdata(ROLE_WRITE) || $this->session->userdata(ROLE_READ_ONLY))
		{
			$reports = $this->get_report($order, $as);
			$data['data'] = $this->session->userdata('report_filter');
			$data['reports'] = $reports['reports'];
			$data['data'][CLIENT_VIEW_PARTNER_INITIALS] = $this->utilities->get_staff_initials($data['data'][CLIENT_VIEW_PARTNER_ID]);
			$data['data'][CLIENT_VIEW_STAFF_INITIALS] = $this->utilities->get_staff_initials($data['data'][CLIENT_VIEW_STAFF_ID]);
			$data['data'][TAX_FORM] = $this->utilities->get_tax_form_name($data['data'][TAX_FORM_ID]);
			$content = $this->load->view('report/view_pdf', $data, TRUE);
			$content = stripslashes($content);
			$pdf = new HTML2FPDF('P', 'mm', 'a3');
			$pdf->AddPage();
			$pdf->WriteHTML($content);
			$pdf->Output('GGG_AdHoc_Report_Printed_For_' . $this->session->userdata(STAFF_LOGIN) . '_On_' . date("Y-m-d") . '.pdf', 'D');

			exit;
			//
			//$this->load->view(MENU);
			//$this->load->view('report/view_pdf', $data);
			//;

		}
		else
		{
			header("Location: " . base_url());
		}
	}
	function printReport()
	{
		if ($this->session->userdata(ROLE_WRITE) || $this->session->userdata(ROLE_READ_ONLY))
		{
			$reports = $this->get_report();
			$data['reports'] = $reports;
			$data['report_data'] = $this->get_report_criteria();
			$content = $this->load->view('report/view_print', $data, TRUE);
			echo $content;
			exit;
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	/*
	function get_report_criteria()
	{
		if ($this->session->userdata(ROLE_WRITE) || $this->session->userdata(ROLE_READ_ONLY))
		{
			$criteria = array();
			$criteria[REPORT_DATE] = $_SESSION['previous_post'][REPORT_DATE];
			$criteria[CLIENT_VIEW_CLIENT_NAME] = $_SESSION['previous_post'][CLIENT_VIEW_CLIENT_NAME] == 'All' ? 'All' : $this->client_model->get_Client_Name($_SESSION['previous_post'][CLIENT_VIEW_CLIENT_NAME]);
			$tmp_array = $_SESSION['previous_post'][TAX_FORM_ID] == 'All' ? array(array('Tax_Form_Name' => 'All')) : $this->report_model->get_form_name_by_id($_SESSION['previous_post'][TAX_FORM_ID]);
			$form_array = array();
			foreach ($tmp_array as $form)
			{
				$form_array[] = $form['form_name'];
			}
			$criteria['Tax_Form'] = implode(',', $form_array);
			$criteria[TAX_YEAR_ID] = $_SESSION['previous_post'][TAX_YEAR_ID];
			$criteria[TAX_QUARTER_ID] = $_SESSION['previous_post'][TAX_QUARTER_ID];
			$criteria[TAX_MONTH_ID] = $_SESSION['previous_post'][TAX_MONTH_ID];
			$criteria[CLIENT_VIEW_PARTNER_ID] = $_SESSION['previous_post'][CLIENT_VIEW_PARTNER_ID];
			$criteria[CLIENT_VIEW_STAFF_ID] = $_SESSION['previous_post'][CLIENT_VIEW_STAFF_ID];
			return $criteria;
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	*/
}
 ?>
