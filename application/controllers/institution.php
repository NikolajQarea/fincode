<?php
class Institution extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Institution_model');
		$this->load->library('Utilities');
	}
	// Delete Bank/Broker
	function delete_institution($id)
	{
		$this->Institution_model->delete_institution($id);
		$this->session->set_flashdata(STATUS_MESSAGE, 'The Bank/Broker was deleted successfully.');
		header("Location: ".base_url()."institution/view");
	}
	// View Mode
	function view($page='', $order=INSTITUTION_VIEW_NAME, $as = 'ASC')
	{
		try
		{
			if($this->session->userdata(ROLE_ADMIN) ||$this->session->userdata(ROLE_WRITE) || $this->session->userdata(ROLE_READ_ONLY) )
			{
				// if (isset($_POST['clear']) || (!$this->session->userdata('institutions_filter')))
				if (!$this->session->userdata('institutions_filter'))
				{
					$institutions = array('page' => 0);
				}
				// elseif (isset($_POST[TAX_RETURN_POST]))
				// {
				// 	$institutions = $_POST[TAX_RETURN_POST];
				// 	$institutions['page'] = 0;
				// 	$institutions['order'] = $order;
				// 	$institutions['as'] = $as;
				// 	$this->session->set_userdata('institutions_filter', $institutions);
				// } 
				else
				{
					$institutions = $this->session->userdata('institutions_filter');
					$institutions['page'] = ($page != '') ? $page : $institutions['page'];
				}

				$this->session->set_userdata('institutions_filter', $institutions);

				$config['base_url'] 		= base_url().'institution/view/';
				$config['total_rows'] 		= $this->Institution_model->get_all();
				$config['cur_page']         = $institutions['page'];
				$config['per_page'] 		= PAGING_RECORDS_PER_PAGE;
				$config['num_links'] 		= PAGING_NUMBER_LINKS;
				$config['uri_segment'] 		= PAGING_URI_SEGMENTS;
				$config['full_tag_open'] 	= PAGING_FULL_OPEN_TAG;
				$config['full_tag_close'] 	= PAGING_FULL_CLOSE_TAG;
				$config['first_link'] 		= PAGING_FIRST;
				$config['last_link'] 		= PAGING_LAST;
				$config['next_link'] 		= PAGING_NEXT;
				$config['prev_link'] 		= PAGING_PREVIOUS;
				$config['cur_tag_open'] 	= PAGING_CURRENT_OPEN_TAG;
				$config['cur_tag_close'] 	= PAGING_CURRENT_CLOSE_TAG;
				$this->pagination->initialize($config);
				$institution['link'] 				= $this->pagination->create_links();
				$institution['page'] 				= $institutions['page'];
				$institution['as'] 				= $as;
				$institution['order'] 			= $order;
				$institution['list'] = $this->Institution_model->view($institutions['page'], PAGING_RECORDS_PER_PAGE, $order , $as);
				$this->load->view(HEADER);
				$this->load->view(MENU);
				$this->load->view('institution/view', $institution);
				$this->load->view(FOOTER);
			}
			else
			{
				header("Location: ".base_url());
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
	}
	// Export to Excel
	function convertToExcel($page=0, $count=0, $order=INSTITUTION_VIEW_NAME, $as = 'ASC')
	{
		try
		{

			if ($this->session->userdata(ROLE_WRITE) || $this->session->userdata(ROLE_READ_ONLY))
			{
				$institution['list'] = $this->Institution_model->view($page, $count, $order , $as);;
				$output = 
					"Institution ID\t" .
					"Institution Name\t" .
					"Address (1)\t" .
					"Address (2)\t" .
					"City\t" .
					"State\t" .
					"Postal Code\t" .
					"Contact Name\t" .
					"Telephone #\t" .
					"ABA\t" .
					"Created On\t" .
					"Created By\t" .
					"Last Edited On\t" .
					"Last Edited By\t" .
					"Times In Use\t\n";
				foreach ($institution['list'] as $key => $v)
				{
					if(!isset($v[INSTITUTION_VIEW_ID]) || $v[INSTITUTION_VIEW_ID] == '')
					{
						$v[INSTITUTION_VIEW_ID] = NOT_AVAILABLE;
					}
					if (!isset($v[INSTITUTION_VIEW_NAME]) || $v[INSTITUTION_VIEW_NAME] == '')
					{
						$v[INSTITUTION_VIEW_NAME] = NOT_AVAILABLE;
					}
					if (!isset($v[INSTITUTION_VIEW_LAST_EDIT_USER_INITIALS]) || $v[INSTITUTION_VIEW_LAST_EDIT_USER_INITIALS] == '')
					{
						$v[INSTITUTION_VIEW_LAST_EDIT_USER_INITIALS] = NOT_AVAILABLE;
					}
					if ($v[INSTITUTION_VIEW_LAST_EDIT_DATE] > 0)
					{
						$last_edit_date = date(DATE_FORMAT, $v[INSTITUTION_VIEW_LAST_EDIT_DATE]);
					}
					else
					{
						$last_edit_date = NOT_AVAILABLE;
					}
					if (!isset($v[INSTITUTION_VIEW_STAMP_USER_INITIALS]) || $v[INSTITUTION_VIEW_STAMP_USER_INITIALS] == '')
					{
						$v[INSTITUTION_VIEW_STAMP_USER_INITIALS] = NOT_AVAILABLE;
					}
					if ($v[INSTITUTION_VIEW_STAMP_DATE] > 0)
					{
						$stamp_date = date(DATE_FORMAT, $v[INSTITUTION_VIEW_STAMP_DATE]);
					}
					else
					{
						$stamp_date = NOT_AVAILABLE;
					}
					$output .= 
						$v[INSTITUTION_VIEW_ID] . "\t" .
						$v[INSTITUTION_VIEW_NAME] . "\t" .
						$v[INSTITUTION_VIEW_ADDRESS_1] . "\t" .
						$v[INSTITUTION_VIEW_ADDRESS_2] . "\t" .
						$v[INSTITUTION_VIEW_CITY] . "\t" .
						$v[INSTITUTION_VIEW_STATE_ID] . "\t" .
						$v[INSTITUTION_VIEW_POSTAL_CODE] . "\t" .
						$v[INSTITUTION_VIEW_CONTACT] . "\t" .
						$v[INSTITUTION_VIEW_TELEPHONE] . "\t" .
						$v[INSTITUTION_ABA] . "\t" .
						$stamp_date . "\t" . 
						$v[INSTITUTION_VIEW_STAMP_USER_INITIALS] . "\t" .
						$last_edit_date . "\t" .
						$v[INSTITUTION_VIEW_LAST_EDIT_USER_INITIALS] . "\t" .
						$v[INSTITUTION_TOTAL_TIMES_USED] . "\t\n";
				}
				$output .= "\nUser Created\t" . $this->session->userdata(STAFF_LOGIN) . "\nDate Created\t" . date('H:i:s m/d/Y');
				header("Content-type: application/vnd.ms-excel");
				header('Content-disposition: attachment; filename="GGG_Bank_&_Brokers_Report_Printed_For_' . $this->session->userdata(STAFF_LOGIN) . '_On_' . date("Y-m-d") . '.xls"');
				print $output;
			}
			else
			{
				header("Location: " . base_url());
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
	}
	// Add Mode
	function add($institution_id = 0)
	{
		try
		{
			if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
			{
				if($institution_id > 0)
				{
					$institution['data'] = $this->Institution_model->get_institution($institution_id);
					$institution['States'] = $this->utilities->get_drop_down_html('institution', INSTITUTION_STATE_ID, $this->utilities->get_states(), STATE_ID, STATE, $institution['data'][INSTITUTION_VIEW_STATE_ID], false, false, 1);
				}
				else
				{
					$institution['States'] = $this->utilities->get_drop_down_html('institution', INSTITUTION_STATE_ID, $this->utilities->get_states(), STATE_ID, STATE, PLEASE_SELECT_ID, false, false, 1);
				}
				
				$this->load->view(HEADER);
				$this->load->view(MENU);
				if($institution_id > 0)
				{
					$this->load->view('institution/edit', $institution);
				}
				else
				{
					$this->load->view('institution/add', $institution);
				}
				$this->load->view(FOOTER);
			}
			else
			{
				header("Location: ".base_url());
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
	}
	// Edit Mode - Piggyback off Add() to reuse common code
	function edit($institution_id)
	{
		$this->add($institution_id);
	}
	// Insert a new Bank/Broker or Updating an existing one.
	function insert_institution($institution_id = 0, $isEdit = false)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$isValid = true;
			try
			{
				if(isset($_POST['institution']))
				{
					$institution = $_POST['institution'];
					// Clear out Watermarks
					// Bank/Broker Name
					if(isset($institution[INSTITUTION_NAME]) && trim($institution[INSTITUTION_NAME]) == JS_INSTITUTION_NAME)
					{
						$institution[INSTITUTION_NAME] = '';
					}
					else
					{
						$institution[INSTITUTION_NAME] = stripslashes(trim($institution[INSTITUTION_NAME]));
					}
					$this->session->set_flashdata(INSTITUTION_NAME, $institution[INSTITUTION_NAME]);
					// Address 1
					if(isset($institution[INSTITUTION_ADDRESS_1]) && trim($institution[INSTITUTION_ADDRESS_1]) == JS_INSTITUTION_ADDRESS_1)
					{
						$institution[INSTITUTION_ADDRESS_1] = '';
					}
					else
					{
						$institution[INSTITUTION_ADDRESS_1] = stripslashes(trim($institution[INSTITUTION_ADDRESS_1]));
					}
					$this->session->set_flashdata(INSTITUTION_ADDRESS_1, $institution[INSTITUTION_ADDRESS_1]);
					// Address 2
					if(isset($institution[INSTITUTION_ADDRESS_2]) && trim($institution[INSTITUTION_ADDRESS_2]) == JS_INSTITUTION_ADDRESS_2)
					{
						$institution[INSTITUTION_ADDRESS_2] = '';
					}
					else
					{
						$institution[INSTITUTION_ADDRESS_2] = stripslashes(trim($institution[INSTITUTION_ADDRESS_2]));
					}
					$this->session->set_flashdata(INSTITUTION_ADDRESS_2, $institution[INSTITUTION_ADDRESS_2]);
					// City
					if(isset($institution[INSTITUTION_CITY]) && trim($institution[INSTITUTION_CITY]) == JS_INSTITUTION_CITY)
					{
						$institution[INSTITUTION_CITY] = '';
					}
					else
					{
						$institution[INSTITUTION_CITY] = stripslashes(trim($institution[INSTITUTION_CITY]));
					}
					$this->session->set_flashdata(INSTITUTION_CITY, $institution[INSTITUTION_CITY]);
					// State
					if ($institution[INSTITUTION_STATE_ID] == PLEASE_SELECT_ID)
					{
						$institution[INSTITUTION_STATE_ID] = '';
					}
					$this->session->set_flashdata(INSTITUTION_STATE_ID, $institution[INSTITUTION_STATE_ID]);
					// Postal/Zip Code
					if(isset($institution[INSTITUTION_POSTAL_CODE]) && trim($institution[INSTITUTION_POSTAL_CODE]) == JS_INSTITUTION_POSTAL_CODE)
					{
						$institution[INSTITUTION_POSTAL_CODE] = '';
					}
					else
					{
						$institution[INSTITUTION_POSTAL_CODE] = stripslashes(trim($institution[INSTITUTION_POSTAL_CODE]));
					}
					$this->session->set_flashdata(INSTITUTION_POSTAL_CODE, $institution[INSTITUTION_POSTAL_CODE]);
					// Contact Name
					if(isset($institution[INSTITUTION_CONTACT]) && trim($institution[INSTITUTION_CONTACT]) == JS_INSTITUTION_CONTACT)
					{
						$institution[INSTITUTION_CONTACT] = '';
					}
					else
					{
						$institution[INSTITUTION_CONTACT] = stripslashes(trim($institution[INSTITUTION_CONTACT]));
					}
					$this->session->set_flashdata(INSTITUTION_CONTACT, $institution[INSTITUTION_CONTACT]);
					// Telephone Number
					if(isset($institution[INSTITUTION_TELEPHONE]) && trim($institution[INSTITUTION_TELEPHONE]) == JS_INSTITUTION_TELEPHONE)
					{
						$institution[INSTITUTION_TELEPHONE] = '';
					}
					else
					{
						$institution[INSTITUTION_TELEPHONE] = stripslashes(trim($institution[INSTITUTION_TELEPHONE]));
					}
					$this->session->set_flashdata(INSTITUTION_TELEPHONE, $institution[INSTITUTION_TELEPHONE]);
					// ABA
					if(isset($institution[INSTITUTION_ABA]) && trim($institution[INSTITUTION_ABA]) == JS_INSTITUTION_ABA)
					{
						$institution[INSTITUTION_ABA] = '';
					}
					else
					{
						$institution[INSTITUTION_ABA] = stripslashes(trim($institution[INSTITUTION_ABA]));
					}
					$this->session->set_flashdata(INSTITUTION_ABA, $institution[INSTITUTION_ABA]);
					//
					$this->session->set_flashdata(INSTITUTION_ID, $institution_id);
					// Validate Bank/Broker Name
					if($institution[INSTITUTION_NAME] == '')
					{
						$this->session->set_flashdata(ERROR_INSTITUTION_NAME, ERROR_STRING_INSTITUTION_NAME);
						$isValid = false;
					}
					if($isValid == true)
					{
						if($this->Institution_model->insert($institution, $institution_id, $isEdit))
						{
							$stat = 'inserted';
							if($isEdit)
							{
								$stat = 'updated';
							}
							$this->session->set_flashdata(STATUS_MESSAGE, 'The Bank/Broker " ' . $institution[INSTITUTION_NAME] . '" was ' . $stat . ' successfully.');
							header("Location: ".base_url()."institution/view");
						}
						else
						{
							$this->session->set_flashdata(ERROR_MESSAGE, 'The Bank/Broker "' . $institution[INSTITUTION_NAME] . '" already exists.');
							if($isEdit)
							{
								header("Location: ".base_url()."institution/edit/" .$institution_id);
							}
							else
							{
								header("Location: ".base_url()."institution/add");
							}
						}

					}
					else
					{
						if($isEdit)
						{
							header("Location: ".base_url()."institution/edit/" .$institution_id);
						}
						else
						{
							header("Location: ".base_url()."institution/add");
						}
					}
				}
				else
				{
					header("Location: ".base_url());
				}
			}
			catch(Exception $ex)
			{
				log_message('error',  $e->getMessage());
				show_error( $e->getMessage());
			}
		}
	}
	// Edit User.  Calls add_user to combine similiar logic into one procedure.
	function update_institution($institution_id)
	{
		$this->insert_institution($institution_id, true);
	}
}
?>