<?php

class Manage extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Client_model');
		$this->load->model('Tax_return_model');
		$this->load->model('form_model');
		$this->load->model('Manage_model');
	}
	// Page VIEW
	function view($id = 0)
	{
		$manage['Category_ID'] = $id;
		switch($id)
		{
			case 0:
				break;
			case CATEGORY_OCCUPATIONS:
				$manage['Occupations'] = $this->Manage_model->view_occupation();
				break;
			case CATEGORY_CLIENT_TYPE:
				$manage['Client_Types'] = $this->Manage_model->view_client_type();
				break;
			case CATEGORY_INSTRUCTIONS:
				$manage['Instructions'] = $this->Manage_model->view_instructions();
				break;
			case CATEGORY_REFERRED_BY:
				$manage['Referred_Bys'] = $this->Manage_model->view_referred_by();
				break;
			case CATEGORY_TAX_FORMS:
				$manage['Tax_Forms'] = $this->Manage_model->view_tax_form();
				break;
			case CATEGORY_TAX_RETURN_STATUS:
				$manage['Tax_Return_Statuses'] = $this->Manage_model->view_tax_return_status();
				break;
			case CATEGORY_RETAINER_STATUS:
				$manage['Retainer_Statuses'] = $this->Manage_model->view_retainer_status();
				break;
			case CATEGORY_STATEMENT_STATUS:
				$manage['Statement_Statuses'] = $this->Manage_model->view_statement_status();
				break;
			case CATEGORY_STATEMENT_TYPE:
				$manage['Statement_Types'] = $this->Manage_model->view_statement_type();
			default:
		}
		$this->load->view(HEADER);
		$this->load->view(MENU);
		$this->load->view('manage/view', $manage);
		$this->load->view(FOOTER);
	}
	//	OCCUPATIONS
	// Occupation: ADD
	function add_occupation()
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$this->load->view(HEADER);
			$this->load->view(MENU);
			$this->load->view('manage/add_occupation');
			$this->load->view(FOOTER);
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	function delete_occupation($id) 
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			if (!$this->Client_model->check_occupation_in_use($id))
			{
				$this->Manage_model->delete_occupation($id);
				$this->session->set_flashdata(STATUS_MESSAGE, 'Record deleted.');
				header("Location: " . base_url() . 'manage/view/' . CATEGORY_OCCUPATIONS);
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Cannot delete this occupation. It is currently in use.');
				header('Location: ' . base_url() . 'manage/edit_occupation/' . $id);
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// Occupations: EDIT
	function edit_occupation($id)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$occupations['data'] = $this->Manage_model->get_occupation($id);
			$occupations['type'] = 'Occupation';
			$occupations['delete'] = $this->Client_model->check_occupation_in_use($id);
			$this->load->view(HEADER);
			$this->load->view(MENU);
			$this->load->view('manage/edit_occupation', $occupations);
			$this->load->view(FOOTER);
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// Occupation: INSERT
	function insert_occupation()
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			if (isset($_POST['manage'][OCCUPATION]) && trim($_POST['manage'][OCCUPATION]) != JS_OCCUPATION_NAME)
			{
				$Occupation= trim($_POST['manage'][OCCUPATION]);
				if (!$this->Client_model->check_occupation_exists($Occupation, 0))
				{
					$id = $this->Client_model->add_occupation($Occupation);
					isset($id) && $id ? $message = 'The Occupation "' . $Occupation. '" has been created successfully.' : '';
					$this->session->set_flashdata(STATUS_MESSAGE, $message);
					header("Location: " .base_url() . "manage/view/" . CATEGORY_OCCUPATIONS);
				}
				else
				{
					$this->session->set_flashdata(ERROR_MESSAGE, 'The Occupation "' . $Occupation. '" is already in use.');
					$this->session->set_flashdata(OCCUPATION, $Occupation);
					header("Location: " . base_url() . "manage/add_occupation");
				}
			}
			else
			{
				$message = 'Please enter an Occupation.';
				$this->session->set_flashdata(ERROR_MESSAGE, $message);
				header("Location: " . base_url() . "manage/add_occupation");
				exit;
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// Occupation:  SET
	function update_occupation($id)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$this->session->set_flashdata('post', $_POST);
			if(isset($_POST['manage'][OCCUPATION]) && trim($_POST['manage'][OCCUPATION]) != JS_OCCUPATION_NAME)
			{
				$Occupation = trim($_POST['manage'][OCCUPATION]);
				if (!$this->Client_model->check_occupation_exists($Occupation, $id))
				{
					$this->Manage_model->update_occupation($id, $Occupation);
					$this->session->set_flashdata(STATUS_MESSAGE, 'The Occupation "' . $Occupation . '" was updated successfully.');
					header("Location: " . base_url() . 'manage/view/' . CATEGORY_OCCUPATIONS);
				}
				else
				{
					$this->session->set_flashdata(ERROR_MESSAGE, 'The Occupation "' . $Occupation . '" is already in use.');
					header('Location: ' . base_url() . 'manage/edit_occupation/' . $id);
				}
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Please enter an Occupation.');
				$this->session->set_flashdata(OCCUPATION, $Occupation);
				header("Location: " . base_url() . "manage/edit_occupation/" . $id);
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	//end Occupation
	// CLIENT TYPE
	// Client Type:  ADD
	function add_client_type()
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$this->load->view(HEADER);
			$this->load->view(MENU);
			$this->load->view('manage/add_client_type');
			$this->load->view(FOOTER);
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	function delete_client_type($id)
	{

		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			if (!$this->Client_model->check_client_type_in_use($id))
			{
				$this->Manage_model->delete_client_type($id);
				$this->session->set_flashdata(STATUS_MESSAGE, 'Record deleted.');
				header("Location: " . base_url() . 'manage/view/' . CATEGORY_CLIENT_TYPE);
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Cannot delete this occupation. It is currently in use.');
				header('Location: ' . base_url() . 'manage/edit_client_type/' . $id);
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// Client Type:  EDIT
	function edit_client_type($id)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$type['data'] = $this->Manage_model->get_client_type($id);
			$type['type'] = 'Client Type';
			$type['delete'] = $this->Client_model->check_client_type_in_use($id);
			$this->load->view(HEADER);
			$this->load->view(MENU);
			$this->load->view('manage/edit_client_type', $type);
			$this->load->view(FOOTER);
		}
		else
		{
			header('Location: ' . base_url());
		}
	}
	// Client Type:  INSERT
	function insert_client_type()
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			if (isset($_POST['manage'][CLIENT_TYPE]) && trim($_POST['manage'][CLIENT_TYPE]) != JS_CLIENT_TYPE)
			{
				$client_type = trim($_POST['manage'][CLIENT_TYPE]);
				if (!$this->Client_model->check_client_type_exists($client_type, 0))
				{
					$id = $this->Client_model->add_client_type($client_type);
					$this->session->set_flashdata(STATUS_MESSAGE, 'The Client Type "' . $client_type . '" was created successfully.');
					header('Location: ' . base_url() . 'manage/view/' . CATEGORY_CLIENT_TYPE);
				}
				else
				{
					$this->session->set_flashdata(ERROR_MESSAGE, ' The Client Type "' . $client_type . ' already exists.');
					header("Location: " . base_url() . 'manage/add_client_type');
				}
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Please enter a Client Type');
				header("Location: " . base_url() . 'manage/add_client_type');
			}
		}
		else
		{
			header('Location: ' . base_url());
		}
	}
	// Client Type: SET
	function update_client_type($id)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			if(isset($_POST['manage'][CLIENT_TYPE]) && trim($_POST['manage'][CLIENT_TYPE] != JS_CLIENT_TYPE))
			{
				$ClientType = trim($_POST['manage'][CLIENT_TYPE]);
				if (!$this->Client_model->check_client_type_exists($client_type, $id))
				{
					$this->Manage_model->update_client_type($id, $ClientType);
					$this->session->set_flashdata(STATUS_MESSAGE, 'The Client Type "' . $ClientType . '" was updated successfully.');
					header('Location: ' . base_url() . 'manage/view/' . CATEGORY_CLIENT_TYPE);
				}
				else
				{
					$this->session->set_flash(ERROR_MESSAGE, 'The Client Type " ' . $ClientType . '" is already in use.');
					header('Location: ' . base_url() . 'manage/edit_client_type/' . $id);
				}
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Please enter a Client Type');
				header('Location: ' . base_url() . 'manage/edit_client_type/' . $id);
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// END ENTITY TYPE 

	// INSTRUCTIONS
	// Instructions:  ADD
	function add_instructions()
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$this->load->view(HEADER);
			$this->load->view(MENU);
			$this->load->view('manage/add_instructions');
			$this->load->view(FOOTER);
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	function delete_instructions($id)
	{

		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			if (!$this->Tax_return_model->check_instructions_in_use($id))
			{
				$this->Manage_model->delete_instructions($id);
				$this->session->set_flashdata(STATUS_MESSAGE, 'Record deleted.');
				header("Location: " . base_url() . 'manage/view/' . CATEGORY_INSTRUCTIONS);
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Cannot delete this instruction. It is currently in use.');
				header('Location: ' . base_url() . 'manage/edit_instructions/' . $id);
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// Instructions:  EDIT
	function edit_instructions($id)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$type['data'] = $this->Manage_model->get_instructions($id);
			$type['type'] = 'Instructions';
			$type['delete'] = $this->Tax_return_model->check_instructions_in_use($id);
			$this->load->view(HEADER);
			$this->load->view(MENU);
			$this->load->view('manage/edit_instructions', $type);
			$this->load->view(FOOTER);
		}
		else
		{
			header('Location: ' . base_url());
		}
	}
	// Instructions:  INSERT
	function insert_instructions()
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			if (isset($_POST['manage'][INSTRUCTIONS]) && trim($_POST['manage'][INSTRUCTIONS]) != JS_INSTRUCTIONS)
			{
				$instructions = trim($_POST['manage'][INSTRUCTIONS]);
				if (!$this->Tax_return_model->check_instructions_exists($instructions, 0))
				{
					$id = $this->Tax_return_model->add_instructions($instructions);
					$this->session->set_flashdata(STATUS_MESSAGE, 'The Instruction "' . $instructions . '" was created successfully.');
					header('Location: ' . base_url() . 'manage/view/' . CATEGORY_INSTRUCTIONS);
				}
				else
				{
					$this->session->set_flashdata(ERROR_MESSAGE, ' The Instruction "' . $instructions . ' already exists.');
					header("Location: " . base_url() . 'manage/add_instructions');
				}
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Please enter an Instruction');
				header("Location: " . base_url() . 'manage/add_instructions');
			}
		}
		else
		{
			header('Location: ' . base_url());
		}
	}
	// Instructions: SET
	function update_instructions($id)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			if(isset($_POST['manage'][INSTRUCTIONS]) && trim($_POST['manage'][INSTRUCTIONS] != JS_INSTRUCTIONS))
			{
				$instructions = trim($_POST['manage'][INSTRUCTIONS]);
				if (!$this->Tax_return_model->check_instructions_exists($instructions, $id))
				{
					$this->Manage_model->update_instructions($id, $instructions);
					$this->session->set_flashdata(STATUS_MESSAGE, 'The Instruction"' . $instructions . '" was updated successfully.');
					header('Location: ' . base_url() . 'manage/view/' . CATEGORY_INSTRUCTIONS);
				}
				else
				{
					$this->session->set_flash(ERROR_MESSAGE, 'The Instruction " ' . $instructions . '" is already in use.');
					header('Location: ' . base_url() . 'manage/edit_instructions/' . $id);
				}
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Please enter an Instruction');
				header('Location: ' . base_url() . 'manage/edit_instructions/' . $id);
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// END INSTRUCTIONS 

	//BEGIN REFERRED BY
	function delete_referred_by($id)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			if (!$this->Client_model->check_referred_by_in_use($id))
			{
				$this->Manage_model->delete_referred_by($id);
				$this->session->set_flashdata(STATUS_MESSAGE, 'Record deleted.');
				header("Location: " . base_url() . 'manage/view/' . CATEGORY_REFERRED_BY);
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Cannot delete this "Referred By". It is currently in use.');
				header('Location: ' . base_url() . 'manage/edit_referred_by/' . $id);
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	function add_referred_by()
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$this->load->view(HEADER);
			$this->load->view(MENU);
			$this->load->view('manage/add_referred_by');
			$this->load->view(FOOTER);
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	function insert_referred_by()
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			if (isset($_POST['manager'][REFERRED_BY]) && trim($_POST['manager'][REFERRED_BY]) != JS_REFERRED_BY)
			{
				$referrence = $_POST['manager'][REFERRED_BY];
				if (!$this->Client_model->check_referred_by_exists($referrence, 0))
				{
					$id = $this->Client_model->add_referred_by($referrence);
					$this->session->set_flashdata(STATUS_MESSAGE, 'The Referred By "' . $referrence . '" was created successfully.');
					header("Location: " . base_url() . 'manage/view/' . CATEGORY_REFERRED_BY);
				}
				else
				{
					$message = 'The Referred By "' .  $referrence . ' is already in use.';
					$this->session->set_flashdata(ERROR_MESSAGE, $message);
					header("Location: " . base_url() . 'manage/add_referred_by');
				}
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Please enter a Referred by');
				header("Location: " . base_url() . 'manage/add_referred_by');
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	function edit_referred_by($id)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$referred['data'] = $this->Manage_model->get_referred_by($id);
			$referred['type'] = 'Referred by';
			$referred['delete'] = $this->Client_model->check_referred_by_in_use($id);
			$this->load->view(HEADER);
			$this->load->view(MENU);
			$this->load->view('manage/edit_referred_by', $referred);
			$this->load->view(FOOTER);
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	function update_referred_by($id)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			if(isset($_POST['manager'][REFERRED_BY]) && trim($_POST['manager'][REFERRED_BY]) != JS_REFERRED_BY)
			{
				$referred = trim($_POST['manager'][REFERRED_BY]);
				if(!$this->Client_model->check_referred_by_in_use($id))
				{
					$this->Manage_model->update_referred_by($id, $referred);
					$this->session->set_flashdata(STATUS_MESSAGE, 'The Referred By "'. $referred .'" was updated successfully.');
					header("Location: " . base_url() . 'manage/view/' . CATEGORY_REFERRED_BY);
				}
				else
				{
					$this->session->set_flashdata(ERROR_MESSAGE, 'The Referred By "' . $referred .'" is already in use.');
					header('Location: ' . base_url() . 'manage/edit_referred_by/' . $id);
				}
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Please enter a Referred by');
				header("Location: " . base_url() . 'manage/edit_referred_by/' . $id);
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// END REFERREN BY
	//BEGIN FORM
	function delete_tax_form($id)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			if (!$this->Client_model->check_tax_form_in_use($id))
			{
				$this->Manage_model->delete_tax_form($id);
				$this->session->set_flashdata(STATUS_MESSAGE, 'Record deleted.');
				header("Location: " . base_url() . 'manage/view/' . CATEGORY_TAX_FORMS);
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Cannot delete this Tax Form. It is currently in use.');
				header('Location: ' . base_url() . 'manage/edit_tax_form/' . $id);
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	function add_tax_form()
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$this->load->view(HEADER);
			$this->load->view(MENU);
			$this->load->view('manage/add_tax_form');
			$this->load->view(FOOTER);
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	function insert_tax_form()
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			if (isset($_POST['manager'][TAX_FORM]) && $_POST['manager'][TAX_FORM])
			{
				$isValid = true;
				$taxform = $_POST['manager'];
				// Clear out Watermarks from textboxes
				// Clear out Tax Form Name
				if(isset($taxform[TAX_FORM]) && trim($taxform[TAX_FORM]) == JS_TAX_FORM_NAME)
				{
					$taxform[TAX_FORM] = '';
				}
				// Clear out Initial Due Date
				if(isset($taxform[TAX_FORM_INITIAL_DUE_DATE]) && trim($taxform[TAX_FORM_INITIAL_DUE_DATE]) == JS_TAX_FORM_DATE_FORMAT)
				{
					$taxform[TAX_FORM_INITIAL_DUE_DATE] = '';
				}
				// Clear out Extension 1
				if(isset($taxform[TAX_FORM_EXTENTION_1]) && trim($taxform[TAX_FORM_EXTENTION_1]) == JS_TAX_FORM_DATE_FORMAT)
				{
					$taxform[TAX_FORM_EXTENTION_1] = '';
				}
				// Clear out Extension 2
				if(isset($taxform[TAX_FORM_EXTENTION_2]) && trim($taxform[TAX_FORM_EXTENTION_2]) == JS_TAX_FORM_DATE_FORMAT)
				{
					$taxform[TAX_FORM_EXTENTION_2] = '';
				}
				// End of clearing out watermarks
				$taxformname = $taxform[TAX_FORM];
				if (!$this->Client_model->check_tax_form_exists($taxformname, 0))
				{
					if (isset($taxform[TAX_FORM_INITIAL_DUE_DATE]) && trim($taxform[TAX_FORM_INITIAL_DUE_DATE] != '') && $taxform[TAX_FORM_INITIAL_DUE_DATE] != JS_TAX_FORM_DATE_FORMAT)
					{
						$taxform[TAX_FORM_INITIAL_DUE_DATE] = trim($taxform[TAX_FORM_INITIAL_DUE_DATE]);
						if (!preg_match("/(\d{2})\/(\d{2})/", $taxform[TAX_FORM_INITIAL_DUE_DATE]))
						{
							$isValid = false;
							$this->session->set_flashdata(ERROR_TAX_FORM_INITIAL_DUE_DATE, ERROR_STRING_TAX_FORM_INITIAL_DUE_DATE);
						}
					}
					else
					{
						$taxform[TAX_FORM_INITIAL_DUE_DATE_TYPE_INITIAL_DATE] = 0;
					}
					if (isset($taxform[TAX_FORM_EXTENTION_1]) && trim($taxform[TAX_FORM_EXTENTION_1]) != '' && trim($taxform[TAX_FORM_EXTENTION_1]) != JS_TAX_FORM_DATE_FORMAT)
					{
						if (!preg_match("/(\d{2})\/(\d{2})/", $taxform[TAX_FORM_EXTENTION_1]))
						{
							$isValid = false;
							$this->session->set_flashdata(ERROR_TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_1, ERROR_STRING_TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_1);
						}
					}
					else
					{
						$taxform[TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_1] = 0;
					}
					if (isset($taxform[TAX_FORM_EXTENTION_2]) && trim($taxform[TAX_FORM_EXTENTION_2]) != '' && trim($taxform[TAX_FORM_EXTENTION_2]) != JS_TAX_FORM_DATE_FORMAT)
					{
						if (!preg_match("/(\d{2})\/(\d{2})/", $taxform[TAX_FORM_EXTENTION_2]))
						{
							$isValid = false;
							$this->session->set_flashdata(ERROR_TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_2, ERROR_STRING_TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_2);
						}
					}
					else
					{
						$taxform[TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_2] = 0;
					}
					if ($isValid)
					{
						$id = $this->Manage_model->add_tax_form($taxform);
						$this->session->set_flashdata(STATUS_MESSAGE, 'The Tax Form "' . $taxformname . '" was created successfully.');
						header('Location: ' . base_url() . 'manage/view/' . CATEGORY_TAX_FORMS);
					}
					else
					{
						$this->session->set_flashdata(ERROR_MESSAGE, 'Please fix the errors below to continue.');
						header('Location: ' . base_url() . 'manage/add_tax_form/');
					}
				}
				else if($taxformname == "")
				{
					$this->session->set_flashdata(ERROR_MESSAGE, 'Please enter a Tax Form Name.');
					header('Location: ' . base_url() . 'manage/add_tax_form');
				}
				else
				{
					$this->session->set_flashdata(ERROR_MESSAGE, 'A Tax Form with the name "' . $taxformname . '" already exists.');
					header('Location: ' . base_url() . 'manage/add_tax_form');
				}
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Please enter Tax Form Information.');
				header("Location: " . base_url() . 'manage/add_tax_form');
				exit;
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	function edit_tax_form($id)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$taxform['data'] = $this->Manage_model->get_tax_form($id);
			$taxform['type'] = 'Form name';
			$taxform['delete'] = $this->Client_model->check_tax_form_in_use($id);
			$this->load->view(HEADER);
			$this->load->view(MENU);
			$this->load->view('manage/edit_tax_form', $taxform);
			$this->load->view(FOOTER);
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	function update_tax_form($id)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$isValid = true;
			// Clear out Watermarks from textboxes
			// Clear out Tax Form Name
			if(isset($_POST['manager'][TAX_FORM]) && trim($_POST['manager'][TAX_FORM]) == JS_TAX_FORM_NAME)
			{
				$_POST['manager'][TAX_FORM] = '';
			}
			// Clear out Initial Due Date
			if(isset($_POST['manager'][TAX_FORM_INITIAL_DUE_DATE]) && trim($_POST['manager'][TAX_FORM_INITIAL_DUE_DATE]) == JS_TAX_FORM_DATE_FORMAT)
			{
				$_POST['manager'][TAX_FORM_INITIAL_DUE_DATE] = '';
			}
			// Clear out Extension 1
			if(isset($_POST['manager'][TAX_FORM_EXTENTION_1]) && trim($_POST['manager'][TAX_FORM_EXTENTION_1]) == JS_TAX_FORM_DATE_FORMAT)
			{
				$_POST['manager'][TAX_FORM_EXTENTION_1] = '';
			}
			// Clear out Extension 2
			if(isset($_POST['manager'][TAX_FORM_EXTENTION_2]) && trim($_POST['manager'][TAX_FORM_EXTENTION_2]) == JS_TAX_FORM_DATE_FORMAT)
			{
				$_POST['manager'][TAX_FORM_EXTENTION_2] = '';
			}
			// End of clearing out watermarks
			if (isset($_POST['manager'][TAX_FORM_INITIAL_DUE_DATE]))
			{
				if (preg_match("/(\d{2})\/(\d{2})/", trim($_POST['manager'][TAX_FORM_INITIAL_DUE_DATE])))
				{
					$_POST['manager'][TAX_FORM_INITIAL_DUE_DATE] = trim($_POST['manager'][TAX_FORM_INITIAL_DUE_DATE]);
				}
				else
				{
					$isValid = false;
					$_POST['manager'][TAX_FORM_INITIAL_DUE_DATE] = 0;
					$this->session->set_flashdata(ERROR_TAX_FORM_INITIAL_DUE_DATE, ERROR_STRING_TAX_FORM_INITIAL_DUE_DATE);
				}
			}
			else
			{
				$_POST['manager'][TAX_FORM_INITIAL_DUE_DATE] = 0;
				if (isset($_POST['manager'][TAX_FORM_EXTENTION_1]))
				{
					if (preg_match("/(\d{2})\/(\d{2})/", trim($_POST['manager'][TAX_FORM_EXTENTION_1])))
					{
						$_POST['manager'][TAX_FORM_EXTENTION_1] = trim($_POST['manager'][TAX_FORM_EXTENTION_1]);
					}
					else
					{
						$isValid = false;
						$_POST['manager'][TAX_FORM_EXTENTION_1] = 0;
						$this->session->set_flashdata(ERROR_TAX_FORM_EXTENTION_1, ERROR_STRING_TAX_FORM_EXTENTION_1);
					}
				}
				if (isset($_POST['manager'][TAX_FORM_EXTENTION_2]))
				{
					if (preg_match("/(\d{2})\/(\d{2})/", trim($_POST['manager'][TAX_FORM_EXTENTION_2])))
					{
						$_POST['manager'][TAX_FORM_EXTENTION_2] = trim($_POST['manager'][TAX_FORM_EXTENTION_2]);
					}
					else
					{
						$isValid = false;
						$_POST['manager'][TAX_FORM_EXTENTION_2] = 0;
						$this->session->set_flashdata(ERROR_TAX_FORM_EXTENTION_2, ERROR_STRING_TAX_FORM_EXTENTION_2);
						header('Location: ' . base_url() . 'manage/edit_tax_form/' . $id);
					}
				}
			}
			$_POST['manager'][TAX_FORM] = trim($_POST['manager'][TAX_FORM]);
			if ($isValid)
			{
				if (!$this->Client_model->check_tax_form_exists($taxformname, $id))
				{
					$this->Manage_model->edit_tax_form($id, $_POST['manager']);
					$this->session->set_flashdata(STATUS_MESSAGE, 'The Tax Form ' . $_POST['manager'][TAX_FORM] . ' was updated successfully.');
					header('Location: ' . base_url() . 'manage/view/' . CATEGORY_TAX_FORMS);
				}
				else
				{
					$this->session->set_flashdata(ERROR_MESSAGE, 'The Tax Form ' . $_POST['manager'][TAX_FORM] . ' is already in use.');
					header('Location: ' . base_url() . 'edit_tax_form/' . $id);
				}
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'There was an error in updating this Tax Form.');
				header('Location: ' . base_url() . 'manage/edit_tax_form/' . $id);
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// END FORM
	//BEGIN Tax Return Status
	function delete_tax_return_status($id)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			if (!$this->Client_model->check_tax_return_status_in_use($id))
			{
				$this->Manage_model->delete_tax_return_status($id);
				$this->session->set_flashdata(STATUS_MESSAGE, 'Record deleted.');
				header("Location: " . base_url() . 'manage/view/' . CATEGORY_TAX_RETURN_STATUS);
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Cannot delete this Tax Return Status. It is currently in use.');
				header('Location: ' . base_url() . 'manage/edit_tax_return_status/' . $id);
			}
		}
		else
		{
			header("Location: " . base_url());
		}

	}
	function add_tax_return_status()
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$this->load->view(HEADER);
			$this->load->view(MENU);
			$this->load->view('manage/add_tax_return_status');
			$this->load->view(FOOTER);
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	function insert_tax_return_status()
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			if (isset($_POST['manager'][TAX_RETURN_STATUS]) && trim($_POST['manager'][TAX_RETURN_STATUS]) != JS_TAX_RETURN_STATUS)
			{
				$Status = $_POST['manager'][TAX_RETURN_STATUS];
				if (!$this->Client_model->check_status_name_exists($Status, 0))
				{
					$id = $this->Manage_model->add_tax_return_status($Status);
					$this->session->set_flashdata(STATUS_MESSAGE, 'The Tax Return Status "' . $Status . '" was created successfully.');
					header ('Location: ' . base_url() . 'manage/view/' . CATEGORY_TAX_RETURN_STATUS);
				}
				else
				{
					$this->session->set_flashdata(ERROR_MESSAGE, 'The Tax Return Status "' . $Status . '" already exists.');
					header("Location: " . base_url() . 'manage/add_tax_return_status');
				}
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Please enter a Tax Return Status.');
				header("Location: " . base_url() . 'manage/add_tax_return_status');
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	function edit_tax_return_status($id)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$tax_return_status['data'] = $this->Manage_model->get_tax_return_status($id);
			$tax_return_status['type'] = 'Status';
			$tax_return_status['delete'] = $this->Client_model->check_tax_return_status_in_use($id);
			$this->load->view(HEADER);
			$this->load->view(MENU);
			$this->load->view('manage/edit_tax_return_status', $tax_return_status);
			$this->load->view(FOOTER);
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	function update_tax_return_status($id)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$Status = trim($_POST['manager'][TAX_RETURN_STATUS]);
			if ($Status != JS_TAX_RETURN_STATUS)
			{
				if (!$this->Client_model->check_status_name_exists($Status, $id))
				{
					$this->Manage_model->update_tax_return_status($id, $Status);
					$this->session->set_flashdata(STATUS_MESSAGE, 'The Tax Return Status "' . $Status . '" was updated successfully.');
					header('Location: ' . base_url() . 'manage/view/' . CATEGORY_TAX_RETURN_STATUS);
				}
				else
				{
					$this->session->set_flashdata(ERROR_MESSAGE, 'The Tax Return Status "' . $Status . '" is already in use.');
					header('Location: ' . base_url() . 'manage/edit_tax_return_status/' . $id);
				}
			}
			else
			{
				$this->session->set-flashdata(ERROR_MESSAGE, 'Please enter a Tax Return Status.');
				header('Location: ' . base_url() . 'manage/edit_tax_return_status/' . $id);
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
  // END ENTITY TYPE 
  	//	RETAINER STATUS
	// Retainer Status: ADD
	function add_retainer_status()
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$this->load->view(HEADER);
			$this->load->view(MENU);
			$this->load->view('manage/add_retainer_status');
			$this->load->view(FOOTER);
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	function delete_retainer_status($id) 
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			if (!$this->Client_model->check_retainer_status_in_use($id))
			{
				$this->Manage_model->delete_retainer_status($id);
				$this->session->set_flashdata(STATUS_MESSAGE, 'Record deleted.');
				header("Location: " . base_url() . 'manage/view/' . CATEGORY_RETAINER_STATUS);
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Cannot delete this Retainer Status. It is currently in use.');
				header('Location: ' . base_url() . 'manage/edit_retainer_status/' . $id);
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// Retainer Status: EDIT
	function edit_retainer_status($id)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$status['data'] = $this->Manage_model->get_retainer_status($id);
			$status['type'] = 'Retainer Status';
			$status['delete'] = $this->Client_model->check_retainer_status_in_use($id);
			$this->load->view(HEADER);
			$this->load->view(MENU);
			$this->load->view('manage/edit_retainer_status', $status);
			$this->load->view(FOOTER);
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// Retainer Status: INSERT
	function insert_retainer_status()
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			if (isset($_POST['manage'][RETAINER_STATUS]) && trim($_POST['manage'][RETAINER_STATUS]) != JS_RETAINER_STATUS)
			{
				$Status = trim($_POST['manage'][RETAINER_STATUS]);
				if (!$this->Client_model->check_retainer_status_exists($Status, 0))
				{
					$id = $this->Manage_model->add_retainer_status($Status);
					isset($id) && $id ? $message = 'The Retainer Status "' . $Status . '" has been created successfully.' : '';
					$this->session->set_flashdata(STATUS_MESSAGE, $message);
					header("Location: " .base_url() . "manage/view/" . CATEGORY_RETAINER_STATUS);
				}
				else
				{
					$this->session->set_flashdata(ERROR_MESSAGE, 'The Retainer Status "' . $Status . '" is already in use.');
					$this->session->set_flashdata(RETAINER_STATUS, $Status);
					header("Location: " . base_url() . "manage/add_retainer_status");
				}
			}
			else
			{
				$message = 'Please enter a Retainer Status.';
				$this->session->set_flashdata(ERROR_MESSAGE, $message);
				header("Location: " . base_url() . "manage/add_retainer_status");
				exit;
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// Retainer Status:  SET
	function update_retainer_status($id)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$this->session->set_flashdata('post', $_POST);
			if(isset($_POST['manage'][RETAINER_STATUS]) && trim($_POST['manage'][RETAINER_STATUS]) != JS_RETAINER_STATUS)
			{
				$Status = trim($_POST['manage'][RETAINER_STATUS]);
				if (!$this->Client_model->check_retainer_status_exists($Status, $id))
				{
					$this->Manage_model->update_retainer_status($id, $Status);
					$this->session->set_flashdata(STATUS_MESSAGE, 'The Retainer Status "' . $Status . '" was updated successfully.');
					header("Location: " . base_url() . 'manage/view/' . CATEGORY_RETAINER_STATUS);
				}
				else
				{
					$this->session->set_flashdata(ERROR_MESSAGE, 'The Retainer Status "' . $Status . '" is already in use.');
					header('Location: ' . base_url() . 'manage/edit_retainer_status/' . $id);
				}
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Please enter a Retainer Status.');
				$this->session->set_flashdata(OCCUPATION, $Status);
				header("Location: " . base_url() . "manage/edit_retainer_status/" . $id);
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	//end Retainer Status
	// STATEMENT STATUSES
	// Statement Status: ADD
	function add_statement_status()
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$this->load->view(HEADER);
			$this->load->view(MENU);
			$this->load->view('manage/add_statement_status');
			$this->load->view(FOOTER);
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// Statement Status: DELETE
	function delete_statement_status($id) 
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			if (!$this->Client_model->check_statement_status_in_use($id))
			{
				$this->Manage_model->delete_statement_status($id);
				$this->session->set_flashdata(STATUS_MESSAGE, 'Record deleted.');
				header("Location: " . base_url() . 'manage/view/' . CATEGORY_STATEMENT_STATUS);
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Cannot delete this Statement Status. It is currently in use.');
				header('Location: ' . base_url() . 'manage/edit_statement_status/' . $id);
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// Statement Status: EDIT
	function edit_statement_status($id)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$status['data'] = $this->Manage_model->get_statement_status($id);
			$status['type'] = 'Statement Status';
			$status['delete'] = $this->Client_model->check_statement_status_in_use($id);
			$this->load->view(HEADER);
			$this->load->view(MENU);
			$this->load->view('manage/edit_statement_status', $status);
			$this->load->view(FOOTER);
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// Statement Status: INSERT
	function insert_statement_status()
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			if (isset($_POST['manage'][STATEMENT_STATUS]) && trim($_POST['manage'][STATEMENT_STATUS]) != JS_STATEMENT_STATUS)
			{
				$Status = trim($_POST['manage'][STATEMENT_STATUS]);
				if (!$this->Client_model->check_statement_status_exists($Status, 0))
				{
					$id = $this->Manage_model->add_statement_status($Status);
					isset($id) && $id ? $message = 'The Statement Status "' . $Status . '" has been created successfully.' : '';
					$this->session->set_flashdata(STATUS_MESSAGE, $message);
					header("Location: " .base_url() . "manage/view/" . CATEGORY_STATEMENT_STATUS);
				}
				else
				{
					$this->session->set_flashdata(ERROR_MESSAGE, 'The Statement Status "' . $Status . '" is already in use.');
					$this->session->set_flashdata(STATEMENT_STATUS, $Status);
					header("Location: " . base_url() . "manage/add_statement_status");
				}
			}
			else
			{
				$message = 'Please enter an Statement Status.';
				$this->session->set_flashdata(ERROR_MESSAGE, $message);
				header("Location: " . base_url() . "manage/add_statement_status");
				exit;
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// Statement Status:  SET
	function update_statement_status($id)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$this->session->set_flashdata('post', $_POST);
			if(isset($_POST['manage'][STATEMENT_STATUS]) && trim($_POST['manage'][STATEMENT_STATUS]) != JS_STATEMENT_STATUS)
			{
				$Status = trim($_POST['manage'][STATEMENT_STATUS]);
				if (!$this->Client_model->check_statement_status_exists($Status, $id))
				{
					$this->Manage_model->update_statement_status($id, $Status);
					$this->session->set_flashdata(STATUS_MESSAGE, 'The Statement Status "' . $Status . '" was updated successfully.');
					header("Location: " . base_url() . 'manage/view/' . CATEGORY_STATEMENT_STATUS);
				}
				else
				{
					$this->session->set_flashdata(ERROR_MESSAGE, 'The Statement Status "' . $Status . '" is already in use.');
					header('Location: ' . base_url() . 'manage/edit_statement_status/' . $id);
				}
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Please enter a Statement Status.');
				$this->session->set_flashdata(STATEMENT_STATUS, $Status);
				header("Location: " . base_url() . "manage/edit_statement_status/" . $id);
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	//end STATEMENT STATUS
	// STATEMENT TYPES
	// Statement Type: ADD
	function add_statement_type()
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$this->load->view(HEADER);
			$this->load->view(MENU);
			$this->load->view('manage/add_statement_type');
			$this->load->view(FOOTER);
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// Statement Type: DELETE
	function delete_statement_type($id) 
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			if (!$this->Client_model->check_statement_type_in_use($id))
			{
				$this->Manage_model->delete_statement_type($id);
				$this->session->set_flashdata(STATUS_MESSAGE, 'Record deleted.');
				header("Location: " . base_url() . 'manage/view/' . CATEGORY_STATEMENT_TYPE);
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Cannot delete this Statement Type. It is currently in use.');
				header('Location: ' . base_url() . 'manage/edit_statement_type/' . $id);
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// Statement Type: EDIT
	function edit_statement_type($id)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$status['data'] = $this->Manage_model->get_statement_type($id);
			$status['type'] = 'Statement Type';
			$status['delete'] = $this->Client_model->check_statement_type_in_use($id);
			$this->load->view(HEADER);
			$this->load->view(MENU);
			$this->load->view('manage/edit_statement_type', $status);
			$this->load->view(FOOTER);
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// Statement Type: INSERT
	function insert_statement_type()
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			if (isset($_POST['manage'][STATEMENT_TYPE]) && trim($_POST['manage'][STATEMENT_TYPE]) != JS_STATEMENT_TYPE)
			{
				$Type = trim($_POST['manage'][STATEMENT_TYPE]);
				if (!$this->Client_model->check_statement_type_exists($Type, 0))
				{
					$id = $this->Manage_model->add_statement_type($Type);
					isset($id) && $id ? $message = 'The Statement Type "' . $Type . '" has been created successfully.' : '';
					$this->session->set_flashdata(STATUS_MESSAGE, $message);
					header("Location: " .base_url() . "manage/view/" . CATEGORY_STATEMENT_TYPE);
				}
				else
				{
					$this->session->set_flashdata(ERROR_MESSAGE, 'The Statement Type "' . $Type . '" is already in use.');
					$this->session->set_flashdata(STATEMENT_STATUS, $Type);
					header("Location: " . base_url() . "manage/add_statement_type");
				}
			}
			else
			{
				$message = 'Please enter a Statement Type.';
				$this->session->set_flashdata(ERROR_MESSAGE, $message);
				header("Location: " . base_url() . "manage/add_statement_type");
				exit;
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// Statement Type:  SET
	function update_statement_type($id)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
		{
			$this->session->set_flashdata('post', $_POST);
			if(isset($_POST['manage'][STATEMENT_TYPE]) && trim($_POST['manage'][STATEMENT_TYPE]) != JS_STATEMENT_TYPE)
			{
				$Type = trim($_POST['manage'][STATEMENT_TYPE]);
				if (!$this->Client_model->check_statement_type_exists($Type, $id))
				{
					$this->Manage_model->update_statement_type($id, $Type);
					$this->session->set_flashdata(STATUS_MESSAGE, 'The Statement Type "' . $Type . '" was updated successfully.');
					header("Location: " . base_url() . 'manage/view/' . CATEGORY_STATEMENT_TYPE);
				}
				else
				{
					$this->session->set_flashdata(ERROR_MESSAGE, 'The Statement Type "' . $Type . '" is already in use.');
					header('Location: ' . base_url() . 'manage/edit_statement_type/' . $id);
				}
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Please enter a Statement Type.');
				$this->session->set_flashdata(STATEMENT_STATUS, $Status);
				header("Location: " . base_url() . "manage/edit_statement_type/" . $id);
			}
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	//end STATEMENT STATUS
}