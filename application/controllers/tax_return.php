<?php
	
	class Tax_return extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->model('Tax_return_model');
			$this->load->model('form_model');
			$this->load->model('client_model');
			$this->load->library('Utilities');
		}
		// Auto Complete
		function autocompleted($title)
		{
			echo $this->utilities->GetClientNameAutoComplete($title);
		}
		
		function get_status_history($tax_return_id, $as)
		{
			$history = $this->Tax_return_model->get_status_history($tax_return_id, $as);
			return $history;
		}
		function add($tax_return_id = 0)
		{
			if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE)))
			{
				$client_id = $this->session->flashdata(CLIENT_ID);

				if($client_id) {
					$partner_id = $this->session->flashdata(TAX_RETURN_VIEW_PARTNER_ID) ? $this->session->flashdata(TAX_RETURN_VIEW_PARTNER_ID) : PLEASE_SELECT_ID;

					$tax_form_id = $this->session->flashdata(TAX_RETURN_VIEW_TAX_FORM_ID) ? $this->session->flashdata(TAX_RETURN_VIEW_TAX_FORM_ID) : PLEASE_SELECT_ID;

					$tax_return['Partners'] = $this->utilities->get_drop_down_html(
						TAX_RETURN_POST, TAX_RETURN_VIEW_PARTNER_ID,  $this->utilities->get_partners(),
						STAFF_ID, STAFF_INITIALS, $partner_id, false);

					$tax_return['Tax_Forms'] = $this->utilities->get_drop_down_html(
						TAX_RETURN_POST, TAX_RETURN_VIEW_TAX_FORM_ID, $this->utilities->get_tax_forms_by_client_id($client_id), 
						TAX_FORM_ID, TAX_FORM, $tax_form_id, false);
				} else {
					$tax_return['Partners'] = 'First select a Client.';
					$tax_return['Tax_Forms'] = 'First select a Client.';
				}

				$tax_return['Tax_Years'] = 
				(
					$this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_TAX_YEAR, $this->utilities->get_tax_years(), TAX_YEAR_ID, TAX_YEAR, 
					$this->session->flashdata(TAX_RETURN_VIEW_TAX_YEAR) ? $this->session->flashdata(TAX_RETURN_VIEW_TAX_YEAR) : 
					PLEASE_SELECT_ID, false)
				);
				$tax_return['Tax_Quarters'] = 
				(
					$this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_TAX_QUARTER, $this->utilities->get_tax_quarters(), TAX_QUARTER_ID, TAX_QUARTER, 
					$this->session->flashdata(TAX_RETURN_VIEW_TAX_QUARTER) ? $this->session->flashdata(TAX_RETURN_VIEW_TAX_QUARTER) : 
					NOT_APPLICABLE_ID, false)
				);
				$tax_return['Tax_Months'] = 
				(
					$this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_TAX_MONTH_ID, $this->utilities->get_tax_months(), TAX_MONTH_ID, TAX_MONTH, 
					$this->session->flashdata(TAX_RETURN_VIEW_TAX_MONTH_ID) ? $this->session->flashdata(TAX_RETURN_VIEW_TAX_MONTH_ID) : 
					NOT_APPLICABLE_ID, false)
				);
				$tax_return['Tax_Return_Status'] = 
				(
					$this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID, $this->utilities->get_tax_return_status(), TAX_RETURN_STATUS_ID, TAX_RETURN_STATUS, 
					$this->session->flashdata(TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID) ? $this->session->flashdata(TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID) : 
					TAX_RETURN_STATUS_TO_BE_DETERMINED_ID, false)
				);
				$tax_return['Staff'] = 
				(
					$this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_STAFF_ID, $this->utilities->get_staff(), STAFF_ID, STAFF_INITIALS, 
					$this->session->flashdata(TAX_RETURN_VIEW_STAFF_ID) ? $this->session->flashdata(TAX_RETURN_VIEW_STAFF_ID) : 
					PLEASE_SELECT_ID, false)
				);
				$tax_return['Reviewer_1'] = 
				(
					$this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_REVIEWER_1_ID, $this->utilities->get_staff(), STAFF_ID, STAFF_INITIALS, 
					$this->session->flashdata(TAX_RETURN_VIEW_REVIEWER_1_ID) ? $this->session->flashdata(TAX_RETURN_VIEW_REVIEWER_1_ID) : 
					PLEASE_SELECT_ID, false)
				);
				$tax_return['Reviewer_2'] = 
				(
					$this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_REVIEWER_2_ID, $this->utilities->get_staff(), STAFF_ID, STAFF_INITIALS, 
					$this->session->flashdata(TAX_RETURN_VIEW_REVIEWER_2_ID) ? $this->session->flashdata(TAX_RETURN_VIEW_REVIEWER_2_ID) : 
					PLEASE_SELECT_ID, false)
				);
				$tax_return['Instructions'] = 
				(				
					$this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_INSTRUCTIONS_ID, $this->utilities->get_tax_return_instructions(), INSTRUCTIONS_ID, INSTRUCTIONS, 
					$this->session->flashdata(TAX_RETURN_VIEW_INSTRUCTIONS_ID) ? $this->session->flashdata(TAX_RETURN_VIEW_INSTRUCTIONS_ID) : 
					PLEASE_SELECT_ID, false)
				);

				$this->load->view(HEADER);
				$this->load->view(MENU);
				$this->load->view(TAX_RETURN_POST.'/add', $tax_return);
				$this->load->view(FOOTER);
			}
			else
			{
				header("Location: " . base_url());
			}
		}
                
                //ADD MULTIPLE
                function add_multiple($tax_return_id = 0)
		{
			if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE)))
			{
                                $client_id = $this->session->flashdata(CLIENT_ID);
				$partner_id = $this->session->flashdata(TAX_RETURN_VIEW_PARTNER_ID) ? $this->session->flashdata(TAX_RETURN_VIEW_PARTNER_ID) : PLEASE_SELECT_ID;
				$tax_form_id = $this->session->flashdata(TAX_RETURN_VIEW_TAX_FORM_ID) ? $this->session->flashdata(TAX_RETURN_VIEW_TAX_FORM_ID) : PLEASE_SELECT_ID;
				$tax_return['Partners'] = $this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_PARTNER_ID,  $this->utilities->get_partners(),STAFF_ID, STAFF_INITIALS, $partner_id, false);
                                $tax_return['Tax_Forms'] = $this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_TAX_FORM_ID, $this->utilities->get_tax_forms(), TAX_FORM_ID, TAX_FORM, $tax_form_id, false );
				$year = $this->utilities->get_tax_years();
                                arsort($year);
                                $last_val = reset($year);                                   
                                $new_year = (int)$last_val['Tax_Year_ID'];
                                $new_year++;                                
                                $year[$new_year]['Tax_Year_ID'] = $new_year;
                                $year[$new_year]['Tax_Year_Label'] = $new_year;                                
                                $new_year2 = $new_year+1;                               
                                $year[$new_year2]['Tax_Year_ID'] = $new_year2;
                                $year[$new_year2]['Tax_Year_Label'] = $new_year2;                                 
                                arsort($year);
                                $tax_return['Tax_Years'] = ($this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_TAX_YEAR, $year, TAX_YEAR_ID, TAX_YEAR, $this->session->flashdata(TAX_RETURN_VIEW_TAX_YEAR) ? $this->session->flashdata(TAX_RETURN_VIEW_TAX_YEAR) : PLEASE_SELECT_ID, false));
				$this->load->view(HEADER);
				$this->load->view(MENU);
				$this->load->view(TAX_RETURN_POST.'/add_multiple', $tax_return);
				$this->load->view(FOOTER);
			}
			else
			{
				header("Location: " . base_url());
			}
		}
		/*
		// automatic add clients/ Print form
		function autoadd()
		{
			if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE)))
			{
				$this->load->view(HEADER);
				$this->load->view(MENU);
				$tax_return['entity_type'] = $this->Tax_return_model->get_entity_type_name();
				$tax_return['forms'] = $this->utilities->get_tax_forms();
				$this->load->view('tax_return/autoadd', $tax_return);
				$this->load->view(FOOTER);
			}
			else
			{
				header("Location: " . base_url());
			}
		}
		*/
		/*
		function autoadd_set()
		{
			$this->Tax_return_model->AutomaticAdd(
				$_POST[TAX_RETURN_POST]['entity'], 
				$_POST[TAX_RETURN_POST]['state'][0],
				$_POST[TAX_RETURN_POST]['form'][0], 
				$_POST[TAX_RETURN_POST]['tax_year']
			);
			$this->session->set_flashdata('auto_add', 'Automatically add records in Tax Returns successfully');
			header("Location: " . base_url() . TAX_RETURN_POST . '/view');
		}
		*/
		function edit($id, $as = 'ASC')
		{
			$data = $this->Tax_return_model->get_tax_return($id);
			//$lock = $data[0]['Is_Locked'] == 1 ? '1' : '';
			//if ((!$this->session->userdata(ROLE_ADMIN) && !$this->session->userdata(ROLE_WRITE)) && $lock)
			//{
			//	header("Location: " . base_url());
			//}
			if ($this->session->userdata(ROLE_ADMIN) || $this->session->userdata(ROLE_WRITE))
			{	$data = $data[0];
				$tax_return['data'] = $data;
				$tax_return['as'] = $as;
				$tax_return['History'] = $this->Tax_return_model->get_tax_return_status_history($id, $as);
				$tax_return['Tax_Forms'] = 
				(
					$this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_TAX_FORM_ID, $this->utilities->get_tax_forms_by_client_id($data[TAX_RETURN_VIEW_CLIENT_ID]), TAX_FORM_ID, TAX_FORM, 
					$this->session->flashdata(TAX_RETURN_VIEW_TAX_FORM_ID) ? $this->session->flashdata(TAX_RETURN_VIEW_TAX_FORM_ID) : 
					$data[TAX_RETURN_VIEW_TAX_FORM_ID] ? $data[TAX_RETURN_VIEW_TAX_FORM_ID] :
					PLEASE_SELECT_ID, false)
				);
				$tax_return['Tax_Years'] = 
				(
					$this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_TAX_YEAR_ID, $this->utilities->get_tax_years(), TAX_YEAR_ID, TAX_YEAR, 
					$this->session->flashdata(TAX_RETURN_TAX_YEAR_ID) ? $this->session->flashdata(TAX_RETURN_TAX_YEAR_ID) : 
					$data[TAX_RETURN_VIEW_TAX_YEAR] ? $data[TAX_RETURN_VIEW_TAX_YEAR] :
					PLEASE_SELECT_ID, false)
				);
				$tax_return['Tax_Quarters'] = 
				(
					$this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_TAX_QUARTER_ID, $this->utilities->get_tax_quarters(), TAX_QUARTER_ID, TAX_QUARTER, 
					$this->session->flashdata(TAX_RETURN_TAX_QUARTER_ID) ? $this->session->flashdata(TAX_RETURN_TAX_QUARTER_ID) : 
					$data[TAX_RETURN_VIEW_TAX_QUARTER_ID] ? $data[TAX_RETURN_VIEW_TAX_QUARTER_ID] : 
					NOT_APPLICABLE_ID, false)
				);
				$tax_return['Tax_Months'] = 
				(
					$this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_TAX_MONTH_ID, $this->utilities->get_tax_months(), TAX_MONTH_ID, TAX_MONTH, 
					$this->session->flashdata(TAX_RETURN_TAX_MONTH_ID) ? $this->session->flashdata(TAX_RETURN_TAX_MONTH_ID) : 
					$data[TAX_RETURN_VIEW_TAX_MONTH_ID] ? $data[TAX_RETURN_VIEW_TAX_MONTH_ID] : 
					NOT_APPLICABLE_ID, false)
				);
				$tax_return['Tax_Return_Status'] = 
				(
					$this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID, $this->utilities->get_tax_return_status(), TAX_RETURN_STATUS_ID, TAX_RETURN_STATUS, 
					$this->session->flashdata(TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID) ? $this->session->flashdata(TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID) : 
					$data[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID] ? $data[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID] : 
					TAX_RETURN_STATUS_TO_BE_DETERMINED_ID, false)
				);
				$tax_return['Partners'] = 
				(
					$this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_PARTNER_ID,  $this->utilities->get_partners(), STAFF_ID, STAFF_INITIALS, 
					$this->session->flashdata(TAX_RETURN_VIEW_TAX_RETURN_ID) ? $this->session->flashdata(TAX_RETURN_VIEW_TAX_RETURN_ID) : 
					$data[TAX_RETURN_VIEW_TAX_RETURN_ID] ? $data[TAX_RETURN_VIEW_TAX_RETURN_ID] : 
					PLEASE_SELECT_ID, false)
				);
				$tax_return['Staff'] = 
				(
					$this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_STAFF_ID, $this->utilities->get_staff(), STAFF_ID, STAFF_INITIALS, 
					$this->session->flashdata(TAX_RETURN_VIEW_STAFF_ID) ? $this->session->flashdata(TAX_RETURN_VIEW_STAFF_ID) : 
					$data[TAX_RETURN_VIEW_STAFF_ID] ? $data[TAX_RETURN_VIEW_STAFF_ID] : 
					PLEASE_SELECT_ID, false)
				);
				$tax_return['Reviewer_1'] = 
				(
					$this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_REVIEWER_1_ID, $this->utilities->get_staff(), STAFF_ID, STAFF_INITIALS, 
					$this->session->flashdata(TAX_RETURN_VIEW_REVIEWER_1_ID) ? $this->session->flashdata(TAX_RETURN_VIEW_REVIEWER_1_ID) : 
					$data[TAX_RETURN_VIEW_REVIEWER_1_ID] ? $data[TAX_RETURN_VIEW_REVIEWER_1_ID] : 
					PLEASE_SELECT_ID, false)
				);
				$tax_return['Reviewer_2'] = 
				(
					$this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_REVIEWER_2_ID, $this->utilities->get_staff(), STAFF_ID, STAFF_INITIALS, 
					$this->session->flashdata(TAX_RETURN_VIEW_REVIEWER_2_ID) ? $this->session->flashdata(TAX_RETURN_VIEW_REVIEWER_2_ID) : 
					$data[TAX_RETURN_VIEW_REVIEWER_2_ID] ? $data[TAX_RETURN_VIEW_REVIEWER_2_ID] : 
					PLEASE_SELECT_ID, false)
				);
				$tax_return['Instructions'] = 
				(				
					$this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_INSTRUCTIONS_ID, $this->utilities->get_tax_return_instructions(), INSTRUCTIONS_ID, INSTRUCTIONS, 
					$this->session->flashdata(TAX_RETURN_VIEW_INSTRUCTIONS_ID) ? $this->session->flashdata(TAX_RETURN_VIEW_INSTRUCTIONS_ID) : 
					$data[TAX_RETURN_VIEW_INSTRUCTIONS_ID] ? $data[TAX_RETURN_VIEW_INSTRUCTIONS_ID] : 
					PLEASE_SELECT_ID, false)
				);
				$this->load->view(HEADER);
				$this->load->view(MENU);
				$this->load->view(TAX_RETURN_POST.'/edit', $tax_return);
				$this->load->view(FOOTER);
			}
			else
			{
				header("Location: " . base_url());
			}
		}
		// Edit specific return
		function update_tax_return($id)
		{
			$lock = ($this->Tax_return_model->select_lock($id)) ? '1' : '';
			if ($this->session->userdata(ROLE_READ_ONLY) && $lock)
			{
				header("Location: " . base_url());
			}
			else if ($this->session->userdata(ROLE_ADMIN) || $this->session->userdata(ROLE_WRITE))
			{
				$isValid = true;
				$return = $_POST[TAX_RETURN_POST];
				$User_ID = $this->session->userdata(STAFF_ID);
				// Clear out Watermark'ed Text Fields and PLEASE SELECT fields
				if(isset($return[TAX_RETURN_VIEW_TAX_FORM_ID]) && $return[TAX_RETURN_VIEW_TAX_FORM_ID] == PLEASE_SELECT_ID)
				{
					$return[TAX_RETURN_VIEW_TAX_FORM_ID] = 0;
				}
				if(isset($return[TAX_RETURN_TAX_YEAR_ID]) && $return[TAX_RETURN_TAX_YEAR_ID] == PLEASE_SELECT_ID)
				{
					$return[TAX_RETURN_TAX_YEAR_ID] = 0;
				}
				if(isset($return[TAX_RETURN_TAX_QUARTER_ID]) && $return[TAX_RETURN_TAX_QUARTER_ID] == PLEASE_SELECT_ID)
				{
					$return[TAX_RETURN_TAX_QUARTER_ID] = 0;
				}
				if(isset($return[TAX_RETURN_TAX_MONTH_ID]) && $return[TAX_RETURN_TAX_MONTH_ID] == PLEASE_SELECT_ID)
				{
					$return[TAX_RETURN_TAX_MONTH_ID] = 0;
				}
				if(isset($return[TAX_RETURN_DATE_RELEASED]) && trim($return[TAX_RETURN_DATE_RELEASED]) == JS_DATE_RELEASED)
				{
					$return[TAX_RETURN_DATE_RELEASED] = '';
				}
				else
				{
					$return[TAX_RETURN_DATE_RELEASED] = trim($return[TAX_RETURN_DATE_RELEASED]);
				}
				if(isset($return[TAX_RETURN_DATE_SENT]) && trim($return[TAX_RETURN_DATE_SENT]) == JS_DATE_SENT)
				{
					$return[TAX_RETURN_DATE_SENT] = '';
				}
				else
				{
					$return[TAX_RETURN_DATE_SENT] = trim($return[TAX_RETURN_DATE_SENT]);
				}
				if(isset($return[TAX_RETURN_VIEW_TAX_RETURN_NOTES]) && trim($return[TAX_RETURN_VIEW_TAX_RETURN_NOTES]) == JS_TAX_RETURN_NOTES)
				{
					$return[TAX_RETURN_VIEW_TAX_RETURN_NOTES] = '';
				}
				else
				{
					$return[TAX_RETURN_VIEW_TAX_RETURN_NOTES] = trim($return[TAX_RETURN_VIEW_TAX_RETURN_NOTES]);
				}
				if(isset($return[TAX_RETURN_VIEW_CLIENT_NAME]) && trim($return[TAX_RETURN_VIEW_CLIENT_NAME]) == JS_CLIENT_NAME)
				{
					$return[TAX_RETURN_VIEW_CLIENT_NAME] = '';
				}
				else
				{
					$return[TAX_RETURN_VIEW_CLIENT_NAME] = trim($return[TAX_RETURN_VIEW_CLIENT_NAME]);
				}
				// Validate Required Information
				// Validate Tax Return Status
				if (isset($return[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID]))
				{
					$this->session->set_flashdata(TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID, $return[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID]);
					if ($return[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID] == PLEASE_SELECT_ID || !$return[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID] > 0)
					{
						$isValid = false;
						$this->session->set_flashdata(ERROR_TAX_RETURN_STATUS_ID, ERROR_STRING_TAX_RETURN_STATUS_ID);
					}
				}
				else
				{
					$isValid = false;
					$this->session->set_flashdata(ERROR_TAX_RETURN_STATUS_ID, ERROR_STRING_TAX_RETURN_STATUS_ID);
				}
				// Validate Prepared By
				if (isset($return[TAX_RETURN_VIEW_STAFF_ID]) && $return[TAX_RETURN_VIEW_STAFF_ID] == PLEASE_SELECT_ID)
				{ 
					$this->session->set_flashdata(TAX_RETURN_VIEW_STAFF_ID, $return[TAX_RETURN_VIEW_STAFF_ID]);
					$return[TAX_RETURN_VIEW_STAFF_ID] = 0;
				}
				// Validate Tax Form ID
				if(isset($return[TAX_RETURN_TAX_FORM_ID]))
				{
					$this->session->set_flashdata(TAX_RETURN_TAX_FORM_ID, $return[TAX_RETURN_TAX_FORM_ID]);
					if($return[TAX_RETURN_TAX_FORM_ID] < 1)
					{
						$isValid = false;
						$this->session->set_flashdata(ERROR_TAX_FORM_ID, ERROR_STRING_TAX_FORM_ID);
					}
				}
				// Validate Tax Year ID
				if(isset($return[TAX_RETURN_TAX_YEAR_ID]))
				{
					$this->session->set_flashdata(TAX_RETURN_TAX_YEAR_ID, $return[TAX_RETURN_TAX_YEAR_ID]);
					if($return[TAX_RETURN_TAX_YEAR_ID] < 1)
					{
						$isValid = false;
						$this->session->set_flashdata(ERROR_TAX_YEAR_ID, ERROR_STRING_TAX_YEAR_ID);
					}
				}
				// Validate Tax Quarter ID
				if(isset($return[TAX_RETURN_TAX_QUARTER_ID]))
				{
					$this->session->set_flashdata(TAX_RETURN_TAX_QUARTER_ID, $return[TAX_RETURN_TAX_QUARTER_ID]);
					if($return[TAX_RETURN_TAX_QUARTER_ID] == 0)
					{
						$isValid = false;
						$this->session->set_flashdata(TAX_RETURN_TAX_QUARTER_ID, PLEASE_SELECT_ID);
						$this->session->set_flashdata(ERROR_TAX_QUARTER_ID, ERROR_STRING_TAX_QUARTER_ID);
					}
				}
				// Validate Tax Month ID
				if(isset($return[TAX_RETURN_TAX_MONTH_ID]))
				{
					$this->session->set_flashdata(TAX_RETURN_TAX_MONTH_ID, $return[TAX_RETURN_TAX_MONTH_ID]);
					if($return[TAX_RETURN_TAX_MONTH_ID] == 0)
					{
						$isValid = false;
						$this->session->set_flashdata(TAX_RETURN_TAX_MONTH_ID, PLEASE_SELECT_ID);
						$this->session->set_flashdata(ERROR_TAX_MONTH_ID, ERROR_STRING_TAX_MONTH_ID);
					}
				}
				// Validate Instructions
				if (isset($return[TAX_RETURN_VIEW_INSTRUCTIONS_ID]))
				{
					$this->session->set_flashdata(TAX_RETURN_VIEW_INSTRUCTIONS_ID, $return[TAX_RETURN_VIEW_INSTRUCTIONS_ID]);
					if($return[TAX_RETURN_VIEW_INSTRUCTIONS_ID] == PLEASE_SELECT_ID || !$return[TAX_RETURN_VIEW_INSTRUCTIONS_ID] > 0)
					{
						$return[TAX_RETURN_VIEW_INSTRUCTIONS_ID] = 0;
					}
				}
				// Validate Date Sent is a valid date
				if (isset($return[TAX_RETURN_DATE_SENT]))
				{
					$this->session->set_flashdata(TAX_RETURN_DATE_SENT, $return[TAX_RETURN_DATE_SENT]);
					if (trim($return[TAX_RETURN_DATE_SENT]) == NOT_SENT)
					{
						$return[TAX_RETURN_DATE_SENT] = 0;
					}
					else if (trim($return[TAX_RETURN_DATE_SENT]) != '')
					{
						$t = strtotime(stripslashes(trim($return[TAX_RETURN_DATE_SENT])));
						if (!checkdate(date('m', $t), date('d', $t), date('Y', $t)))
						{
							$isValid = false;
							$this->session->set_flashdata(ERROR_TAX_RETURN_DATE_SENT, ERROR_STRING_TAX_RETURN_DATE_SENT);
						}
					}
				}
				// Validate Date Released is a valid date
				if (isset($return[TAX_RETURN_DATE_RELEASED]))
				{
					$this->session->set_flashdata(TAX_RETURN_DATE_RELEASED, $return[TAX_RETURN_DATE_RELEASED]);
					if (trim($return[TAX_RETURN_DATE_RELEASED]) == NOT_SENT || trim($return[TAX_RETURN_DATE_RELEASED]) == '')
					{
						$return[TAX_RETURN_DATE_RELEASED] = 0;
					}
					else if (trim($return[TAX_RETURN_DATE_RELEASED]) != '')
					{
						$t = strtotime(stripslashes(trim($return[TAX_RETURN_DATE_RELEASED])));
						if (!checkdate(date('m', $t), date('d', $t), date('Y', $t)))
						{
							$isValid = false;
							$this->session->set_flashdata(ERROR_TAX_RETURN_DATE_RELEASED, ERROR_STRING_TAX_RETURN_DATE_RELEASED);
						}
					}
				}
				// Check Reviewer 1
				if (isset($return[TAX_RETURN_VIEW_REVIEWER_1_ID]) && $return[TAX_RETURN_VIEW_REVIEWER_1_ID] == PLEASE_SELECT_ID)
				{
					$this->session->set_flashdata(TAX_RETURN_VIEW_REVIEWER_1_ID, $return[TAX_RETURN_VIEW_REVIEWER_1_ID]);
					$return[TAX_RETURN_VIEW_REVIEWER_1_ID] = 0;
				}
				// Check Reviewer 2
				if (isset($return[TAX_RETURN_VIEW_REVIEWER_2_ID]) && $return[TAX_RETURN_VIEW_REVIEWER_2_ID] == PLEASE_SELECT_ID)
				{
					$this->session->set_flashdata(TAX_RETURN_VIEW_REVIEWER_2_ID, $return[TAX_RETURN_VIEW_REVIEWER_2_ID]);
					$return[TAX_RETURN_VIEW_REVIEWER_2_ID] = 0;
				}
				// if all is valid, build objects and update/insert the new data
				if ($isValid)
				{
					// array to UPDATE Tax Return Information
					$Return = array
					(
						TAX_RETURN_TAX_FORM_ID			=> $return[TAX_RETURN_TAX_FORM_ID],
						TAX_RETURN_TAX_YEAR_ID			=> $return[TAX_RETURN_TAX_YEAR_ID],
						TAX_RETURN_TAX_QUARTER_ID		=> $return[TAX_RETURN_TAX_QUARTER_ID],
						TAX_RETURN_TAX_MONTH_ID			=> $return[TAX_RETURN_TAX_MONTH_ID],
						TAX_RETURN_STATUS_ID			=> $return[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID],
						TAX_RETURN_STAFF_ID				=> $return[TAX_RETURN_VIEW_STAFF_ID],
						TAX_RETURN_REVIEWER_1_ID		=> $return[TAX_RETURN_VIEW_REVIEWER_1_ID],
						TAX_RETURN_REVIEWER_2_ID		=> $return[TAX_RETURN_VIEW_REVIEWER_2_ID],
						TAX_RETURN_NOTES				=> stripslashes(trim($return[TAX_RETURN_VIEW_TAX_RETURN_NOTES])),
						TAX_RETURN_INSTRUCTIONS_ID		=> $return[TAX_RETURN_VIEW_INSTRUCTIONS_ID],
						TAX_RETURN_DATE_RELEASED		=> strtotime(stripslashes(trim($return[TAX_RETURN_DATE_RELEASED]))),
						TAX_RETURN_DATE_SENT			=> strtotime(stripslashes(trim($return[TAX_RETURN_DATE_SENT]))),
						TAX_RETURN_LAST_EDIT_DATE		=> time(),
						TAX_RETURN_LAST_EDIT_USER_ID	=> $User_ID
					);
					// array to INSERT a new Status History record
					$History = array
					(
						TAX_RETURN_HISTORY_LAST_EDIT_DATE		=> time(),
						TAX_RETURN_HISTORY_LAST_EDIT_USER_ID	=> $User_ID,
						TAX_RETURN_HISTORY_REVIEWER_1_ID		=> $return[TAX_RETURN_VIEW_REVIEWER_1_ID] > 0 	? $return[TAX_RETURN_VIEW_REVIEWER_1_ID] : 0,
						TAX_RETURN_HISTORY_REVIEWER_2_ID		=> $return[TAX_RETURN_VIEW_REVIEWER_2_ID] > 0 	? $return[TAX_RETURN_VIEW_REVIEWER_2_ID] : 0,
						TAX_RETURN_HISTORY_STAFF_ID				=> $return[TAX_RETURN_VIEW_STAFF_ID] > 0 		? $return[TAX_RETURN_VIEW_STAFF_ID] : 0,
						TAX_RETURN_HISTORY_TAX_RETURN_ID		=> $id,
						TAX_RETURN_HISTORY_TAX_RETURN_STATUS_ID	=> $return[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID]
					);
					$this->Tax_return_model->update_return($id, $Return, $History);
					$this->session->set_flashdata(STATUS_MESSAGE, 'Tax Return was updated successfully.');
					header('Location: ' . base_url() . TAX_RETURN_POST . '/view');
				}
				else
				{
					$this->session->set_flashdata(ERROR_MESSAGE, 'An error occurred while updating this Tax Form. Please fix the errors below.');
					header("Location: " . base_url() . TAX_RETURN_POST . '/edit/' . $id);
				}
			}
			else
			{
				header("Location: " . base_url());
			}
		}
		
		// Add a new Tax Return
		function add_tax_return()
		{       
			if ($this->session->userdata(ROLE_ADMIN) || $this->session->userdata(ROLE_WRITE))
			{
				try
				{
					if (isset($_POST[TAX_RETURN_POST]))
					{
						$isValid = true;
						$return = $_POST[TAX_RETURN_POST];
						if (isset($_POST[TAX_RETURN_VIEW_990PF_DATE]))
						{
							$return[TAX_RETURN_VIEW_990PF_DATE] = stripslashes(trim($_POST[TAX_RETURN_VIEW_990PF_DATE]));
							if ($return[TAX_RETURN_VIEW_990PF_DATE] == '' || $_POST[TAX_RETURN_VIEW_990PF_DATE] == JS_990PF_DATE)
							{
								$return[TAX_RETURN_VIEW_990PF_DATE] = 0;
								$this->session->set_flashdata(ERROR_TAX_RETURN_VIEW_990PF_DATE, ERROR_STRING_TAX_RETURN_VIEW_990PF_DATE);
							}
							else
							{
								$this->session->set_flashdata(TAX_RETURN_VIEW_990PF_DATE, $return[TAX_RETURN_VIEW_990PF_DATE]);
							}
						}
						else
						{
							$return[TAX_RETURN_VIEW_990PF_DATE] = 0;
						}
						// Get UserID
						$User_ID = $this->session->userdata(STAFF_ID);
						// Validate Client Name
						if (isset($return[CLIENT_NAME]) && $return[CLIENT_NAME] != ANY_VALUE)
						{
							$id = $this->utilities->get_client_id($return[CLIENT_NAME]);
							$this->session->set_flashdata(CLIENT_ID, $id);
							if (!is_numeric($id) || $id < 1)
							{
								// Invalid Client ID... try again
								$this->session->set_flashdata(ERROR_CLIENT_ID, 'The client you chose was invalid or does not exist.');
								$isValid = false;
							}
							else
							{
								$this->session->set_flashdata(TAX_RETURN_VIEW_CLIENT_NAME, $return[CLIENT_NAME]);
							}
						}
						else
						{
							$this->session->set_flashdata(ERROR_CLIENT_ID, ERROR_STRING_CLIENT_ID);
							$isValid = false;
						}
						// Validate Date Sent
						if (isset($return[TAX_RETURN_DATE_SENT]))
						{
							$return[TAX_RETURN_DATE_SENT] = stripslashes(trim($return[TAX_RETURN_DATE_SENT]));
							if($return[TAX_RETURN_DATE_SENT] == JS_DATE_SENT && $return[TAX_RETURN_DATE_SENT] == NOT_SENT)
							{
								$return[TAX_RETURN_DATE_SENT] = '';
							}
							else if($return[TAX_RETURN_DATE_SENT] != '')
							{
								//check to make sure its a valid date
								$t = strtotime(stripslashes(trim($return[TAX_RETURN_DATE_SENT])));
								if (!checkdate(date('m', $t), date('d', $t), date('Y', $t)))
								{
									$isValid = false;
									$this->session->set_flashdata(ERROR_TAX_RETURN_DATE_SENT, ERROR_STRING_TAX_RETURN_DATE_SENT);
								}
							}
							else if($return[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID] == '')
							{
								$isValid = false;
								$this->session->set_flashdata(ERROR_TAX_RETURN_DATE_SENT, ERROR_STRING_TAX_RETURN_DATE_SENT);
							}
							$this->session->set_flashdata(TAX_RETURN_DATE_SENT, $return[TAX_RETURN_DATE_SENT]);
						}
				        // Validate Date Released
				        if(isset($return[TAX_RETURN_DATE_RELEASED]))
				        {
				        	$return[TAX_RETURN_DATE_RELEASED] = stripslashes(trim($return[TAX_RETURN_DATE_RELEASED]));
				        	if($return[TAX_RETURN_DATE_RELEASED] == JS_DATE_RELEASED || $return[TAX_RETURN_DATE_RELEASED] == NOT_SENT)
				        	{
				        		$return[TAX_RETURN_DATE_RELEASED] == '';
				        	}
							else if ($return[TAX_RETURN_DATE_RELEASED] != '')
							{
								//check to make sure its a valid date
								$t = strtotime($return[TAX_RETURN_DATE_RELEASED]);
								if (!checkdate(date('m', $t), date('d', $t), date('Y', $t)))
								{
									$isValid = false;
									$this->session->set_flashdata(ERROR_TAX_RETURN_DATE_RELEASED, ERROR_STRING_TAX_RETURN_DATE_RELEASED);
								}
							}
							$this->session->set_flashdata(TAX_RETURN_DATE_RELEASED, $return[TAX_RETURN_DATE_RELEASED]);
						}
						// Validate Tax Form  ID
						if (isset($return[TAX_RETURN_VIEW_TAX_FORM_ID]) && $return[TAX_RETURN_VIEW_TAX_FORM_ID] != PLEASE_SELECT_ID && $return[TAX_RETURN_VIEW_TAX_FORM_ID] > 0)
						{
							 $this->session->set_flashdata(TAX_RETURN_VIEW_TAX_FORM_ID, $return[TAX_RETURN_VIEW_TAX_FORM_ID]);
						}
						else
						{
							$isValid = false;
							$this->session->set_flashdata(ERROR_TAX_FORM_ID, ERROR_STRING_TAX_FORM_ID);
						}
						// Validate Tax Year
						if (isset($return[TAX_RETURN_VIEW_TAX_YEAR]) && $return[TAX_RETURN_VIEW_TAX_YEAR] != PLEASE_SELECT_ID && $return[TAX_RETURN_VIEW_TAX_YEAR] > 0 )
						{
							$this->session->set_flashdata(TAX_RETURN_VIEW_TAX_YEAR, $return[TAX_RETURN_VIEW_TAX_YEAR]);
						}
						else
						{
							$isValid = false;
							$this->session->set_flashdata(ERROR_TAX_YEAR_ID, ERROR_STRING_TAX_YEAR_ID);
						}
						// Validate Tax Quarter
						if (isset($return[TAX_RETURN_VIEW_TAX_QUARTER]) && $return[TAX_RETURN_VIEW_TAX_QUARTER] != PLEASE_SELECT_ID)
						{
							$this->session->set_flashdata(TAX_RETURN_VIEW_TAX_QUARTER, $return[TAX_RETURN_VIEW_TAX_QUARTER]);
						}
						else
						{
							$return[TAX_RETURN_VIEW_TAX_QUARTER] = NOT_APPLICABLE_ID;
							$this->session->set_flashdata(TAX_RETURN_VIEW_TAX_QUARTER, $return[TAX_RETURN_VIEW_TAX_QUARTER]);
						}
						// Valid Tax Month
						if (isset($return[TAX_RETURN_VIEW_TAX_MONTH_ID]) && $return[TAX_RETURN_VIEW_TAX_MONTH_ID] != PLEASE_SELECT_ID)
						{
							$this->session->set_flashdata(TAX_RETURN_VIEW_TAX_MONTH_ID, $return[TAX_RETURN_VIEW_TAX_MONTH_ID]);
						}
						else
						{
							$return[TAX_RETURN_VIEW_TAX_MONTH_ID] = NOT_APPLICABLE_ID;
							$this->session->set_flashdata(TAX_RETURN_VIEW_TAX_MONTH_ID, $return[TAX_RETURN_VIEW_TAX_MONTH_ID]);
						}
						// Validate Status ID
						if (isset($return[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID]) && $return[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID] != PLEASE_SELECT_ID && $return[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID] > 0)
						{
							$this->session->set_flashdata(TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID, $return[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID]);
						}
						else
						{
							$isValid = false;
							$this->session->set_flashdata(ERROR_TAX_RETURN_STATUS_ID, ERROR_STRING_TAX_RETURN_STATUS_ID);
						}
						// Valide Instructions ID
						if (isset($return[TAX_RETURN_VIEW_INSTRUCTIONS_ID]) && $return[TAX_RETURN_VIEW_INSTRUCTIONS_ID] != PLEASE_SELECT_ID)
						{
							$this->session->set_flashdata(TAX_RETURN_VIEW_INSTRUCTIONS_ID, $return[TAX_RETURN_VIEW_INSTRUCTIONS_ID]);
						}
						else
						{
							$return[TAX_RETURN_VIEW_INSTRUCTIONS_ID] = 0;
							$this->session->set_flashdata(TAX_RETURN_VIEW_INSTRUCTIONS_ID, $return[TAX_RETURN_VIEW_INSTRUCTIONS_ID]);
						}
						// Validate Parter ID
						if(isset($return[TAX_RETURN_VIEW_PARTNER_ID]) && $return[TAX_RETURN_VIEW_PARTNER_ID] != PLEASE_SELECT_ID && $return[TAX_RETURN_VIEW_PARTNER_ID] > 0)
						{
							$this->session->set_flashdata(TAX_RETURN_VIEW_PARTNER_ID, $return[TAX_RETURN_VIEW_PARTNER_ID]);
						}
						else
						{
							$isValid = false;
							$this->session->set_flashdata(ERROR_TAX_RETURN_PARTNER_ID, ERROR_STRING_TAX_RETURN_PARTNER_ID);
						}
						// Validate STAFF ID
						if(isset($return[TAX_RETURN_VIEW_STAFF_ID]) && $return[TAX_RETURN_VIEW_STAFF_ID] != PLEASE_SELECT_ID && $return[TAX_RETURN_VIEW_STAFF_ID] > 0)
						{
							$this->session->set_flashdata(TAX_RETURN_VIEW_STAFF_ID, $return[TAX_RETURN_VIEW_STAFF_ID]);
						}
						else
						{
							$return[TAX_RETURN_VIEW_STAFF_ID] = 0;
						}
						// Validate 1st Reviewer
						if(isset($return[TAX_RETURN_REVIEWER_1_ID]) && $return[TAX_RETURN_REVIEWER_1_ID] != PLEASE_SELECT_ID && $return[TAX_RETURN_REVIEWER_1_ID] > 0)
						{
							$this->session->set_flashdata(TAX_RETURN_REVIEWER_1_ID, $return[TAX_RETURN_REVIEWER_1_ID]);
						}
						else
						{
							$return[TAX_RETURN_REVIEWER_1_ID] = 0;
						}
						// Validate 2nd Reviewer
						if(isset($return[TAX_RETURN_REVIEWER_2_ID]) && $return[TAX_RETURN_REVIEWER_2_ID] != PLEASE_SELECT_ID && $return[TAX_RETURN_REVIEWER_2_ID] > 0)
						{
							$this->session->set_flashdata(TAX_RETURN_REVIEWER_2_ID, $return[TAX_RETURN_REVIEWER_2_ID]);
						}
						else
						{
							$return[TAX_RETURN_REVIEWER_2_ID] = 0;
						}
						// Validate Tax Return Notes
						if(isset($return[TAX_RETURN_NOTES]) && trim($return[TAX_RETURN_NOTES]) == JS_TAX_RETURN_NOTES)
						{
							$return[TAX_RETURN_NOTES] = '';
							$this->session->set_flashdata(TAX_RETURN_NOTES, $return[TAX_RETURN_NOTES]);
						}
						else if(isset($return[TAX_RETURN_NOTES]))
						{
							$return[TAX_RETURN_NOTES] = trim($return[TAX_RETURN_NOTES]);
							$this->session->set_flashdata(TAX_RETURN_NOTES, $return[TAX_RETURN_NOTES]);
						}
						// if all is validated, insert the new return
						if ($isValid)
						{
							// array to INSERT Tax Return Information
							$Return = array
							(
								TAX_RETURN_CLIENT_ID			=> $id,
								TAX_RETURN_STATUS_ID			=> $return[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID],
								TAX_RETURN_TAX_FORM_ID			=> $return[TAX_RETURN_VIEW_TAX_FORM_ID],
								TAX_RETURN_TAX_YEAR_ID			=> $return[TAX_RETURN_VIEW_TAX_YEAR],
								TAX_RETURN_TAX_QUARTER_ID		=> $return[TAX_RETURN_VIEW_TAX_QUARTER],
								TAX_RETURN_TAX_MONTH_ID			=> $return[TAX_RETURN_VIEW_TAX_MONTH_ID],
								TAX_RETURN_PARTNER_ID			=> $return[TAX_RETURN_VIEW_PARTNER_ID],
								TAX_RETURN_STAFF_ID				=> $return[TAX_RETURN_VIEW_STAFF_ID],
								TAX_RETURN_REVIEWER_1_ID		=> $return[TAX_RETURN_VIEW_REVIEWER_1_ID],
								TAX_RETURN_REVIEWER_2_ID		=> $return[TAX_RETURN_VIEW_REVIEWER_2_ID],
								TAX_RETURN_NOTES				=> stripslashes(trim($return[TAX_RETURN_VIEW_TAX_RETURN_NOTES])),
								TAX_RETURN_INSTRUCTIONS_ID		=> $return[TAX_RETURN_VIEW_INSTRUCTIONS_ID],
								TAX_RETURN_DATE_RELEASED		=> strtotime(stripslashes(trim($return[TAX_RETURN_DATE_RELEASED]))),
								TAX_RETURN_DATE_SENT			=> strtotime(stripslashes(trim($return[TAX_RETURN_DATE_SENT]))),
								TAX_RETURN_VIEW_990PF_DATE		=> strtotime(stripslashes(trim($return[TAX_RETURN_VIEW_990PF_DATE]))),
								TAX_RETURN_STAMP_DATE			=> time(),
								TAX_RETURN_STAMP_USER_ID		=> $User_ID,
								TAX_RETURN_LAST_EDIT_DATE		=> time(),
								TAX_RETURN_LAST_EDIT_USER_ID	=> $User_ID
							);
							if ($this->Tax_return_model->add($Return))
							{
								$this->session->set_flashdata(STATUS_MESSAGE, 'Tax return added successfully.');
								header('Location: ' . base_url() . '/tax_return/view');
							}
							else
							{
								$this->session->set_flashdata(ERROR_MESSAGE, 'There was a problem adding this Tax Form. Please review the errors below and try again.');
								header('Location: ' . base_url() . '/tax_return/add');
							}
						}
						else
						{
							$this->session->set_flashdata(ERROR_MESSAGE, 'There was a problem adding this Tax Form. Please review the errors below and try again.');
							header('Location: ' . base_url() . '/tax_return/add');
						}
					}
					else
					{
						header('Location: ' . base_url() . '/tax_return/add');
					}
				}
				catch(Exception $ex)
				{
					log_message('error',  $e->getMessage());
					show_error( $e->getMessage());
				}
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'You need Administrative or Write access to access this page.');
				header('Location: ' . base_url());
			}
		}
		
function add_tax_multiple()
{
      if ($this->session->userdata(ROLE_ADMIN) || $this->session->userdata(ROLE_WRITE)) {
            try {
                if ($this->session->userdata('multiple')) {
                    $list = $this->session->userdata('multiple');                    
                    $month = array();
                    $quarter = array();
                    $status = array();                    
                    foreach ($_POST['tax_return'] as $key => $value) {                        
                       if (strpos($key, '_Status_ID_')) {
                          $id = str_replace('Tax_Return_Status_ID_', '', $key);
                          $status[$id] = $value; 
                       }                         
                       if (strpos($key, '_Month_ID')) {
                          $id = str_replace('Tax_Month_ID_', '', $key);
                          $month[$id] = $value; 
                       }                         
                       if (strpos($key, '_Quarter_')) {
                          $id = str_replace('Tax_Quarter_', '', $key);
                          $quarter[$id] = $value; 
                       }                         
                    }                    
                    foreach ($list as $key => $value) {                        
                        $return = $value;                        
                        $isValid = true;
                        
                        $return[TAX_RETURN_VIEW_TAX_QUARTER] = $quarter[$value[TAX_RETURN_ID]];
                        $return[TAX_RETURN_VIEW_TAX_MONTH_ID] = $month[$value[TAX_RETURN_ID]];
                        $return[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID] = $status[$value[TAX_RETURN_ID]];
                        if ($value[TAX_RETURN_VIEW_TAX_YEAR_MULTIPLE]) {
                            $return[TAX_RETURN_VIEW_TAX_YEAR] = $value[TAX_RETURN_VIEW_TAX_YEAR_MULTIPLE];
                        }
                        
                        //$return = $_POST[TAX_RETURN_POST];
                        if (isset($_POST[TAX_RETURN_VIEW_990PF_DATE])) {
                            $return[TAX_RETURN_VIEW_990PF_DATE] = stripslashes(trim($_POST[TAX_RETURN_VIEW_990PF_DATE]));
                            if ($return[TAX_RETURN_VIEW_990PF_DATE] == '' || $_POST[TAX_RETURN_VIEW_990PF_DATE] == JS_990PF_DATE) {
                                $return[TAX_RETURN_VIEW_990PF_DATE] = 0;
                                $this->session->set_flashdata(ERROR_TAX_RETURN_VIEW_990PF_DATE, ERROR_STRING_TAX_RETURN_VIEW_990PF_DATE);
                            } else {
                                $this->session->set_flashdata(TAX_RETURN_VIEW_990PF_DATE, $return[TAX_RETURN_VIEW_990PF_DATE]);
                            }
                        } else {
                            $return[TAX_RETURN_VIEW_990PF_DATE] = 0;
                        }
                        // Get UserID
                        $User_ID = $this->session->userdata(STAFF_ID);
                        // Validate Client Name
                        if (isset($return[CLIENT_NAME]) && $return[CLIENT_NAME] != ANY_VALUE) {
                            $id = $this->utilities->get_client_id($return[CLIENT_NAME]);
                            $this->session->set_flashdata(CLIENT_ID, $id);
                            if (!is_numeric($id) || $id < 1) {
                                // Invalid Client ID... try again
                                $this->session->set_flashdata(ERROR_CLIENT_ID, 'The client you chose was invalid or does not exist.');
                                $isValid = false;
                            } else {
                                $this->session->set_flashdata(TAX_RETURN_VIEW_CLIENT_NAME, $return[CLIENT_NAME]);
                            }
                        } else {
                            $this->session->set_flashdata(ERROR_CLIENT_ID, ERROR_STRING_CLIENT_ID);
                            $isValid = false;
                        }
                        // Validate Date Sent
                        if (isset($return[TAX_RETURN_DATE_SENT])) {
                            $return[TAX_RETURN_DATE_SENT] = stripslashes(trim($return[TAX_RETURN_DATE_SENT]));
                            if ($return[TAX_RETURN_DATE_SENT] == JS_DATE_SENT && $return[TAX_RETURN_DATE_SENT] == NOT_SENT) {
                                $return[TAX_RETURN_DATE_SENT] = '';
                            } else if ($return[TAX_RETURN_DATE_SENT] != '') {
                                //check to make sure its a valid date
                                $t = strtotime(stripslashes(trim($return[TAX_RETURN_DATE_SENT])));
                                if (!checkdate(date('m', $t), date('d', $t), date('Y', $t))) {
                                    $isValid = false;
                                    $this->session->set_flashdata(ERROR_TAX_RETURN_DATE_SENT, ERROR_STRING_TAX_RETURN_DATE_SENT);
                                }
                            } else if ($return[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID] == '') {
                                $isValid = false;
                                $this->session->set_flashdata(ERROR_TAX_RETURN_DATE_SENT, ERROR_STRING_TAX_RETURN_DATE_SENT);
                            }
                            $this->session->set_flashdata(TAX_RETURN_DATE_SENT, $return[TAX_RETURN_DATE_SENT]);
                        }
                        // Validate Date Released
                        if (isset($return[TAX_RETURN_DATE_RELEASED])) {
                            $return[TAX_RETURN_DATE_RELEASED] = stripslashes(trim($return[TAX_RETURN_DATE_RELEASED]));
                            if ($return[TAX_RETURN_DATE_RELEASED] == JS_DATE_RELEASED || $return[TAX_RETURN_DATE_RELEASED] == NOT_SENT) {
                                $return[TAX_RETURN_DATE_RELEASED] == '';
                            } else if ($return[TAX_RETURN_DATE_RELEASED] != '') {
                                //check to make sure its a valid date
                                $t = strtotime($return[TAX_RETURN_DATE_RELEASED]);
                                if (!checkdate(date('m', $t), date('d', $t), date('Y', $t))) {
                                    $isValid = false;
                                    $this->session->set_flashdata(ERROR_TAX_RETURN_DATE_RELEASED, ERROR_STRING_TAX_RETURN_DATE_RELEASED);
                                }
                            }
                            $this->session->set_flashdata(TAX_RETURN_DATE_RELEASED, $return[TAX_RETURN_DATE_RELEASED]);
                        }
                        // Validate Tax Form  ID
                        if (isset($return[TAX_RETURN_VIEW_TAX_FORM_ID]) && $return[TAX_RETURN_VIEW_TAX_FORM_ID] != PLEASE_SELECT_ID && $return[TAX_RETURN_VIEW_TAX_FORM_ID] > 0) {
                            $this->session->set_flashdata(TAX_RETURN_VIEW_TAX_FORM_ID, $return[TAX_RETURN_VIEW_TAX_FORM_ID]);
                        } else {
                            $isValid = false;
                            $this->session->set_flashdata(ERROR_TAX_FORM_ID, ERROR_STRING_TAX_FORM_ID);
                        }
                        // Validate Tax Year
                        if (isset($return[TAX_RETURN_VIEW_TAX_YEAR]) && $return[TAX_RETURN_VIEW_TAX_YEAR] != PLEASE_SELECT_ID && $return[TAX_RETURN_VIEW_TAX_YEAR] > 0) {
                            $this->session->set_flashdata(TAX_RETURN_VIEW_TAX_YEAR, $return[TAX_RETURN_VIEW_TAX_YEAR]);
                        } else {
                            $isValid = false;
                            $this->session->set_flashdata(ERROR_TAX_YEAR_ID, ERROR_STRING_TAX_YEAR_ID);
                        }
                        // Validate Tax Quarter
                        if (isset($return[TAX_RETURN_VIEW_TAX_QUARTER]) && $return[TAX_RETURN_VIEW_TAX_QUARTER] != PLEASE_SELECT_ID) {
                            $this->session->set_flashdata(TAX_RETURN_VIEW_TAX_QUARTER, $return[TAX_RETURN_VIEW_TAX_QUARTER]);
                        } else {
                            $return[TAX_RETURN_VIEW_TAX_QUARTER] = NOT_APPLICABLE_ID;
                            $this->session->set_flashdata(TAX_RETURN_VIEW_TAX_QUARTER, $return[TAX_RETURN_VIEW_TAX_QUARTER]);
                        }                        
                        // Valid Tax Month
                        if (isset($return[TAX_RETURN_VIEW_TAX_MONTH_ID]) && $return[TAX_RETURN_VIEW_TAX_MONTH_ID] != PLEASE_SELECT_ID) {
                            $this->session->set_flashdata(TAX_RETURN_VIEW_TAX_MONTH_ID, $return[TAX_RETURN_VIEW_TAX_MONTH_ID]);
                        } else {
                            $return[TAX_RETURN_VIEW_TAX_MONTH_ID] = NOT_APPLICABLE_ID;
                            $this->session->set_flashdata(TAX_RETURN_VIEW_TAX_MONTH_ID, $return[TAX_RETURN_VIEW_TAX_MONTH_ID]);
                        }
                        // Validate Status ID
                        if (isset($return[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID]) && $return[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID] != PLEASE_SELECT_ID && $return[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID] > 0) {
                            $this->session->set_flashdata(TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID, $return[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID]);
                        } else {
                            $isValid = false;
                            $this->session->set_flashdata(ERROR_TAX_RETURN_STATUS_ID, ERROR_STRING_TAX_RETURN_STATUS_ID);
                        }
                        // Valide Instructions ID
                        if (isset($return[TAX_RETURN_VIEW_INSTRUCTIONS_ID]) && $return[TAX_RETURN_VIEW_INSTRUCTIONS_ID] != PLEASE_SELECT_ID) {
                            $this->session->set_flashdata(TAX_RETURN_VIEW_INSTRUCTIONS_ID, $return[TAX_RETURN_VIEW_INSTRUCTIONS_ID]);
                        } else {
                            $return[TAX_RETURN_VIEW_INSTRUCTIONS_ID] = 0;
                            $this->session->set_flashdata(TAX_RETURN_VIEW_INSTRUCTIONS_ID, $return[TAX_RETURN_VIEW_INSTRUCTIONS_ID]);
                        }
                        // Validate Parter ID
                        if (isset($return[TAX_RETURN_VIEW_PARTNER_ID]) && $return[TAX_RETURN_VIEW_PARTNER_ID] != PLEASE_SELECT_ID && $return[TAX_RETURN_VIEW_PARTNER_ID] > 0) {
                            $this->session->set_flashdata(TAX_RETURN_VIEW_PARTNER_ID, $return[TAX_RETURN_VIEW_PARTNER_ID]);
                        } else {
                            $isValid = false;
                            $this->session->set_flashdata(ERROR_TAX_RETURN_PARTNER_ID, ERROR_STRING_TAX_RETURN_PARTNER_ID);
                        }
                        // Validate STAFF ID
                        if (isset($return[TAX_RETURN_VIEW_STAFF_ID]) && $return[TAX_RETURN_VIEW_STAFF_ID] != PLEASE_SELECT_ID && $return[TAX_RETURN_VIEW_STAFF_ID] > 0) {
                            $this->session->set_flashdata(TAX_RETURN_VIEW_STAFF_ID, $return[TAX_RETURN_VIEW_STAFF_ID]);
                        } else {
                            $return[TAX_RETURN_VIEW_STAFF_ID] = 0;
                        }
                        // Validate 1st Reviewer
                        if (isset($return[TAX_RETURN_REVIEWER_1_ID]) && $return[TAX_RETURN_REVIEWER_1_ID] != PLEASE_SELECT_ID && $return[TAX_RETURN_REVIEWER_1_ID] > 0) {
                            $this->session->set_flashdata(TAX_RETURN_REVIEWER_1_ID, $return[TAX_RETURN_REVIEWER_1_ID]);
                        } else {
                            $return[TAX_RETURN_REVIEWER_1_ID] = 0;
                        }
                        // Validate 2nd Reviewer
                        if (isset($return[TAX_RETURN_REVIEWER_2_ID]) && $return[TAX_RETURN_REVIEWER_2_ID] != PLEASE_SELECT_ID && $return[TAX_RETURN_REVIEWER_2_ID] > 0) {
                            $this->session->set_flashdata(TAX_RETURN_REVIEWER_2_ID, $return[TAX_RETURN_REVIEWER_2_ID]);
                        } else {
                            $return[TAX_RETURN_REVIEWER_2_ID] = 0;
                        }
                        // Validate Tax Return Notes
                        if (isset($return[TAX_RETURN_NOTES]) && trim($return[TAX_RETURN_NOTES]) == JS_TAX_RETURN_NOTES) {
                            $return[TAX_RETURN_NOTES] = '';
                            $this->session->set_flashdata(TAX_RETURN_NOTES, $return[TAX_RETURN_NOTES]);
                        } else if (isset($return[TAX_RETURN_NOTES])) {
                            $return[TAX_RETURN_NOTES] = trim($return[TAX_RETURN_NOTES]);
                            $this->session->set_flashdata(TAX_RETURN_NOTES, $return[TAX_RETURN_NOTES]);
                        }
                        //krumo('isValid=' . $isValid);
                        // if all is validated, insert the new return
                        if ($isValid) {
                            // array to INSERT Tax Return Information
                            $Return = array
                                (
                                TAX_RETURN_CLIENT_ID => $id,
                                TAX_RETURN_STATUS_ID => $return[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS_ID],
                                TAX_RETURN_TAX_FORM_ID => $return[TAX_RETURN_VIEW_TAX_FORM_ID],
                                TAX_RETURN_TAX_YEAR_ID => $return[TAX_RETURN_VIEW_TAX_YEAR],
                                TAX_RETURN_TAX_QUARTER_ID => $return[TAX_RETURN_VIEW_TAX_QUARTER],
                                TAX_RETURN_TAX_MONTH_ID => $return[TAX_RETURN_VIEW_TAX_MONTH_ID],
                                TAX_RETURN_PARTNER_ID => $return[TAX_RETURN_VIEW_PARTNER_ID],
                                TAX_RETURN_STAFF_ID => $return[TAX_RETURN_VIEW_STAFF_ID],
                                TAX_RETURN_REVIEWER_1_ID => $return[TAX_RETURN_VIEW_REVIEWER_1_ID],
                                TAX_RETURN_REVIEWER_2_ID => $return[TAX_RETURN_VIEW_REVIEWER_2_ID],
                                TAX_RETURN_NOTES => stripslashes(trim($return[TAX_RETURN_VIEW_TAX_RETURN_NOTES])),
                                TAX_RETURN_INSTRUCTIONS_ID => $return[TAX_RETURN_VIEW_INSTRUCTIONS_ID],
                                TAX_RETURN_DATE_RELEASED => strtotime(stripslashes(trim($return[TAX_RETURN_DATE_RELEASED]))),
                                TAX_RETURN_DATE_SENT => strtotime(stripslashes(trim($return[TAX_RETURN_DATE_SENT]))),
                                TAX_RETURN_VIEW_990PF_DATE => strtotime(stripslashes(trim($return[TAX_RETURN_VIEW_990PF_DATE]))),
                                TAX_RETURN_STAMP_DATE => time(),
                                TAX_RETURN_STAMP_USER_ID => $User_ID,
                                TAX_RETURN_LAST_EDIT_DATE => time(),
                                TAX_RETURN_LAST_EDIT_USER_ID => $User_ID
                            );
                            
                            if ($this->Tax_return_model->add($Return)) {
                                $this->session->set_flashdata(STATUS_MESSAGE, 'Tax return added successfully.');
                                header('Location: ' . base_url() . '/tax_return/view');
                            } else {
                                $this->session->set_flashdata(ERROR_MESSAGE, 'There was a problem adding this Tax Form. Please review the errors below and try again.');
                                header('Location: ' . base_url() . '/tax_return/view_multiple/0/0/Client_Name/ASC');
                            }
                            
                        } else {
                            $this->session->set_flashdata(ERROR_MESSAGE, 'There was a problem adding this Tax Form. Please review the errors below and try again.');
                            header('Location: ' . base_url() . '/tax_return/view_multiple/0/0/Client_Name/ASC');
                        }
                    }
                } else {
                    header('Location: ' . base_url() . '/tax_return/add_multiple');
                }
            } catch (Exception $ex) {
                log_message('error', $e->getMessage());
                show_error($e->getMessage());
            }
        } else {
            $this->session->set_flashdata(ERROR_MESSAGE, 'You need Administrative or Write access to access this page.');
            header('Location: ' . base_url());
        }
    }
		
                
                //------------START MULTIPLE
		// Add a new Tax Return
		function add_tax_return_multiple()
		{
			if ($this->session->userdata(ROLE_ADMIN) || $this->session->userdata(ROLE_WRITE))
			{
				try
				{
					if (isset($_POST[TAX_RETURN_POST]))
					{
						$isValid = true;
						$return = $_POST[TAX_RETURN_POST];
                                                                                         
                                                // Validate Parter ID
						if(isset($return[TAX_RETURN_VIEW_PARTNER_ID]) && $return[TAX_RETURN_VIEW_PARTNER_ID] != PLEASE_SELECT_ID && $return[TAX_RETURN_VIEW_PARTNER_ID] > 0)
						{
							$this->session->set_flashdata(TAX_RETURN_VIEW_PARTNER_ID, $return[TAX_RETURN_VIEW_PARTNER_ID]);
						}
						else
						{
							$isValid = false;
							$this->session->set_flashdata(ERROR_TAX_RETURN_PARTNER_ID, ERROR_STRING_TAX_RETURN_PARTNER_ID);
						}                                                                                          
						// Validate Tax Form  ID
						if (isset($return[TAX_RETURN_VIEW_TAX_FORM_ID]) && $return[TAX_RETURN_VIEW_TAX_FORM_ID] != PLEASE_SELECT_ID && $return[TAX_RETURN_VIEW_TAX_FORM_ID] > 0)
						{
							 $this->session->set_flashdata(TAX_RETURN_VIEW_TAX_FORM_ID, $return[TAX_RETURN_VIEW_TAX_FORM_ID]);
						}
						else
						{
							$isValid = false;
							$this->session->set_flashdata(ERROR_TAX_FORM_ID, ERROR_STRING_TAX_FORM_ID);
						}
						// Validate Tax Year
						if (isset($return[TAX_RETURN_VIEW_TAX_YEAR]) && $return[TAX_RETURN_VIEW_TAX_YEAR] != PLEASE_SELECT_ID && $return[TAX_RETURN_VIEW_TAX_YEAR] > 0 )
						{
							$this->session->set_flashdata(TAX_RETURN_VIEW_TAX_YEAR, $return[TAX_RETURN_VIEW_TAX_YEAR]);
						}
						else
						{
							$isValid = false;
							$this->session->set_flashdata(ERROR_TAX_YEAR_ID, ERROR_STRING_TAX_YEAR_ID);
						}

	

						// if all is validated, insert the new return
						if ($isValid)
						{
							// array to INSERT Tax Return Information
							$Return = array
							(
								TAX_RETURN_TAX_FORM_ID			=> $return[TAX_RETURN_VIEW_TAX_FORM_ID],
								TAX_RETURN_TAX_YEAR_ID			=> $return[TAX_RETURN_VIEW_TAX_YEAR],
								TAX_RETURN_PARTNER_ID			=> $return[TAX_RETURN_VIEW_PARTNER_ID]
							);    
                            $this->view_multiple($Return);
						}
						else
						{
							$this->session->set_flashdata(ERROR_MESSAGE, 'There was a problem adding this Tax Form. Please review the errors below and try again.');
							header('Location: ' . base_url() . '/tax_return/add_multiple');
						}
					}
					else
					{
						header('Location: ' . base_url() . '/tax_return/add_multiple');
					}
				}
				catch(Exception $ex)
				{
					log_message('error',  $e->getMessage());
					show_error( $e->getMessage());
				}
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'You need Administrative or Write access to access this page.');
				header('Location: ' . base_url());
			}
		}
                //------------END MULTIPLE  
                
		function ajax_get_info()
		{
			print_r($this->Tax_return_model->select());
			return $this->Tax_return_model->select();
		}
		// Page View
		function view($page='', $order=TAX_RETURN_VIEW_CLIENT_NAME, $as = SORT_ORDER)
		{
			try
			{
				if ($this->session->userdata(ROLE_WRITE) || $this->session->userdata(ROLE_READ_ONLY))
				{
					if (isset($_POST['clear']) || (!$this->session->userdata('tax_filter')))
					{
						$tax_returns = array('page' => 0);
					}
					elseif (isset($_POST[TAX_RETURN_POST]))
					{
						$tax_returns = $_POST[TAX_RETURN_POST];
						$tax_returns['page'] = 0;
						$tax_returns['order'] = $order;
						$tax_returns['as'] = $as;
						$this->session->set_userdata('tax_filter', $tax_returns);
					} 
					else
					{
						$tax_returns = $this->session->userdata('tax_filter');
						$tax_returns['page'] = ($page != '') ? $page : $tax_returns['page'];
					}

					$this->session->set_userdata('tax_filter', $tax_returns);

					// Check Exclude Sent Returns Checkbox Filter
					$send_date = '';
					if (isset($tax_returns['sent_returns']) && $tax_returns['sent_returns'] == 'on')
					{
						$send_date = 1; 
					}
					// Check Exclude Released checkbox filter
					$released_date = '';
					if (isset($tax_returns['released_returns']) && $tax_returns['released_returns'] == 'on')
					{
						$released_date = 1;
					}
					// Check Exclude Prepared By Client Checkbox Filter
					$exclude_prepared_by_client = '';
					if (isset($tax_returns['exclude_prepared_by_client']) && $tax_returns['exclude_prepared_by_client'] == 'on')
					{
						$exclude_prepared_by_client = 1;
					}
					// Check Exclude Signature Pages Checkbox Filter
					$exclude_signature_pages = '';
					if (isset($tax_returns['exclude_signature_pages']) && $tax_returns['exclude_signature_pages'] == 'on')
					{
						$exclude_signature_pages = 1;
					}
					// Check Extension(s) Filed Filter
					$extension_filed = '';
					if (isset($tax_returns['extension_filed']) && $tax_returns['extension_filed'] == 'on')
					{
						$extension_filed = 1;
					}
					if(isset($_POST['clear']))
					{
						$send_date = null;
						$exclude_prepared_by_client = null;
						$exclude_signature_pages = null;
						$extension_filed = null;
					}
					$tax_returns[TAX_RETURN_VIEW_CLIENT_NAME]	 		= isset($tax_returns[TAX_RETURN_VIEW_CLIENT_NAME]) 			? $tax_returns[TAX_RETURN_VIEW_CLIENT_NAME] 				: ANY_LABEL;
					$tax_returns[TAX_RETURN_VIEW_PARTNER_ID] 			= isset($tax_returns[TAX_RETURN_VIEW_PARTNER_ID]) 			? $tax_returns[TAX_RETURN_VIEW_PARTNER_ID] 				: ANY_VALUE;
					$tax_returns[TAX_RETURN_VIEW_TAX_FORM_ID]	 		= isset($tax_returns[TAX_RETURN_VIEW_TAX_FORM_ID]) 			? $tax_returns[TAX_RETURN_VIEW_TAX_FORM_ID] 				: ANY_VALUE;
					$tax_returns[TAX_RETURN_VIEW_TAX_YEAR] 				= isset($tax_returns[TAX_RETURN_VIEW_TAX_YEAR]) 			? $tax_returns[TAX_RETURN_VIEW_TAX_YEAR] 				: ANY_VALUE;
					$tax_returns[TAX_RETURN_VIEW_TAX_QUARTER] 			= isset($tax_returns[TAX_RETURN_VIEW_TAX_QUARTER]) 			? $tax_returns[TAX_RETURN_VIEW_TAX_QUARTER] 				: ANY_VALUE;
					$tax_returns[TAX_RETURN_VIEW_TAX_MONTH_ID] 			= isset($tax_returns[TAX_RETURN_VIEW_TAX_MONTH_ID]) 		? $tax_returns[TAX_RETURN_VIEW_TAX_MONTH_ID]				: ANY_VALUE;
					$tax_returns[TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID] 	= isset($tax_returns[TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID]) 	? $tax_returns[TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID]			: ANY_VALUE;
					$tax_returns[TAX_RETURN_VIEW_DATE_SENT] 			= isset($tax_returns[TAX_RETURN_VIEW_DATE_SENT])  			? stripslashes(trim($tax_returns[TAX_RETURN_VIEW_DATE_SENT]))	: ANY_LABEL;
					$tax_returns[TAX_RETURN_VIEW_DATE_RELEASED] 		= isset($tax_returns[TAX_RETURN_VIEW_DATE_RELEASED]) 		? stripslashes(trim($tax_returns[TAX_RETURN_VIEW_DATE_RELEASED]))	: ANY_LABEL;
					$tax_returns[TAX_RETURN_VIEW_TAX_RETURN_NOTES] 		= isset($tax_returns[TAX_RETURN_VIEW_TAX_RETURN_NOTES]) 	? stripslashes(trim($tax_returns[TAX_RETURN_VIEW_TAX_RETURN_NOTES])) : ANY_LABEL;
					$tax_returns[TAX_RETURN_VIEW_STAFF_ID] 				= isset($tax_returns[TAX_RETURN_VIEW_STAFF_ID]) 			? $tax_returns[TAX_RETURN_VIEW_STAFF_ID]				: ANY_VALUE;
					$tax_returns[TAX_RETURN_VIEW_REVIEWER_1_ID]			= isset($tax_returns[TAX_RETURN_VIEW_REVIEWER_1_ID]) 		? $tax_returns[TAX_RETURN_VIEW_REVIEWER_1_ID]			: ANY_VALUE;
					$tax_returns[TAX_RETURN_VIEW_REVIEWER_2_ID]			= isset($tax_returns[TAX_RETURN_VIEW_REVIEWER_2_ID]) 		? $tax_returns[TAX_RETURN_VIEW_REVIEWER_2_ID] 			: ANY_VALUE;
					$config['base_url'] = base_url() . TAX_RETURN_POST . '/view/';
					$config['cur_page'] = $tax_returns['page'];
					$config['per_page'] = PAGING_RECORDS_PER_PAGE;
					$config['num_links'] = PAGING_NUMBER_LINKS;
					$config['uri_segment'] = PAGING_URI_SEGMENTS;
					$config['full_tag_open'] = PAGING_FULL_OPEN_TAG;
					$config['full_tag_close'] = PAGING_FULL_CLOSE_TAG;
					$config['first_link'] = PAGING_FIRST;
					$config['last_link'] = PAGING_LAST;
					$config['next_link'] = PAGING_NEXT;
					$config['prev_link'] = PAGING_PREVIOUS;
					$config['cur_tag_open'] = PAGING_CURRENT_OPEN_TAG;
					$config['cur_tag_close'] = PAGING_CURRENT_CLOSE_TAG;
					$tax_return['list'] = $this->Tax_return_model->view($tax_returns, $tax_returns['page'], PAGING_RECORDS_PER_PAGE, $order, $as, 
						$send_date, $released_date, $exclude_signature_pages, $exclude_prepared_by_client, $extension_filed);                                       
					if ($tax_return['list'] != NO_RECORDS)
					{
						$config['total_rows'] = $tax_return['list'][0]['Total_Rows'];
					}
					else
					{
						$config['total_rows'] = 0;
					}
					$this->pagination->initialize($config);
					$tax_return['link'] = $this->pagination->create_links();
					$tax_return['page'] = $tax_returns['page'];
					$tax_return['as'] = $as;
					$tax_return['order'] = $order;
					$tax_return['Partners'] = $this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_PARTNER_ID, $this->utilities->get_partners(), STAFF_ID, STAFF_INITIALS, $tax_returns[TAX_RETURN_VIEW_PARTNER_ID], true);
					$tax_return['Tax_Forms'] = $this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_TAX_FORM_ID, $this->utilities->get_tax_forms(), TAX_FORM_ID, TAX_FORM, $tax_returns[TAX_RETURN_VIEW_TAX_FORM_ID], true);
					$tax_return['Tax_Years'] = $this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_TAX_YEAR, $this->utilities->get_tax_years(), TAX_YEAR_ID, TAX_YEAR, $tax_returns[TAX_RETURN_VIEW_TAX_YEAR], true);
					$tax_return['Tax_Quarters'] = $this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_TAX_QUARTER, $this->utilities->get_tax_quarters(), TAX_QUARTER_ID, TAX_QUARTER, $tax_returns[TAX_RETURN_VIEW_TAX_QUARTER], true);
					$tax_return['Tax_Months'] = $this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_TAX_MONTH_ID, $this->utilities->get_tax_months(), TAX_MONTH_ID, TAX_MONTH, $tax_returns[TAX_RETURN_VIEW_TAX_MONTH_ID], true);
					$tax_return['Tax_Status'] = $this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID, $this->utilities->get_tax_return_status(), TAX_RETURN_STATUS_ID, TAX_RETURN_STATUS, $tax_returns[TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID], true);
					$tax_return['Staff'] = $this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_STAFF_ID, $this->utilities->get_staff(), STAFF_ID, STAFF_INITIALS, $tax_returns[TAX_RETURN_VIEW_STAFF_ID], true);
					$tax_return['Reviewer1'] = $this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_REVIEWER_1_ID, $this->utilities->get_staff(), STAFF_ID, STAFF_INITIALS, $tax_returns[TAX_RETURN_VIEW_REVIEWER_1_ID], true);
					$tax_return['Reviewer2'] = $this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_REVIEWER_2_ID, $this->utilities->get_staff(), STAFF_ID, STAFF_INITIALS, $tax_returns[TAX_RETURN_VIEW_REVIEWER_2_ID], true);
					$tax_return['post'] = $tax_returns;
					$this->load->view(HEADER);
					$this->load->view(MENU);
					$this->load->view(TAX_RETURN_POST . '/view', $tax_return);
					$this->load->view(FOOTER);
				}
				else
				{
					header("Location: " . base_url());
				}
			}
			catch(Exception $ex)
			{
				log_message('error',  $e->getMessage());
				show_error( $e->getMessage());
			}
		}
    // View multiple table
    function view_multiple($tax_return, $page = '', $order = TAX_RETURN_VIEW_CLIENT_NAME, $as = SORT_ORDER) {
        try {
            if (!$tax_return) {
                $tax_return = $this->session->userdata('tax_return_multiple_post');
                $tax_return_updated_list = $this->session->userdata('multiple');
            } else {
                $tax_return_updated_list = 0;
                $this->session->unset_userdata('multiple');
                $this->session->set_userdata('tax_return_multiple_post', $tax_return);
            }

            if ($this->session->userdata(ROLE_WRITE) || $this->session->userdata(ROLE_READ_ONLY)) {
                if (isset($_POST['clear']) || (!$this->session->userdata('tax_filter'))) {
                    $tax_returns = array('page' => 0);
                } elseif (isset($_POST[TAX_RETURN_POST])) {
                    $tax_returns = $_POST[TAX_RETURN_POST];
                    $tax_returns['page'] = 0;
                    $tax_returns['order'] = $order;
                    $tax_returns['as'] = $as;
                    $this->session->set_userdata('tax_filter', $tax_returns);
                } else {
                    $tax_returns = $this->session->userdata('tax_filter');
                    $tax_returns['page'] = ($page != '') ? $page : $tax_returns['page'];
                }

                $this->session->set_userdata('tax_filter', $tax_returns);

                if (isset($_POST['clear'])) {
                    $send_date = null;
                    $exclude_prepared_by_client = null;
                    $exclude_signature_pages = null;
                    $extension_filed = null;
                }
                $tax_returns[TAX_RETURN_VIEW_CLIENT_NAME] = isset($tax_returns[TAX_RETURN_VIEW_CLIENT_NAME]) ? $tax_returns[TAX_RETURN_VIEW_CLIENT_NAME] : ANY_LABEL;
                $tax_returns[TAX_RETURN_VIEW_PARTNER_ID] = isset($tax_returns[TAX_RETURN_VIEW_PARTNER_ID]) ? $tax_returns[TAX_RETURN_VIEW_PARTNER_ID] : ANY_VALUE;
                $tax_returns[TAX_RETURN_VIEW_TAX_FORM_ID] = isset($tax_returns[TAX_RETURN_VIEW_TAX_FORM_ID]) ? $tax_returns[TAX_RETURN_VIEW_TAX_FORM_ID] : ANY_VALUE;
                $tax_returns[TAX_RETURN_VIEW_TAX_YEAR] = isset($tax_returns[TAX_RETURN_VIEW_TAX_YEAR]) ? $tax_returns[TAX_RETURN_VIEW_TAX_YEAR] : ANY_VALUE;
                $tax_returns[TAX_RETURN_VIEW_TAX_QUARTER] = isset($tax_returns[TAX_RETURN_VIEW_TAX_QUARTER]) ? $tax_returns[TAX_RETURN_VIEW_TAX_QUARTER] : ANY_VALUE;
                $tax_returns[TAX_RETURN_VIEW_TAX_MONTH_ID] = isset($tax_returns[TAX_RETURN_VIEW_TAX_MONTH_ID]) ? $tax_returns[TAX_RETURN_VIEW_TAX_MONTH_ID] : ANY_VALUE;
                $tax_returns[TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID] = isset($tax_returns[TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID]) ? $tax_returns[TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID] : ANY_VALUE;
                $tax_returns[TAX_RETURN_VIEW_DATE_SENT] = isset($tax_returns[TAX_RETURN_VIEW_DATE_SENT]) ? stripslashes(trim($tax_returns[TAX_RETURN_VIEW_DATE_SENT])) : ANY_LABEL;
                $tax_returns[TAX_RETURN_VIEW_DATE_RELEASED] = isset($tax_returns[TAX_RETURN_VIEW_DATE_RELEASED]) ? stripslashes(trim($tax_returns[TAX_RETURN_VIEW_DATE_RELEASED])) : ANY_LABEL;
                $tax_returns[TAX_RETURN_VIEW_TAX_RETURN_NOTES] = isset($tax_returns[TAX_RETURN_VIEW_TAX_RETURN_NOTES]) ? stripslashes(trim($tax_returns[TAX_RETURN_VIEW_TAX_RETURN_NOTES])) : ANY_LABEL;
                $tax_returns[TAX_RETURN_VIEW_STAFF_ID] = isset($tax_returns[TAX_RETURN_VIEW_STAFF_ID]) ? $tax_returns[TAX_RETURN_VIEW_STAFF_ID] : ANY_VALUE;
                $tax_returns[TAX_RETURN_VIEW_REVIEWER_1_ID] = isset($tax_returns[TAX_RETURN_VIEW_REVIEWER_1_ID]) ? $tax_returns[TAX_RETURN_VIEW_REVIEWER_1_ID] : ANY_VALUE;
                $tax_returns[TAX_RETURN_VIEW_REVIEWER_2_ID] = isset($tax_returns[TAX_RETURN_VIEW_REVIEWER_2_ID]) ? $tax_returns[TAX_RETURN_VIEW_REVIEWER_2_ID] : ANY_VALUE;


                $config['base_url'] = base_url() . TAX_RETURN_POST . '/view/';
                $config['cur_page'] = $tax_returns['page'];
                $config['per_page'] = PAGING_RECORDS_PER_PAGE;
                $config['num_links'] = PAGING_NUMBER_LINKS;
                $config['uri_segment'] = PAGING_URI_SEGMENTS;
                $config['full_tag_open'] = PAGING_FULL_OPEN_TAG;
                $config['full_tag_close'] = PAGING_FULL_CLOSE_TAG;
                $config['first_link'] = PAGING_FIRST;
                $config['last_link'] = PAGING_LAST;
                $config['next_link'] = PAGING_NEXT;
                $config['prev_link'] = PAGING_PREVIOUS;
                $config['cur_tag_open'] = PAGING_CURRENT_OPEN_TAG;
                $config['cur_tag_close'] = PAGING_CURRENT_CLOSE_TAG;

                //$tax_return['list'] = $this->Tax_return_model->view($tax_returns, $tax_returns['page'], PAGING_RECORDS_PER_PAGE, $order, $as,	$send_date, $released_date, $exclude_signature_pages, $exclude_prepared_by_client, $extension_filed);

                $ids = NULL;

                if ($tax_return_updated_list) {
                    $ids = array();
                    foreach ($tax_return_updated_list as $value) {
                        array_push($ids, $value['Tax_Return_ID']);
                    }
                    $ids = implode(',', $ids);
                }
                $tax_return['list'] = $this->Tax_return_model->view_multiple($tax_returns, $tax_returns['page'], PAGING_RECORDS_PER_PAGE, $order, $as, $ids);
                //krumo($tax_return['list']);
                $this->pagination->initialize($config);
                $tax_return['link'] = $this->pagination->create_links();
                $tax_return['page'] = $tax_returns['page'];
                $tax_return['as'] = $as;
                $tax_return['order'] = $order;
	                
                if ($tax_return['list'] && is_array($tax_return['list']) && $tax_return['list'] != 'No Records Found') {
	                // Set new values
	                $tax_return['list'] = $this->update_tax_returns_for_multiple($tax_return['list'], $tax_return['Tax_Year']);
	                //krumo($tax_return['list']); die();
	                foreach ($tax_return['list'] as $key => $value) {
	                    $month = $value['Tax_Month_ID'];
	                    if (!$month)
	                        $month = 12;
	                    $tax_return['Tax_Quarter_Drop'][$value[TAX_RETURN_ID]] = $this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_TAX_QUARTER, $this->utilities->get_tax_quarters(), TAX_QUARTER_ID, TAX_QUARTER, $value['Tax_Quarter_ID'], false);
	                    $tax_return['Tax_Month_Drop'][$value[TAX_RETURN_ID]] = $this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_TAX_MONTH_ID, $this->utilities->get_tax_months(), TAX_MONTH_ID, TAX_MONTH, $month, false);
	                    $tax_return['Tax_Status_Drop'][$value[TAX_RETURN_ID]] = $this->utilities->get_drop_down_html(TAX_RETURN_POST, TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID, $this->utilities->get_tax_return_status(), TAX_RETURN_STATUS_ID, TAX_RETURN_STATUS, 15, false);
	                }
	                if ($tax_return['list'] != NO_RECORDS) {
	                    $config['total_rows'] = $tax_return['list'][0]['Total_Rows'];
	                } else {
	                    $config['total_rows'] = 0;
	                }	                
	                //krumo($tax_return['list']);
	                $this->session->set_userdata('multiple', $tax_return['list']);
	                $this->load->view(HEADER);
	                $this->load->view(MENU);
	                $this->load->view(TAX_RETURN_POST . '/view_multiple', $tax_return);
	                $this->load->view(FOOTER);
            	}
            	else {
            		$this->load->view(HEADER);
	                $this->load->view(MENU);
	                $this->load->view(TAX_RETURN_POST . '/view_multiple', $tax_return);
	                $this->load->view(FOOTER);
            	}
            } else {
                header("Location: " . base_url());
            }
        } catch (Exception $ex) {
            log_message('error', $e->getMessage());
            show_error($e->getMessage());
        }
    }

    function update_tax_returns_for_multiple($list, $year) {
        foreach ($list as $n => $value) {
            if ($year != $value['Tax_Year']) {
                $value['Tax_Return_Status_ID'] = 15;
                $value['Tax_Return_Status'] = 'To Be Determined';
            }
            $value[TAX_RETURN_VIEW_TAX_YEAR_MULTIPLE] = $year;
            $value['Tax_Return_Notes'] = '';
            $value['Date_Released'] = '';
            $value['Date_Sent'] = '';
            $list[$n] = $value;
        }
        return $list;
    }

    //$this->load->view(TAX_RETURN_POST . '/view_multiple', $add_return);

    function delete_row($id) {
        $list = $this->session->userdata('multiple');
        foreach ($list as $key => $item) {
            if ($item['Tax_Return_ID'] == $id) {
                unset($list[$key]);
            }
        }
        $this->session->set_userdata('multiple', $list);
        $this->load->view(HEADER);
        $this->load->view(MENU);
        $this->load->view(TAX_RETURN_POST . '/view_multiple', $list);
        $this->load->view(FOOTER);
    }

    function delete_ajax_row() {
        $list = $this->session->userdata('multiple');
        foreach ($list as $key => $item) {
            if ($item['Tax_Return_ID'] == $_POST['id']) {
                unset($list[$key]);
            }
        }
        $this->session->set_userdata('multiple', $list);
        return 1;
    }
                
		function delete($id)
		{
			if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
			{
				$this->Tax_return_model->delete_tax_return($id);
				header("Location: " . base_url() . TAX_RETURN_POST . '/view');
			}
			else
			{
				header("Location: " . base_url());
			}
		}
                
                // Convert to Excel
	function convertToExcel()
	{
		try
		{
			if ($this->session->userdata(ROLE_WRITE) || $this->session->userdata(ROLE_READ_ONLY))
			{
				$list = $this->session->userdata('multiple');
				$output = 
					"Client Name\t" .
					"Partner\t" .
					"Form\t" .
					"Year\t" .
					"Year (Will be)\t" .
					"Quarter\t" .
					"Month\t" .
					"Return Status\t" .
					"Sent\t" .
					"Released\t" .
					"Notes\t" .
					"Preparer\t" .
					"1st Rev.\t" .
					"2und Rev.\t";
               
				foreach ($list as $key => $v)
				{
                                        if (!$v[TAX_RETURN_VIEW_DATE_SENT]) {
                                            $v[TAX_RETURN_VIEW_DATE_SENT] = 'Not send';
                                        }
                                        if (!$v[TAX_RETURN_VIEW_DATE_RELEASED]) {
                                            $v[TAX_RETURN_VIEW_DATE_RELEASED] = 'Not send';
                                        }
					$output .= 
						$v[TAX_RETURN_VIEW_CLIENT_NAME] . "\t" . 
						$v[CLIENT_VIEW_PARTNER_INITIALS] . "\t" . 
						$v[TAX_FORM] . "\t" .
						$v[TAX_RETURN_VIEW_TAX_YEAR] . "\t" .
						$v[TAX_RETURN_VIEW_TAX_YEAR_MULTIPLE] . "\t" . 
						$v[TAX_RETURN_VIEW_TAX_QUARTER] . "\t" . 
						$v[TAX_RETURN_VIEW_TAX_MONTH] . "\t" .
						$v[TAX_RETURN_VIEW_TAX_RETURN_STATUS] . "\t" . 
						$v[CLIENT_VIEW_CLIENT_STATUS] . "\t" . 
						$v[TAX_RETURN_VIEW_DATE_SENT] . "\t" .						
						$v[TAX_RETURN_VIEW_DATE_RELEASED] . "\t" .
						$v[TAX_RETURN_VIEW_TAX_RETURN_NOTES] . "\t" . 
						$v[CLIENT_VIEW_STAFF_INITIALS] . "\t" .
						$v[TAX_RETURN_VIEW_REVIEWER_1_INITIALS] . "\t" . 
						$v[TAX_RETURN_VIEW_REVIEWER_2_INITIALS] . "\t";
				}
				$output .= "\nUser Created\t" . $this->session->userdata(STAFF_LOGIN) . "\nDate Created\t" . date('H:i:s m/d/Y');
				header("Content-type: application/vnd.ms-excel");
				header('Content-disposition: attachment; filename="GGG_TAX_RETURN_MULTIPLE_FOR' . $this->session->userdata(STAFF_LOGIN) . '_On_' . date("Y-m-d") . '.xls"');
				print $output;
			}
			else
			{
				header("Location: " . base_url());
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
	}
		/*
		function get_input()
		{
			$string_input = '';
			$status_id = $_POST['id'];
			$status_name = $_POST['name'];
			$inputs = $this->Tax_return_model->get_input($status_id);
			if ($inputs != NULL)
			{
				foreach ($inputs as $input)
				{
					if ($input['field_required'] == 'date')
					{
						$string_input .= '<input type="text" class="input datepicker" name="date_' . preg_replace('~\s+~', '_', $status_name) . '" /><br />';
					}
					if ($input['field_required'] == 'select')
					{
						$values = explode(',', $input['values']);
						if (count($values))
						{
							$string_input .= '<select class ="input" name="select_' . preg_replace('~\s+~', '_', $status_name) . '">';
							foreach ($values as $value)
							{
								$string_input.='<option value="' . $value . '">' . $value . '</option>';
							}
							$string_input .= '</select><br />';
						}
					}
				}
			}
			echo $string_input;
		}
		*/
		function fill_select()
		{
			if(isset($_POST[CLIENT_NAME]) && trim($_POST[CLIENT_NAME]) != '') {
				$clientName = trim($_POST[CLIENT_NAME]);
				$clientID = $this->utilities->get_client_id($clientName);
				$selectPartner = $this->utilities->get_tax_return_client_partner($clientID, TAX_RETURN_POST, CLIENT_PARTNER_ID);
				$selectForm = $this->utilities->get_tax_return_client_tax_forms($clientID, TAX_RETURN_POST, TAX_FORM_ID);
				$select = array('found' => '1', TAX_FORM_ID => $selectForm, CLIENT_PARTNER_ID => $selectPartner);
			} else {
				$select = array('found' => '0');
			}

			echo json_encode($select);
		}
		
		function filter($id) {
		    $Client_Name = $this->utilities->get_client_name($id);
		    //$Company_Name = $this->Tax_return_model->get_Company_Name($id);
		    //var_dump($Company_Name);
		    //var_dump($Client_Name);
		    if ($this->session->userdata('tax_filter')) {
		      $return = $this->session->userdata('tax_filter');
		    }
		    (isset($Client_Name) && $Client_Name) ? $return['Client_Name'] = $Client_Name : $return['Client_Name'] = '';
		    //var_dump($return['Client_Name']);
		    //(isset($Company_Name) && $Company_Name) ? $return[COMPANY_NAME] = $Company_Name : $return[COMPANY_NAME] = '';
		    $this->session->set_userdata('tax_filter', $return);
		
		    header("Location: " . base_url() . 'tax_return/view');
		  }
		
		function lock($id)
		{
			$this->Tax_return_model->lock($id);
			header("Location: " . base_url() . TAX_RETURN_POST . '/view');
		}
		function unlock($id)
		{
			$this->Tax_return_model->unlock($id);
			header("Location: " . base_url() . TAX_RETURN_POST . '/view');
		}
		function add_instructions()
		{
			if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
			{
				if (isset($_POST['add_instructions']) && $_POST['add_instructions'])
				{
					$add_instructions = $_POST['add_instructions'];
					if ($this->Tax_return_model->check_instructions_name($add_instructions))
					{
						$id = $this->Tax_return_model->add_instructions($add_instructions);
						isset($id) && $id ? $message_succes = 'Instruction added successfully' : '';
					}
					else
					{
						$message = 'Sorry, but this Instruction already exists';
					}
				}
				else
				{
					$message = 'Please, enter Instruction';
				}
				$instructions = $this->Tax_return_model->get_instructions();
				$message = array('instructions' => $instructions, 'success' => isset($message_succes) ? $message_succes : '', 'error' => isset($message) ? $message : '');
				$message = json_encode($message);
				echo $message;
			}
			else
			{
				header("Location: " . base_url());
			}
		}

	}
 ?>