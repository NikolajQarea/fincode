<?php
class User extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
	}
	
	// Add Mode
	function add($staff_id = 0)
	{
		try
		{
			if($this->session->userdata(ROLE_ADMIN))
			{
				if($staff_id > 0)
				{
					$staff['data'] = $this->User_model->get_user($staff_id);
				}
				$this->load->view(HEADER);
				$this->load->view(MENU);
				if($staff_id > 0)
				{
					$this->load->view('user/edit', $staff);
				}
				else
				{
					$this->load->view('user/add');
				}
				$this->load->view(FOOTER);
			}
			else
			{
				header("Location: ".base_url());
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
	}
	// Edit Mode - Piggyback off Add() to reuse common code
	function edit($staff_id)
	{
		$this->add($staff_id);
	}
	// Add User. Used for Adding a new user or Updating an existing one.
	function add_user($staff_id = 0, $isEdit = false)
	{
		if($this->session->userdata(ROLE_ADMIN))
		{
			$isValid = true;
			try
			{
				if(isset($_POST['staff']))
				{
					$staff = $_POST['staff'];
					// Clear out Watermarks
					if(isset($staff[STAFF_NAME_LAST]) && trim($staff[STAFF_NAME_LAST]) == JS_STAFF_NAME_LAST)
					{
						$staff[STAFF_NAME_LAST] = '';
					}
					else
					{
						$staff[STAFF_NAME_LAST] = trim($staff[STAFF_NAME_LAST]);
					}
					if(isset($staff[STAFF_NAME_FIRST]) && trim($staff[STAFF_NAME_FIRST]) == JS_STAFF_NAME_FIRST)
					{
						$staff[STAFF_NAME_FIRST] = '';
					}
					else
					{
						$staff[STAFF_NAME_FIRST] = trim($staff[STAFF_NAME_FIRST]);
					}
					if(isset($staff[STAFF_INITIALS]) && trim($staff[STAFF_INITIALS]) == JS_STAFF_INITIALS)
					{
						$staff[STAFF_INITIALS] = '';
					}
					else
					{
						$staff[STAFF_INITIALS] = trim($staff[STAFF_INITIALS]);
					}
					if(isset($staff[STAFF_LOGIN]) && trim($staff[STAFF_LOGIN]) == JS_STAFF_LOGIN_ID)
					{
						$staff[STAFF_LOGIN] = '';
					}
					else
					{
						$staff[STAFF_LOGIN] = trim($staff[STAFF_LOGIN]);
					}
					if(isset($staff[STAFF_EMAIL]) && trim($staff[STAFF_EMAIL]) == JS_STAFF_EMAIL)
					{
						$staff[STAFF_EMAIL] = '';
					}
					else
					{
						$staff[STAFF_EMAIL] = trim($staff[STAFF_EMAIL]);
					}
					$this->session->set_flashdata(STAFF_ID, $staff_id);
					// Validate Last Name
					if($staff[STAFF_NAME_LAST])
					{
						$this->session->set_flashdata(STAFF_NAME_LAST, $staff[STAFF_NAME_LAST]);
					}
					else
					{
						$this->session->set_flashdata(ERROR_STAFF_NAME_LAST, ERROR_STRING_STAFF_NAME_LAST);
						$isValid = false;
					}
					// Validate First Name
					if($staff[STAFF_NAME_FIRST])
					{
						$this->session->set_flashdata(STAFF_NAME_FIRST, $staff[STAFF_NAME_FIRST]);
					}
					else
					{
						$this->session->set_flashdata(ERROR_STAFF_NAME_FIRST, ERROR_STRING_STAFF_NAME_FIRST);
						$isValid = false;
					}
					// Validate Initials
					if($staff[STAFF_INITIALS])
					{
						if($this->User_model->check_staff_initials($staff[STAFF_INITIALS], $staff_id))
						{
							$this->session->set_flashdata(ERROR_STAFF_INITIALS, 'The Staff Initials <strong><emp>[' . $staff[STAFF_INITIALS] . ']</emp></strong> are already in use by another user. Please try another.');
							$this->session->set_flashdata(STAFF_INITIALS, $staff[STAFF_INITIALS]);
							$isValid = false;
						}
						else
						{
							$this->session->set_flashdata(STAFF_INITIALS, $staff[STAFF_INITIALS]);
						}
					}
					else
					{
						$this->session->set_flashdata(ERROR_STAFF_INITIALS, ERROR_STRING_STAFF_INITIALS);
						$isValid = false;
					}
					// Validate Login_ID
					if($staff[STAFF_LOGIN])
					{
						if($this->User_model->check_staff_login($staff[STAFF_LOGIN], $staff_id))
						{
							$this->session->set_flashdata(ERROR_STAFF_LOGIN, 'The Login ID <strong><emp>[' . $staff[STAFF_LOGIN] . ']</emp></strong> is already in use by another user. Please try another.');
							$this->session->set_flashdata(STAFF_LOGIN, $staff[STAFF_LOGIN]);
							$isValid = false;
						}
						else
						{
							$this->session->set_flashdata(STAFF_LOGIN, $staff[STAFF_LOGIN]);
						}
					}
					else
					{
						$this->session->set_flashdata(ERROR_STAFF_LOGIN, ERROR_STRING_STAFF_LOGIN);
						$isValid = false;
					}
					// Validate Password
					if(!$isEdit)
					{
						if($staff[STAFF_PASSWORD])
						{
							$this->session->set_flashdata(STAFF_PASSWORD, $staff[STAFF_PASSWORD]);
						}
						else
						{
							$this->session->set_flashdata(ERROR_STAFF_PASSWORD, ERROR_STRING_STAFF_PASSWORD);
							$isValid = false;
						}
					}
					else
					{
						if(isset($staff[STAFF_PASSWORD_NEW]) && $staff[STAFF_PASSWORD_NEW] != '' && isset($staff[STAFF_PASSWORD_CONFIRM]) && $staff[STAFF_PASSWORD_CONFIRM] != '')
						{
							$staff[STAFF_PASSWORD_NEW] = trim($staff[STAFF_PASSWORD_NEW]);
							$staff[STAFF_PASSWORD_CONFIRM] = trim($staff[STAFF_PASSWORD_CONFIRM]);
							$this->session->set_flashdata(STAFF_PASSWORD_CONFIRM, $staff[STAFF_PASSWORD_CONFIRM]);
							$this->session->set_flashdata(STAFF_PASSWORD, $staff[STAFF_PASSWORD_NEW]);
							if($staff[STAFF_PASSWORD_NEW] != $staff[STAFF_PASSWORD_CONFIRM])
							{
								$this->session->set_flashdata(ERROR_STAFF_PASSWORD_CONFIRM, ERROR_STRING_STAFF_PASSWORD_CONFIRM);
								$isValid = false;
							}
							else
							{
								$staff[STAFF_PASSWORD] = $staff[STAFF_PASSWORD_NEW];
							}
						}
					}
					// Validate Email Address
					if(!$staff[STAFF_EMAIL])
					{
						$this->session->set_flashdata(ERROR_STAFF_EMAIL, ERROR_STRING_STAFF_EMAIL);
						$this->session->set_flashdata(STAFF_EMAIL, $staff[STAFF_EMAIL]);
						$isValid = false;
					}
					elseif($this->User_model->check_staff_email($staff[STAFF_EMAIL], $staff_id))
					{
						$this->session->set_flashdata(ERROR_STAFF_EMAIL, 'The Email Address <strong><emp>[' . $staff[STAFF_EMAIL] . ']</emp></strong> is already in use by another user. Please try another.');
						$this->session->set_flashdata(STAFF_EMAIL, $staff[STAFF_EMAIL]);
						$isValid = false;
					}
					elseif(!preg_match("~^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$~", $staff[STAFF_EMAIL]))
					{
						$this->session->set_flashdata(ERROR_STAFF_EMAIL, ERROR_STRING_STAFF_EMAIL);
						$this->session->set_flashdata(STAFF_EMAIL, trim($staff[STAFF_EMAIL]));
						$isValid = false;
					}
					else
					{	
						$this->session->set_flashdata(STAFF_EMAIL, $staff[STAFF_EMAIL]);
					}
					// Validate Status
					if($staff[STAFF_STATUS_ID])
					{
						$this->session->set_flashdata(STAFF_STATUS_ID, $staff[STAFF_STATUS_ID]);
					}
					else
					{
						$this->session->set_flashdata(ERROR_STAFF_STATUS_ID, ERROR_STRING_STAFF_STATUS_ID);
						$isValid = false;
					}
					// Validate User Roles
					$roles;
					if (isset($_POST[STAFF_ROLE_ID]))
					{
						$roles = $_POST[STAFF_ROLE_ID];
					}
					else
					{
						$this->session->set_flashdata(ERROR_STAFF_ROLE_ID, ERROR_STRING_STAFF_ROLE_ID);
						$isValid = false;
					}
					if($isValid == true)
					{
						if($this->User_model->add($staff, $roles, $staff_id, $isEdit))
						{
							if($isEdit)
							{
								$this->session->set_flashdata(STATUS_MESSAGE, 'User was updated successfully.');
							}
							else
							{
								$this->session->set_flashdata(STATUS_MESSAGE, 'User was added successfully.');
							}
							header("Location: ".base_url()."user/view");
						}
						else
						{
							$this->session->set_flashdata(ERROR_MESSAGE, 'This user already exists.');
							if($isEdit)
							{
								header("Location: ".base_url()."user/edit/" .$staff_id);
							}
							else
							{
								header("Location: ".base_url()."user/add");
							}
						}

					}
					else
					{
						if($isEdit)
						{
							header("Location: ".base_url()."user/edit/" .$staff_id);
						}
						else
						{
							header("Location: ".base_url()."user/add");
						}
					}
				}
				else
				{
					header("Location: ".base_url());
				}
			}
			catch(Exception $ex)
			{
				log_message('error',  $e->getMessage());
				show_error( $e->getMessage());
			}
		}
	}
	// Edit User.  Calls add_user to combine similiar logic into one procedure.
	function edit_user($staff_id)
	{
		$this->add_user($staff_id, true);
	}
	// View Mode
	function view($page = 0, $order = STAFF_LOGIN, $as = 'ASC') {
        try {
            if ($this->session->userdata(ROLE_ADMIN)) {
                
               $include_terminated = $this->session->userdata('include_terminated');
               if (isset($_POST['user'])) {
                    $user = $_POST['user'];
                    $val = $user['include_terminated'];
                    $this->session->set_userdata('include_terminated', $val);
                    
                }
                else 
                {
                    if (empty($include_terminated))
                    {
                        $this->session->set_userdata('include_terminated', 'off');
                    }
                }
                $include_terminated = $this->session->userdata('include_terminated');
                
                $config['base_url'] = base_url() . 'user/view/';
                $config['total_rows'] = $this->User_model->get_all($include_terminated);
                $config['per_page'] = PAGING_RECORDS_PER_PAGE;
                $config['num_links'] = PAGING_NUMBER_LINKS;
                $config['uri_segment'] = PAGING_URI_SEGMENTS;
                $config['full_tag_open'] = PAGING_FULL_OPEN_TAG;
                $config['full_tag_close'] = PAGING_FULL_CLOSE_TAG;
                $config['first_link'] = PAGING_FIRST;
                $config['last_link'] = PAGING_LAST;
                $config['next_link'] = PAGING_NEXT;
                $config['prev_link'] = PAGING_PREVIOUS;
                $config['cur_tag_open'] = PAGING_CURRENT_OPEN_TAG;
                $config['cur_tag_close'] = PAGING_CURRENT_CLOSE_TAG;
                
                $this->pagination->initialize($config);
                
                $staff['post'] = $include_terminated;
                $staff['link'] = $this->pagination->create_links();
                $staff['page'] = $page;
                $staff['as'] = $as;
                $staff['order'] = $order;
                $staff['list'] = $this->User_model->view($page, PAGING_RECORDS_PER_PAGE, $order, $as, $include_terminated);

                $this->load->view(HEADER);
                $this->load->view(MENU);
                $this->load->view('user/view', $staff);
                $this->load->view(FOOTER);
                
            } else {
                header("Location: " . base_url());
            }
            
        } catch (Exception $ex) {
            log_message('error', $e->getMessage());
            show_error($e->getMessage());
        }
    }
	// Delete User
	function delete($staff_id)
	{
		if($this->session->userdata(ROLE_ADMIN))
		{
			$this->User_model->delete_user($staff_id);
			header("Location: ".base_url().'user/view');
		}
		else
		{
			header("Location: ".base_url());
		}
	}
}
 ?>