<?php

class Form extends CI_Controller{
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('form_model');
    }
    
    function view(){
        if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE))) 
        {
            $config['base_url'] = base_url().'client/view/';
            $config['per_page'] = '20'; 
            $config['num_links'] = 2;
            $config['uri_segment'] = 3;
            $config['full_tag_open'] = '<p>';
            $config['full_tag_close'] = '</p>';
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Back';
            $config['cur_tag_open'] = '<b>';
            $config['cur_tag_close'] = '</b>';
            
            $data['forms'] = $this->form_model->view();
    
            $this->pagination->initialize($config); 
    
            $data['link'] = $this->pagination->create_links();
            $this->load->view(HEADER);
            $this->load->view(MENU);
            $this->load->view('form/view', $data);
            $this->load->view(FOOTER);
           
        }
        else
        {
            header("Location: ".base_url());
        }
    }
    
}
 ?>