<?php

class Main extends CI_Controller
{
	function Main()
	{
		parent::__construct();
	}
	// Index
	function index()
	{
		$this->load->view(HEADER);
		$this->load->view(MENU);
		$this->load->view('welcome');
		$this->load->view(FOOTER);
	}
	// Load Menu
	function load_menu()
	{
		try
		{
			if(isset($_POST['login']))
			{
				$login = $_POST['login'];
				if(isset($login[STAFF_LOGIN]) && 
					$login[STAFF_LOGIN] &&
					isset($login[STAFF_PASSWORD]) &&
					$login[STAFF_PASSWORD]
				)
				{
					$this->load->model('User_model');
					$userData = $this->User_model->login($login);
					if($this->session->userdata(STAFF_ID))
					{
						$this->load->view(HEADER);
						$this->load->view(MENU);
						$this->load->view('welcome');
						$this->load->view(FOOTER);
					}
					else
					{
						$this->session->set_flashdata(ERROR_MESSAGE, ERROR_STRING_LOGIN); 
						header("Location: ".base_url());
					}
				}
				else
				{
					$this->session->set_flashdata(ERROR_MESSAGE, 'Please enter a valid Login ID and Password.');
					header("Location: ".base_url());
				}
			}
			else
			{
				$this->session->set_flashdata(ERROR_MESSAGE, 'Please enter a valid Login ID and Password.');
				header("Location: ".base_url());
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
	}
	// Log Out
	function exit_user()
	{
		try
		{
			$this->session->sess_destroy();
			session_start();
			session_destroy();
			header("Location: ".base_url());
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
	}
}
 ?>