<?php
	
class Statement extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Statement_model');
		$this->load->library('Utilities');
	}
	// Auto Complete
	function autocompleted($title)
	{
		echo $this->utilities->GetClientNameAutoComplete($title);
	}
	// Ajax Call To Prepopulate ABA Field From Institution's ABA Field (when Institution is Chosen)
	function ajax_get_info()
	{
		print_r($this->Statement_model->select());
		return $this->Statement_model->select();
	}
	// Add Client Statement
	function add($id = 0)
	{
		if ($this->session->userdata(ROLE_ADMIN) || ($this->session->userdata(ROLE_WRITE)))
		{
			try
			{
				$statement['StatementTypes'] = 
				(
					$this->utilities->get_drop_down_html('statement', STATEMENT_STATEMENT_TYPE_ID, $this->utilities->get_statement_types(), STATEMENT_TYPE_ID, STATEMENT_TYPE, 
					$this->session->flashdata(STATEMENT_STATEMENT_TYPE_ID) ? $this->session->flashdata(STATEMENT_STATEMENT_TYPE_ID) : 
					PLEASE_SELECT_ID, false)
				);
				$statement['Institutions'] = 
				(
					$this->utilities->get_drop_down_html('statement', STATEMENT_INSTITUTION_ID, $this->utilities->get_institutions(), INSTITUTION_ID, INSTITUTION_NAME, 
					$this->session->flashdata(STATEMENT_INSTITUTION_ID) ? $this->session->flashdata(STATEMENT_INSTITUTION_ID) : 
					PLEASE_SELECT_ID, false)
				);
				$statement['StatementStatuses'] = 
				(
					$this->utilities->get_drop_down_html('statement', STATEMENT_STATEMENT_STATUS_ID, $this->utilities->get_statement_status(), STATEMENT_STATUS_ID, STATEMENT_STATUS, 
					$this->session->flashdata(STATEMENT_STATUS_ID) ? $this->session->flashdata(STATEMENT_STATUS_ID) : 
					PLEASE_SELECT_ID, false)
				);
				$statement['Staff'] = 
				(
					$this->utilities->get_drop_down_html('statement', STAFF_ID, $this->utilities->get_staff(), STAFF_ID, STAFF_INITIALS,
                                        $this->session->flashdata(STAFF_ID) ? $this->session->flashdata(STAFF_ID) : 
					PLEASE_SELECT_ID, false)                                        
				);
			}
			catch(Exception $ex)
			{
				log_message('error',  $e->getMessage());
				show_error( $e->getMessage());
			}
			$this->load->view(HEADER);
			$this->load->view(MENU);
			$this->load->view('statement'.'/add', $statement);
			$this->load->view(FOOTER);
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// Edit Client Statement
	function edit($id = 0, $as = 'DESC')
	{
		if($id == 0)
		{
			return $this->ajax_get_info();
		}
		$data = $this->Statement_model->get_client_statement($id);
		//krumo($data); die();

		if (($this->session->userdata(ROLE_ADMIN) || $this->session->userdata(ROLE_WRITE)) && $id > 0)
		{
			try
			{	
				//if ($data) {
					$data = $data[0];
				//}
				$statement['data'] = $data;				
				$statement['as'] = $as;
				$statement['History'] = $this->Statement_model->get_client_statement_history($id, $as);
				$statement['StatementTypes'] = 
				(
					$this->utilities->get_drop_down_html('statement', STATEMENT_STATEMENT_TYPE_ID, $this->utilities->get_statement_types(), STATEMENT_TYPE_ID, STATEMENT_TYPE, 
					$this->session->flashdata(STATEMENT_STATEMENT_TYPE_ID) ? $this->session->flashdata(STATEMENT_STATEMENT_TYPE_ID) : 
					$data[STATEMENT_STATEMENT_TYPE_ID] ? $data[STATEMENT_STATEMENT_TYPE_ID] :
					PLEASE_SELECT_ID, false)
				);
				$statement['Institutions'] = 
				(
					$this->utilities->get_drop_down_html('statement', STATEMENT_INSTITUTION_ID, $this->utilities->get_institutions(), INSTITUTION_ID, INSTITUTION_NAME, 
					$this->session->flashdata(STATEMENT_INSTITUTION_ID) ? $this->session->flashdata(STATEMENT_INSTITUTION_ID) : 
					$data[STATEMENT_INSTITUTION_ID] ? $data[STATEMENT_INSTITUTION_ID] :
					PLEASE_SELECT_ID, false)
				);
				$statement['StatementStatuses'] = 
				(
					$this->utilities->get_drop_down_html('statement', STATEMENT_STATEMENT_STATUS_ID, $this->utilities->get_statement_status(), STATEMENT_STATUS_ID, STATEMENT_STATUS, 
					$this->session->flashdata(STATEMENT_STATUS_ID) ? $this->session->flashdata(STATEMENT_STATUS_ID) : 
					$data[STATEMENT_STATEMENT_STATUS_ID] ? $data[STATEMENT_STATEMENT_STATUS_ID] :
					PLEASE_SELECT_ID, false)
				);

				$statement['Staff'] = 
				(   
					$this->utilities->get_drop_down_html('statement', STAFF_ID, $this->utilities->get_staff(), STAFF_ID, STAFF_INITIALS,
                    $this->session->flashdata(STAFF_ID) ? $this->session->flashdata(STAFF_ID) : 
					$data[STATEMENT_VIEW_CLIENT_STAFF_ID] ? $data[STATEMENT_VIEW_CLIENT_STAFF_ID] :
					PLEASE_SELECT_ID, false)			                                     
				);
			}
			catch(Exception $ex)
			{
				log_message('error',  $e->getMessage());
				show_error( $e->getMessage());
			}
			$this->load->view(HEADER);
			$this->load->view(MENU);
			$this->load->view('statement/edit', $statement);
			$this->load->view(FOOTER);
		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// Edit a specific Client Statement
	function insert_update_client_statement($id = 0)
	{
		if ($this->session->userdata(ROLE_ADMIN) || $this->session->userdata(ROLE_WRITE))
		{
			try
			{
				$isValid = true;
				$status = 'update';
				$mode = 'edit';
				if($id == 0)
				{
					$mode = 'add';
					$status = 'insert';
				}
				$statement = $_POST['statement'];

                                //krumo($statement);
				$User_ID = $this->session->userdata($id);		
				// Clear out Watermark'ed Text Fields and PLEASE SELECT fields
				// Client Name
				if(isset($statement[STATEMENT_VIEW_CLIENT_NAME]))
				{                                    
					$statement[STATEMENT_VIEW_CLIENT_NAME] = stripslashes(trim($statement[STATEMENT_VIEW_CLIENT_NAME]));
					if($statement[STATEMENT_VIEW_CLIENT_NAME] == ANY_LABEL || $statement[STATEMENT_VIEW_CLIENT_NAME] == ANY_VALUE || $statement[STATEMENT_VIEW_CLIENT_NAME] == '')
					{
						$statement[STATEMENT_VIEW_CLIENT_NAME] = '';
						$isValid = false;
						$this->session->set_flashdata(ERROR_STATEMENT_CLIENT_ID, ERROR_STRING_STATEMENT_CLIENT_ID);
					}
					else
					{
						$statement[STATEMENT_CLIENT_ID] = $this->utilities->get_client_id($statement[STATEMENT_VIEW_CLIENT_NAME]);
						if($statement[STATEMENT_CLIENT_ID] < 1 || $statement[STATEMENT_CLIENT_ID] == '')
						{
							$isValid = false;
							$this->session->set_flashdata(ERROR_STATEMENT_CLIENT_ID, ERROR_STRING_STATEMENT_CLIENT_ID);
						}
					}
				}
				else
				{
					$statement[STATEMENT_VIEW_CLIENT_NAME] = '';
					$isValid = false;
					$this->session->set_flashdata(ERROR_STATEMENT_CLIENT_ID, ERROR_STRING_STATEMENT_CLIENT_ID);
				}
				$this->session->set_flashdata(STATEMENT_VIEW_CLIENT_NAME, $statement[STATEMENT_VIEW_CLIENT_NAME]);
				// Statement Type
				if(isset($statement[STATEMENT_STATEMENT_TYPE_ID]))
				{
					$this->session->set_flashdata(STATEMENT_STATEMENT_TYPE_ID, $statement[STATEMENT_STATEMENT_TYPE_ID]);
					if($statement[STATEMENT_STATEMENT_TYPE_ID] == PLEASE_SELECT_ID)
					{
						$statement[STATEMENT_STATEMENT_TYPE_ID] = 0;
						$isValid = false;
						$this->session->set_flashdata(ERROR_STATEMENT_TYPE_ID, ERROR_STRING_STATEMENT_TYPE_ID);
					}
				}
				else
				{
					$statement[STATEMENT_STATEMENT_TYPE_ID] = 0;
					$isValid = false;
					$this->session->set_flashdata(ERROR_STATEMENT_TYPE_ID, ERROR_STRING_STATEMENT_TYPE_ID);
					$this->session->set_flashdata(STATEMENT_STATEMENT_TYPE_ID, PLEASE_SELECT_ID);
				}
				// Institution
				if(isset($statement[STATEMENT_INSTITUTION_ID]))
				{
					$this->session->set_flashdata(STATEMENT_INSTITUTION_ID, $statement[STATEMENT_INSTITUTION_ID]);
					if($statement[STATEMENT_INSTITUTION_ID] == PLEASE_SELECT_ID)
					{
						$statement[STATEMENT_INSTITUTION_ID] = 0;
						$isValid = false;
						$this->session->set_flashdata(ERROR_INSTITUTION_ID, ERROR_STRING_INSTITUTION_ID);
					}
				}
				else
				{
					$statement[STATEMENT_INSTITUTION_ID] = 0;
					$isValid = false;
					$this->session->set_flashdata(ERROR_INSTITUTION_ID, ERROR_STRING_INSTITUTION_ID);
					$this->session->set_flashdata(STATEMENT_INSTITUTION_ID, PLEASE_SELECT_ID);
				}
				// Account Number
				if(isset($statement[STATEMENT_ACCOUNT_NUMBER]))
				{
					$statement[STATEMENT_ACCOUNT_NUMBER] = stripslashes(trim($statement[STATEMENT_ACCOUNT_NUMBER]));
					if($statement[STATEMENT_ACCOUNT_NUMBER] == JS_STATEMENT_ACCOUNT_NUMBER || $statement[STATEMENT_ACCOUNT_NUMBER] == '')
					{
						$statement[STATEMENT_ACCOUNT_NUMBER] = '';
						//$isValid = false;
						//$this->session->set_flashdata(ERROR_STATEMENT_ACCOUNT_NUMBER, ERROR_STRING_STATEMENT_ACCOUNT_NUMBER);
					}
				}
				else
				{
					$statement[STATEMENT_ACCOUNT_NUMBER] = '';
					//$isValid = false;
					//$this->session->set_flashdata(ERROR_STATEMENT_ACCOUNT_NUMBER, ERROR_STRING_STATEMENT_ACCOUNT_NUMBER);
				}
				$this->session->set_flashdata(STATEMENT_ACCOUNT_NUMBER, $statement[STATEMENT_ACCOUNT_NUMBER]);
				// ABA Number
				if(isset($statement[STATEMENT_ABA_NUMBER]))
				{
					$statement[STATEMENT_ABA_NUMBER] = stripslashes(trim($statement[STATEMENT_ABA_NUMBER]));
					if($statement[STATEMENT_ABA_NUMBER] == JS_STATEMENT_ABA_NUMBER)
					{
						$statement[STATEMENT_ABA_NUMBER] = '';
					}
				}
				else
				{
					$statement[STATEMENT_ABA_NUMBER] = '';
				}
				$this->session->set_flashdata(STATEMENT_ABA_NUMBER, $statement[STATEMENT_ABA_NUMBER]);
				// Statement Start Date
				if(isset($statement[STATEMENT_START_DATE]))
				{
					$statement[STATEMENT_START_DATE] = stripslashes(trim($statement[STATEMENT_START_DATE]));
					if($statement[STATEMENT_START_DATE] == JS_STATEMENT_START_DATE)
					{
						$statement[STATEMENT_START_DATE] = '';
					}
					else if($this->utilities->is_valid_date($statement[STATEMENT_START_DATE] == false))
					{
						$isValid = false;
						$this->session->set_flashdata(ERROR_STATEMENT_START_DATE, ERROR_STRING_STATEMENT_START_DATE);
					}
				}
				else
				{
					$statement[STATEMENT_START_DATE] = '';
				}
				$this->session->set_flashdata(STATEMENT_START_DATE, strtotime($statement[STATEMENT_START_DATE]));
				// Statement End Date
				if(isset($statement[STATEMENT_END_DATE]))
				{
					$statement[STATEMENT_END_DATE] = stripslashes(trim($statement[STATEMENT_END_DATE]));
					if($statement[STATEMENT_END_DATE] == JS_STATEMENT_END_DATE)
					{
						$statement[STATEMENT_END_DATE] = '';
					}
					else if(!$this->utilities->is_valid_date($statement[STATEMENT_END_DATE]))
					{
						$isValid = false;
						$this->session->set_flashdata(ERROR_STATEMENT_END_DATE, ERROR_STRING_STATEMENT_END_DATE);
					}
				}
				else
				{
					$statement[STATEMENT_END_DATE] = '';
				}
				$this->session->set_flashdata(STATEMENT_END_DATE, strtotime($statement[STATEMENT_END_DATE]));
				// Statement Last Reconciliation Date
				if(isset($statement[STATEMENT_LAST_RECONCILIATION_DATE]))
				{
					$statement[STATEMENT_LAST_RECONCILIATION_DATE] = trim($statement[STATEMENT_LAST_RECONCILIATION_DATE]);
					if($statement[STATEMENT_LAST_RECONCILIATION_DATE] == JS_STATEMENT_LAST_RECONCILIATION_DATE)
					{
						$statement[STATEMENT_LAST_RECONCILIATION_DATE] = '';
					}
					else if(!$this->utilities->is_valid_date($statement[STATEMENT_LAST_RECONCILIATION_DATE]))
					{
						$isValid = false;
						$this->session->set_flashdata(ERROR_STATEMENT_LAST_RECONCILIATION_DATE, ERROR_STRING_STATEMENT_LAST_RECONCILIATION_DATE);
					}
				}
				else
				{
					$statement[STATEMENT_LAST_RECONCILIATION_DATE] = '';
				}
				$this->session->set_flashdata(STATEMENT_LAST_RECONCILIATION_DATE, strtotime($statement[STATEMENT_LAST_RECONCILIATION_DATE]));
				// Statement Notes
				if(isset($statement[STATEMENT_NOTES]))
				{
					$statement[STATEMENT_NOTES] = stripslashes(trim($statement[STATEMENT_NOTES]));
					if($statement[STATEMENT_NOTES] == JS_STATEMENT_NOTES)
					{
						$statement[STATEMENT_NOTES] = '';
					}
				}
				else
				{
					$statement[STATEMENT_NOTES] = '';
				}
				$this->session->set_flashdata(STATEMENT_NOTES, $statement[STATEMENT_NOTES]);
				// Statement Status
				if(isset($statement[STATEMENT_STATEMENT_STATUS_ID]))
				{
					$this->session->set_flashdata(STATEMENT_STATEMENT_STATUS_ID, $statement[STATEMENT_STATEMENT_STATUS_ID]);
					if($statement[STATEMENT_STATEMENT_STATUS_ID] == PLEASE_SELECT_ID)
					{
						$statement[STATEMENT_STATEMENT_STATUS_ID] = 0;
						$isValid = false;
						$this->session->set_flashdata(ERROR_STATEMENT_STATEMENT_STATUS_ID, ERROR_STRING_STATEMENT_STATEMENT_STATUS_ID);
					}
				}
				else
				{
					$statement[STATEMENT_STATEMENT_STATUS_ID] = 0;
					$isValid = false;
					$this->session->set_flashdata(ERROR_STATEMENT_STATUS_ID, ERROR_STRING_STATEMENT_STATUS_ID);
					$this->session->set_flashdata(STATEMENT_STATEMENT_STATUS_ID, PLEASE_SELECT_ID);
				}
                                // Statement Staff 
				if(isset($statement[STAFF_ID]))
				{ 
					$this->session->set_flashdata(STAFF_ID, $statement[STAFF_ID]);
                                        
					if($statement[STAFF_ID] == PLEASE_SELECT_ID)
					{                                         
						$statement[STATEMENT_VIEW_CLIENT_STAFF_ID] = 0;
						$isValid = false;
						$this->session->set_flashdata(ERROR_STATEMENT_VIEW_CLIENT_STAFF_NAME, ERROR_STRING_STATEMENT_VIEW_CLIENT_STAFF_NAME_ID);
					}
				}
				else
				{
					$statement[STATEMENT_VIEW_CLIENT_STAFF_ID] = 0;
					$isValid = false;
					$this->session->set_flashdata(ERROR_STATEMENT_VIEW_CLIENT_STAFF_NAME, ERROR_STRING_STATEMENT_VIEW_CLIENT_STAFF_NAME);
					$this->session->set_flashdata(STATEMENT_STATEMENT_VIEW_CLIENT_STAFF_NAME, PLEASE_SELECT_ID);
				}

				// if all is valid, build objects and update/insert the new data
				if ($isValid)
				{
					// array to UPDATE Tax Return Information
					$Statement = array
					(
						STATEMENT_STATEMENT_TYPE_ID			=> $statement[STATEMENT_STATEMENT_TYPE_ID],
						STATEMENT_INSTITUTION_ID			=> $statement[STATEMENT_INSTITUTION_ID],
						STATEMENT_ACCOUNT_NUMBER			=> $statement[STATEMENT_ACCOUNT_NUMBER],
						STATEMENT_ABA_NUMBER				=> $statement[STATEMENT_ABA_NUMBER],
						STATEMENT_START_DATE				=> $statement[STATEMENT_START_DATE] != '' ?  strtotime($statement[STATEMENT_START_DATE]) : 0,
						STATEMENT_END_DATE				=> $statement[STATEMENT_END_DATE] != '' ?  strtotime($statement[STATEMENT_END_DATE]) : 0,
						STATEMENT_LAST_RECONCILIATION_DATE              => $statement[STATEMENT_LAST_RECONCILIATION_DATE] != '' ?  strtotime($statement[STATEMENT_LAST_RECONCILIATION_DATE]) : 0,
						STATEMENT_NOTES					=> $statement[STATEMENT_NOTES],
						STATEMENT_STATEMENT_STATUS_ID                   => $statement[STATEMENT_STATEMENT_STATUS_ID],
						STATEMENT_LAST_EDIT_DATE			=> time(),
						STATEMENT_LAST_EDIT_USER_ID			=> $User_ID,
						//STATEMENT_VIEW_CLIENT_STAFF_ID			=> $statement[STAFF_ID],
                                                //STAFF_ID                => $statement[STAFF_ID] // ololo
					);
					if($id == 0)
					{
						$Statement[STATEMENT_CLIENT_ID]		= $statement[STATEMENT_CLIENT_ID];
						$Statement[STATEMENT_STAMP_DATE]	= time();
						$Statement[STATEMENT_STAMP_USER_ID]	= $User_ID;
					}
					// array to INSERT a new Statement History record
					$History = array
					(
						STATEMENT_HISTORY_STATEMENT_ID			=> $id,
						STATEMENT_HISTORY_STATEMENT_STATUS_ID	=> $statement[STATEMENT_STATEMENT_STATUS_ID],
						STATEMENT_HISTORY_STAMP_DATE			=> time(),
						STATEMENT_HISTORY_STAMP_USER_ID			=> $User_ID,
						STATEMENT_HISTORY_LAST_EDIT_DATE		=> time(),
						STATEMENT_HISTORY_LAST_EDIT_USER_ID		=> $User_ID
					);            
					//krumo($Statement);    
					//die();               	       
					$this->Statement_model->insert_update_client_statement($id, $Statement, $History);
					$this->session->set_flashdata(STATUS_MESSAGE, 'Client Statement ' . $status . ' was successful.');
					header('Location: ' . base_url() . 'statement/view');
				}
				else
				{       krumo('error');
					$this->session->set_flashdata(ERROR_MESSAGE, 'An error occurred while trying to ' . $status . ' this Client Statement. Please fix the errors below.');
					header("Location: " . base_url() . 'statement/' . $mode . '/' . $id);
				}
			}
			catch(Exception $ex)
			{
				log_message('error',  $e->getMessage());
				show_error( $e->getMessage());
			}

		}
		else
		{
			header("Location: " . base_url());
		}
	}
	// Page View
	function view($page = '', $order = STATEMENT_VIEW_CLIENT_NAME, $as = SORT_ORDER)
	{           
		try
		{
			if ($this->session->userdata(ROLE_WRITE) || $this->session->userdata(ROLE_READ_ONLY))
			{
				$statements = $this->get_data($page, PAGING_RECORDS_PER_PAGE, $order, $as);
                                //krumo($statements);    
				$config['base_url'] = base_url() . 'statement/view/';
				$config['cur_page'] = $statements['page'];
				$config['per_page'] = PAGING_RECORDS_PER_PAGE;
				$config['num_links'] = PAGING_NUMBER_LINKS;
				$config['uri_segment'] = PAGING_URI_SEGMENTS;
				$config['full_tag_open'] = PAGING_FULL_OPEN_TAG;
				$config['full_tag_close'] = PAGING_FULL_CLOSE_TAG;
				$config['first_link'] = PAGING_FIRST;
				$config['last_link'] = PAGING_LAST;
				$config['next_link'] = PAGING_NEXT;
				$config['prev_link'] = PAGING_PREVIOUS;
				$config['cur_tag_open'] = PAGING_CURRENT_OPEN_TAG;
				$config['cur_tag_close'] = PAGING_CURRENT_CLOSE_TAG;

				if ($statements['list'] != NO_RECORDS)
				{
					$config['total_rows'] = $statements['list'][0]['Total_Rows'];
				}
				else
				{
					$config['total_rows'] = 0;
				}
				$this->pagination->initialize($config);
				$statements['link'] = $this->pagination->create_links();
				$statements['page'] = $page;
				$statements['as'] = $as;
				$statements['order'] = $order;

				$statements['Statement_Status'] = 
				(
					$this->utilities->get_drop_down_html('statement', STATEMENT_VIEW_STATEMENT_STATUS, $this->utilities->get_statement_status(), STATEMENT_STATUS_ID, STATEMENT_STATUS, $statements[STATEMENT_VIEW_STATEMENT_STATUS], true)
				);
				$statements['Institutions'] = 
				(
					$this->utilities->get_drop_down_html('statement', STATEMENT_VIEW_INSTITUTION_NAME, $this->utilities->get_institutions(), INSTITUTION_ID, INSTITUTION_NAME, $statements[STATEMENT_VIEW_INSTITUTION_NAME], true)
				);
				$statements['Statement_Types'] = 
				(
					$this->utilities->get_drop_down_html('statement', STATEMENT_VIEW_STATEMENT_TYPE, $this->utilities->get_statement_types(), STATEMENT_TYPE_ID, STATEMENT_TYPE, $statements[STATEMENT_VIEW_STATEMENT_TYPE], true)
				);
				$statements['Partners'] =
				(
					$this->utilities->get_drop_down_html('statement', STATEMENT_VIEW_CLIENT_PARTNER_NAME, $this->utilities->get_partners(), STAFF_ID, STAFF_INITIALS, $statements[STATEMENT_VIEW_CLIENT_PARTNER_NAME], true)
				);
				$statements['Staff'] = 
				(
					$this->utilities->get_drop_down_html('statement', STATEMENT_VIEW_CLIENT_STAFF_NAME, $this->utilities->get_staff(), STAFF_ID, STAFF_INITIALS, $statements[STATEMENT_VIEW_CLIENT_STAFF_NAME], true)                                      
				);
				$statements['post'] = $statements;
				$this->load->view(HEADER);
				$this->load->view(MENU);
				$this->load->view('statement/view', $statements);
				$this->load->view(FOOTER);
			}
			else
			{
				header("Location: " . base_url());
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
	}
	// Export to Excel
	function convertToExcel($page=0, $count=0, $order=INSTITUTION_VIEW_NAME, $as = 'ASC')
	{
		try
		{

			if ($this->session->userdata(ROLE_WRITE) || $this->session->userdata(ROLE_READ_ONLY))
			{
				$page = 0;
				$statements = $this->get_data($page, $count, $order, $as);
				$output = 
					"Client Name\t" .
					"Institution Name\t" .
					"Statement Type\t" .
					"Account Number\t" .
					"ABA Number\t" .
					"Start Date\t" .
					"End Date\t" .
					"Last Reconciled\t" .
					"Partner\t" .
					"Preparer\t" .
					"Status\t" .
					"Notes\t" .
					"Created By\t" .
					"Created On\t" .
					"Last Updated By\t" .
					"Last Updated On\t\n";
				foreach ($statements['list'] as $key => $v)
				{
					if (!isset($v[STATEMENT_VIEW_CLIENT_NAME]) || $v[STATEMENT_VIEW_CLIENT_NAME] == '')
					{
						$v[STATEMENT_VIEW_CLIENT_NAME] = NOT_AVAILABLE;
					}
					if(!isset($v[STATEMENT_VIEW_INSTITUTION_NAME]) || $v[STATEMENT_VIEW_INSTITUTION_NAME] == '')
					{
						$v[STATEMENT_VIEW_INSTITUTION_NAME] = NOT_AVAILABLE;
					}
					if(!isset($v[STATEMENT_VIEW_STATEMENT_TYPE]))
					{
						$v[STATEMENT_VIEW_STATEMENT_TYPE] = '';
					}
					if(!isset($v[STATEMENT_VIEW_STATEMENT_ACCOUNT_NUMBER]))
					{
						$v[STATEMENT_VIEW_STATEMENT_ACCOUNT_NUMBER] = '';
					}
					if(!isset($v[STATEMENT_VIEW_STATEMENT_ABA_NUMBER]))
					{
						$v[STATEMENT_VIEW_STATEMENT_ABA_NUMBER] = '';
					}
					if(!isset($v[STATEMENT_VIEW_STATEMENT_START_DATE]) || $v[STATEMENT_VIEW_STATEMENT_START_DATE] < 1)
					{
						$v[STATEMENT_VIEW_STATEMENT_START_DATE] = '';
					}
					else
					{
						$v[STATEMENT_VIEW_STATEMENT_START_DATE] = date(DATE_FORMAT, $v[STATEMENT_VIEW_STATEMENT_START_DATE]);
					}
					if(!isset($v[STATEMENT_VIEW_STATEMENT_END_DATE]) || $v[STATEMENT_VIEW_STATEMENT_END_DATE] < 1)
					{
						$v[STATEMENT_VIEW_STATEMENT_END_DATE] = '';
					}
					else
					{
						$v[STATEMENT_VIEW_STATEMENT_END_DATE] = date(DATE_FORMAT, $v[STATEMENT_VIEW_STATEMENT_END_DATE]);
					}
					if(!isset($v[STATEMENT_VIEW_STATEMENT_LAST_RECONCILIATION_DATE]) || $v[STATEMENT_VIEW_STATEMENT_LAST_RECONCILIATION_DATE] < 1)
					{
						$v[STATEMENT_VIEW_STATEMENT_LAST_RECONCILIATION_DATE] = '';
					}
					else
					{
						$v[STATEMENT_VIEW_STATEMENT_LAST_RECONCILIATION_DATE] = date(DATE_FORMAT, $v[STATEMENT_VIEW_STATEMENT_LAST_RECONCILIATION_DATE]);
					}
					if (!isset($v[STATEMENT_VIEW_CLIENT_PARTNER_INITIALS]) || $v[STATEMENT_VIEW_CLIENT_PARTNER_INITIALS] == '')
					{
						$v[STATEMENT_VIEW_CLIENT_PARTNER_INITIALS] = '';
					}
					if (!isset($v[STATEMENT_VIEW_CLIENT_STAFF_INITIALS]) || $v[STATEMENT_VIEW_CLIENT_STAFF_INITIALS] == '')
					{
						$v[STATEMENT_VIEW_CLIENT_STAFF_INITIALS] = '';
					}
					if(!isset($v[STATEMENT_VIEW_STATEMENT_STATUS]))
					{
						$v[STATEMENT_VIEW_STATEMENT_STATUS] = '';
					}
					if(!isset($v[STATEMENT_VIEW_STATEMENT_NOTES]))
					{
						$v[STATEMENT_VIEW_STATEMENT_NOTES] = '';
					}
					else
					{
						$v[STATEMENT_VIEW_STATEMENT_NOTES] = stripslashes($v[STATEMENT_VIEW_STATEMENT_NOTES]);
					}
					if(!isset($v[STATEMENT_VIEW_STATEMENT_STAMP_USER_INITIALS]))
					{
						$v[STATEMENT_VIEW_STATEMENT_STAMP_USER_INITIALS] = '';
					}
					if(!isset($v[STATEMENT_VIEW_STATEMENT_STAMP_DATE]) || $v[STATEMENT_VIEW_STATEMENT_STAMP_DATE] < 1)
					{
						$v[STATEMENT_VIEW_STATEMENT_STAMP_DATE] = '';
					}
					else
					{
						$v[STATEMENT_VIEW_STATEMENT_STAMP_DATE] = date(DATE_FORMAT, $v[STATEMENT_VIEW_STATEMENT_STAMP_DATE]);
					}
					if(!isset($v[STATEMENT_VIEW_STATEMENT_LAST_EDIT_USER_INITIALS]))
					{
						$v[STATEMENT_VIEW_STATEMENT_LAST_EDIT_USER_INITIALS] = '';
					}
					if(!isset($v[STATEMENT_VIEW_STATEMENT_LAST_EDIT_DATE]) || $v[STATEMENT_VIEW_STATEMENT_LAST_EDIT_DATE] < 1)
					{
						$v[STATEMENT_VIEW_STATEMENT_LAST_EDIT_DATE] = '';
					}
					else
					{
						$v[STATEMENT_VIEW_STATEMENT_LAST_EDIT_DATE] = date(DATE_FORMAT, $v[STATEMENT_VIEW_STATEMENT_LAST_EDIT_DATE]);
					}
					$output .= 
						$v[STATEMENT_VIEW_CLIENT_NAME] 							. "\t" .
						$v[STATEMENT_VIEW_INSTITUTION_NAME]  					. "\t" .
						$v[STATEMENT_VIEW_STATEMENT_TYPE]						. "\t" .
						$v[STATEMENT_VIEW_STATEMENT_ACCOUNT_NUMBER]				. "\t" .
						$v[STATEMENT_VIEW_STATEMENT_ABA_NUMBER]					. "\t" .
						$v[STATEMENT_VIEW_STATEMENT_START_DATE]					. "\t" .
						$v[STATEMENT_VIEW_STATEMENT_END_DATE]					. "\t" .
						$v[STATEMENT_VIEW_STATEMENT_LAST_RECONCILIATION_DATE]	. "\t" .
						$v[STATEMENT_VIEW_CLIENT_PARTNER_INITIALS] 				. "\t" .
						$v[STATEMENT_VIEW_CLIENT_STAFF_INITIALS]				. "\t" .
						$v[STATEMENT_VIEW_STATEMENT_STATUS]						. "\t" .
						$v[STATEMENT_VIEW_STATEMENT_NOTES]						. "\t" .
						$v[STATEMENT_VIEW_STATEMENT_STAMP_USER_INITIALS]		. "\t" .
						$v[STATEMENT_VIEW_STATEMENT_STAMP_DATE]					. "\t" .
						$v[STATEMENT_VIEW_STATEMENT_LAST_EDIT_USER_INITIALS]	. "\t" .
						$v[STATEMENT_VIEW_STATEMENT_LAST_EDIT_DATE]				. "\t\n";
				}
				$output .= "\nUser Created\t" . $this->session->userdata(STAFF_LOGIN) . "\nDate Created\t" . date('H:i:s m/d/Y');
				header("Content-type: application/vnd.ms-excel");
				header('Content-disposition: attachment; filename="GGG_Client_Statements_Report_Printed_For_' . $this->session->userdata(STAFF_LOGIN) . '_On_' . date("Y-m-d") . '.xls"');
				print $output;
			}
			else
			{
				header("Location: " . base_url());
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
	}

	function get_data($page = '', $records = 0, $order = STATEMENT_VIEW_CLIENT_NAME, $as = SORT_ORDER)
	{
		try
		{
			if ($this->session->userdata(ROLE_WRITE) || $this->session->userdata(ROLE_READ_ONLY))
			{
				if (isset($_POST['clear']) || (!$this->session->userdata('statement_filter')))
				{
					$statements = array('page' => 0);
				}
				elseif (isset($_POST['statement']))
				{
					$statements = $_POST['statement'];
					$statements['page'] = 0;
					$statements['order'] = $order;
					$statements['as'] = $as;
					$this->session->set_userdata('statement_filter', $statements);
				} 
				else
				{
					$statements = $this->session->userdata('statement_filter');
					$statements['page'] = ($page != '') ? $page : $statements['page'];
				}

				$this->session->set_userdata('statement_filter', $statements);

				$statements[STATEMENT_VIEW_CLIENT_NAME]	 						= isset($statements[STATEMENT_VIEW_CLIENT_NAME]) 				? $statements[STATEMENT_VIEW_CLIENT_NAME] 				: ANY_LABEL;
				$statements[STATEMENT_VIEW_STATEMENT_TYPE] 						= isset($statements[STATEMENT_VIEW_STATEMENT_TYPE]) 				? $statements[STATEMENT_VIEW_STATEMENT_TYPE] 			: ANY_VALUE;
				$statements[STATEMENT_VIEW_INSTITUTION_NAME]					= isset($statements[STATEMENT_VIEW_INSTITUTION_NAME]) 				? $statements[STATEMENT_VIEW_INSTITUTION_NAME] 			: ANY_VALUE;
				$statements[STATEMENT_VIEW_STATEMENT_ACCOUNT_NUMBER]			= isset($statements[STATEMENT_VIEW_STATEMENT_ACCOUNT_NUMBER]) 			? $statements[STATEMENT_VIEW_STATEMENT_ACCOUNT_NUMBER] 		: ANY_LABEL;
				$statements[STATEMENT_VIEW_STATEMENT_END_DATE] 					= isset($statements[STATEMENT_VIEW_STATEMENT_END_DATE]) 			? $statements[STATEMENT_VIEW_STATEMENT_END_DATE] 			: ANY_LABEL;
				$statements[STATEMENT_VIEW_STATEMENT_LAST_RECONCILIATION_DATE] 	= isset($statements[STATEMENT_VIEW_STATEMENT_LAST_RECONCILIATION_DATE]) 	? $statements[STATEMENT_VIEW_STATEMENT_LAST_RECONCILIATION_DATE] 	: ANY_LABEL;
				$statements[STATEMENT_VIEW_STATEMENT_STATUS] 					= isset($statements[STATEMENT_VIEW_STATEMENT_STATUS]) 				? $statements[STATEMENT_VIEW_STATEMENT_STATUS] 			: ANY_VALUE;
				$statements[STATEMENT_VIEW_CLIENT_STAFF_NAME] 					= isset($statements[STATEMENT_VIEW_CLIENT_STAFF_NAME]) 			? $statements[STATEMENT_VIEW_CLIENT_STAFF_NAME]			: ANY_VALUE;
				//$statement[STATEMENT_VIEW_CLIENT_STAFF_NAME] 					= isset($statements[STATEMENT_VIEW_CLIENT_STAFF_NAME]) 			? $statements[STATEMENT_VIEW_CLIENT_STAFF_NAME]			: ANY_VALUE;
				$statements[STATEMENT_VIEW_CLIENT_PARTNER_NAME] 				= isset($statements[STATEMENT_VIEW_CLIENT_PARTNER_NAME]) 			? $statements[STATEMENT_VIEW_CLIENT_PARTNER_NAME]			: ANY_VALUE;
				$statements[STATEMENT_VIEW_STATEMENT_NOTES] 					= isset($statements[STATEMENT_VIEW_STATEMENT_NOTES])  				? stripslashes(trim($statements[STATEMENT_VIEW_STATEMENT_NOTES]))	: ANY_LABEL;
				$statements['include_terminated']                               = isset($statements['include_terminated'])                          ? $statements['include_terminated'] : '';

				$statements['list'] = $this->Statement_model->view($statements, $statements['page'], $records, $order, $as);
				return $statements;
			}
			else
			{
				header("Location: " . base_url());
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
	}
}
 ?>