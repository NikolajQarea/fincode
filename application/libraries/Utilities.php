<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Utilities extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	// Function to clean a field for a MySQL '=' statement which may contain an ' in the name
	function clean_mysql_equals_statement($field)
	{
		$return = '';
		if(trim($field) != '')
		{
			$return = str_replace("'", "\\\\''", (stripslashes(urldecode(trim($field)))));
		}
		return $return;
	}
	// Function to clean a field for a MySQL 'LIKE' statement which may contain an ' in the name
	function clean_mysql_like_statement($field)
	{
		$return = '';
		if(trim($field) != '')
		{
			$return = str_replace("'", "\\\\\\\\''", (stripslashes(urldecode(trim($field)))));
		}
		return $return;
	}
	// Retrieve a list of Accounting Software Packages
	function get_accounting_software()
	{
		$sql = $this->db->select(ACCOUNTING_SOFTWARE_ID)->select(ACCOUNTING_SOFTWARE)->order_by(ACCOUNTING_SOFTWARE)->get(ACCOUNTING_SOFTWARE_TABLE);
		$result = $sql->result_array();
		return $result;
	}
	// Retrieve a list of Active Partners (initials)	
	function get_active_partners()
	{
		$sql = $this->db->distinct()->select(CLIENT_VIEW_PARTNER_ID)->select(CLIENT_VIEW_PARTNER_INITIALS)->where(CLIENT_VIEW_PARTNER_ID.' IS NOT NULL')->order_by(CLIENT_VIEW_PARTNER_INITIALS)->get(CLIENT_VIEW);
		$result = $sql->result_array();
		return $result;
	}
	// Retrieve a list of Active Staff (initials)
	function get_active_staff()
	{
		$sql = $this->db->distinct()->select(CLIENT_VIEW_STAFF_ID)->select(CLIENT_VIEW_STAFF_INITIALS)->where(CLIENT_VIEW_STAFF_ID.' IS NOT NULL')->order_by(CLIENT_VIEW_STAFF_INITIALS)->get(CLIENT_VIEW);
		$result = $sql->result_array();
		return $result;
	}
	// Retrieve a list of Active States
	function get_active_states()
	{
		$sql = $this->db->select(STATE_ID)->join(CLIENT_VIEW, STATE_TABLE.'.'.STATE_ID.' LIKE \'%'.CLIENT_VIEW.'.'.CLIENT_VIEW_STATES.'%\'').order_by(STATE_TABLE.'.'.STATE_ID)->get(STATE_TABLE);
		$result = $sql->result_array();
		return $result;
	}
	// Generic N/A item
	function get_any($id, $val)
	{
		return array($id => ANY_VALUE, $val => ANY_LABEL);
	}
	// Autocomplete Client Name Lookup
	function GetClientNameAutoComplete($term, $filtered = FALSE)
	{  
    //echo $filtered;
    $term = str_replace('%20', ' ', $term);
    if ($filtered) {
     $name = str_replace("\\\'", "'", json_encode($this->db->select(CLIENT_NAME)->where(CLIENT_STATUS_TABLE. ' IN (\'Active\',\'Pending\')')->like(CLIENT_NAME, $term)->order_by(CLIENT_NAME)->get(CLIENT_VIEW)->result_array()));
    }
    else {
      $name = str_replace("\\\'", "'", json_encode($this->db->select(CLIENT_NAME)->like(CLIENT_NAME, $term)->order_by(CLIENT_NAME)->get(CLIENT_VIEW)->result_array()));
    }
		return $name;
	}
  	// Get Client ID From Client Name
	function get_client_id($name)
	{
		$query = $this->db->select(CLIENT_VIEW_CLIENT_ID)->where(CLIENT_VIEW_CLIENT_NAME, mysql_real_escape_string(trim($name)))->get(CLIENT_VIEW)->result_array();
		return $query[0][CLIENT_VIEW_CLIENT_ID];
	}
	// Get Client Name from Client ID
	function get_client_name($id)
	{
		$res = $this->db->select(CLIENT_VIEW_CLIENT_NAME)->where(CLIENT_VIEW_CLIENT_ID, $id)->get(CLIENT_VIEW)->row_array();
		return $res[CLIENT_VIEW_CLIENT_NAME];
	}
	// Get Client Names from view
	function get_client_names()
	{
		return $this->db->select(CLIENT_VIEW_CLIENT_ID)->select(CLIENT_VIEW_CLIENT_NAME)->order_by(CLIENT_VIEW_CLIENT_NAME)->get(CLIENT_VIEW)->result_array();
	}
	// Retrieve all Client Status records
	function get_client_status()
	{
		$sql = $this->db->select(CLIENT_STATUS_ID)->select(CLIENT_STATUS)->order_by(CLIENT_STATUS)->get(CLIENT_STATUS_TABLE);
		$result = $sql->result_array();
		return $result;
	}
	// Get list of Client Types
	function get_client_types()
	{
		$sql = $this->db->select(CLIENT_TYPE_ID)->select(CLIENT_TYPE)->order_by(CLIENT_TYPE)->get(CLIENT_TYPE_TABLE);
		$result = $sql->result_array();
		return $result;
	}

	// Helper function to populate drop down lists automatically
	function get_drop_down_html($form_name, $form_id, $dataset, $id, $val, $sel_val = 0, $isFilter = false, $isMulti = false, $size = 1)
	{
		// set up Select element
		$select = CONTROL_SELECT;
		$select = str_replace('@formname@', $form_name, $select);
		$select = str_replace('@id@', $form_id, $select);
		$options = '';
		if ($isMulti)
		{
			$select = str_replace('@multi@', ' multiple="multiple" ', $select);
			$select = str_replace('@size@',  ' size="' . $size . '" ', $select);
		}
		else
		{
			$select = str_replace('@multi@', '', $select);
			$select = str_replace('@size@',  '', $select);
			$select = str_replace('[]', '', $select);
			$newOption = CONTROL_OPTION;
			if ($isFilter)
			{
				$newOption = str_replace('@value@', stripslashes(ANY_VALUE), $newOption);
				$newOption = str_replace('@label@', stripslashes(ANY_LABEL), $newOption);
				$newOption = str_replace('@selected@', $sel_val == ANY_VALUE ? ' selected="selected"' : '', $newOption);
				// if a filter, make it auto submit back
				$select = str_replace('"input"', '"input autoSubmit"', $select);
			}
			else
			{
				$newOption = str_replace('@value@', stripslashes(PLEASE_SELECT_ID), $newOption);
				$newOption = str_replace('@label@', stripslashes(PLEASE_SELECT), $newOption);
				$newOption = str_replace('@selected@', $sel_val == ANY_VALUE ? ' selected="selected"' : '',$newOption);
			}
			$options = $newOption;
		}
		// setup a default first option
		if (is_array($dataset))
		{
			foreach($dataset as $row)
			{
				$newOption = CONTROL_OPTION;
				$newOption = str_replace('@value@', stripslashes($row[$id]), $newOption);
				$newOption = str_replace('@label@', stripslashes($row[$val]), $newOption);
				if (is_array($sel_val))
				{
					foreach($sel_val as $selectedValue)
					{
						$newOption = ($row[$id] == trim($selectedValue) ? str_replace('@selected@', ' selected="selected"', $newOption) : $newOption);
					}
					 str_replace('@selected@', '', $newOption);
				}
				else
				{
					$newOption = str_replace('@selected@', $row[$id] == $sel_val ? ' selected="selected"' : '', $newOption);
				}
				$options .= $newOption;
			}
		}
		$select = str_replace('@options@', $options, $select);
		return $select;
	}
	// Retrieve a list of existing initial years
	function get_initial_years()
	{
		$sql = $this->db->query("
			SELECT DISTINCT " . CLIENT_INITIAL_YEAR_ID . " AS " . CLIENT_INITIAL_YEAR_ID . ", 
				IF(" . CLIENT_INITIAL_YEAR_ID . " = 0, 'Unknown', " . CLIENT_INITIAL_YEAR_ID . ") AS " . CLIENT_INITIAL_YEAR . " 
				FROM " . CLIENT_TABLE . " 
				ORDER BY " . CLIENT_INITIAL_YEAR_ID . " DESC");
		$result = $sql->result_array();
		return $result;
	}
	// Get Institutions
	function get_institutions($order = INSTITUTION_VIEW_NAME, $as = SORT_ORDER, $start = 0, $count = 0)
	{
		$UserData = NO_RECORDS;
		try
		{
			$sql = "SELECT * FROM " . INSTITUTION_VIEW . " ORDER BY " . $order . " " . $as;
			if ($count > 0)
			{
				$sql .= " LIMIT " . $start . ", " . $count;
			}
			$query = $this->db->query($sql);
			if($query->result_array())
			{
				$UserData = $query->result_array();
				
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
		return $UserData;
	}
	// Retrieve Tax Return Instructions
	function get_tax_return_instructions()
	{
		return $this->db->select(INSTRUCTIONS_ID)->select(INSTRUCTIONS)->order_by(INSTRUCTIONS)->get(INSTRUCTIONS_TABLE)->result_array();
	}
	// Generic N/A item
	function get_not_available($id, $val)
	{
		return array($id => '0', $val => NOT_AVAILABLE);
	}
	// Generic NO RECORDS item
	function get_no_records($id, $val)
	{
		return array($id => '0', $val => NO_RECORDS);
	}
	// Generic NOT APPLICABLE item
	function get_not_applicable($id, $val)
	{
		return array($id => -1, $val => NOT_APPLICABLE);
	}
	// Get list of Occupations
	function get_occupations()
	{
		$sql = $this->db->query('SELECT * FROM Occupation ORDER BY Occupation_Name ASC');
		$result = $sql->result_array();
		return $result;
	}
	// Retrieve a list of Partners (initials)
	function get_partners($order = STAFF_INITIALS, $as = 'ASC', $start = 0, $count = 0)
	{
		$UserData = NO_RECORDS;
		try
		{
			$sql = "SELECT s." . STAFF_ID . ", s." . STAFF_INITIALS . " FROM " . STAFF_ROLE_LINK_TABLE . " AS srl LEFT JOIN " . STAFF_TABLE . " AS s ON srl." . STAFF_ROLE_LINK_STAFF_ID . " = s." . STAFF_ID . " WHERE srl." . STAFF_ROLE_LINK_ROLE_ID . " = '" . PARTNER_ROLE_ID . "' ORDER BY " . $order;
			if ($count > 0)
			{
				$sql .= " LIMIT " . $start . ", " . $count;
			}
			$query = $this->db->query($sql);
			if($query->result_array())
			{
				$UserData = $query->result_array();
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
		return $UserData;
	}
	// GET ALL PARTNERS FOR DROP DOWN
	function get_partners_dropdown($isFilter = false)
	{
		$Data = array();
		if($isFilter)
		{
			$Data[0] = $this->get_any(STAFF_ID, STAFF_INITIALS);
		}
		else
		{
			$Data[0] = $this->get_select(STAFF_ID, STAFF_INITIALS);
		}
		$UserData = array();
		$UserData[0] = $this->get_no_records(STAFF_ID, STAFF_INITIALS);
		try
		{
			$sql = "
				SELECT 		s." . STAFF_ID . ", 
							s." . STAFF_INITIALS . "
				FROM		" . STAFF_ROLE_LINK_TABLE . " srl 
				LEFT JOIN 	" . STAFF_TABLE . " s
					ON		s." . STAFF_ID . " = srl." . STAFF_ID . "
				WHERE		srl." . ROLE_ID . " = " . PARTNER_ROLE_ID . "
				ORDER BY 	" . STAFF_INITIALS;
			$query = $this->db->query($sql);
			if($query->result_array())
			{
				$UserData = $query->result_array();
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
		return array_merge($Data, $UserData );
	}
	// Get List of Referred By's
	function get_referred_bys()
	{
		return $this->db->select(REFERRED_BY_ID)->select(REFERRED_BY)->order_by(REFERRED_BY)->get(REFERRED_BY_TABLE)->result_array();
	}
	// Get Retainer Status's
	function get_retainer_status()
	{
		return $this->db->select(RETAINER_STATUS_ID)->select(RETAINER_STATUS)->order_by(RETAINER_STATUS)->get(RETAINER_STATUS_TABLE)->result_array();
	}
	// Get Client Retainer Years for Drop Downs
	function get_retainer_years()
	{
		$years = array();
		$years[0] = $this->get_select(CLIENT_RETAINER_YEAR, RETAINER_YEAR);
		$years[1] = array(CLIENT_RETAINER_YEAR => 1970, RETAINER_YEAR => NOT_AVAILABLE);
		$curYear = date('Y');
		for ($i = ($curYear + 1); $i > 1999; $i--)
		{
			$years[$i] = array(CLIENT_RETAINER_YEAR => $i, RETAINER_YEAR => $i);
		}
		return $years;
	}
	// Get Staff Initials From ID
	function get_staff_initials($id)
	{
		$return = '';
		if($id > 0)
		{
			$query = $this->db->select(STAFF_INITIALS)->where(STAFF_ID, $id)->get(STAFF_TABLE)->result_array();
			$return = $query[0][STAFF_INITIALS];
		}
		return $return;
	}
	// Retrieve All Statement Status
    function get_statement_status()
    {
		$UserData = array();
		$UserData[] = $this->get_no_records(STATEMENT_STATUS_ID, STATEMENT_STATUS);
		try
		{
			$query = $this->db->select()->order_by(STATEMENT_STATUS)->get(STATEMENT_STATUS_TABLE);
			if($query->result_array())
			{
				$UserData = $query->result_array();
				
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
		return array_merge($UserData);
	}
	// Retrieve All Statement Types
    function get_statement_types()
    {
		$UserData = array();
		$UserData[] = $this->get_no_records(STATEMENT_TYPE_ID, STATEMENT_TYPE);
		try
		{
			$query = $this->db->select()->order_by(STATEMENT_TYPE)->get(STATEMENT_TYPE_TABLE);
			if($query->result_array())
			{
				$UserData = $query->result_array();
				
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
		return array_merge($UserData);
	}
	// Retrieve a list of all States
	function get_states()
	{
		$sql = $this->db->select(STATE_ID)->order_by(STATE_ID)->get(STATE_TABLE);
		$result = $sql->result_array();
		return $result;
	}
	// Generic SELECT item
	function get_select($id, $val)
	{
		return array($id => PLEASE_SELECT_ID, $val => PLEASE_SELECT);
	}
	// GET ALL STAFF
	function get_staff($order = STAFF_INITIALS, $as = 'ASC', $start = 0, $count = 0, $include_terminated = '')
	{

        $where = ($include_terminated != 'off') ? "''" : "'Terminated'";
		$UserData = NO_RECORDS;
		try
		{
			$sql = "SELECT * FROM " . STAFF_VIEW . " WHERE Staff_Status_Label != " . $where .  " ORDER BY " . $order . " " . $as;
			if ($count > 0)
			{
				$sql .= " LIMIT " . $start . ", " . $count;
			}
			$query = $this->db->query($sql);
			if($query->result_array())
			{
				$UserData = $query->result_array();
				
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
		return $UserData;
	}
	// GET ALL STAFF FOR DROP DOWN
	function get_staff_dropdown($isFilter = false)
	{
		$Data = array();
		if($isFilter)
		{
			$Data[0] = $this->get_any(STAFF_ID, STAFF_INITIALS);
		}
		else
		{
			$Data[0] = $this->get_select(STAFF_ID, STAFF_INITIALS);
		}
		$UserData = array();
		$UserData[] = $this->get_no_records(STAFF_ID, STAFF_INITIALS);
		try
		{
			$query = $this->db->query("SELECT " . STAFF_ID . ", " . STAFF_INITIALS . " FROM " . STAFF_TABLE . " ORDER BY " . STAFF_INITIALS);
			if($query->result_array())
			{
				$UserData = $query->result_array();
				
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
		return array_merge($Data, $UserData);
	}
	// Get All Tax Forms
    function get_tax_forms()
    {
		$UserData = array();
		$UserData[] = $this->get_no_records(TAX_FORM_ID, TAX_FORM);
		try
		{
			$query = $this->db->query("SELECT * FROM " . TAX_FORM_TABLE . " ORDER BY " . TAX_FORM . " ASC");
			if($query->result_array())
			{
				$UserData = $query->result_array();
				
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
		return array_merge($UserData);
	}
	// Get Tax Form Name from ID
	function get_tax_form_name($id)
	{
		$return = '';
		if($id > 0)
		{
			$query = $this->db->select(TAX_FORM)->where(TAX_FORM_ID, $id)->get(TAX_FORM_TABLE)->result_array();
			$return = $query[0][TAX_FORM];
		}
		return $return;
	}
	// Get All Tax Forms
    function get_tax_forms_by_client_id($id)
    {
		$UserData = array();
		$UserData[] = $this->get_no_records(TAX_FORM_ID, TAX_FORM);
		try
		{
			$query = $this->db
				->select(TAX_FORM_TABLE.'.'.TAX_FORM_ID)
				->select(TAX_FORM_TABLE.'.'.TAX_FORM)
				->join(TAX_FORM_TABLE, TAX_FORM_TABLE .'.'. TAX_FORM_ID .' = '.CLIENT_TAX_FORM_LINK_TABLE.'.'.CLIENT_TAX_FORM_LINK_TAX_FORM_ID, 'left')
				->where(CLIENT_TAX_FORM_LINK_TABLE.'.'.CLIENT_TAX_FORM_LINK_CLIENT_ID, $id)
				->order_by(TAX_FORM_TABLE .'.'. TAX_FORM)
				->get(CLIENT_TAX_FORM_LINK_TABLE);
			if($query->result_array())
			{
				$UserData = $query->result_array();
				
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
		return array_merge($UserData);
	}
	// GET Tax Months for Drop Down Populatin
	function get_tax_months()
	{
		$months = array();
		$months [0] = $this->get_not_applicable(TAX_MONTH_ID, TAX_MONTH);
		try
		{
			for($i = 1; $i < 13; $i++)
			{
				$months[$i] = array(TAX_MONTH_ID => $i, TAX_MONTH => date('F', mktime(0, 0, 0, $i, 1, 2000)));
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
		return $months;
	}
	// GET Tax Quarters for Drop Down Population
	function get_tax_quarters()
	{
		$quarters = array();
		$quarters[0] = array(TAX_QUARTER_ID => -1, TAX_QUARTER => NOT_APPLICABLE);
		$quarters[1] = array(TAX_QUARTER_ID => 1, TAX_QUARTER => TAX_QUARTER_1);
		$quarters[2] = array(TAX_QUARTER_ID => 2, TAX_QUARTER => TAX_QUARTER_2);
		$quarters[3] = array(TAX_QUARTER_ID => 3, TAX_QUARTER => TAX_QUARTER_3);
		$quarters[4] = array(TAX_QUARTER_ID => 4, TAX_QUARTER => TAX_QUARTER_4);
		return $quarters;
	}
	// Created a Tax Return Filter Drop Down
	function get_tax_return_filter()
	{
		$Data = array();
		$Data[0] = array('id' => 1, 'label' => 'With Returns');
		$Data[1] = array('id' => 2, 'label' => 'Without Returns');
		return $Data;		
	}
	// Create a Drop Down for a Tax Return -> Client Partner
	function get_tax_return_client_partner($clientID, $formName, $formID)
	{
		$UserData = array();
		$UserData[0] = $this->get_no_records(TAX_RETURN_STATUS_ID, TAX_RETURN_STATUS);
		try
		{
			$query = $this->db->select(CLIENT_VIEW_PARTNER_ID)->select(CLIENT_VIEW_PARTNER_NAME)->where(CLIENT_VIEW_CLIENT_ID, $clientID)->get(CLIENT_VIEW);
			if($query->num_rows() > 0)
			{
				$UserData = $query->result_array();
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
		return $this->get_drop_down_html($formName, $formID, $this->get_partners(), STAFF_ID, STAFF_INITIALS, $UserData[0][CLIENT_VIEW_PARTNER_ID], false, false, 0);
	}
	// Create a Drop Down for a Tax Return -> Client -> Tax Forms
	function get_tax_return_client_tax_forms($clientID, $formName, $formID)
	{
		$UserData = array();
		$UserData[0] = $this->get_no_records(TAX_FORM_ID, TAX_FORM);
		try
		{
			$query = $this->db
			->select(TAX_FORM_TABLE.'.'.TAX_FORM_ID)
			->select(TAX_FORM_TABLE.'.'.TAX_FORM)
			->join(TAX_FORM_TABLE, TAX_FORM_TABLE.'.'.TAX_FORM_ID.' = '.CLIENT_TAX_FORM_LINK_TABLE.'.'.CLIENT_TAX_FORM_LINK_TAX_FORM_ID, 'left')
			->where(CLIENT_TAX_FORM_LINK_TABLE.'.'.CLIENT_TAX_FORM_LINK_CLIENT_ID, $clientID)
			->order_by(TAX_FORM)
			->get(CLIENT_TAX_FORM_LINK_TABLE);
			if($query->num_rows() > 0)
			{
				$UserData = $query->result_array();
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
		return $this->get_drop_down_html($formName, $formID, $UserData, TAX_FORM_ID, TAX_FORM, PLEASE_SELECT_ID, false, false, 0);
	}
	//Retrieves a list of all tax return statuses
	function get_tax_return_status()
	{
		$Data = array();
		$UserData = array();
		$UserData[0] = $this->get_no_records(TAX_RETURN_STATUS_ID, TAX_RETURN_STATUS);
		try
		{
			$query = $this->db->select()->order_by(TAX_RETURN_STATUS)->get(TAX_RETURN_STATUS_TABLE);
			if($query->result_array())
			{
				$UserData = $query->result_array();
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
		return array_merge($Data, $UserData);
	}
	// GET Tax Years for Drop Down Population
	function get_tax_years()
	{
		$years = array();
		$years[1] = array(TAX_YEAR_ID => 1970, TAX_YEAR => NOT_AVAILABLE);
		$curYear = date('Y');
		for ($i = ($curYear); $i > 1999; $i--)
		{
			$years[$i] = array(TAX_YEAR_ID => $i, TAX_YEAR => $i);
		}
		return $years;
	}
	function is_valid_date($date)
	{
		$check = false;
		try
		{
			$date = stripslashes(trim($date));
			if($date != '')
			{
				$t = strtotime($date);
				if (checkdate(date('m', $t), date('d', $t), date('Y', $t)))
				{
					$check = true;
				}
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
		return $check;
	}
}

 ?>