<?php
class User_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('Utilities');
	}
	
	// Private User Object
	private $userData = array
	(
			STAFF_ID 						=> '',
			STAFF_NAME_LAST  				=> '',
			STAFF_NAME_FIRST  				=> '',
			STAFF_NAME_FULL					=> '',
			STAFF_INITIALS  				=> '',
			STAFF_LOGIN  					=> '',
			STAFF_EMAIL						=> '',
			STAFF_STAFF_STATUS_ID			=> '',
			STAFF_STATUS					=> '',
			'Roles'							=> array(),
			STAFF_USER_ROLES				=> '',
			STAFF_STAMP_DATE				=> '',
			STAFF_STAMP_USER_ID				=> '',
			STAFF_STAMP_USER_NAME_LAST		=> '',
			STAFF_STAMP_USER_NAME_FIRST		=> '',
			STAFF_STAMP_USER_NAME_FULL		=> '',
			STAFF_STAMP_USER_INITIALS		=> '',
			STAFF_LAST_EDIT_DATE			=> '',
			STAFF_LAST_EDIT_USER_ID			=> '',
			STAFF_LAST_EDIT_USER_NAME_LAST	=> '',
			STAFF_LAST_EDIT_USER_NAME_FIRST	=> '',
			STAFF_LAST_EDIT_USER_NAME_FULL	=> '',
			STAFF_LAST_EDIT_USER_INITIALS	=> '',
			'HasSysAdmin'					=> false,
			ROLE_ADMIN						=> false,
			ROLE_WRITE						=> false,
			ROLE_READ_ONLY					=> false,
			'HasGuest'						=> true
	);
	// Private Staff_ID
	private $staffID;
	// Login through Main Page
	function login($user)
	{
		try
		{
			// Retrieve User
			$queryUser = $this->db->select(STAFF_ID)->where(STAFF_LOGIN, $user[STAFF_LOGIN])->where(STAFF_PASSWORD, md5($user[STAFF_PASSWORD]))->get(STAFF_TABLE);
	       	// If Valid User, Get Array
			if($queryUser->row_array())
			{
				$User = $queryUser->row_array();
				// get this users information
				$this->staffID = $User[STAFF_ID];
				$this->get_user($this->staffID);
				$this->session->set_userdata($this->userData);
				//$this->session->set_userdata(STAFF_ID, $this->staffID);
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
		// Return User Data
		return $this->userData;
	}
	function add($user, $roles, $staff_id = 0, $isEdit = false)
	{
		try
		{
			// If passes validation, commit to database
			if (
				isset($roles) &&
				$user[STAFF_NAME_LAST] && 
				$user[STAFF_NAME_FIRST] && 
				$user[STAFF_LOGIN] && 
				$user[STAFF_EMAIL] && 
				$user[STAFF_INITIALS] &&
				$user[STAFF_STAFF_STATUS_ID]
			)
			{ 
				$newUser = array
				(
					STAFF_INITIALS 					=> mysql_real_escape_string(trim($user[STAFF_INITIALS])),
					STAFF_NAME_LAST 				=> mysql_real_escape_string(trim($user[STAFF_NAME_LAST])),
					STAFF_NAME_FIRST 				=> mysql_real_escape_string(trim($user[STAFF_NAME_FIRST])),
					STAFF_LOGIN 					=> mysql_real_escape_string(trim($user[STAFF_LOGIN])),
					STAFF_EMAIL 					=> mysql_real_escape_string(trim($user[STAFF_EMAIL])),
					STAFF_LAST_EDIT_DATE 			=> time(),
					STAFF_LAST_EDIT_USER_ID 		=> $this->session->userdata(STAFF_ID),
					STAFF_STAFF_STATUS_ID			=> $user[STAFF_STAFF_STATUS_ID]
				);
				if(!$isEdit)
				{
					$newUser[STAFF_STAMP_DATE] 		= time();
					$newUser[STAFF_STAMP_USER_ID] 	= $this->session->userdata(STAFF_ID);
					$newUser[STAFF_PASSWORD]		= md5($user[STAFF_PASSWORD]);
					$this->db->insert(STAFF_TABLE, $newUser);
					// Get New Staff_ID
					$staff_id = $this->db->insert_id();
				}
				else
				{
					if(isset($user[STAFF_PASSWORD_NEW]) && 
						$user[STAFF_PASSWORD_NEW] != '' && 
						isset($user[STAFF_PASSWORD_CONFIRM]) && 
						$user[STAFF_PASSWORD_CONFIRM] != '' &&
						trim($user[STAFF_PASSWORD_NEW]) == trim($user[STAFF_PASSWORD_CONFIRM])
					)
					{
						$newUser[STAFF_PASSWORD]	= md5(trim($user[STAFF_PASSWORD_NEW]));
					}
					$this->db->where(STAFF_ID, $staff_id);
					$this->db->update(STAFF_TABLE, $newUser);
					// Remove existing user roles here, then add below
					$this->db->where(STAFF_ID, $staff_id);
					$this->db->delete(STAFF_ROLE_LINK_TABLE);
				}
				// Process user roles
				foreach ($roles as $key => $value)
				{
					$sqlRole = $this->db->insert(STAFF_ROLE_LINK_TABLE, array(STAFF_ID => $staff_id, STAFF_ROLE_ID => $value));
				}
				// Return
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
	}
	   
	function edit()
	{
	}
    
	function view($start, $count, $order, $as, $include_terminated)
	{
		return $this->utilities->get_staff($order, $as, $start, $count, $include_terminated);
	}
	function get_user($staff_id)
	{
		try
		{
			$queryUser = $this->db->select()->where(STAFF_ID, $staff_id)->get(STAFF_VIEW);
			if ($queryUser ->num_rows() > 0)
			{
				$User = $queryUser->row_array();
				$this->userData[STAFF_ID] 							= $User[STAFF_ID];
				$this->userData[STAFF_NAME_LAST] 					= $User[STAFF_NAME_LAST];
				$this->userData[STAFF_NAME_FIRST] 					= $User[STAFF_NAME_FIRST];
				$this->userData[STAFF_NAME_FULL] 					= $User[STAFF_NAME_FULL];
				$this->userData[STAFF_INITIALS] 					= $User[STAFF_INITIALS];
				$this->userData[STAFF_LOGIN]						= $User[STAFF_LOGIN];
				$this->userData[STAFF_EMAIL] 						= $User[STAFF_EMAIL];
				$this->userData[STAFF_STAFF_STATUS_ID]				= $User[STAFF_STAFF_STATUS_ID];
				$this->userData[STAFF_STATUS]						= $User[STAFF_STATUS];
				$this->userData[STAFF_USER_ROLES]					= $User[STAFF_USER_ROLES];
				$this->userData[STAFF_STAMP_DATE]					= $User[STAFF_STAMP_DATE];
				$this->userData[STAFF_STAMP_USER_ID]				= $User[STAFF_STAMP_USER_ID];
				$this->userData[STAFF_STAMP_USER_NAME_LAST]			= $User[STAFF_STAMP_USER_NAME_LAST];
				$this->userData[STAFF_STAMP_USER_NAME_FIRST]		= $User[STAFF_STAMP_USER_NAME_FIRST];
				$this->userData[STAFF_STAMP_USER_NAME_FULL]			= $User[STAFF_STAMP_USER_NAME_FULL];
				$this->userData[STAFF_STAMP_USER_INITIALS]			= $User[STAFF_STAMP_USER_INITIALS];
				$this->userData[STAFF_LAST_EDIT_DATE]				= $User[STAFF_LAST_EDIT_DATE];
				$this->userData[STAFF_LAST_EDIT_USER_ID]			= $User[STAFF_LAST_EDIT_USER_ID];
				$this->userData[STAFF_LAST_EDIT_USER_NAME_LAST]		= $User[STAFF_LAST_EDIT_USER_NAME_LAST];
				$this->userData[STAFF_LAST_EDIT_USER_NAME_FIRST]	= $User[STAFF_LAST_EDIT_USER_NAME_FIRST];
				$this->userData[STAFF_LAST_EDIT_USER_NAME_FULL]		= $User[STAFF_LAST_EDIT_USER_NAME_FULL];
				$this->userData[STAFF_LAST_EDIT_USER_INITIALS]		= $User[STAFF_LAST_EDIT_USER_INITIALS];
				// Assign User Access
				$queryRoles = $this->get_user_roles($staff_id);
				$Roles = $queryRoles->row_array();
				 if ($queryRoles->num_rows() > 0)
				 {
				 	$this->userData['Roles']		= $queryRoles->row_array();
				    foreach ($queryRoles->result() as $row)
				    {
				    	switch($row->Role_ID)
				    	{
				    		// Administrator
				    		case 1:
				    		$this->userData['HasSysAdmin'] = true;
				    		$this->userData[ROLE_ADMIN] = true;
				    		$this->userData[ROLE_WRITE] = true;
				    		$this->userData[ROLE_READ_ONLY] = true;
				    		// Partner
				    		case 2:
				    		$this->userData[ROLE_ADMIN] = true;
				    		$this->userData[ROLE_WRITE] = true;
				    		$this->userData[ROLE_READ_ONLY] = true;
				    		// Editor
				    		case 3:
				    		$this->userData[ROLE_WRITE] = true;
				    		$this->userData[ROLE_READ_ONLY] = true;
				    		// Read Only
				    		case 4:
				    		$this->userData[ROLE_READ_ONLY] = true;
						}
					}
				}
			}
			return $this->userData;
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
	}
	// DEPRECATED
	/*
	// Set User Data
	function set_data($staff_id, $user)
	{
		try
		{
			// Update Staff User Data
			$queryUpdate = $this->db->query("
				UPDATE 	staff
				SET 	Staff_Last_Name = '" . mysql_real_escape_string(trim($user['Name_Last'])) . "',
						Staff_First_Name` = '" . mysql_real_escape_string(trim($user['Name_First'])) . "',
						Initials = '" . mysql_real_escape_string(trim($user[STAFF_ID])) . "',
						Login_ID = '" . mysql_real_escape_string(trim($user['username'])) . "',
						Email = '" . mysql_real_escape_string(trim($user[STAFF_EMAIL])) . "'
				WHERE 	Staff_ID = " . $staff_id
			);
			// Delete Existing Roles for Staff User
			$queryDelete = $this->db->query("
				DELETE	Staff_Role_Link
				WHERE	Staff_ID = " . $staff_id
			);
			// Add New Roles For Staff User
			if(!empty($user['Roles']))
			{
				while(list($key, $val) = each($user['Roles']))
				{
					$sqlRole = "INSERT INTO Staff_Role_Link
					(
						Staff_ID,
						Role_ID
					)
					SELECT	" . $staff_id . ",
							r.Role_ID
					FROM	Role r
					WHERE	Role_Name = '" . mysql_real_escape_string(trim($val)) . "'";
					$this->db->query($sqlRole);
				}
			}
			// If Changing Password...
			(isset($user[STAFF_PASSWORD]) && $user[STAFF_PASSWORD]) ? $this->db->query("UPDATE staff SET Password = '" . md5($user[STAFF_PASSWORD]). "' WHERE 	Staff_ID = '" . $staff_id . "'") : '';
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
	}
*/	//Delete User
	function delete_user($staff_id)
	{
		try
		{
			$this->db->delete(STAFF_TABLE, array(STAFF_ID => $staff_id));
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
	}
	// Get Count All Users	
	function get_all($include_terminated)
	{
                if ($include_terminated == 'off')
                {
                    $this->db->where('Staff_Status_Label' , 'Active');
                }
                $this->db->from(STAFF_VIEW);
                $res = $this->db->count_all_results();
                return $res; 
        }
        // Check to make sure a set of Initials is not already in use when adding or modifying a Staff Account
	function check_staff_initials($initials, $staff_id = 0)
	{
		$valid = false;
		try
		{
			$query = $this->db->where(STAFF_INITIALS, mysql_real_escape_string(trim($initials)))->where_not_in(STAFF_ID, $staff_id)->get(STAFF_TABLE);
			if($query->num_rows())
			{
				$valid = true;
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
		return $valid;
	}
    // Check to make sure an email address is not already in use when adding or modifying a Staff Account
	function check_staff_email($email, $staff_id = 0)
	{
		$valid = false;
		try
		{
			$query = $this->db->where(STAFF_EMAIL, mysql_real_escape_string(trim($email)))->where_not_in(STAFF_ID, $staff_id)->get(STAFF_TABLE);
			if($query->num_rows())
			{
				$valid = true;
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
		return $valid;
	}
    // Check to make sure a Login_ID is not already in use when adding or modifying a Staff Account
	function check_staff_login($login_id, $staff_id = 0)
	{
		$valid = false;
		try
		{
			$query = $this->db->where(STAFF_LOGIN, mysql_real_escape_string(trim($login_id)))->where_not_in(STAFF_ID, $staff_id)->get(STAFF_TABLE);
			if($query->num_rows())
			{
				$valid = true;
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
		return $valid;
	}
	// Return a set of User Roles.  If Staff_ID is passed in, returns only roles for that staff member, else returns all roles.
	function get_user_roles($staff_id = 0)
	{
		$query;
		
		try
		{
			// If Staff_ID is passed, pull only the roles for that staff member
			if ($staff_id > 0)
			{
				$query = $this->db->query("
					SELECT		r." . ROLE_ROLE_ID. ",
								r." . ROLE_ROLE . "
					FROM		" . STAFF_ROLE_LINK_TABLE . " srl
					LEFT JOIN 	" . ROLE_TABLE . " r
					ON			r." . ROLE_ROLE_ID . " = srl." . STAFF_ROLE_LINK_ROLE_ID . "
					WHERE 		srl." . STAFF_ROLE_LINK_STAFF_ID . " = " . $staff_id . "
					ORDER BY	r." . ROLE_ROLE . " ASC" 
				);
			}
			else
			{
				$query = $this->db->select(ROLE_ROLE_ID)->select(ROLE_ROLE)->order_by(ROLE_ROLE)->get(ROLE_TABLE);
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
		return $query;
	}
	function get_user_roles_options($staff_id = 0)
	{
		try
		{
			$query = $this->db->query("
				SELECT		CONCAT('<option value=\'', r." . ROLE_ROLE_ID . ", '\' ', IF(srl." . STAFF_ROLE_LINK_STAFF_ID . " IS NOT NULL, 'selected=\'selected\'', ''), '>', r." . ROLE_ROLE . ", '</option>') AS SelectOption
				FROM		" . ROLE_TABLE . " r
				LEFT JOIN	" . STAFF_ROLE_LINK_TABLE . " srl 
				ON 			srl." . STAFF_ROLE_LINK_ROLE_ID . " = r." . ROLE_ROLE_ID . " 
				AND 		srl." . STAFF_ROLE_LINK_STAFF_ID . " = " . $staff_id . ";"
			);
			foreach ($query->result() as $row)
			 {
			   echo $row->SelectOption;
			 }
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
	}
	function get_user_status_options($staff_id = 0)
	{
		try
		{
			$query = $this->db->query("
				SELECT		CONCAT('<option value=\'', ss." . STAFF_STATUS_ID . ", '\' ', IF(s." . STAFF_ID . " IS NOT NULL, 'selected=\'selected\'', ''), '>', ss." . STAFF_STATUS . ", '</option>') AS SelectOption
				FROM		" . STAFF_STATUS_TABLE . " ss
				LEFT JOIN	" . STAFF_TABLE . " s 
					ON		s." . STAFF_STAFF_STATUS_ID . " = ss." . STAFF_STATUS_ID . " 
					AND		s." . STAFF_ID . " = " . $staff_id . ";"
			);
			foreach ($query->result() as $row)
			 {
			   echo $row->SelectOption;
			 }
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
	}
}
 ?>