<?php
class Manage_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	//occupation
	function view_occupation()
	{
		$sql = $this->db->query("
			SELECT 		" . OCCUPATION_ID . ",
						" . OCCUPATION . ",
						COUNT(c.".CLIENT_ID.") AS Status
			FROM 		" . OCCUPATION_TABLE . " o
			LEFT JOIN 	" . CLIENT_TABLE . " c on o.".OCCUPATION_ID." = c.".CLIENT_OCCUPATION_ID." or o.".OCCUPATION_ID." = c.".SPOUSE_OCCUPATION_ID."
			GROUP BY 	o.".OCCUPATION_ID."
			ORDER BY 	o.".OCCUPATION
		);
		$result = $sql->result_array();
		return $result;
		
	}
	function delete_occupation($id)
	{
		$this->db->delete(OCCUPATION_TABLE, array(OCCUPATION_ID => $id));
	}
	function update_occupation($id, $occupation)
	{
		$this->db->where(OCCUPATION_ID, $id)->update(OCCUPATION_TABLE, array(OCCUPATION => mysql_real_escape_string(trim($occupation))));
	}
	function get_occupation($id)
	{
		return $this->db->select(OCCUPATION_ID)->select(OCCUPATION)->where(OCCUPATION_ID, $id)->get(OCCUPATION_TABLE)->result_array();
	}
	function add_occupation($occupation)
	{
		$this->db->insert(OCCUPATION_TABLE, array(OCCUPATION  => mysql_real_escape_string(trim($occupation[OCCUPATION]))));
		return $this->db->insert_id();
	}
	//occupation end
	//Client Type
	function view_client_type()
	{
		$sql = $this->db->query("
			SELECT 		ct." . CLIENT_TYPE_ID . ",
						ct." . CLIENT_TYPE . ",
						COUNT(c." . CLIENT_ID . ") AS Status
			FROM 		" . CLIENT_TYPE_TABLE . " ct
			LEFT JOIN 	" . CLIENT_TABLE . " c on ct.".CLIENT_TYPE_ID." = c.".CLIENT_TYPE_ID."
			GROUP BY 	ct.".CLIENT_TYPE_ID."
			ORDER BY 	ct.".CLIENT_TYPE
		);
		$result = $sql->result_array();
		return $result;
	}
	function delete_client_type($id)
	{
		$this->db->delete(CLIENT_TYPE_TABLE, array(CLIENT_TYPE_ID => mysql_real_escape_string(trim($id))));
	}
	function update_client_type($id, $client_type)
	{
		$query = $this->db->where(CLIENT_TYPE_ID, $id)->update(CLIENT_TYPE_TABLE, array(CLIENT_TYPE => mysql_real_escape_string(trim($client_type))));
	}
	function get_client_type($id)
	{
		$sql = $this->db->select('*')->where(CLIENT_TYPE_ID, $id)->get(CLIENT_TYPE_TABLE);
		$result = $sql->result_array();
		return $result;
	}
	function add_client_type($client_type)
	{
		$this->db->insert(CLIENT_TYPE, array(CLIENT_TYPE => mysql_real_escape_string(trim($client_type[CLIENT_TYPE]))));
	}
	//entity type end

	// Instructions
	function view_instructions()
	{
		$sql = $this->db->query("
			SELECT 		i." . INSTRUCTIONS_ID . ",
						i." . INSTRUCTIONS . ",
						COUNT(tr." . TAX_RETURN_ID . ") AS Status
			FROM 		" . INSTRUCTIONS_TABLE . " i
			LEFT JOIN 	" . TAX_RETURN_TABLE . " tr on i.".INSTRUCTIONS_ID." = tr.".INSTRUCTIONS_ID."
			GROUP BY 	i.".INSTRUCTIONS_ID."
			ORDER BY 	i.".INSTRUCTIONS
		);
		$result = $sql->result_array();
		return $result;
	}
	function delete_instructions($id)
	{
		$this->db->delete(INSTRUCTIONS_TABLE, array(INSTRUCTIONS_ID => mysql_real_escape_string(trim($id))));
	}
	function update_instructions($id, $instructions)
	{
		$query = $this->db->where(INSTRUCTIONS_ID, $id)->update(INSTRUCTIONS_TABLE, array(INSTRUCTIONS => mysql_real_escape_string(trim($instructions))));
	}
	function get_instructions($id)
	{
		$sql = $this->db->select('*')->where(INSTRUCTIONS_ID, $id)->get(INSTRUCTIONS_TABLE);
		$result = $sql->result_array();
		return $result;
	}
	function add_instructions($instructions)
	{
		$this->db->insert(INSTRUCTIONS, array(INSTRUCTIONS => mysql_real_escape_string(trim($instructions[INSTRUCTIONS]))));
	}
	// Instructions end

	//Referred By
	function view_referred_by()
	{
		$sql = $this->db->query("
			SELECT 		rb." . REFERRED_BY_ID . ",
						rb." . REFERRED_BY . ",
						COUNT(c." . CLIENT_ID . ") AS Status
			FROM 		" . REFERRED_BY_TABLE . " rb
			LEFT JOIN 	" . CLIENT_TABLE . " c on rb.".REFERRED_BY_ID." = c.".REFERRED_BY_ID."
			GROUP BY 	rb.".REFERRED_BY_ID."
			ORDER BY 	rb.".REFERRED_BY
		);
		$result = $sql->result_array();
		return $result;
	}
	function delete_referred_by($id)
	{
		$this->db->delete(REFERRED_BY_TABLE, array(REFERRED_BY_ID => $id));
	}
	function update_referred_by($id, $referred_by)
	{
		$query = $this->db->where(REFERRED_BY_ID, $id)->update(REFERRED_BY_TABLE, array(REFERRED_BY => mysql_real_escape_string(trim($referred_by))));
	}
	function get_referred_by($id)
	{
		$sql = $this->db->select(REFERRED_BY_ID)->select(REFERRED_BY)->where(REFERRED_BY_ID, $id)->get(REFERRED_BY_TABLE);
		$result = $sql->result_array();
		return $result;
	}
	function add_referred_by($referred_by)
	{
		$this->db->insert(REFERRED_BY_TABLE, array(REFERRED_BY => mysql_real_escape_string(trim($referred_by[REFERRED_BY]))));
	}
	//Referred By end
	//Statement Status
	function view_statement_status()
	{
		$sql = $this->db->query("
			SELECT 		ss." . STATEMENT_STATUS_ID . ",
						ss." . STATEMENT_STATUS . ",
						COUNT(sh.".STATEMENT_HISTORY_STATEMENT_ID.") AS Status
			FROM 		" . STATEMENT_STATUS_TABLE . " ss
			LEFT JOIN 	" . STATEMENT_HISTORY_TABLE . " sh on ss.".STATEMENT_STATUS_ID." = sh.".STATEMENT_HISTORY_STATEMENT_ID."
			GROUP BY 	ss.".STATEMENT_STATUS_ID."
			ORDER BY 	ss.".STATEMENT_STATUS
		);
		$result = $sql->result_array();
		return $result;
		
	}
	function delete_statement_status($id)
	{
		$this->db->delete(STATEMENT_STATUS_TABLE, array(STATEMENT_STATUS_ID => $id));
	}
	function update_statement_status($id, $status)
	{
		$this->db->where(STATEMENT_STATUS_ID, $id)->update(STATEMENT_STATUS_TABLE, array(STATEMENT_STATUS => mysql_real_escape_string(trim($status))));
	}
	function get_statement_status($id)
	{
		return $this->db->select(STATEMENT_STATUS_ID)->select(STATEMENT_STATUS)->where(STATEMENT_STATUS_ID, $id)->get(STATEMENT_STATUS_TABLE)->result_array();
	}
	function add_statement_status($status)
	{
		$this->db->insert(STATEMENT_STATUS_TABLE, array(STATEMENT_STATUS => mysql_real_escape_string(trim($status))));
		return $this->db->insert_id();
	}
	//Statement Status end
	//Statement Type
	function view_statement_type()
	{
		$sql = $this->db->query("
			SELECT 		st." . STATEMENT_TYPE_ID . ",
						st." . STATEMENT_TYPE . ",
						COUNT(s.".STATEMENT_ID.") AS Status
			FROM 		" . STATEMENT_TYPE_TABLE . " st
			LEFT JOIN 	" . STATEMENT_TABLE . " s on st.".STATEMENT_TYPE_ID." = s.".STATEMENT_STATEMENT_TYPE_ID."
			GROUP BY 	st.".STATEMENT_TYPE_ID."
			ORDER BY 	st.".STATEMENT_TYPE
		);
		$result = $sql->result_array();
		return $result;
		
	}
	function delete_statement_type($id)
	{
		$this->db->delete(STATEMENT_TYPE_TABLE, array(STATEMENT_TYPE_ID => $id));
	}
	function update_statement_type($id, $type)
	{
		$this->db->where(STATEMENT_TYPE_ID, $id)->update(STATEMENT_TYPE_TABLE, array(STATEMENT_TYPE => mysql_real_escape_string(trim($type))));
	}
	function get_statement_type($id)
	{
		return $this->db->select(STATEMENT_TYPE_ID)->select(STATEMENT_TYPE)->where(STATEMENT_TYPE_ID, $id)->get(STATEMENT_TYPE_TABLE)->result_array();
	}
	function add_statement_type($type)
	{
		$this->db->insert(STATEMENT_TYPE_TABLE, array(STATEMENT_TYPE=> mysql_real_escape_string(trim($type))));
		return $this->db->insert_id();
	}
	//Statement Type end
	//Begin Tax Form
	function view_tax_form()
	{
		$sql = $this->db->query("
			SELECT 		tf." . TAX_FORM_ID . ",
						tf." . TAX_FORM . ",
						COUNT(ctfl.".CLIENT_TAX_FORM_LINK_CLIENT_ID.") AS Status
			FROM 		" . TAX_FORM_TABLE . " tf
			LEFT JOIN 	" . CLIENT_TAX_FORM_LINK_TABLE . " ctfl on tf.".TAX_FORM_ID." = ctfl.".CLIENT_TAX_FORM_LINK_TAX_FORM_ID."
			GROUP BY 	tf.".TAX_FORM_ID."
			ORDER BY 	tf.".TAX_FORM
		);
		$result = $sql->result_array();
		return $result;
	}
	function delete_tax_form($id)
	{
		$this->db->delete(TAX_FORM_TABLE, array(TAX_FORM_ID => $id));
		$this->db->delete(TAX_FORM_INITIAL_DUE_DATE_TABLE, array(TAX_FORM_ID => $id));
	}
	function edit_tax_form($id, $tax_form)
	{
		
		$query = $this->db->query("
			UPDATE 	".TAX_FORM_TABLE."
			SET 	".TAX_FORM." = '" . mysql_real_escape_string(trim($tax_form[TAX_FORM])) . "',
					".TAX_FORM_INITIAL_DUE_DATE." = '" . mysql_real_escape_string(trim($tax_form[TAX_FORM_INITIAL_DUE_DATE])) . "',
					".TAX_FORM_EXTENTION_1." = '" . mysql_real_escape_string(trim($tax_form[TAX_FORM_EXTENTION_1])) . "',
					".TAX_FORM_EXTENTION_2." = '" . mysql_real_escape_string(trim($tax_form[TAX_FORM_EXTENTION_2])) . "'
			WHERE 	".TAX_FORM_ID." = " . $id
		);
		if ($tax_form[TAX_FORM_INITIAL_DUE_DATE] == 1)
		{
			if (!$this->cheketdueday($id, TAX_FORM_INITIAL_DUE_DATE_TYPE))
			{
				$query = $this->db->query("
					UPDATE 	".TAX_FORM_INITIAL_DUE_DATE_TABLE."
					SET		".TAX_FORM_INITIAL_DUE_DATE." = '" . mysql_real_escape_string(trim($tax_form[TAX_FORM_INITIAL_DUE_DATE])) . "'
					WHERE	".TAX_FORM_ID." = " . $id . " 
					AND 	".TAX_FORM_INITIAL_DUE_DATE_TYPE." = '".TAX_FORM_INITIAL_DUE_DATE_TYPE_INITIAL_DATE."'"
				);
			}
			else
			{
				$this->Insert_Form($id, mysql_real_escape_string(trim($tax_form[TAX_FORM_INITIAL_DUE_DATE])), TAX_FORM_INITIAL_DUE_DATE_TYPE_INITIAL_DATE);
			}
		}
		else
		{
			if (!$this->cheketdueday($id, TAX_FORM_INITIAL_DUE_DATE_TYPE_INITIAL_DATE))
			{
				$this->db->delete(TAX_FORM_INITIAL_DUE_DATE_TABLE, array(TAX_FORM_ID => $id, TAX_FORM_INITIAL_DUE_DATE_TYPE => TAX_FORM_INITIAL_DUE_DATE_TYPE_INITIAL_DATE));
			}
		}
		if ($tax_form[TAX_FORM_EXTENTION_1] == 1)
		{
			if (!$this->cheketdueday($id, TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_1))
			{
				$query = $this->db->query("
					UPDATE 	".TAX_FORM_INITIAL_DUE_DATE_TABLE."
					SET		".TAX_FORM_INITIAL_DUE_DATE_DATE." = '" . mysql_real_escape_string(trim($tax_form[TAX_FORM_EXTENTION_1])) . "'
					WHERE	".TAX_FORM_ID." =" . $id . " 
					AND  	".TAX_FORM_INITIAL_DUE_DATE_TYPE." = '".TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_1."'"
				);
			}
			else
			{
				$this->Insert_Form($id, $tax_form[TAX_FORM_EXTENTION_1], TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_1);
			}
		}
		else
		{
			if (!$this->cheketdueday($id, TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_1))
			{
				$this->db->delete(TAX_FORM_INITIAL_DUE_DATE_TABLE, array(TAX_FORM_ID => $id, TAX_FORM_INITIAL_DUE_DATE_TYPE => TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_1));
			}
		}
		if ($tax_form[TAX_FORM_EXTENTION_2] == 1)
		{
			if (!$this->cheketdueday($id, TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_2))
			{
				$query = $this->db->query("
					UPDATE 	".TAX_FORM_INITIAL_DUE_DATE_TABLE."
					SET 	".TAX_FORM_INITIAL_DUE_DATE_DATE." = '" . mysql_real_escape_string(trim($tax_form[TAX_FORM_EXTENTION_2])) . "'
					WHERE	".TAX_FORM_ID." =" . $id . " 
					AND  	".TAX_FORM_INITIAL_DUE_DATE_TYPE." = '".TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_2."'"
				);
			}
			else
			{
				$this->Insert_Form($id, $tax_form[TAX_FORM_EXTENTION_2], TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_2);
			}
		}
		else
		{
			if (!$this->cheketdueday($id, TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_2))
			{
				$this->db->delete(TAX_FORM_INITIAL_DUE_DATE_TABLE, array(TAX_FORM_ID => $id, TAX_FORM_INITIAL_DUE_DATE_TYPE => TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_2));
			}
		}
	}
	function cheketdueday($id_form, $type)
	{
		$sql = $this->db->where(TAX_FORM_ID, $id_form);
		$sql = $this->db->where(TAX_FORM_INITIAL_DUE_DATE_TYPE, mysql_real_escape_string(trim($type)));
		$sql = $this->db->get(TAX_FORM_INITIAL_DUE_DATE_DATE);
		$result = $sql->row_array() ? FALSE : TRUE;
		return $result;
	}
	function get_tax_form($id)
	{
		$select = "
			SELECT 		f.".TAX_FORM.", 
						f.".TAX_FORM_ID.",  
						tfid.".TAX_FORM_INITIAL_DUE_DATE_TYPE.",  
						tfid.".TAX_FORM_INITIAL_DUE_DATE_DATE."
			FROM 		".TAX_FORM_TABLE." f
			LEFT JOIN  	".TAX_FORM_INITIAL_DUE_DATE_TABLE." tfid ON f.".TAX_FORM_ID." = tfid.".TAX_FORM_ID."
			WHERE 		f.".TAX_FORM_ID." = " . $id;
		$sql = $this->db->query($select);
		$result = $sql->result_array();
		return $result;
	}
	function add_tax_form($tax_form)
	{
		$this->db->insert(TAX_FORM_TABLE, array(
			TAX_FORM => mysql_real_escape_string(trim($tax_form[TAX_FORM])),
			TAX_FORM_INITIAL_DUE_DATE => mysql_real_escape_string(trim($tax_form[TAX_FORM_INITIAL_DUE_DATE])),
			TAX_FORM_EXTENTION_1 => mysql_real_escape_string(trim($tax_form[TAX_FORM_EXTENTION_1])),
			TAX_FORM_EXTENTION_1 => mysql_real_escape_string(trim($tax_form[TAX_FORM_EXTENTION_2]))
		));
		$id_form = $this->get_tax_form_by_name($tax_form[TAX_FORM]);
		if ($tax_form[TAX_FORM_INITIAL_DUE_DATE] != '')
		{
			$this->insert_tax_form($id_form[TAX_FORM_ID], mysql_real_escape_string(trim($tax_form[TAX_FORM_INITIAL_DUE_DATE])), TAX_FORM_INITIAL_DUE_DATE_TYPE_INITIAL_DATE);
		}
		if ($tax_form[TAX_FORM_EXTENTION_1] != '')
		{
			$this->insert_tax_form($id_form[TAX_FORM_ID], mysql_real_escape_string(trim($tax_form[TAX_FORM_EXTENTION_1])), TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_1);
		}
		if ($tax_form[TAX_FORM_EXTENTION_2] != '')
		{
			$this->insert_tax_form($id_form[TAX_FORM_ID], mysql_real_escape_string(trim($tax_form[TAX_FORM_EXTENTION_2])), TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_2);
		}
	}
	function insert_tax_form($tax_form_id, $initial_date, $type)
	{
		$this->db->insert(TAX_FORM_INITIAL_DUE_DATE_TABLE, array(
			TAX_FORM_INITIAL_DUE_DATE_ID => $tax_form_id,
			TAX_FORM_INITIAL_DUE_DATE_DATE => mysql_real_escape_string(trim($initial_date)),
			TAX_FORM_INITIAL_DUE_DATE_TYPE => mysql_real_escape_string(trim($type))
		));
	}
	function get_tax_form_by_name($name)
	{
		$sql = $this->db->where(TAX_FORM, mysql_real_escape_string(trim($name)));
		$sql = $this->db->get(TAX_FORM_TABLE);
		$result = $sql->row_array();
		return $result;
	}
	//Tax Form end
	//Tax Return Status type
	function view_tax_return_status()
	{
		$sql = $this->db->query("
			SELECT 		trs." . TAX_RETURN_STATUS_ID . ",
						trs." . TAX_RETURN_STATUS . ",
						COUNT(tr.".TAX_RETURN_ID.") AS Status
			FROM 		" . TAX_RETURN_STATUS_TABLE . " trs
			LEFT JOIN 	" . TAX_RETURN_TABLE . " tr on trs.".TAX_RETURN_STATUS_ID." = tr.".TAX_RETURN_TAX_RETURN_STATUS_ID."
			GROUP BY 	trs.".TAX_RETURN_STATUS_ID."
			ORDER BY 	trs.".TAX_RETURN_STATUS
		);
		$result = $sql->result_array();
		return $result;
	}
	function delete_tax_return_status($id)
	{
		$this->db->delete(TAX_RETURN_STATUS_TABLE, array(TAX_RETURN_STATUS_ID => $id));
	}
	function update_tax_return_status($id, $trs)
	{
		$query = $this->db->where(TAX_RETURN_STATUS_ID, $id)->update(TAX_RETURN_STATUS_TABLE, array(TAX_RETURN_STATUS	=> mysql_real_escape_string(trim($trs))));
	}
	function get_tax_return_status($id)
	{
		$sql = $this->db->select(TAX_RETURN_STATUS_ID)->select(TAX_RETURN_STATUS)->where(TAX_RETURN_STATUS_ID, $id)->get(TAX_RETURN_STATUS_TABLE);
		$result = $sql->result_array();
		return $result;
	}
	function add_tax_return_status($trs)
	{
		$this->db->insert(TAX_RETURN_STATUS_TABLE, array(TAX_RETURN_STATUS => mysql_real_escape_string(trim($trs))));
		return $this->db->insert_id();
	}
	//entity type end
	//retainer status
	function view_retainer_status()
	{
		$sql = $this->db->query("
			SELECT 		rs." . RETAINER_STATUS_ID . ",
						rs." . RETAINER_STATUS . ",
						COUNT(c.".CLIENT_ID.") AS Status
			FROM 		" . RETAINER_STATUS_TABLE . " rs
			LEFT JOIN 	" . CLIENT_TABLE . " c on rs.".RETAINER_STATUS_ID." = c.".RETAINER_STATUS_ID."
			GROUP BY 	rs.".RETAINER_STATUS_ID."
			ORDER BY 	rs.".RETAINER_STATUS
		);
		$result = $sql->result_array();
		return $result;
	}
	function delete_retainer_status($id)
	{
		$this->db->delete(RETAINER_STATUS_TABLE, array(RETAINER_STATUS_ID => $id));
	}
	function update_retainer_status($id, $retainer_status)
	{
		$this->db->where(RETAINER_STATUS_ID, $id)->update(RETAINER_STATUS_TABLE, array(RETAINER_STATUS => mysql_real_escape_string(trim($retainer_status))));
	}
	function get_retainer_status($id)
	{
		return $this->db->select(RETAINER_STATUS_ID)->select(RETAINER_STATUS)->where(RETAINER_STATUS_ID, $id)->get(RETAINER_STATUS_TABLE)->result_array();
	}
	function add_retainer_status($retainer_status)
	{
		$this->db->insert(RETAINER_STATUS_TABLE, array(RETAINER_STATUS  => mysql_real_escape_string(trim($retainer_status))));
		return $this->db->insert_id();
	}
	//retainer status end
}