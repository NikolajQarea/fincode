<?php

class Tax_return_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	// Retrieve Tax Form
	function get_tax_return($tax_return_id)
	{
		return $this->db->select()->select_max(TAX_RETURN_VIEW_LAST_EDIT_DATE)->where(TAX_RETURN_VIEW_TAX_RETURN_ID, $tax_return_id)->order_by(TAX_RETURN_VIEW_CLIENT_NAME)->get(TAX_RETURN_VIEW)->result_array();
	}
	// Retrieve History from the Tax Return History View
	function get_tax_return_status_history($tax_return_id, $as)
	{
		return $this->db->select()->where(TAX_RETURN_HISTORY_VIEW_TAX_RETURN_ID, $tax_return_id)->order_by(TAX_RETURN_HISTORY_VIEW_LAST_EDIT_DATE, $as)->get(TAX_RETURN_HISTORY_VIEW)->result_array();
	}

	/*
	// Add Tax Return
	function add($tax_return)
	{
		$user_id = $this->session->userdata('Staff_ID');
		$data = array(CLIENT_ID 			=> $tax_return[CLIENT_ID],
			'Partner_ID' 					=> $tax_return['Partner_ID'],
			TAX_FORM_ID						=> $tax_return[TAX_FORM_ID],
			'Tax_Year' 						=> $tax_return['tax_year'],
			'Tax_Month_ID' 					=> $tax_return['Tax_Month_ID'],
			'Tax_Quarter' 					=> $tax_return['Tax_Quarter'],
			TAX_RETURN_STATUS_ID 			=> $tax_return[TAX_RETURN_STATUS_ID],
			'Tax_Return_Notes' 				=> mysql_real_escape_string($tax_return['Tax_Return_Notes']),
			'Instruction_ID' 				=> $tax_return['Instructions_ID'],
			'Date_Sent' 					=> strtotime($tax_return['Date_Sent']),
			'Date_Released' 				=> strtotime($tax_return['Date_Released']),
			'Last_Edit_User' 				=> $user_id,
			'Last_Edit_Date' 				=> time(),
			'Staff_ID' 						=> $tax_return['Staff_ID'],
			'Reviewer_1_ID' 				=> $tax_return['Reviewer_1_ID'],
			'Reviewer_2_ID' 				=> $tax_return['Reviewer_2_ID']
		);
		if (isset($tax_return['date_form']))
		{
			$data['date'] = strtotime($tax_return['date_form']);
		}
		$this->db->insert('Tax_Return', $data);
		$return_id = $this->db->insert_id();
		//
		$create_date = time();
		$this->db->set('Tax_Return_ID', $return_id);
		$this->db->set('status_name', mysql_real_escape_string(trim($tax_return['status'])));
		$this->db->set('modified_by', $tax_return['user_id']);
		$this->db->set('modified_date', $create_date);
		if (isset($tax_return['date']))
		{
			$this->db->set('date', mysql_real_escape_string(trim($tax_return['date'])));
		}
		if (isset($tax_return['transmitted']))
		{
			$this->db->set('transmitted', mysql_real_escape_string(trim($tax_return['transmitted'])));
		}
		if (isset($tax_return['review_status']))
		{
			$this->db->set('review_status', mysql_real_escape_string(trim($tax_return['review_status'])));
		}
		$this->db->insert('return_status_history');
		return $return_id;
	}
	*/
	function add($return)
	{
		$this->db->insert(TAX_RETURN_TABLE, $return);
		if ($this->db->insert_id() > 0)
		{
			return true;
		}
		return false;
	}

  function edit() {
    
  }

  function select() {
    $this->db->select(array(CLIENT_ID, COMPANY_NAME, PARTNER_ID, TAX_FORM_ID));
    (isset($_GET[CLIENT_ID])) ? $this->db->where(array(CLIENT_ID => $_GET[CLIENT_ID], 'removed' => '0')) : $this->db->where(array('removed' => '0'));
    $names = $this->db->get(CLIENT_TABLE);
    $names = $names->result_array();
    foreach ($names as &$one)
      $one[TAX_FORM_ID] = explode(',', $one[TAX_FORM_ID]);
    $names = json_encode($names);
    return $names;
  }

  function get_all() {
    $query = $this->db->query("SELECT * FROM tax_return WHERE removed = '0'");
    $res = $query->num_rows();
    return $res;
  }


	function delete_tax_return($id)
	{
		$this->db->delete(TAX_RETURN_HISTORY_TABLE, array(TAX_RETURN_HISTORY_TAX_RETURN_ID => $id));
		$this->db->delete(TAX_RETURN_TABLE, array(TAX_RETURN_ID => $id));
	}
  /*
	// Get Tax Return
	function get_tax_return($id)
	{
		return $this->db->select()->select_max(TAX_RETURN_HISTORY_VIEW_LAST_EDIT_DATE)->where(TAX_RETURN_HISTORY_VIEW_TAX_RETURN_ID, $id)->get(TAX_RETURN_HISTORY_VIEW)->row_array();
	}
*/
	// Update Tax Return
	// required the Tax_Return_ID, an arrray for Tax Return update, and array for new Status history insert.
	function update_return($id, $return, $history)
	{
		$this->db->where(TAX_RETURN_ID, $id)->update(TAX_RETURN_TABLE, $return);
		$this->db->insert(TAX_RETURN_HISTORY_TABLE, $history);
	}
	// Tax Return View Page
	function view($tax_return, $start, $count, $order, $as, $date_sent = '', $date_released = '', 
        $exclude_signature_pages = '', $exclude_prepared_by_client = '', $extension_filed = '')
	{
		//$this->db->select('COUNT('.TAX_RETURN_VIEW_TAX_RETURN_ID.') AS Total_Rows');
		$select = 'SELECT COUNT(' . TAX_RETURN_VIEW_TAX_RETURN_ID . ') AS Total_Rows FROM ' . TAX_RETURN_VIEW . ' ';
		$where = ' WHERE ' . TAX_RETURN_VIEW_TAX_RETURN_ID . ' IS NOT NULL ';
		// Pull From Tax_Return_info view
		//$this->db->where(TAX_RETURN_VIEW_TAX_RETURN_ID . ' IS NOT NULL');
		// Check Partner ID Filter
		if ($tax_return[TAX_RETURN_VIEW_PARTNER_ID] != ANY_VALUE)
		{
			//$this->db->where(TAX_RETURN_VIEW_PARTNER_ID, $tax_return[TAX_RETURN_VIEW_PARTNER_ID]);
			$where .= ' AND ' . TAX_RETURN_VIEW_PARTNER_ID . ' = ' . $tax_return[TAX_RETURN_VIEW_PARTNER_ID];
		}
		// Check Date Sent Filter
		if (trim($tax_return[TAX_RETURN_VIEW_DATE_SENT]) != '' && trim($tax_return[TAX_RETURN_VIEW_DATE_SENT]) != ANY_LABEL)
		{
			//$this->db->where(TAX_RETURN_VIEW_DATE_SENT, $tax_return[TAX_RETURN_VIEW_DATE_SENT]);
			$where .= ' AND ' . TAX_RETURN_VIEW_DATE_SENT . ' = ' . $tax_return[TAX_RETURN_VIEW_DATE_SENT];
		}
		// Check Tax Year Filter
		if ($tax_return[TAX_RETURN_VIEW_TAX_YEAR] != ANY_VALUE)
		{
			//$this->db->where(TAX_RETURN_VIEW_TAX_YEAR, $tax_return[TAX_RETURN_VIEW_TAX_YEAR]);
			$where .= ' AND ' . TAX_RETURN_VIEW_TAX_YEAR . ' = ' . $tax_return[TAX_RETURN_VIEW_TAX_YEAR];
		}
		// Check Tax Month (ID) Filter
		if ($tax_return[TAX_RETURN_VIEW_TAX_MONTH_ID] != ANY_VALUE)
		{
			//$this->db->where(TAX_RETURN_VIEW_TAX_MONTH_ID, $tax_return[TAX_RETURN_VIEW_TAX_MONTH_ID]);
			$where .= ' AND ' . TAX_RETURN_VIEW_TAX_MONTH_ID . ' = ' . $tax_return[TAX_RETURN_VIEW_TAX_MONTH_ID];
		}
		// Check Date Released Filter
		if (trim($tax_return[TAX_RETURN_VIEW_DATE_RELEASED]) != '' && trim($tax_return[TAX_RETURN_VIEW_DATE_RELEASED]) != ANY_LABEL)
		{
			//$this->db->where(TAX_RETURN_VIEW_DATE_RELEASED, $tax_return[TAX_RETURN_VIEW_DATE_RELEASED]);
			$where .= ' AND ' . TAX_RETURN_VIEW_DATE_RELEASED . ' = ' . $tax_return[TAX_RETURN_VIEW_DATE_RELEASED];
		}
		// Check Tax Return Notes Filter
		if (trim($tax_return[TAX_RETURN_VIEW_TAX_RETURN_NOTES]) != '' && trim($tax_return[TAX_RETURN_VIEW_TAX_RETURN_NOTES]) != ANY_LABEL)
		{
			//$this->db->like(TAX_RETURN_VIEW_TAX_RETURN_NOTES, $tax_return[TAX_RETURN_VIEW_TAX_RETURN_NOTES]);
			$where .= ' AND ' . TAX_RETURN_VIEW_TAX_RETURN_NOTES . ' LIKE \'%' . $tax_return[TAX_RETURN_VIEW_TAX_RETURN_NOTES] . '%\'';
		}
		// Check Tax Quarter Filter
		if ($tax_return[TAX_RETURN_VIEW_TAX_QUARTER] != ANY_VALUE && $tax_return[TAX_RETURN_VIEW_TAX_QUARTER] != '')
		{
			if($tax_return[TAX_RETURN_VIEW_TAX_QUARTER] == NOT_APPLICABLE_ID)
			{
				//$this->db->where(TAX_RETURN_VIEW_TAX_QUARTER . ' IS NULL ');
				$where .= ' AND ' . TAX_RETURN_VIEW_TAX_QUARTER . ' IS NULL ';
			}
			else
			{
				//$this->db->where(TAX_RETURN_VIEW_TAX_QUARTER, $tax_return[TAX_RETURN_VIEW_TAX_QUARTER]);
				$where .= ' AND ' . TAX_RETURN_VIEW_TAX_QUARTER . ' = ' . $tax_return[TAX_RETURN_VIEW_TAX_QUARTER];
			}
		}
		// Check Staff ID Filter
		if ($tax_return[TAX_RETURN_VIEW_STAFF_ID] != ANY_VALUE)
		{
			//$this->db->where(TAX_RETURN_VIEW_STAFF_ID, $tax_return[TAX_RETURN_VIEW_STAFF_ID]);
			$where .= ' AND ' . TAX_RETURN_VIEW_STAFF_ID . ' = ' . $tax_return[TAX_RETURN_VIEW_STAFF_ID];
		}
		// Check Reviewer 1 ID Filter
		if ($tax_return[TAX_RETURN_VIEW_REVIEWER_1_ID] != ANY_VALUE)
		{
			//$this->db->where(TAX_RETURN_VIEW_REVIEWER_1_ID, $tax_return[TAX_RETURN_VIEW_REVIEWER_1_ID]);
			$where .= ' AND ' . TAX_RETURN_VIEW_REVIEWER_1_ID . ' = ' . $tax_return[TAX_RETURN_VIEW_REVIEWER_1_ID];
		}
		// Check Reviewer 2 ID Filter
		if ($tax_return[TAX_RETURN_VIEW_REVIEWER_2_ID] != ANY_VALUE)
		{
			//$this->db->where(TAX_RETURN_VIEW_REVIEWER_1_ID, $tax_return[TAX_RETURN_VIEW_REVIEWER_1_ID]);
			$where .= ' AND ' . TAX_RETURN_VIEW_REVIEWER_2_ID . ' = ' . $tax_return[TAX_RETURN_VIEW_REVIEWER_2_ID];
		}
		// Check Tax Return Status ID Filter
		if ($tax_return[TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID] != ANY_VALUE)
		{
			//$this->db->where(TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID, $tax_return[TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID]);
			$where .= ' AND ' . TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID . ' = ' . $tax_return[TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID];
		}
		// Check Tax Form ID Filter
		if ($tax_return[TAX_RETURN_VIEW_TAX_FORM_ID] != ANY_VALUE)
		{
			//$this->db->where(TAX_RETURN_VIEW_TAX_FORM_ID, $tax_return[TAX_RETURN_VIEW_TAX_FORM_ID]);
			$where .= ' AND ' . TAX_RETURN_VIEW_TAX_FORM_ID . ' = ' . $tax_return[TAX_RETURN_VIEW_TAX_FORM_ID];
		}
		// Check if exclude sent to client checkbox was checked
		if ($date_sent == 1)
		{
			$where .= ' AND ' . TAX_RETURN_VIEW_DATE_SENT . ' IS NULL';
		}
        // Check if exclude released checkbox was checked
        if ($date_released == 1)
        {
            $where .= ' AND ' . TAX_RETURN_VIEW_DATE_RELEASED . ' IS NULL';
        }
		// Check if checkbox for Signature Pages was checked
		if ($exclude_signature_pages == 1)
		{
			//$this->db->where_not_in(TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID, 21);
			$where .= ' AND ' . TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID . ' NOT IN (21)';
		}
		// Check if checkbox for Extensions Filed was checked
		if ($extension_filed == 1)
		{
			$where .= ' AND ' . TAX_RETURN_VIEW_TAX_RETURN_STATUS . ' LIKE \'%Extension%\'';
		}
		// Check if checkbox for Prepared by Client was checked
		if ($exclude_prepared_by_client == 1)
		{
			//$this->db->where_not_in(TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID, 34);
			$where .= ' AND ' . TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID . ' NOT IN (34)';
		}
		// Check Client Name filter
		if (trim($tax_return[TAX_RETURN_VIEW_CLIENT_NAME]) != '' && trim($tax_return[TAX_RETURN_VIEW_CLIENT_NAME]) != ANY_VALUE && trim($tax_return[TAX_RETURN_VIEW_CLIENT_NAME]) != ANY_LABEL)
		{
			//$this->db->like(TAX_RETURN_VIEW_CLIENT_NAME, $this->utilities->clean_mysql_like_statement($tax_return[TAX_RETURN_VIEW_CLIENT_NAME]));
			$where .= ' AND ' . TAX_RETURN_VIEW_CLIENT_NAME . ' LIKE \'%' . $this->utilities->clean_mysql_like_statement($tax_return[TAX_RETURN_VIEW_CLIENT_NAME]) . '%\'';
		}		
		$query = $this->db->query($select . $where);
		
		if ($query->num_rows() > 0)
		{
			$res = $query->result_array();
			$totalrows = $res[0]['Total_Rows'];
			if ($totalrows > 0)
			{
				$select = 'SELECT *, CONCAT('.$totalrows.') AS Total_Rows FROM ' . TAX_RETURN_VIEW . ' ';
				//$lastSQL = str_replace('`', '', str_replace('SELECT COUNT('.TAX_RETURN_VIEW_TAX_RETURN_ID.') AS Total_Rows', ' *, CONCAT('.$totalrows.') AS Total_Rows ', $this->db->last_query()));
				$query = $this->db->query($select . $where . ' ORDER BY ' . $order . ' ' . $as . ' LIMIT ' . $start . ', ' . $count);
				if ($query->num_rows() > 0)
				{
					$newdata = $query->result_array();
				}
				else
				{
					$newdata = NO_RECORDS;
				}
			}
			else
			{
				$newdata = NO_RECORDS;
			}
		}
		else
		{
			$newdata = NO_RECORDS;
		}
		return $newdata;
	}

	// Tax Return View Multiple Page
	function view_multiple($tax_return, $start, $count, $order, $as, $ids = NULL)
	{
		$select = 'SELECT COUNT(' . TAX_RETURN_VIEW_TAX_RETURN_ID . ') AS Total_Rows FROM ' . TAX_RETURN_VIEW . ' ';
		$where = ' WHERE ' . TAX_RETURN_VIEW_TAX_RETURN_ID . ' IS NOT NULL ';
		// Pull From Tax_Return_info view
		//$this->db->where(TAX_RETURN_VIEW_TAX_RETURN_ID . ' IS NOT NULL');
                //
		// Check Partner ID Filter
		if ($tax_return[TAX_RETURN_VIEW_PARTNER_ID] != ANY_VALUE)
		{
			//$this->db->where(TAX_RETURN_VIEW_PARTNER_ID, $tax_return[TAX_RETURN_VIEW_PARTNER_ID]);
			$where .= ' AND ' . TAX_RETURN_VIEW_PARTNER_ID . ' = ' . $tax_return[TAX_RETURN_VIEW_PARTNER_ID];
		}		
		// Check Tax Year Filter
		if ($tax_return[TAX_RETURN_VIEW_TAX_YEAR] != ANY_VALUE)
		{
			//$this->db->where(TAX_RETURN_VIEW_TAX_YEAR, $tax_return[TAX_RETURN_VIEW_TAX_YEAR]);
			$where .= ' AND (' . TAX_RETURN_VIEW_TAX_YEAR . ' = ' . $tax_return[TAX_RETURN_VIEW_TAX_YEAR] .' OR ' . TAX_RETURN_VIEW_TAX_YEAR . ' = ' . ($tax_return[TAX_RETURN_VIEW_TAX_YEAR]-1) . ')';
		}		
		// Check Tax Form ID Filter
		if ($tax_return[TAX_RETURN_VIEW_TAX_FORM_ID] != ANY_VALUE)
		{
			//$this->db->where(TAX_RETURN_VIEW_TAX_FORM_ID, $tax_return[TAX_RETURN_VIEW_TAX_FORM_ID]);
			$where .= ' AND ' . TAX_RETURN_VIEW_TAX_FORM_ID . ' = ' . $tax_return[TAX_RETURN_VIEW_TAX_FORM_ID];
		}
                
                if ($ids) {
                    $where .= ' AND ' . TAX_RETURN_VIEW_TAX_RETURN_ID . ' IN (' . $ids . ')';
                }
        		
		$query = $this->db->query($select . $where);		
		if ($query->num_rows() > 0)
		{
			$res = $query->result_array();
			$totalrows = $res[0]['Total_Rows'];
			if ($totalrows > 0)
			{
				$select = 'SELECT *, CONCAT('.$totalrows.') AS Total_Rows FROM ' . TAX_RETURN_VIEW . ' ';
				$query = $this->db->query($select . $where . ' ORDER BY ' . $order . ' ' . $as . ' LIMIT ' . $start . ', ' . $count);
				if ($query->num_rows() > 0)
				{
					$newdata = $query->result_array();
				}
				else
				{
					$newdata = NO_RECORDS;
				}
			}
			else
			{
				$newdata = NO_RECORDS;
			}
		}
		else
		{
			$newdata = NO_RECORDS;
		}
		return $newdata;
	}
/*
  function get_input($status_id) {
    $this->db->where('status_id', $status_id);
    $query = $this->db->get('status');
    if ($query->num_rows() > 0)
      return $query->result_array();

    return NULL;
  }
*/
/*
  function get_form_id_for_client($client_id) {
    $this->db->where(CLIENT_ID, $client_id);
    $query = $this->db->get('client');
    $result = $query->row_array();
    //var_dump($result);
    return $result['form_id'];
  }


*/
/*
	// Deprecated
	// Get Tax Forms By ID
	function get_tax_forms_by_id($form_ids)
	{
    if (count($form_ids)) {
      $select = "SELECT * FROM form ";
      $where = "WHERE form_id = '" . $form_ids[0] . "'";
      if (count($form_ids) > 1) {
        for ($i = 1; $i < count($form_ids); $i++) {
          $where .= "OR form_id = '" . $form_ids[$i] . "'";
        }
      }
      $order = " ORDER BY form_name ASC";
      $query = $select . $where .$order;
      $sql = $this->db->query($query);
      $result = $sql->result_array();
      return $result;
    }
  }
*/
/*
  function get_form_name_by_id($form_id) {
    if ($form_id) {
      if (is_array($form_id) && !empty($form_id)) {
        $select = "SELECT form_name FROM form ";
        $where = "WHERE form_id = '" . $form_id[0] . "'";
        if (count($form_id) > 1) {
          for ($i = 1; $i < count($form_id); $i++) {
            $where .= "OR form_id = '" . $form_id[$i] . "'";
          }
        }
        $order = " ORDER BY form_name ASC";
        $query = $select . $where . $order;
        $sql = $this->db->query($query);
        $result = $sql->row_array();
      } else {
        $select = "SELECT form_name FROM form ";
        $where = "WHERE form_id = '" . $form_id . "'";
        $order = " ORDER BY form_name ASC";
        $query = $select . $where . $order;
        $sql = $this->db->query($query);
        $result = $sql->row_array();
      }

      return $result;
    }
  }
  */
  /*
	// Retrieve a list of existing tax years
	function get_tax_years()
	{
		$sql = $this->db->query("SELECT DISTINCT Tax_Year FROM Tax_Return ORDER BY Tax_Year ASC");
		$result = $sql->result_array();
		return $result;
	}
	*/
	/*
  function get_tax_return_data($tax_return_id) {
    $this->db->where('id_tax_return', $tax_return_id);
    $sql = $this->db->get('tax_return');
    $result = $sql->row_array();
    return $result['date'];
  }
  */
  	// Get Client ID From Client Name
	function get_client_id_from_name($name)
	{
		$query = $this->db->select(CLIENT_VIEW_CLIENT_ID)->where(CLIENT_VIEW_CLIENT_NAME, stripslashes(trim($name)))->get(CLIENT_VIEW)->result_array();
		return $query[0][CLIENT_VIEW_CLIENT_ID];
	}

	function get_client_name($id)
	{
		$res = $this->db->select(CLIENT_VIEW_CLIENT_NAME)->where(CLIENT_VIEW_CLIENT_ID, $id)->get(CLIENT_VIEW)->row_array();
		return $res['Client_Name'];
	}
/*
  function get_Company_Name($id) {
    $sql = $this->db->query("SELECT ".COMPANY_NAME." FROM ".CLIENT_TABLE." WHERE " . CLIENT_ID . " = '" . $id . "'");
    $result = $sql->row_array();
    $res = $result[COMPANY_NAME];
    // var_dump($res);
    return $res;
  }
*/
  function lock($id) {
    $query = $this->db->query("UPDATE tax_return tr
									  SET tr.lock = '1'
									WHERE tr.id_tax_return = '" . $id . "'");
  }

  function unlock($id) {
    $query = $this->db->query("UPDATE tax_return tr 
									  SET tr.lock = '0'
									WHERE tr.id_tax_return = '" . $id . "'");
  }

	// Get Lock
	function select_lock($id)
	{
		$sql = $this->db->select(TAX_RETURN_HISTORY_VIEW_IS_LOCKED)->where(TAX_RETURN_HISTORY_VIEW_IS_LOCKED, 1)->where(TAX_RETURN_HISTORY_VIEW_TAX_RETURN_ID, $id)->get(TAX_RETURN_HISTORY_VIEW);
		$result = $sql->row_array();
		return ($result) ? TRUE : FALSE;
	}
/*
  function get_client_partner($client_id) {
    $this->db->where(CLIENT_ID, $client_id);
    $query = $this->db->get('client');
    $result = $query->row_array();
    return $result['partner'];
  }
  */
  /*
	// Get Instructions
	function get_instructions()
	{
		$sql = $this->db->order_by('Instructions_Label', 'ASC');
		$query = $this->db->get('Instructions');
		$result = $query->result_array();
		return $result;
	}
*/
/*
  function check_instructions_name($name) {
    $sql = $this->db->where('name_instructions', mysql_real_escape_string(trim($name)));
    $sql = $this->db->get('instructions');
    $result = $sql->row_array() ? FALSE : TRUE;
    //var_dump($result);
    return $result;
  }
  */
  /*
	// Get Client_ID from Client_Name
	function get_client_id($name)
	{
		$id = 0;
		try
		{	
			$query = $this->db->select(CLIENT_ID)->where(CLIENT_NAME, stripslashes(trim($name)))->get(CLIENT_VIEW);
			if($query->result_array())
			{
				$res = $query->result_array();
				$id = $res[0][CLIENT_ID];
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
		return $id;
	}
*/
    // Check Instructions to see if it already exists
    function check_instructions_exists($name, $id = 0)
    {
        $sql = $this->db->select(INSTRUCTIONS)->where(INSTRUCTIONS, mysql_real_escape_string(trim($name)))->where_not_in(INSTRUCTIONS_ID, $id)->get(INSTRUCTIONS_TABLE);
        $result = $sql->num_rows() > 0 ? true : false;
        return $result;
    }
    // Check if an Instructions is in use (before delete)
    function check_instructions_in_use($id)
    {
        $sql = $this->db->select(TAX_RETURN_ID)->where(INSTRUCTIONS_ID, $id)->get(TAX_RETURN_TABLE);
        $result = $sql->num_rows() > 0 ? true : false;
        return $result;
    }

	// Add Instructions
	function add_instructions($name)
	{
		$data = array('Instructions_Label' => mysql_real_escape_string(trim($name)));
		$this->db->insert(INSTRUCTIONS_TABLE, $data);
		return $this->db->insert_id();
	}
/*
  function get_all_Client_Name() {
    $query_client = $this->db->query("SELECT IF(".COMPANY_NAME." = '', CONCAT(".CLIENT_NAME_LAST.", ' ', ".CLIENT_NAME_FIRST."), ".COMPANY_NAME.") AS ".CLIENT_NAME."
                                                    FROM ".CLIENT_TABLE."
                                                GROUP BY ".COMPANY_NAME." ASC");

    $res = $query_client->result_array();
    return $res;
  }

*/
/*
  function AutomaticAdd($entity, $state, $form_id, $tax_year) {
    $query_client = $this->db->query("SELECT IF(".COMPANY_NAME." = '', CONCAT(".CLIENT_NAME_LAST.", ' ', ".CLIENT_NAME_FIRST."), 
                                      ".COMPANY_NAME.") AS ".CLIENT_NAME.", state, entity, partner, " . CLIENT_ID . "
                                                    FROM ".CLIENT_TABLE."
                                                    WHERE removed = '0' AND entity=" . $entity);

    $res = $query_client->result_array();
    $client = "";
$session_id = $this->session->userdata('Staff_ID');
    for ($i = 0; $i < count($res); $i++) {
      $state = explode(',', $res[$i]['state']);
      if (isset($res[$i]['state'])) {
        $state = explode(',', $res[$i]['state']);
      }
      $j = 0;
      $states = '';
      while ($j < count($state)) {
        if ($state[$j] == $state) {
          $client[] = $res[$i];
        }
        $j++;
      }
      $list_client = array(CLIENT_ID 	=> $res[$i][CLIENT_ID],
        'Partner_ID' 					=> $res[$i]['partner'],
        TAX_FORM_ID 					=> $form_id,
        'Tax_Year' 						=> $tax_year,
        'Tax_Month_ID' 					=> "",
        'Tax_Quarter' 					=> "",
        TAX_RETURN_STATUS_ID		 	=> "",
        'Tax_Return_Notes' 				=> "",
        'Instructions_ID' 				=> 19,
        'Date_Sent' 					=> "",
        'Date_Released' 				=> "",
        'Last_Edit_User'				=> $session_id,
        'Last_Edit_Date' 				=> time(),
        'Staff_ID' 						=> "",
        'Reviewer_1_ID' 				=> "",
        'Reviewer_2_ID' 				=> ""
    );
    
    $this->add($list_client);
    }
  }
  */
  /*
  function get_entity_type_name() {

    $sql = $this->db->order_by('name_entity_type', 'asc');
    $sql = $this->db->get('entity_type');
    $result = $sql->result_array();
    //var_dump($result);
    return $result;
  }
*/
}

 ?>
