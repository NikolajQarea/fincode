<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Form_model extends CI_Model{
    function __construct(){
         parent::__construct();
    }
    
    function view(){
          
        $query = $this->db->get('form');
        $result = $query->result_array();
        foreach($result as &$res)
        {
            $res['dates'] = $this->get_form_dates($res['form_id']);
        }
        return $result;
        
    }
    
    private function get_form_dates($form_id)
    {
        $this->db->select('initial_date');
        $this->db->where('form_id', mysql_real_escape_string(trim($form_id)));
        $query = $this->db->get('form_initial_date');
        $temp = $query->result_array();
        $result = array();
        foreach($temp as $t)
        {
            $result[] = $t['initial_date'];
        }
        
        return $result;
        
    }
}
 ?>
