<?php

class Report_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	function get_tax_form_details($tax_form_id)
	{
		return $this->db->select()->where(TAX_FORM_ID, $tax_form_id)->get(TAX_FORM_TABLE)->result_array();
	}
	function get_form_dates($tax_form_id, $types)
	{
		return $this->db
			->select(TAX_FORM_INITIAL_DUE_DATE_DATE)
			->where(TAX_FORM_INITIAL_DUE_DATE_ID, $tax_form_id)
			//->where_in(TAX_FORM_INITIAL_DUE_DATE_TYPE, $types)
			->get(TAX_FORM_INITIAL_DUE_DATE_TABLE)
			->result_array();
	}
	function get_report($report)
	{
		$this->db->select();
		$this->db->where(TAX_RETURN_VIEW_TAX_RETURN_ID . ' IS NOT NULL');

		try
		{
			if ($report[CLIENT_VIEW_CLIENT_NAME] != '' && $report[CLIENT_VIEW_CLIENT_NAME] > 0)
			{
				$this->db->where(TAX_RETURN_VIEW_CLIENT_ID, $report[CLIENT_VIEW_CLIENT_NAME]);
			}
			if ($report[CLIENT_VIEW_PARTNER_ID] != '' && $report[CLIENT_VIEW_PARTNER_ID] > 0)
			{
				$this->db->where(CLIENT_VIEW_PARTNER_ID, $report[CLIENT_VIEW_PARTNER_ID]);
			}
			if ($report[TAX_YEAR_ID] != '')
			{
				$this->db->where(TAX_RETURN_VIEW_TAX_YEAR, $report[TAX_YEAR_ID]);
			}
			if ($report[TAX_QUARTER_ID] != '')
			{
				$this->db->where(TAX_RETURN_VIEW_TAX_QUARTER_ID, $report[TAX_QUARTER_ID]);
			}
			if ($report[TAX_MONTH_ID] != '')
			{
				$this->db->where(TAX_RETURN_VIEW_TAX_MONTH_ID, $report[TAX_MONTH_ID]);
			}
			if ($report[CLIENT_VIEW_STAFF_ID] != '' && $report[CLIENT_VIEW_STAFF_ID] > 0)
			{
				$this->db->where(CLIENT_VIEW_STAFF_ID, $report[CLIENT_VIEW_STAFF_ID]);
			}
			if ($report[TAX_RETURN_STATUS_ID] != '' & $report[TAX_RETURN_STATUS_ID] > 0)
			{
				$this->db->where(TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID, $report[TAX_RETURN_STATUS_ID]);
			}
			if (isset($report['Exclude_Sent']) && $report['Exclude_Sent'] == 1)
			{
				$this->db->where_not_in(TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID, 16);
			}
			if ($report[CLIENT_STATUS_ID] != '' && $report[CLIENT_STATUS_ID] > 0)
			{
				$this->db->where(TAX_RETURN_VIEW_CLIENT_STATUS_ID, $report[CLIENT_STATUS_ID]);
			}
			if($report[TAX_FORM_ID] != '' && $report[TAX_FORM_ID] > 0)
			{
				$this->db->where(TAX_FORM_ID, $report[TAX_FORM_ID]);
			}
			$this->db->order_by($report['order'], $report['as']);
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
		$return = $this->db->get(TAX_RETURN_VIEW)->result_array();
		//log_message('error', $this->db->last_query());
		return $return;
	}
	function get_single_date($tax_form_id)
	{
		$this->db->where(TAX_FORM_ID, $tax_form_id);
		$query = $this->db->get('Tax_Return');
		$result = $query->result_array();
		return $result;
	}
	function get_tax_return_statuses()
	{
		$sql = $this->db->query("SELECT * FROM Tax_Return_Status ORDER BY Tax_Return_Status_Label ASC");
		$result = $sql->result_array();
		return $result;
	}
	function get_tax_years()
	{
		$sql = $this->db->query("SELECT DISTINCT Tax_Year FROM Tax_Return_Info ORDER BY Tax_Year ASC");
		$result = $sql->result_array();
		return $result;
	}
	/*
	function get_form_name_by_id($form_id)
	{
		if ($form_id)
		{
			if (is_array($form_id) && !empty($form_id))
			{
				$select = "SELECT `form_name` FROM `form` ";
				$where = "WHERE `form_id` = '" . $form_id[0] . "'";
				if (count($form_id) > 1)
				{
					for ($i = 1; $i < count($form_id); $i++)
					{
						$where .= "OR `form_id` = '" . $form_id[$i] . "'";
					}
				}
				$query = $select . $where;
				$sql = $this->db->query($query);
				$result = $sql->result_array();
			}
			else
			{
				$select = "SELECT `form_name` FROM `form` ";
				$where = "WHERE `form_id` = '" . $form_id . "'";
				$query = $select . $where;
				$sql = $this->db->query($query);
				$result = $sql->result_array();
			}
			return $result;
		}
	}
	*/
}
 ?>
