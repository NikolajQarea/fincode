<?php
class Institution_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('Utilities');
	}	
	// Private User Object
	private $institutionData = array
	(
			INSTITUTION_VIEW_ID							=> '',
			INSTITUTION_VIEW_NAME						=> '',
			INSTITUTION_VIEW_ADDRESS_1					=> '',
			INSTITUTION_VIEW_ADDRESS_2					=> '',
			INSTITUTION_VIEW_CITY						=> '',
			INSTITUTION_VIEW_STATE_ID					=> PLEASE_SELECT_ID,
			INSTITUTION_VIEW_POSTAL_CODE				=> '',
			INSTITUTION_VIEW_CONTACT					=> '',
			INSTITUTION_VIEW_TELEPHONE					=> '',
			INSTITUTION_VIEW_ABA						=> '',
			INSTITUTION_VIEW_STAMP_USER_ID				=> '',
			INSTITUTION_VIEW_STAMP_USER_INITIALS		=> '',
			INSTITUTION_VIEW_STAMP_USER_LOGIN_ID		=> '',
			INSTITUTION_VIEW_STAMP_DATE					=> '',
			INSTITUTION_VIEW_LAST_EDIT_USER_ID			=> '',
			INSTITUTION_VIEW_LAST_EDIT_USER_INITIALS	=> '',
			INSTITUTION_VIEW_LASST_EDIT_USER_LOGIN_ID	=> '',
			INSTITUTION_VIEW_LAST_EDIT_DATE				=> ''
	);
	// Private Institution_ID
	private $institutionID;
	// INSERT Instution
	function insert($institution, $institution_id = 0, $isEdit = false)
	{
            try
		{
			// If passes validation, commit to database
			if (
				$institution[INSTITUTION_VIEW_NAME]
			)
			{ 
				$newInstitution = array
				(
					INSTITUTION_NAME 					=> mysql_real_escape_string(trim($institution[INSTITUTION_NAME])),
					INSTITUTION_ADDRESS_1 				=> mysql_real_escape_string(trim($institution[INSTITUTION_ADDRESS_1])),
					INSTITUTION_ADDRESS_2 				=> mysql_real_escape_string(trim($institution[INSTITUTION_ADDRESS_2])),
					INSTITUTION_CITY 					=> mysql_real_escape_string(trim($institution[INSTITUTION_CITY])),
					INSTITUTION_STATE_ID				=> mysql_real_escape_string(trim($institution[INSTITUTION_STATE_ID])),
					INSTITUTION_POSTAL_CODE				=> mysql_real_escape_string(trim($institution[INSTITUTION_POSTAL_CODE])),
					INSTITUTION_CONTACT					=> mysql_real_escape_string(trim($institution[INSTITUTION_CONTACT])),
					INSTITUTION_TELEPHONE				=> mysql_real_escape_string(trim($institution[INSTITUTION_TELEPHONE])),
					INSTITUTION_ABA						=> mysql_real_escape_string(trim($institution[INSTITUTION_ABA])),
					INSTITUTION_LAST_EDIT_DATE			=> time(),
					INSTITUTION_LAST_EDIT_USER_ID		=> $this->session->userdata(STAFF_ID)
				);
				if(!$isEdit)
				{
					$newInstitution[INSTITUTION_STAMP_DATE]	= time();
					$newInstitution[INSTITUTION_STAMP_USER_ID]	= $this->session->userdata(STAFF_ID);
					$this->db->insert(INSTITUTION_TABLE, $newInstitution);
					// Get New Staff_ID
					$institution_id = $this->db->insert_id();
				}
				else
				{
					$this->db->where(INSTITUTION_ID, $institution_id);
					$this->db->update(INSTITUTION_TABLE, $newInstitution);
				}
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
	}
	function view($start, $count, $order, $as)
	{
		return $this->utilities->get_institutions($order, $as, $start, $count);
	}
	function get_institution($institution_id)
	{
		try
		{
			$query = $this->db->select()->where(INSTITUTION_VIEW_ID, $institution_id)->get(INSTITUTION_VIEW);
			if ($query->num_rows() > 0)
			{
				$Institution = $query->row_array();
				$this->institutionData[INSTITUTION_ID] 								= $Institution[INSTITUTION_VIEW_ID];
				$this->institutionData[INSTITUTION_NAME] 							= $Institution[INSTITUTION_VIEW_NAME];
				$this->institutionData[INSTITUTION_ADDRESS_1] 						= $Institution[INSTITUTION_VIEW_ADDRESS_1];
				$this->institutionData[INSTITUTION_ADDRESS_2] 						= $Institution[INSTITUTION_VIEW_ADDRESS_2];
				$this->institutionData[INSTITUTION_CITY] 							= $Institution[INSTITUTION_VIEW_CITY];
				$this->institutionData[INSTITUTION_STATE_ID]						= $Institution[INSTITUTION_VIEW_STATE_ID];
				$this->institutionData[INSTITUTION_POSTAL_CODE] 					= $Institution[INSTITUTION_VIEW_POSTAL_CODE];
				$this->institutionData[INSTITUTION_CONTACT]							= $Institution[INSTITUTION_VIEW_CONTACT];
				$this->institutionData[INSTITUTION_TELEPHONE]						= $Institution[INSTITUTION_VIEW_TELEPHONE];
				$this->institutionData[INSTITUTION_ABA]								= $Institution[INSTITUTION_VIEW_ABA];
				$this->institutionData[INSTITUTION_VIEW_STAMP_DATE]					= $Institution[INSTITUTION_VIEW_STAMP_DATE];
				$this->institutionData[INSTITUTION_VIEW_STAMP_USER_LOGIN_ID]		= $Institution[INSTITUTION_VIEW_STAMP_USER_LOGIN_ID];
				$this->institutionData[INSTITUTION_VIEW_LAST_EDIT_DATE]				= $Institution[INSTITUTION_VIEW_LAST_EDIT_DATE];
				$this->institutionData[INSTITUTION_VIEW_LASST_EDIT_USER_LOGIN_ID]	= $Institution[INSTITUTION_VIEW_LASST_EDIT_USER_LOGIN_ID];
				$this->institutionData[INSTITUTION_TOTAL_TIMES_USED]				= $Institution[INSTITUTION_TOTAL_TIMES_USED];
			}
			return $this->institutionData;
		}
		catch(Exception $ex)
		{
			log_message('error',  $e->getMessage());
			show_error( $e->getMessage());
		}
	}
	// Get Count All Users	
	function get_all()
	{
        return $this->db->count_all(INSTITUTION_VIEW);
    }
    // Check if Institution is in User
    function check_institution_in_use($id)
    {
    	return $this->db->select(STATEMENT_INSTITUTION_ID)->where(STATEMENT_INSTITUTION_ID, $id)->count_all_results(STATEMENT_TABLE);
    }
    // Delete an Institution
    function delete_institution($id)
    {
    	if($this->check_institution_in_use($id) == 0)
    	{
    		$this->db->delete(INSTITUTION_TABLE, array(INSTITUTION_ID => $id));
    	}
    }
}
 ?>