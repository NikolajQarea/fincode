<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// STAFF TABLE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('STAFF_TABLE', 									'Staff');
define('STAFF_EMAIL', 									'Email');
define('STAFF_ID', 										'Staff_ID');
define('STAFF_INITIALS', 								'Initials');
define('STAFF_LAST_EDIT_DATE',							'Last_Edit_Date');
define('STAFF_LAST_EDIT_USER_ID',						'Last_Edit_User');
define('STAFF_LAST_EDIT_USER_INITIALS',					'Last_Edit_User_Initials');
define('STAFF_LAST_EDIT_USER_NAME_FIRST',				'Last_Edit_User_Name_First');
define('STAFF_LAST_EDIT_USER_NAME_FULL',				'Last_Edit_User_Name_Full');
define('STAFF_LAST_EDIT_USER_NAME_LAST',				'Last_Edit_User_Name_Last');
define('STAFF_LOGIN', 									'Login_ID');
define('STAFF_NAME_FIRST', 								'Staff_Name_First');
define('STAFF_NAME_FULL', 								'Staff_Name_Full');
define('STAFF_NAME_LAST', 								'Staff_Name_Last');
define('STAFF_PASSWORD',								'Password');
define('STAFF_PASSWORD_CONFIRM',						'Password_Confirm');
define('STAFF_PASSWORD_NEW',							'Password_New');
define('STAFF_ROLE_ID',									'Role_ID');
define('STAFF_STAMP_DATE',								'Stamp_Date');
define('STAFF_STAMP_USER_ID',							'Stamp_User');
define('STAFF_STAMP_USER_INITIALS',						'Stamp_User_Initials');
define('STAFF_STAMP_USER_NAME_FIRST',					'Stamp_User_Name_First');
define('STAFF_STAMP_USER_NAME_FULL',					'Stamp_User_Name_Full');
define('STAFF_STAMP_USER_NAME_LAST',					'Stamp_User_Name_Last');
define('STAFF_STAFF_STATUS_ID',							'Staff_Status_ID');
define('STAFF_USER_ROLES',								'User_Roles');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// STAFF VIEW
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('STAFF_VIEW', 									'Staff_Info');

define('ERROR_STAFF_EMAIL',								'Error_Staff_Email');
define('ERROR_STAFF_INITIALS',							'Error_Staff_Initials');
define('ERROR_STAFF_LOGIN',								'Error_Staff_Login_ID');
define('ERROR_STAFF_NAME_FIRST',						'Error_Staff_Name_First');
define('ERROR_STAFF_NAME_LAST',							'Error_Staff_Name_Last');
define('ERROR_STAFF_PASSWORD',							'Error_Password');
define('ERROR_STAFF_PASSWORD_CONFIRM',					'Error_Password_Confirm');
define('ERROR_STAFF_PASSWORD_NEW',						'Error_Password_New');
define('ERROR_STAFF_ROLE_ID',							'Error_Staff_Role_ID');
define('ERROR_STAFF_STATUS_ID',							'Error_Staff_Status_ID');
define('ERROR_STRING_LOGIN',							'The specified user does not exist or Login ID and Password do not match.');
define('ERROR_STRING_STAFF_EMAIL',						'Please enter a valid Email Address.');
define('ERROR_STRING_STAFF_INITIALS',					'Please enter this persons Initials.');
define('ERROR_STRING_STAFF_LOGIN',						'Please enter a Login ID');
define('ERROR_STRING_STAFF_NAME_FIRST',					'Please enter a First Name.');
define('ERROR_STRING_STAFF_NAME_LAST',					'Please enter a Last Name.');
define('ERROR_STRING_STAFF_PASSWORD',					'Please enter a Password.');
define('ERROR_STRING_STAFF_PASSWORD_CONFIRM',			'The Passwords do not match.');
define('ERROR_STRING_STAFF_ROLE_ID',					'Please select at least one Role.');
define('ERROR_STRING_STAFF_STATUS_ID',					'Please select a Status.');

define('LABEL_STAFF_EMAIL',								'Email:');
define('LABEL_STAFF_ID',								'Staff:');
define('LABEL_STAFF_INITIALS',							'Initials:');
define('LABEL_STAFF_LAST_EDIT_DATE',					'Last Edited On:');
define('LABEL_STAFF_LAST_EDIT_USER_ID',					'Last Edited By:');
define('LABEL_STAFF_LOGIN',								'Login ID:');
define('LABEL_STAFF_NAME_FIRST',						'First Name:');
define('LABEL_STAFF_NAME_LAST',							'Last Name:');
define('LABEL_STAFF_PASSWORD',							'Password:');
define('LABEL_STAFF_PASSWORD_CONFIRM',					'Confirm Password:');
define('LABEL_STAFF_PASSWORD_NEW',						'New Password:');
define('LABEL_STAFF_ROLE_ID',							'Role(s):');
define('LABEL_STAFF_STAMP_DATE',						'Created On:');
define('LABEL_STAFF_STAMP_USER_ID',						'Created By:');
define('LABEL_STAFF_STATUS_ID',							'Status:');
define('LABEL_STAFF_VIEW_LAST_EDIT_USER_NAME_FULL',		'Last_Edit_User_Name_Full');
define('LABEL_STAFF_VIEW_STAMP_USER_NAME_FULL',			'Stamp_User_Name_Full');

 ?>