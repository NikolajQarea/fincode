<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// CLIENT TABLE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('CLIENT_TABLE',									'Client');
define('CLIENT_ACCOUNTING_SOFTWARE_ID',					'Accounting_Software_ID');
define('CLIENT_BILLING_ID',								'Billing_ID');
define('CLIENT_DOCUMENT_ID',							'Document_ID');
define('CLIENT_ID',										'Client_ID');
define('CLIENT_INACTIVE_DATE',							'Inactive_Date');
define('CLIENT_INITIAL_YEAR',							'Initial_Year_Label');
define('CLIENT_INITIAL_YEAR_ID',						'Initial_Year');
define('CLIENT_LAST_EDIT_DATE',							'Last_Edit_Date');
define('CLIENT_LAST_EDIT_USER_ID',						'Last_Edit_User');
define('CLIENT_MONTH',									'Month');
define('CLIENT_MONTH_ID',								'Month_ID');
define('CLIENT_NAME',									'Client_Name');
define('CLIENT_NAME_FIRST',								'Client_Name_First');
define('CLIENT_NAME_FULL',								'Client_Name_Full');
define('CLIENT_NAME_LAST',								'Client_Name_Last');
define('CLIENT_NOTES',									'Client_Notes');
define('CLIENT_OCCUPATION',								'Client_Occupation');
define('CLIENT_OCCUPATION_ID',							'Client_Occupation_ID');
define('CLIENT_ORGANIZER_ID',							'Organizer');
define('CLIENT_PARTNER',								'Initials');
define('CLIENT_PARTNER_ID',								'Partner_ID');
define('CLIENT_PRINCIPAL',								'Principal_Name');
define('CLIENT_REFERRED_BY_ID',							'Referred_By_ID');
define('CLIENT_RETAINER_STATUS_ID',						'Retainer_Status_ID');
define('CLIENT_RETAINER_YEAR',							'Retainer_Year');
define('CLIENT_SSN_EIN',								'Client_SSN_EIN');
define('CLIENT_STAFF',									'Initials');
define('CLIENT_STAFF_ID',								'Staff_ID');
define('CLIENT_STAMP_DATE',								'Stamp_Date');
define('CLIENT_STAMP_USER_ID',							'Stamp_User');
define('CLIENT_STATE_ID',								'State_ID');
define('CLIENT_STATUS',									'Client_Status_Label');
define('CLIENT_STATUS_ID',								'Client_Status_ID');
define('CLIENT_STATUS_TABLE',							'Client_Status');
define('SPOUSE_NAME_FIRST',								'Spouse_Name_First');
define('SPOUSE_NAME_FULL',								'Spouse_Name_Full');
define('SPOUSE_NAME_LAST',								'Spouse_Name_Last');
define('SPOUSE_OCCUPATION',								'Spouse_Occupation');
define('SPOUSE_OCCUPATION_ID',							'Spouse_Occupation_ID');
define('SPOUSE_SSN_EIN',								'Spouse_SSN_EIN');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// CLIENT TYPE TABLE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('CLIENT_TYPE_TABLE',								'Client_Type');
define('CLIENT_TYPE',									'Client_Type_Label');
define('CLIENT_TYPE_ID',								'Client_Type_ID');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// CLIENT TAX RETURN LINK
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// CLIENT VIEW
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('CLIENT_VIEW',									'Client_Info');
define('CLIENT_VIEW_ACCOUNTING_SOFTWARE',				'Accounting_Software_Name');
define('CLIENT_VIEW_ACCOUNTING_SOFTWARE_ID',			'Accounting_Software_ID');
define('CLIENT_VIEW_CLIENT_BILLING_ID',					'Billing_ID');
define('CLIENT_VIEW_CLIENT_DOCUMENT_ID',				'Document_ID');
define('CLIENT_VIEW_CLIENT_ID',							'Client_ID');
define('CLIENT_VIEW_CLIENT_INACTIVE_DATE',				'Inactive_Date');
define('CLIENT_VIEW_CLIENT_NAME',						'Client_Name');
define('CLIENT_VIEW_CLIENT_NAME_FIRST',					'Client_Name_First');
define('CLIENT_VIEW_CLIENT_NAME_LAST',					'Client_Name_Last');
define('CLIENT_VIEW_CLIENT_NOTES',						'Client_Notes');
define('CLIENT_VIEW_CLIENT_OCCUPATION',					'Client_Occupation');
define('CLIENT_VIEW_CLIENT_OCCUPATION_ID',				'Client_Occupation_ID');
define('CLIENT_VIEW_CLIENT_PRINCIPAL',					'Principal_Name');
define('CLIENT_VIEW_CLIENT_SSN_EIN',					'Client_SSN_EIN');
define('CLIENT_VIEW_CLIENT_STATUS',						'Client_Status');
define('CLIENT_VIEW_CLIENT_STATUS_ID',					'Client_Status_ID');
define('CLIENT_VIEW_CLIENT_TYPE',						'Client_Type');
define('CLIENT_VIEW_CLIENT_TYPE_ID',					'Client_Type_ID');
define('CLIENT_VIEW_COMPANY_NAME',						'Company_Name');
define('CLIENT_VIEW_INACTIVE_DATE',						'Inactive_Date');
define('CLIENT_VIEW_INITIAL_YEAR',						'Initial_Year');
define('CLIENT_VIEW_INITIAL_YEAR_ID',					'Initial_Year');
define('CLIENT_VIEW_LAST_EDIT_DATE',					'Last_Edit_Date');
define('CLIENT_VIEW_LAST_EDIT_USER',					'Last_Edit_User_Name_Full');
define('CLIENT_VIEW_LAST_EDIT_USER_EMAIL',				'Last_Edit_User_Email');
define('CLIENT_VIEW_LAST_EDIT_USER_ID',					'Last_Edit_User');
define('CLIENT_VIEW_LAST_EDIT_USER_INITIALS',			'Last_Edit_User_Initials');
define('CLIENT_VIEW_LAST_EDIT_USER_NAME_FIRST',			'Last_Edit_User_Name_First');
define('CLIENT_VIEW_LAST_EDIT_USER_NAME_LAST',			'Last_Edit_User_Name_Last');
define('CLIENT_VIEW_LAST_EDIT_USER_LOGIN_ID',			'Last_Edit_User_Login_ID');
define('CLIENT_VIEW_MONTH',								'Month');
define('CLIENT_VIEW_MONTH_ID',							'Month_ID');
define('CLIENT_VIEW_ORGANIZER',							'Organizer_Label');
define('CLIENT_VIEW_ORGANIZER_ID',						'Organizer');
define('CLIENT_VIEW_PARTNER_ID',						'Partner_ID');
define('CLIENT_VIEW_PARTNER_INITIALS',					'Partner_Initials');
define('CLIENT_VIEW_PARTNER_LOGIN',						'Partner_Login_ID');
define('CLIENT_VIEW_PARTNER_NAME',						'Partner_Name_Full');
define('CLIENT_VIEW_PRINCIPAL',							'Principal_Name');
define('CLIENT_VIEW_REFERRED_BY',						'Referred_By_Name');
define('CLIENT_VIEW_REFERRED_BY_ID',					'Referred_By_ID');
define('CLIENT_VIEW_RETAINER_STATUS',					'Retainer_Status');
define('CLIENT_VIEW_RETAINER_STATUS_ID',				'Retainer_Status_ID');
define('CLIENT_VIEW_RETAINER_YEAR',						'Retainer_Year');
define('CLIENT_VIEW_SPOUSE_NAME',						'Spouse_Name');
define('CLIENT_VIEW_SPOUSE_NAME_FIRST',					'Spouse_Name_First');
define('CLIENT_VIEW_SPOUSE_NAME_LAST',					'Spouse_Name_Last');
define('CLIENT_VIEW_SPOUSE_OCCUPATION',					'Spouse_Occupation');
define('CLIENT_VIEW_SPOUSE_OCCUPATION_ID',				'Spouse_Occupation_ID');
define('CLIENT_VIEW_SPOUSE_SSN_EIN',					'Spouse_SSN_EIN');
define('CLIENT_VIEW_STAFF_ID',							'Staff_ID');
define('CLIENT_VIEW_STAFF_INITIALS',					'Staff_Initials');
define('CLIENT_VIEW_STAFF_LOGIN',						'Staff_Login_ID');
define('CLIENT_VIEW_STAFF_NAME',						'Staff_Name_Full');
define('CLIENT_VIEW_STAMP_DATE',						'Stamp_Date');
define('CLIENT_VIEW_STAMP_USER',						'Stamp_User_Name_Full');
define('CLIENT_VIEW_STAMP_USER_EMAIL',					'Stamp_User_Email');
define('CLIENT_VIEW_STAMP_USER_ID',						'Stamp_User');
define('CLIENT_VIEW_STAMP_USER_INITIALS',				'Stamp_User_Initials');
define('CLIENT_VIEW_STAMP_USER_NAME_FIRST',				'Stamp_User_Name_First');
define('CLIENT_VIEW_STAMP_USER_NAME_LAST',				'Stamp_User_Name_Last');
define('CLIENT_VIEW_STAMP_USER_LOGIN_ID',				'Stamp_User_Login_ID');
define('CLIENT_VIEW_STATES',							'States');
define('CLIENT_VIEW_TAX_FORMS',							'Tax_Forms');
define('CLIENT_VIEW_TAX_FORM_IDS',						'Tax_Form_IDs');
define('CLIENT_VIEW_CLIENT_TAX_FORM_LINK',				'Client_Tax_Form_Link');
define('CLIENT_VIEW_CLIENT_TAX_FORM_LINK_ID',			'Client_Tax_Form_Link_ID');
define('CLIENT_VIEW_TAX_RETURNS',						'Tax_Returns');
define('CLIENT_VIEW_TAX_RETURN_IDS',					'Tax_Return_IDs');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// CLIENT DATA LABELS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('LABEL_CLIENT_ACCOUNTING_SOFTWARE_ID',			'Accounting Package:');
define('LABEL_CLIENT_BILLING_ID',						'Billing ID:');
define('LABEL_CLIENT_DOCUMENT_ID',						'Document ID:');
define('LABEL_CLIENT_ID',								'Client:');
define('LABEL_CLIENT_INACTIVE_DATE',					'Inactive Date:');
define('LABEL_CLIENT_INITIAL_YEAR_ID',					'Initial Year:');
define('LABEL_CLIENT_MONTH_ID',							'1st Month of Tax Year:');
define('LABEL_CLIENT_NAME',								'Client Name:');
define('LABEL_CLIENT_NAME_FIRST',						'Client First Name:');
define('LABEL_CLIENT_NAME_LAST',						'Client Last Name:');
define('LABEL_CLIENT_NOTES',							'Client Note(s):');
define('LABEL_CLIENT_OCCUPATION_ID',					'Client Occupation:');
define('LABEL_CLIENT_ORGANIZER_ID',						'Organizer:');
define('LABEL_CLIENT_PARTNER_ID',						'Client Partner:');
define('LABEL_CLIENT_PRINCIPAL',						'Principal:');
define('LABEL_CLIENT_REFERRED_BY_ID',					'Referred By:');
define('LABEL_CLIENT_RETAINER_STATUS_ID',				'Retainer Status:');
define('LABEL_CLIENT_RETAINER_YEAR',					'Retainer Year:');
define('LABEL_CLIENT_REVIEWER_1_ID',					'1st Reviewer:');
define('LABEL_CLIENT_REVIEWER_2_ID',					'2nd Reviewer:');
define('LABEL_CLIENT_SSN_EIN',							'Client SSN or EIN:');
define('LABEL_CLIENT_STAFF_ID',							'Preparer:');
define('LABEL_CLIENT_STATE_ID',							'State:');
define('LABEL_CLIENT_STATUS_ID',						'Client Status:');
define('LABEL_CLIENT_TAX_FORM_IDS',						'Tax Form(s):');
define('LABEL_CLIENT_TYPE_ID',							'Client Type:');
define('LABEL_SPOUSE_NAME_FIRST',						'Spouse First Name:');
define('LABEL_SPOUSE_NAME_LAST',						'Spouse Last Name:');
define('LABEL_SPOUSE_OCCUPATION_ID',					'Spouse Occupation:');
define('LABEL_SPOUSE_SSN_EIN',							'Spouse SSN:');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// CLIENT ERROR TAGS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('ERROR_CLIENT_ACCOUNTING_SOFTWARE_ID',			'Error_Client_Accounting_Software_ID');
define('ERROR_CLIENT_BILLING_ID',						'Error_Client_Billing_ID');
define('ERROR_CLIENT_DOCUMENT_ID',						'Error_Client_Document_ID');
define('ERROR_CLIENT_ID',								'Error_Client_ID');
define('ERROR_CLIENT_INITIAL_YEAR_ID',					'Error_Inital_Year_ID');
define('ERROR_CLIENT_MONTH_ID',							'Error_Client_Month_ID');
define('ERROR_CLIENT_NAME',								'Error_Client_Name');
define('ERROR_CLIENT_NAME_FIRST',						'Error_Client_Name_First');
define('ERROR_CLIENT_NAME_LAST',						'Error_Client_Name_Last');
define('ERROR_CLIENT_NOTES',							'Error_Client_Notes');
define('ERROR_CLIENT_OCCUPATION_ID',					'Error_Client_Occupation_ID');
define('ERROR_CLIENT_ORGANIZER_ID',						'Error_Client_Organizer_ID');
define('ERROR_CLIENT_PARTNER_ID',						'Error_Client_Partner_ID');
define('ERROR_CLIENT_PRINCIPAL',						'Error_Client_Principal');
define('ERROR_CLIENT_REFERRED_BY_ID',					'Error_Client_Referred_By_ID');
define('ERROR_CLIENT_RETAINER_STATUS_ID',				'Error_Client_Retainer_Status_ID');
define('ERROR_CLIENT_RETAINER_YEAR',					'Error_Client_Retainer_Year');
define('ERROR_CLIENT_REVIEWER_1_ID',					'Error_Client_Reviewer_1_ID');
define('ERROR_CLIENT_REVIEWER_2_ID',					'Error_Client_Reviewer_2_ID');
define('ERROR_CLIENT_SSN_EIN',							'Error_Client_SSN_EIN');
define('ERROR_CLIENT_STAFF_ID',							'Error_Client_Staff_ID');
define('ERROR_CLIENT_STATUS_ID',						'Error_Client_Status_ID');
define('ERROR_CLIENT_STATE_ID',							'Error_Client_State_ID');
define('ERROR_CLIENT_TYPE_ID',							'Error_Client_Type_ID');
define('ERROR_SPOUSE_OCCUPATION_ID',					'Error_Spouse_Occupation_ID');
define('ERROR_SPOUSE_NAME_FIRST',						'Error_Spouse_Name_First');
define('ERROR_SPOUSE_NAME_LAST',						'Error_Spouse_Name_Last');
define('ERROR_SPOUSE_SSN_EIN',							'Error_Spouse_SSN_EIN');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// CLIENT ERROR LABELS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('ERROR_STRING_CLIENT_ACCOUNTING_SOFTWARE_ID',	'Please select an Accounting Package.');
define('ERROR_STRING_CLIENT_BILLING_ID',				'Please enter a Client Billing ID.');
define('ERROR_STRING_CLIENT_DOCUMENT_ID',				'Please enter a Client Document ID.');
define('ERROR_STRING_CLIENT_ID',						'Please choose a Client.');
define('ERROR_STRING_CLIENT_INITIAL_YEAR_ID',			'Please select an Initial Year.');
define('ERROR_STRING_CLIENT_MONTH_ID',					'Please select a Month.');
define('ERROR_STRING_CLIENT_NAME',						'Please enter the Clients Name.');
define('ERROR_STRING_CLIENT_NAME_FIRST',				'Please enter a First Name.');
define('ERROR_STRING_CLIENT_NAME_LAST',					'Please enter a Last Name.');
define('ERROR_STRING_CLIENT_NOTES',						'Please enter some Client Note(s)');
define('ERROR_STRING_CLIENT_PARTNER_ID',				'Please select a Partner.');
define('ERROR_STRING_CLIENT_PRINCIPAL',					'Please enter a Client Principal.');
define('ERROR_STRING_CLIENT_OCCUPATION_ID',				'Please select a Client Occupation.');
define('ERROR_STRING_CLIENT_ORGANIZER_ID',				'Please select if an Organizer.');
define('ERROR_STRING_CLIENT_REFERRED_BY_ID',			'Please select a Referral.');
define('ERROR_STRING_CLIENT_RETAINER_STATUS_ID',		'Please select a Retainer Status.');
define('ERROR_STRING_CLIENT_RETAINER_YEAR',				'Please enter a Retainer Year.');
define('ERROR_STRING_CLIENT_REVIEWER_1_ID',				'Please select a 1st Reviewer.');
define('ERROR_STRING_CLIENT_REVIEWER_2_ID',				'Please select a 2nd Reviewer.');
define('ERROR_STRING_CLIENT_SSN_EIN',					'Please enter a Client SSN or EIN.');
define('ERROR_STRING_CLIENT_STAFF_ID',					'Please select a Preparer.');
define('ERROR_STRING_CLIENT_STATE_ID',					'Please select a State.');
define('ERROR_STRING_CLIENT_STATUS_ID',					'Please select a Client Status.');
define('ERROR_STRING_CLIENT_TYPE_ID',					'Please select a Client Type.');
define('ERROR_STRING_SPOUSE_ID',						'Please select a Spouse Occupation.');
define('ERROR_STRING_SPOUSE_NAME_FIRST',				'Please enter a Spouse First Name.');
define('ERROR_STRING_SPOUSE_NAME_LAST',					'Please enter a Spouse Last Name.');
define('ERROR_STRING_SPOUSE_SSN_EIN',					'Invalid or missing Spouse SSN');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 ?>