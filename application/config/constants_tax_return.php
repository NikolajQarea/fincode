<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// TAX RETURN TABLE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('TAX_RETURN_TABLE',							'Tax_Return');
define('TAX_RETURN_990PF_DATE',							'990PF_Date');
define('TAX_RETURN_POST',							'tax_return');
define('TAX_RETURN_MULTIPLE_POST',						'tax_return_multiple');
define('TAX_RETURN_CLIENT_ID',							'Client_ID');
define('TAX_RETURN_DATE_RELEASED',						'Date_Released');
define('TAX_RETURN_DATE_SENT',							'Date_Sent');
define('TAX_RETURN_ID',									'Tax_Return_ID');
define('TAX_RETURN_INSTRUCTIONS_ID',					'Instructions_ID');
define('TAX_RETURN_IS_LOCKED',							'Is_Locked');
define('TAX_RETURN_LAST_EDIT_DATE',						'Last_Edit_Date');
define('TAX_RETURN_LAST_EDIT_USER_ID',					'Last_Edit_User');
define('TAX_RETURN_NOTES',								'Tax_Return_Notes');
define('TAX_RETURN_PARTNER_ID',							'Partner_ID');
define('TAX_RETURN_REVIEWER_1_ID',						'Reviewer_1_ID');
define('TAX_RETURN_REVIEWER_2_ID',						'Reviewer_2_ID');
define('TAX_RETURN_STAFF_ID',							'Staff_ID');
define('TAX_RETURN_STAMP_DATE',							'Stamp_Date');
define('TAX_RETURN_STAMP_USER_ID',						'Stamp_User');
define('TAX_RETURN_TAX_FORM_ID',						'Tax_Form_ID');
define('TAX_RETURN_TAX_MONTH_ID',						'Tax_Month');
define('TAX_RETURN_TAX_QUARTER_ID',						'Tax_Quarter');
define('TAX_RETURN_TAX_RETURN_STATUS_ID',				'Tax_Return_Status_ID');
define('TAX_RETURN_TAX_YEAR_ID',						'Tax_Year');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// TAX RETURN STATUS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('TAX_RETURN_STATUS_TABLE', 						'Tax_Return_Status');
define('TAX_RETURN_STATUS', 							'Tax_Return_Status_Label');
define('TAX_RETURN_STATUS_ID',							'Tax_Return_Status_ID');
define('TAX_RETURN_STATUS_TO_BE_DETERMINED_ID',         '15');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// TAX RETURN VIEW
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('TAX_RETURN_VIEW',								'Tax_Return_Info');
define('TAX_RETURN_VIEW_990PF_DATE',					'990PF_Date');
define('TAX_RETURN_VIEW_CLIENT_ID',						'Client_ID');
define('TAX_RETURN_VIEW_CLIENT_NAME',					'Client_Name');
define('TAX_RETURN_VIEW_CLIENT_STATES',					'Client_States');
define('TAX_RETURN_VIEW_CLIENT_STATUS',					'Client_Status_Label');
define('TAX_RETURN_VIEW_CLIENT_STATUS_ID',				'Client_Status_ID');
define('TAX_RETURN_VIEW_DATE_RELEASED',					'Date_Released');
define('TAX_RETURN_VIEW_DATE_SENT',						'Date_Sent');
define('TAX_RETURN_VIEW_INSTRUCTIONS',					'Instructions');
define('TAX_RETURN_VIEW_INSTRUCTIONS_ID',				'Instructions_ID');
define('TAX_RETURN_VIEW_IS_LOCKED',						'Is_Locked');
define('TAX_RETURN_VIEW_LAST_EDIT_DATE',				'Last_Edit_Date');
define('TAX_RETURN_VIEW_LAST_EDIT_USER',				'Last_Edit_User_Name_Full');
define('TAX_RETURN_VIEW_LAST_EDIT_USER_EMAIL',			'Last_Edit_User_Email');
define('TAX_RETURN_VIEW_LAST_EDIT_USER_ID',				'Last_Edit_User_ID');
define('TAX_RETURN_VIEW_LAST_EDIT_USER_INITIALS',		'Last_Edit_User_Initials');
define('TAX_RETURN_VIEW_LAST_EDIT_USER_LOGIN',			'Last_Edit_User_Login');
define('TAX_RETURN_VIEW_LAST_EDIT_USER_NAME_FIRST',		'Last_Edit_User_Name_First');
define('TAX_RETURN_VIEW_LAST_EDIT_USER_NAME_LAST',		'Last_Edit_User_Name_Last');
define('TAX_RETURN_VIEW_PARTNER',						'Partner_Name_Full');
define('TAX_RETURN_VIEW_PARTNER_EMAIL',					'Partner_Email');
define('TAX_RETURN_VIEW_PARTNER_ID',					'Partner_ID');
define('TAX_RETURN_VIEW_PARTNER_INITIALS',				'Partner_Initials');
define('TAX_RETURN_VIEW_PARTNER_LOGIN',					'Partner_Login');
define('TAX_RETURN_VIEW_PARTNER_NAME_FIRST',			'Partner_Name_First');
define('TAX_RETURN_VIEW_PARTNER_NAME_LAST',				'Partner_Name_Last');
define('TAX_RETURN_VIEW_REVIEWER_1',					'Reviewer_1_Name_Full');
define('TAX_RETURN_VIEW_REVIEWER_1_EMAIL',				'Reviewer_1_Email');
define('TAX_RETURN_VIEW_REVIEWER_1_ID',					'Reviewer_1_ID');
define('TAX_RETURN_VIEW_REVIEWER_1_INITIALS',			'Reviewer_1_Initials');
define('TAX_RETURN_VIEW_REVIEWER_1_LOGIN',				'Reviewer_1_Login');
define('TAX_RETURN_VIEW_REVIEWER_1_NAME_FIRST',			'Reviewer_1_Name_First');
define('TAX_RETURN_VIEW_REVIEWER_1_NAME_LAST',			'Reviewer_1_Name_Last');
define('TAX_RETURN_VIEW_REVIEWER_2',					'Reviewer_2_Name_Full');
define('TAX_RETURN_VIEW_REVIEWER_2_EMAIL',				'Reviewer_2_Email');
define('TAX_RETURN_VIEW_REVIEWER_2_ID',					'Reviewer_2_ID');
define('TAX_RETURN_VIEW_REVIEWER_2_INITIALS',			'Reviewer_2_Initials');
define('TAX_RETURN_VIEW_REVIEWER_2_LOGIN',				'Reviewer_2_Login');
define('TAX_RETURN_VIEW_REVIEWER_2_NAME_FIRST',			'Reviewer_2_Name_First');
define('TAX_RETURN_VIEW_REVIEWER_2_NAME_LAST',			'Reviewer_2_Name_Last');
define('TAX_RETURN_VIEW_STAFF',							'Staff_Name_Full');
define('TAX_RETURN_VIEW_STAFF_EMAIL',					'Staff_Email');
define('TAX_RETURN_VIEW_STAFF_ID',						'Staff_ID');
define('TAX_RETURN_VIEW_STAFF_INITIALS',				'Staff_Initials');
define('TAX_RETURN_VIEW_STAFF_LOGIN',					'Staff_Login');
define('TAX_RETURN_VIEW_STAFF_NAME_FIRST',				'Staff_Name_First');
define('TAX_RETURN_VIEW_STAFF_NAME_LAST',				'Staff_Name_Last');
define('TAX_RETURN_VIEW_STAMP_USER',					'Stamp_User_Name_Full');
define('TAX_RETURN_VIEW_STAMP_USER_EMAIL',				'Stamp_User_Email');
define('TAX_RETURN_VIEW_STAMP_USER_ID',					'Stamp_User_ID');
define('TAX_RETURN_VIEW_STAMP_USER_INITIALS',			'Stamp_User_Initials');
define('TAX_RETURN_VIEW_STAMP_USER_LOGIN',				'Stamp_User_Login');
define('TAX_RETURN_VIEW_STAMP_USER_NAME_FIRST',			'Stamp_User_Name_First');
define('TAX_RETURN_VIEW_STAMP_USER_NAME_LAST',			'Stamp_User_Name_Last');
define('TAX_RETURN_VIEW_TAX_FORM',						'Tax_Form_Name');
define('TAX_RETURN_VIEW_TAX_FORM_ID',					'Tax_Form_ID');
define('TAX_RETURN_VIEW_TAX_RETURN_ID',					'Tax_Return_ID');
define('TAX_RETURN_VIEW_TAX_MONTH',						'Tax_Month');
define('TAX_RETURN_VIEW_TAX_MONTH_ID',					'Tax_Month_ID');
define('TAX_RETURN_VIEW_TAX_QUARTER',					'Tax_Quarter');
define('TAX_RETURN_VIEW_TAX_QUARTER_ID',				'Tax_Quarter_ID');
define('TAX_RETURN_VIEW_TAX_RETURN_NOTES',				'Tax_Return_Notes');
define('TAX_RETURN_VIEW_TAX_RETURN_STATUS',				'Tax_Return_Status');
define('TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID',			'Tax_Return_Status_ID');
define('TAX_RETURN_VIEW_TAX_YEAR',						'Tax_Year');
define('TAX_RETURN_VIEW_TAX_YEAR_MULTIPLE',						'Tax_Year_Multiple');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// TAX RETURN DATA LABELS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('LABEL_TAX_RETURN_990PF_DATE',					'990PF Date:');
define('LABEL_TAX_RETURN_DATE_RELEASED',				'Date Released:');
define('LABEL_TAX_RETURN_DATE_SENT',					'Date Sent to Client:');
define('LABEL_TAX_RETURN_NOTES',						'Tax Return Note(s):');
define('LABEL_TAX_RETURN_REVIEWER_1_ID',				'1st Reviewer:');
define('LABEL_TAX_RETURN_REVIEWER_2_ID',				'2nd Reviewer:');
define('LABEL_TAX_RETURN_STAFF_ID',						'Prepared By:');
define('LABEL_TAX_RETURN_STATUS_ID',					'Tax Return Status:');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// TAX RETURN ERROR TAGS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('ERROR_TAX_RETURN_990PF_DATE',					'Error_Tax_Return_990PF_Date');
define('ERROR_TAX_RETURN_DATE_RELEASED',				'Error_Tax_Return_Date_Released');
define('ERROR_TAX_RETURN_DATE_SENT',					'Error_Tax_Return_Date_Sent');
define('ERROR_TAX_RETURN_NOTES',						'Error_Tax_Return_Notes');
define('ERROR_TAX_RETURN_PARTNER_ID',					'Error_Tax_Return_Partner_ID');
define('ERROR_TAX_RETURN_REVIEWER_1_ID',				'Error_Tax_Return_Reviewer_1_ID');
define('ERROR_TAX_RETURN_REVIEWER_2_ID',				'Error_Tax_Return_Reviewer_2_ID');
define('ERROR_TAX_RETURN_STAFF_ID',						'Error_Tax_Return_Staff_ID');
define('ERROR_TAX_RETURN_STATUS_ID',					'Error_Tax_Return_Status_ID');
define('ERROR_TAX_YEAR_ID',								'Error_Tax_Year');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// TAX RETURN ERROR LABELS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('ERROR_STRING_TAX_RETURN_990PF_DATE',			'990PF Date ie empty or not a valid date.');
define('ERROR_STRING_TAX_RETURN_DATE_RELEASED',			'Date Released is empty or not a valid date.');
define('ERROR_STRING_TAX_RETURN_DATE_SENT',				'Date Sent is empty or not a valid date.');
define('ERROR_STRING_TAX_RETURN_NOTES',					'Please enter Tax Return Note(s).');
define('ERROR_STRING_TAX_RETURN_PARTNER_ID',			'Please select a Partner.');
define('ERROR_STRING_TAX_RETURN_REVIEWER_1_ID',			'Please select a 1st Reviewer.');
define('ERROR_STRING_TAX_RETURN_REVIEWER_2_ID',			'Please select a 2nd Reviewer.');
define('ERROR_STRING_TAX_RETURN_STAFF_ID',				ERROR_STRING_CLIENT_STAFF_ID);
define('ERROR_STRING_TAX_RETURN_STATUS_ID',				'Please select a Tax Return Status.');
define('ERROR_STRING_TAX_YEAR_ID',						'Please select a Tax Year.');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 ?>