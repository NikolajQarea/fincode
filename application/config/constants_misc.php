<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// USER ROLES/SECURITY
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('ROLE_ADMIN',									'System_Administrator');
define('ROLE_READ_ONLY',								'Read_Only');
define('ROLE_WRITE',									'Read_And_Write');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// DEFAULT VALUES
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('ANY_LABEL',										'-- Any --');
define('ANY_VALUE',										'Any');
define('CATEGORY_CLIENT_TYPE',							2);
define('CATEGORY_INSTRUCTIONS',                         9);
define('CATEGORY_OCCUPATIONS',							1);
define('CATEGORY_REFERRED_BY',							3);
define('CATEGORY_RETAINER_STATUS',						6);
define('CATEGORY_STATEMENT_STATUS',						7);
define('CATEGORY_STATEMENT_TYPE',						8);
define('CATEGORY_TAX_FORMS',							4);
define('CATEGORY_TAX_RETURN_STATUS',					5);
define('CONTROL_SELECT', 								'<select class="input" id="@formname@[@id@][]" name="@formname@[@id@][]"@multi@@size@>@options@</select>');
define('CONTROL_OPTION', 								'<option value="@value@" @selected@>@label@</option>');
define('CONTROL_SSN_FORMAT',	 						'<span class="sub">Format is ###-##-#### (SSN)</span>');
define('CONTROL_SSN_EIN_FORMAT', 						'<span class="sub">Format is ###-##-#### (SSN) or ##-####### (EIN)</span>');
define('CONTROL_MULTI_SELECT_TEXT',						'<span class="sub">use Ctrl+click for multiple select</span>');
define('CONTROL_ORGANIZER_1040',						'<span class="sub">Applies to Form 1040 Only</span>');
define('CLASS_INPUT',									'input');
define('COMPANY_NAME',									'Company_Name');
define('DATE_FORMAT',									'm/d/Y');
define('FOOTER',										'footer');
define('HEADER',										'header');
define('JS_990PF_DATE',									'Form Date');
define('JS_CLIENT_BILLING_ID',							'Client Billing ID');
define('JS_CLIENT_DOCUMENT_ID',							'Client Document ID');
define('JS_CLIENT_LAST_NAME',							'Client Last Name');
define('JS_CLIENT_FIRST_NAME',							'Client First Name');
define('JS_CLIENT_NAME',								'Client Name');
define('JS_CLIENT_NOTES',								'Client Notes');
define('JS_CLIENT_TYPE',								'Client Type');
define('JS_CLIENT_PRINCIPAL',							'Principal Name');
define('JS_CLIENT_SSN_EIN',								'Client SSN or EIN');
define('JS_COMPANY_NAME',								'Company Name');
define('JS_DATE_RELEASED',								'Date Released (MM/DD/YYYY)');
define('JS_DATE_SENT',									'Date Sent (MM/DD/YYYY)');
define('JS_INSTITUTION_ABA',							'ABA Number');
define('JS_INSTITUTION_ADDRESS_1',						'Address (Line 1)');
define('JS_INSTITUTION_ADDRESS_2',						'Address (Line 2)');
define('JS_INSTITUTION_CITY',							'City');
define('JS_INSTITUTION_CONTACT',						'Contact Name');
define('JS_INSTITUTION_NAME',							'Institution Name');
define('JS_INSTITUTION_POSTAL_CODE',					'Postal/Zip Code');
define('JS_INSTITUTION_TELEPHONE',						'Telephone Number');
define('JS_INSTRUCTIONS',                               'Instructions');
define('JS_OCCUPATION_NAME',							'Occupation');
define('JS_RETAINER_STATUS',							'Retainer Status');
define('JS_REFERRED_BY',								'Referred By');
define('JS_STAFF_EMAIL',								'Email Address');
define('JS_STAFF_INITIALS',								'Initials');
define('JS_STAFF_LOGIN_ID',								'Login ID');
define('JS_STAFF_NAME_LAST',							'Last Name');
define('JS_STAFF_NAME_FIRST',							'First Name');
define('JS_STATEMENT_ABA_NUMBER',						'ABA Number');
define('JS_STATEMENT_ACCOUNT_NUMBER',					'Account Number');
define('JS_STATEMENT_END_DATE',							'End Date');
define('JS_STATEMENT_LAST_RECONCILIATION_DATE',			'Reconciled Date');
define('JS_STATEMENT_NOTES',							'Statement Note(s)');
define('JS_STATEMENT_START_DATE',						'Start Date');
define('JS_STATEMENT_STATUS',							'Statement Status');
define('JS_STATEMENT_TYPE',								'Statement Type');
define('JS_SPOUSE_LAST_NAME',							'Spouse Last Name').
define('JS_SPOUSE_FIRST_NAME',							'Spouse First Name').
define('JS_SPOUSE_SSN',									'Spouse SSN');
define('JS_TAX_FORM_DATE_FORMAT',						'MM/DD');
define('JS_TAX_FORM_NAME',								'Tax Form Name/ID');
define('JS_TAX_RETURN_NOTES',							'Tax Return Notes');
define('JS_TAX_RETURN_STATUS',							'Tax Return Status');
define('MENU',											'menu/menu');
define('NO_RECORDS', 									'No Records Found');
define('NOT_APPLICABLE',								'Not Applicable');
define('NOT_APPLICABLE_ID',								'-1');
define('NOT_AVAILABLE',									'N/A');
define('NOT_SENT',										'Not Sent');
define('PARTNER_ROLE_ID', 								'2');
define('PLEASE_SELECT', 								'-- Please Select --');
define('PLEASE_SELECT_ID',								'select');
define('REPORT_DATE',									'Report_Date');
define('REQUIRED_FIELD',								'<span class="errors">*</span>&nbsp;');
define('SORT_ORDER',									'ASC');
define('STATUS_MESSAGE',								'Status_Message');
define('ERROR_MESSAGE',									'Error_Message');
define('TOTAL_TIMES_USED',								'Times_In_Use');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// ACCOUNTING SOFTWARE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('ACCOUNTING_SOFTWARE_TABLE',						'Accounting_Software');
define('ACCOUNTING_SOFTWARE',							'Accounting_Software_Name');
define('ACCOUNTING_SOFTWARE_ID',						'Accounting_Software_ID');
define('ERROR_ACCOUNTING_SOFTWARE_ID',					'Error_Accounting_Software_ID');
define('ERROR_STRING_ACCOUNTING_SOFTWARE_ID',			'Please enter an Accounting Software Package.');
define('LABEL_ACCOUNTING_SOFTWARE_ID',					'Accounting Package:');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// CLIENT_TAX_FORM_LINK
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('CLIENT_TAX_FORM_LINK_TABLE',						'Client_Tax_Form_Link');
define('CLIENT_TAX_FORM_LINK_ID',							'Client_Tax_Form_Link_ID');
define('CLIENT_TAX_FORM_LINK_CLIENT_ID',					'Client_ID');
define('CLIENT_TAX_FORM_LINK_TAX_FORM_ID',					'Tax_Form_ID');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// CLIENT_STATE_LINK
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('CLIENT_STATE_LINK_TABLE',							'Client_State_Link');
define('CLIENT_STATE_LINK_ID',								'Client_State_Link_ID');
define('CLIENT_STATE_LINK_CLIENT_ID',						'Client_ID');
define('CLIENT_STATE_LINK_STATE_ID',						'State_ID');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// INSTRUCTIONS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('INSTRUCTIONS_TABLE',							'Instructions');
define('INSTRUCTIONS',									'Instructions_Label');
define('INSTRUCTIONS_ID',								'Instructions_ID');
define('ERROR_INSTRUCTIONS_ID',							'Error_Instructions_ID');
define('ERROR_STRING_INSTRUCTIONS_ID',					'Please select an Instruction.');
define('LABEL_INSTRUCTIONS_ID',							'Instructions:');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// OCCUPATIONS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('OCCUPATION_TABLE',								'Occupation');
define('OCCUPATION',									'Occupation_Name');
define('OCCUPATION_ID',									'Occupation_ID');
define('ERROR_OCCUPATION_ID',							'Error_Occupation_ID');
define('ERROR_STRING_OCCUPATION_ID',					'Please select an Occupation.');
define('LABEL_OCCUPATION_ID',							'Occupation:');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// PAGING
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('PAGING_CURRENT_OPEN_TAG',						'<strong>&nbsp;');
define('PAGING_CURRENT_CLOSE_TAG',						'&nbsp;</strong>');
define('PAGING_FIRST',									'First');
define('PAGING_FULL_OPEN_TAG',							'<p>');
define('PAGING_FULL_CLOSE_TAG',							'</p>');
define('PAGING_LAST',									'Last');
define('PAGING_NEXT',									'Next');
define('PAGING_NUMBER_LINKS',							2);
define('PAGING_PREVIOUS',								'Previous');
define('PAGING_RECORDS_PER_PAGE',						20);
define('PAGING_URI_SEGMENTS',							3);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// REFERRED BY
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('REFERRED_BY_TABLE',								'Referred_By');
define('REFERRED_BY',									'Referred_By_Name');
define('REFERRED_BY_ID',								'Referred_By_ID');
define('ERROR_REFERRED_BY_ID',							'Error_Referred_By_ID');
define('ERROR_STRING_REFERRED_BY_ID',					'Please enter a Referred By Name.');
define('LABEL_REFERRED_BY_ID',							'Referred By:');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// RETAINER STATUS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('RETAINER_STATUS_TABLE',							'Retainer_Status');
define('RETAINER_STATUS',								'Retainer_Status_Label');
define('RETAINER_STATUS_ID',							'Retainer_Status_ID');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// ROLES
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('ROLE_TABLE',									'Role');
define('ROLE_ROLE',										'Role_Name');
define('ROLE_ROLE_ID',									'Role_ID');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// STAFF STATUS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('STAFF_STATUS_TABLE',							'Staff_Status');
define('STAFF_STATUS',									'Staff_Status_Label');
define('STAFF_STATUS_ID',								'Staff_Status_ID');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// STATES TABLE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('STAFF_ROLE_LINK_TABLE',							'Staff_Role_Link');
define('STAFF_ROLE_LINK_ID',							'Staff_Role_Link_ID');
define('STAFF_ROLE_LINK_ROLE_ID',						'Role_ID');
define('STAFF_ROLE_LINK_STAFF_ID',						'Staff_ID');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// STATES TABLE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('STATE_TABLE',									'State');
define('STATE',											'State_ID');
define('STATE_ID',										'State_ID');
define('ERROR_STATE_ID',								'Error_State_ID');
define('ERROR_STRING_STATE_ID',							'Please select a State.');
define('LABEL_STATE_ID',								'State:');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// TAX FORM INITIAL DATE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('TAX_FORM_INITIAL_DUE_DATE_TABLE',							'Tax_Form_Initial_Date');
define('TAX_FORM_INITIAL_DUE_DATE_DATE',							'Initial_Date');
define('TAX_FORM_INITIAL_DUE_DATE_ID',								'Tax_Form_ID');
define('TAX_FORM_INITIAL_DUE_DATE_TYPE',							'Type');
define('TAX_FORM_INITIAL_DUE_DATE_TYPE_INITIAL_DATE',				'init');
define('TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_1',				'ext1');
define('TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_2',				'ext2');
define('ERROR_TAX_FORM_INITIAL_DUE_DATE_ID',						'Error_Initial_Tax_Form_Date_ID');
define('ERROR_TAX_FORM_INITIAL_DUE_DATE',							'Error_Tax_Form_Initial_Date');
define('ERROR_TAX_FORM_INITIAL_DUE_DATE_TYPE',						'Error_Tax_Form_Initial_Date_Type');
define('ERROR_TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_1',			'Error_Tax_Form_Initial_Date_Type_Extension_1');
define('ERROR_TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_2',			'Error_Tax_Form_Initial_Date_Type_Extension_2');
define('ERROR_STRING_TAX_FORM_INITIAL_DUE_DATE_ID',					'Please select a Tax Form.');
define('ERROR_STRING_TAX_FORM_INITIAL_DUE_DATE_TYPE',				'Please select a Type.');
define('ERROR_STRING_TAX_FORM_INITIAL_DUE_DATE',					'Initial Due Date is either empty or not a valid format (MM/DD).');
define('ERROR_STRING_TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_1',	'1st Extension is either empty or not a valid format (MM/DD).');
define('ERROR_STRING_TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_2',	'2nd Extension is either empty or not a valid format (MM/DD).');
define('LABEL_TAX_FORM_INITIAL_DUE_DATE',							'Initial Due Date:');
define('LABEL_TAX_FORM_INITIAL_DUE_DATE_ID',						'Tax Form Initial Date:');
define('LABEL_TAX_FORM_INITIAL_DUE_DATE_TYPE',						'Type:');
define('LABEL_TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_1',			'1st Extension');
define('LABEL_TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_2',			'2nd Extension');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// TAX MONTH
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('TAX_MONTH',										'Tax_Month_Label');
define('TAX_MONTH_ID',									'Tax_Month_ID');
define('ERROR_TAX_MONTH_ID',							'Error_Tax_Month_ID');
define('ERROR_STRING_TAX_MONTH_ID',						'Please select a Tax Month.');
define('LABEL_TAX_MONTH_ID',							'1st Month of Tax Year:');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// TAX QUARTER
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('TAX_QUARTER',									'Tax_Quarter_Label');
define('TAX_QUARTER_1',									'1st Quarter');
define('TAX_QUARTER_2',									'2nd Quarter');
define('TAX_QUARTER_3',									'3rd Quarter');
define('TAX_QUARTER_4',									'4th Quarter');
define('TAX_QUARTER_ID',								'Tax_Quarter_ID');
define('ERROR_TAX_QUARTER_ID',							'Error_Tax_Quarter');
define('ERROR_STRING_TAX_QUARTER_ID',					'Please select a Tax Quarter.');
define('LABEL_TAX_QUARTER_ID',							'Tax Quarter:');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// TAX YEAR
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
define('TAX_YEAR_ID',									'Tax_Year_ID');
define('TAX_YEAR',										'Tax_Year_Label');
define('LABEL_TAX_YEAR_ID',								'Tax Year:');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 ?>