<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	if (defined('ENVIRONMENT') AND file_exists(APPPATH.'config/'.ENVIRONMENT.'/constants'.EXT))
	{
		require(APPPATH.'config/'.ENVIRONMENT.'/constants_client'.EXT);
		require(APPPATH.'config/'.ENVIRONMENT.'/constants_institution'.EXT);
		require(APPPATH.'config/'.ENVIRONMENT.'/constants_misc'.EXT);
		require(APPPATH.'config/'.ENVIRONMENT.'/constants_staff'.EXT);
		require(APPPATH.'config/'.ENVIRONMENT.'/constants_statement'.EXT);
		require(APPPATH.'config/'.ENVIRONMENT.'/constants_tax_return'.EXT);
		require(APPPATH.'config/'.ENVIRONMENT.'/constants_tax_return_history'.EXT);
	}
	else
	{
		require(APPPATH.'config/constants_client'.EXT);
		require(APPPATH.'config/constants_institution'.EXT);
		require(APPPATH.'config/constants_misc'.EXT);
		require(APPPATH.'config/constants_staff'.EXT);
		require(APPPATH.'config/constants_statement'.EXT);
		require(APPPATH.'config/constants_tax_return'.EXT);
		require(APPPATH.'config/constants_tax_return_history'.EXT);
	}

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 								0644);
define('FILE_WRITE_MODE',								0666);
define('DIR_READ_MODE', 								0755);
define('DIR_WRITE_MODE', 								0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',									'rb');
define('FOPEN_READ_WRITE',								'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',				'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',			'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',							'ab');
define('FOPEN_READ_WRITE_CREATE',						'a+b');
define('FOPEN_WRITE_CREATE_STRICT',						'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',				'x+b');



/* End of file constants.php */
/* Location: ./application/config/constants.php */
define('PARTNER_ID',									'Parnter_ID');
define('RETAINER_YEAR',									'RetainerYear');
define('ROLE_ID', 										'Role_ID');
define('TAX_FORM',										'Tax_Form_Name');
define('TAX_FORM_EXTENTION_1',							'Extension_1');
define('TAX_FORM_EXTENTION_2',							'Extension_2');
define('TAX_FORM_INITIAL_DUE_DATE', 					'Initial_Due_Date');
define('TAX_FORM_TABLE',								'Tax_Form');
define('TAX_FORM_DATE',									'Tax_Form_Date');
define('TAX_FORM_ID',									'Tax_Form_ID');
//////////////////////////////////////////// TAX RETURN HISTORY TABLE/FIELDS/VIEW/VIEW FIELDS ////////////////////////////////////////////

//////////////////////////////////////////// TAX RETURN TABLE/FIELDS/VIEW/VIEW FIELDS ////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
define('ERROR_COMPANY_NAME',							'Error_Company_Name');
define('ERROR_REVIEWER_1_ID',							'Error_Reviewer_1_ID');
define('ERROR_REVIEWER_2_ID',							'Error_Reviewer_2_ID');
define('ERROR_TAX_FORM_DATE',							'Error_Tax_Form_Date');
define('ERROR_TAX_FORM_ID',								'Error_Tax_Form');
/////////////////////////////////////////////////////////////////////////////////
define('ERROR_STRING_COMPANY_NAME',						'Please enter a Company Name.');
define('ERROR_STRING_REVIEWER_1_ID',					'Please select a 1st Reviewer.');
define('ERROR_STRING_REVIEWER_2_ID',					'Please select a 2nd Reviewer');
define('ERROR_STRING_TAX_FORM_DATE',					'Please enter a Tax Form Date.');
define('ERROR_STRING_TAX_FORM_ID', 						'Please select a Tax Form.');
/////////////////////////////////////////////////////////////////////////////////
define('LABEL_COMPANY_NAME',							'Company Name:');
define('LABEL_LAST_EDIT_DATE',							'Last Edited On:');
define('LABEL_LAST_EDIT_USER_ID',						'Last Edited By:');
define('LABEL_STAMP_DATE',								'Created On:');
define('LABEL_STAMP_USER_ID',							'Created By:');
define('LABEL_TAX_FORM_DATE',							'Tax Form Date:');
define('LABEL_TAX_FORM_ID',								'Tax Form:');
define('LABEL_TAX_REPORT_DATE',							'Tax Report Date:');
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 ?>