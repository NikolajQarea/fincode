<div id="content" class="clearfix">
<?php echo form_open(base_url() . 'client/add_client/'); ?>
	<fieldset style="width: 725px;">
		<legend>
			Add New Client Information
		</legend>
		<table class="editform">
			<colgroup>
				<col style="white-space: nowrap; width: 25%;" />
				<col style="width: 40%;"/>
				<col style="width: 45%;"/>
			</colgroup>
			<tr>
				<td colspan="3">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_CLIENT_TYPE_ID ?>
				</td>
				<td>
					<?= str_replace('"input"', '"input client_type"', $Client_Types) ?>
				</td>
				<td class="errors">
					<?php if ($this->session->userdata(ROLE_ADMIN)): ?>
					<img class="quick_add_client_type pointer" src ="<?php print $this->config->item('base_url'); ?>application/views/css/images/add.png" />
					<?php endif; ?>
					<?php print $this->session->flashdata(ERROR_CLIENT_TYPE_ID) ?>
				</td>
			</tr>
			<?php if ($this->session->userdata(ROLE_ADMIN)): ?>
			<tr class="Company">
				<td>
					<?= REQUIRED_FIELD . LABEL_COMPANY_NAME ?>
				</td>
				<td>
					<input class="input" id="tbNameCompany" type="text" name="client[<?= COMPANY_NAME ?>]" value ="<?php print $this->session->flashdata(COMPANY_NAME) ?>">
				</td>
				<td class="errors">
					<?php print $this->session->flashdata(ERROR_COMPANY_NAME) ?>
				</td>
			</tr>
			<tr class="Client">
				<td>
					<?= REQUIRED_FIELD . LABEL_CLIENT_NAME_LAST ?>
				</td>
				<td>
					<input class="input" id="tbClientNameLast" type="text" name="client[<?= CLIENT_NAME_LAST ?>]" value ="<?php print $this->session->flashdata(CLIENT_NAME_LAST) ?>">
				</td>
				<td class="errors">
					<?php print $this->session->flashdata(ERROR_CLIENT_NAME_LAST) ?>
				</td>
			</tr>
			<tr class="Client">
				<td>
					<?= REQUIRED_FIELD . LABEL_CLIENT_NAME_FIRST ?>
				</td>
				<td>
					<input class="input" id="tbClientNameFirst" type="text" name="client[<?= CLIENT_NAME_FIRST ?>]" value ="<?php print $this->session->flashdata(CLIENT_NAME_FIRST) ?>">
				</td>
				<td class="errors">
					<?php print $this->session->flashdata(ERROR_CLIENT_NAME_FIRST) ?>
				</td>
			</tr>
			<?php else : ?>
			<tr class="Company">
				<td>
					<?= REQUIRED_FIELD . LABEL_COMPANY_NAME ?>
				</td>
				<td>
					<input class="input" id="tbNameCompany" style='display: none' type="text" name="client[<?= COMPANY_NAME ?>]" value ="<?php print $this->session->flashdata(COMPANY_NAME) ?>">
					<?php print $this->session->flashdata(COMPANY_NAME) ?>
				</td>
				<td class="errors">
					<?php print $this->session->flashdata(ERROR_COMPANY_NAME) ?>
				</td>
			</tr>
			<tr class="Client">
				<td>
					<?= REQUIRED_FIELD . LABEL_CLIENT_NAME_LAST ?>
				</td>
				<td>
					<input class="input" id="tbClientNameLast" type="text" style='display: none' name="client[<?= CLIENT_NAME_LAST ?>]" value ="<?php print $this->session->flashdata(CLIENT_NAME_LAST) ?>">
					<?php print $this->session->flashdata(CLIENT_NAME_LAST) ?>
				</td>
				<td class="errors">
					<?php print $this->session->flashdata(ERROR_CLIENT_NAME_LAST) ?>
				</td>
			</tr>
			<tr class="Client">
				<td>
					<?= REQUIRED_FIELD . LABEL_CLIENT_NAME_FIRST ?>
				</td>
				<td>
					<input class="input" id="tblClientNameFirst" type="text" style='display: none' name="client[<?= CLIENT_NAME_FIRST ?>]" value ="<?php print $this->session->flashdata(CLIENT_NAME_FIRST) ?>">
					<?php print $this->session->flashdata(CLIENT_NAME_FIRST) ?>
				</td>
				<td class="errors">
					<?php print $this->session->flashdata(ERROR_CLIENT_NAME_FIRST) ?>
				</td>
			</tr>
			<?php endif; ?>
			<tr class="client_form">
				<td>
					<?= LABEL_CLIENT_SSN_EIN ?>
				</td>
				<td>
					<input class="input" id="tbClientSSN_EIN" type="text" name="client[<?= CLIENT_SSN_EIN ?>]" value ="<?php print $this->session->flashdata(CLIENT_SSN_EIN) ?>">
					<br />
					<?= CONTROL_SSN_EIN_FORMAT ?>
				</td>
				<td class ="errors">
					<?php print $this->session->flashdata(ERROR_CLIENT_SSN_EIN) ?>
				</td>
			</tr>
			<tr class="client_form">
				<td>
					<?= LABEL_CLIENT_OCCUPATION_ID ?>
				</td>
				<td>
					<?= str_replace('"input"', '"input occupation"', $Client_Occupations) ?>
				</td>
				<td class="errors">
					<?php if ($this->session->userdata(ROLE_ADMIN)): ?>
					<img class="quick_add_occupation pointer" alt="Add a new Occupation" src ="<?php print $this->config->item('base_url'); ?>application/views/css/images/add.png" title="Add a new Occupation" />
					<?php endif; ?>
					<?php print $this->session->flashdata(ERROR_CLIENT_OCCUPATION_ID) ?>
				</td>
			</tr>
			<tr class="client_form">
				<td colspan="3">
					<hr />
				</td>
			</tr>
			<?php if ($this->session->userdata(ROLE_ADMIN)): ?>
			<tr class="Client">
				<td>
					<?= LABEL_SPOUSE_NAME_LAST ?>
				</td>
				<td>
					<input class="input" id="tbSpouseNameLast" type="text" name="client[<?= SPOUSE_NAME_LAST ?>]" value ="<?php print $this->session->flashdata(SPOUSE_NAME_LAST) ?>">
				</td>
				<td class="errors">
					<?php print $this->session->flashdata(ERROR_SPOUSE_NAME_LAST) ?>
				</td>
			</tr>
			<tr class="Client">
				<td>
					<?= LABEL_SPOUSE_NAME_FIRST ?>
				</td>
				<td>
					<input class="input" id="tbSpouseNameFirst" type="text" name="client[<?= SPOUSE_NAME_FIRST ?>]" value ="<?php print $this->session->flashdata(SPOUSE_NAME_FIRST) ?>">
				</td>
				<td class="errors">
					<?php print $this->session->flashdata(ERROR_SPOUSE_NAME_FIRST) ?>
				</td>
			</tr>
			<?php else : ?>
			<tr class="Client">
				<td>
					<?= LABEL_SPOUSE_NAME_LAST ?>
				</td>
				<td>
					<?php print stripslashes($data[SPOUSE_NAME_LAST]) ?>
					<input class="input" id="tbSpouseNameLast" style='display: none' type="text" name="client[<?= SPOUSE_NAME_LAST ?>]" value ="<?php print $this->session->flashdata(SPOUSE_NAME_LAST) ?>">
				</td>
				<td class="errors">
					<?php print $this->session->flashdata(ERROR_SPOUSE_NAME_LAST) ?>
				</td>
			</tr>
			<tr class="Client">
				<td>
					<?= LABEL_SPOUSE_NAME_FIRST ?>
				</td>
				<td>
					<?php print stripslashes($data[SPOUSE_NAME_FIRST]); ?>
					<input class="input" id="tbSpouseNameFirst" type="text" style='display: none' name="client[<?= SPOUSE_NAME_FIRST ?>]" value ="<?php print $this->session->flashdata(SPOUSE_NAME_FIRST) ?>">
				</td>
				<td class="errors">
					<?php print $this->session->flashdata(ERROR_SPOUSE_NAME_FIRST) ?>
				</td>
			</tr>
			<?php endif; ?>
			<tr class="Client">
				<td>
					<?= LABEL_SPOUSE_SSN_EIN ?>
				</td>
				<td>
					<input class="input ssn" id="tbSpouseSSN" type="text" name="client[<?= SPOUSE_SSN_EIN ?>]" value ="<?php print $this->session->flashdata(SPOUSE_SSN_EIN) ?>">
					<br />
					<?= CONTROL_SSN_FORMAT ?>
				</td>
				<td class ="errors">
					<?php print $this->session->flashdata(ERROR_SPOUSE_SSN_EIN) ?>
				</td>
			</tr>
			<tr class="Client">
				<td>
					<?= LABEL_SPOUSE_OCCUPATION_ID ?>
				</td>
				<td>
					<?= str_replace('"input"', '"input occupation"', $Spouse_Occupations) ?>
				</td>
				<td class="errors">
					<?php if ($this->session->userdata(ROLE_ADMIN)): ?>
					<img class="quick_add_occupation pointer" alt="Add a new Occupation" src ="<?php print $this->config->item('base_url'); ?>application/views/css/images/add.png" title="Add a new Occupation"/>
					<?php endif; ?>
					<?php print $this->session->flashdata(ERROR_SPOUSE_OCCUPATION_ID) ?>
				</td>
			</tr>
			<tr class="Client">
				<td colspan="3">
					<hr />
				</td>
			</tr>
			<tr class="client_form">
				<td style="vertical-align: top;">
					<?= LABEL_CLIENT_TAX_FORM_IDS ?>
				</td>
				<td style="vertical-align: top;">
					<?= str_replace('"input"', '"input TaxForm"', $Tax_Forms) ?>
				</td>
				<td class="errors" style="vertical-align: top;">
					<?= CONTROL_MULTI_SELECT_TEXT ?>
					<?php print $this->session->flashdata(ERROR_TAX_FORM_ID) ?>
				</td>
			</tr>
			<tr class="client_form">
				<td class="tdOrganizer">
					<?= LABEL_CLIENT_ORGANIZER_ID ?>
				</td>
				<td class="tdOrganizer">
					<input class="cbOrganizer" type="checkbox" id="client[<?= CLIENT_ORGANIZER_ID ?>]" name="client[<?= CLIENT_ORGANIZER_ID ?>]" title="Is Organizer? (Form 1040 Only)"<?= $this->session->flashdata(CLIENT_ORGANIZER_ID) == 1 ? ' checked" ' : '' ?> />
					<?= CONTROL_ORGANIZER_1040 ?>
				</td>
				<td class="errors tdOrganizer">
					<?= $this->session->flashdata(ERROR_CLIENT_ORGANIZER_ID) ?>
				</td>
			</tr>
			<tr class="client_form">
				<td style="vertical-align: top;">
					<?= LABEL_CLIENT_STATE_ID ?>
				</td>
				<td style="vertical-align: top;">
					<?= $States ?>
				</td>
				<td class="errors" style="vertical-align: top;">
					<?= CONTROL_MULTI_SELECT_TEXT ?>
					<?php print $this->session->flashdata(ERROR_CLIENT_STATE_ID) ?>
				</td>
			</tr>
			<tr class="client_form">
				<td>
					<?= LABEL_CLIENT_STAFF_ID ?>
				</td>
				<td>
					<?= $Staff ?>
				</td>
				<td class ="errors">
					<?php print $this->session->flashdata(ERROR_CLIENT_STAFF_ID) ?>
				</td>
			</tr>
			<tr class="client_form">
				<td>
					<?= REQUIRED_FIELD . LABEL_CLIENT_PARTNER_ID ?>
				</td>
				<td>
					<?= $Partners ?>
				</td>
				<td class ="errors">
					<?php print $this->session->flashdata(ERROR_CLIENT_PARTNER_ID) ?>
				</td>
			</tr>
			<tr class="client_form">
				<td>
					<?= LABEL_CLIENT_BILLING_ID ?>
				</td>
				<td>
					<input class="input" id="tbBillingID" type="text" name="client[<?= CLIENT_BILLING_ID ?>]" value ="<?php print $this->session->flashdata(CLIENT_BILLING_ID) ?>">
				</td>
				<td class="errors">
					<?php print $this->session->flashdata(ERROR_CLIENT_BILLING_ID) ?>
				</td>
			</tr>
			<tr class="client_form">
				<td>
					<?= LABEL_CLIENT_DOCUMENT_ID ?>
				</td>
				<td>
					<input class="input" id="tbDocumentID" type="text" name="client[<?= CLIENT_DOCUMENT_ID ?>]" value ="<?php print $this->session->flashdata(CLIENT_DOCUMENT_ID) ?>">
				</td>
				<td class="errors">
					<?php print $this->session->flashdata(ERROR_CLIENT_DOCUMENT_ID) ?>
				</td>
			</tr>
			<tr class="client_form">
				<td colspan="3">
					<hr />
				</td>
			</tr>
			<tr class="client_form">
				<td>
					<?= LABEL_CLIENT_MONTH_ID ?>
				</td>
				<td>
					<?= $Tax_Months ?>
				</td>
				<td class="errors">
					<?php print $this->session->flashdata(ERROR_CLIENT_MONTH_ID) ?>
				</td>
			</tr>
			<tr class="client_form">
				<td>
					<?= LABEL_CLIENT_INITIAL_YEAR_ID ?>
				</td>
				<td>
					<?= $Initial_Years ?>
				</td>
				<td class="errors">
					<input class="manual_initial" name="" type="text" size="2" min="1980" max="2099" maxlength="4" style="display: none">
					<?php print $this->session->flashdata(ERROR_CLIENT_INITIAL_YEAR_ID) ?>
				</td>
			</tr>
			<tr class="client_form">
				<td>
					<?= LABEL_CLIENT_RETAINER_STATUS_ID ?>
				</td>
				<td>
					<?= $Retainer_Status ?>
				</td>
				<td class="errors">
					<?php print $this->session->flashdata(ERROR_CLIENT_RETAINER_STATUS_ID) ?>
				</td>
			</tr>
			<tr class="client_form">
				<td>
					<?= LABEL_CLIENT_RETAINER_YEAR ?>
				</td>
				<td>
					<?= $Retainer_Years ?>
				</td>
				<td class="errors">
					<input class ="input" class="manual_retainer" name="" type="text" size="2" min="1980" max="2099" maxlength="4" style="display: none">
					<?php print $this->session->flashdata(ERROR_CLIENT_RETAINER_YEAR) ?>
				</td>
			</tr>
			<tr class="client_form">
				<td colspan="3">
					<hr />
				</td>
			</tr>
			<tr class="client_form">
				<td>
					<?= LABEL_CLIENT_ACCOUNTING_SOFTWARE_ID ?>
				</td>
				<td>
					<?= $Accounting_Packages ?>
				</td>
				<td class="errors">
					<?php print $this->session->flashdata(ERROR_CLIENT_ACCOUNTING_SOFTWARE_ID) ?>
				</td>
			</tr>
			<tr class="client_form">
				<td>
					<label for="client[<?= CLIENT_PRINCIPAL ?>]">
						<span class="errors">
							&nbsp;
						</span>
						<?= LABEL_CLIENT_PRINCIPAL ?>
					</label>
				</td>
				<td>
					<input class="input" id="tbPrincipal" type="text" name="client[<?= CLIENT_PRINCIPAL ?>]" value ="<?php print $this->session->flashdata(CLIENT_PRINCIPAL) ?>">
				</td>
				<td class="errors">
					<?php print $this->session->flashdata(ERROR_CLIENT_PRINCIPAL) ?>
				</td>
			</tr>
			<tr class="client_form">
				<td>
					<?= LABEL_CLIENT_REFERRED_BY_ID ?>
				</td>
				<td>
					<?= str_replace('"input"', '"input referred_by"', $Referred_Bys) ?>
				</td>
				<td class="errors">
					<?php if ($this->session->userdata(ROLE_ADMIN)): ?>
					<img class="quick_add_referred_by pointer" alt="Add a new Referred By record" src ="<?php print $this->config->item('base_url'); ?>application/views/css/images/add.png" title="Add a new Referred By record"/>
					<?php endif; ?>
				</td>
			</tr>
			<tr class="client_form">
				<td>
					<?= LABEL_CLIENT_NOTES ?>
				</td>
				<td>
					<textarea class="input" id="tbClientNotes" name="client[<?= CLIENT_NOTES ?>]" style="height: 100px;"><?php print stripslashes(trim($this->session->flashdata(CLIENT_NOTES))) ?></textarea>
				</td>
				<td class="errors">
					<?php print $this->session->flashdata(ERROR_CLIENT_NOTES) ?>

				</td>
			</tr>
			<tr class="client_form">
				<td>
					<?= REQUIRED_FIELD . LABEL_CLIENT_STATUS_ID ?>
				</td>
				<td>
					<?= $Client_Status ?>
				<td class ="errors">
					<?php print $this->session->flashdata(ERROR_CLIENT_STATUS_ID) ?>
				</td>
			</tr>
			<tr class="client_form">
				<td colspan="3">
					<hr />
				</td>
			</tr>
			<tr class="client_form">
				<td align="right">
					<input class ="submit" onClick="history.go(-1)" type="button" value ="Cancel" style="width: 95%;">
				</td>
				<td align="left">
					<input class="submit" type="submit" value="Save" style="width: 95%;">
				</td>
				<td class="errors">
					&nbsp;
				</td>
			</tr>
		</table>
	</fieldset>
	<?php echo form_close(); ?>
	<div class="clearfix">
		&nbsp;
	</div>
	<div class="overlay" <?php print $this->session->flashdata('add_occupation') || $this->session->flashdata('error_add_occupation') ? 'style="display: table-row;"' : '' ?>>
	</div>
	<div class="add_new_occupation">
		<img align="right" class="cancel" src ="<?php print $this->config->item('base_url'); ?>application/views/css/images/exit_add_occupation.png"/>
		<?php print form_open(base_url() . 'client/add_occupation'); ?>
		<table cellpadding="5px" cellspasing="5px">
			<tr>
				<td align="center">
					<span class="message_success_occupation"></span>
					<span class ="error"></span>
				</td>
			</tr>
			<tr>
				<td>
					Enter New Occupation
				</td>
			</tr>
			<tr>
				<td>
					<input class ="input" id="add_occupation" type="text" name="add_occupation" value ="<?php print $this->session->flashdata('add_occupation') ?>">
				</td>
			</tr>
			<tr>
				<td align="center"><input class="submit ok_occupation" type="button" value="Add">
					<input class="submit cancel_occupation" type="button" value="Cancel">
				</td>
			</tr>
		</table>
		<?php print form_close(); ?>
	</div>
	<div class="add_new_client_type">
		<img align="right" class="cancel_client_type" src ="<?php print $this->config->item('base_url'); ?>application/views/css/images/exit_add_occupation.png"/>
		<?php print form_open(base_url() . 'client/add_client_type'); ?>
		<table cellpadding="5px" cellspasing="5px">
			<tr>
				<td align="center">
					<span class="message_success_client_type"></span>
					<span class ="error"></span>
				</td>
			</tr>
			<tr>
				<td>
					Enter a new Client Type
				</td>
			</tr>
			<tr>
				<td>
					<input class ="input" id="add_client_type" type="text" name="add_client_type" value ="">
				</td>
			</tr>
			<tr>
				<td align="center">
					<input class="submit ok_client_type" type="button" value="Add">
					<input class="submit cancel_client_type" type="button" value="Cancel">
				</td>
			</tr>
		</table>
		<?php print form_close(); ?>
	</div>
	<div class="add_new_referred_by">
		<img align="right" class="cancel_referred_by" src ="<?php print $this->config->item('base_url'); ?>application/views/css/images/exit_add_occupation.png"/>
		<?php print form_open(base_url() . 'client/add_referred_by'); ?>
		<table cellpadding="5px" cellspasing="5px">
			<tr>
				<td align="center">
					<span class="message_success_referred_by"></span>
					<span class ="error"></span>
				</td>
			</tr>
			<tr>
				<td>
					Enter a new Referred By
				</td>
			</tr>
			<tr>
				<td>
					<input class ="input" id="add_referred_by" type="text" name="add_referred_by" value ="">
				</td>
			</tr>
			<tr>
				<td align="center">
					<input class="submit ok_referred_by" type="button" value="Add">
					<input class="submit cancel_referred_by" type="button" value="Cancel">
				</td>
			</tr>
		</table>
		<?php print form_close(); ?>
	</div>
</div>
<script type="text/jscript">

	$(document).ready(function ()
	{
		var <?= CLIENT_TYPE_ID ?> = $('select[name="client[<?= CLIENT_TYPE_ID ?>]"]').val();
		if(<?= CLIENT_TYPE_ID ?> == '<?= PLEASE_SELECT_ID ?>')
	    {
	    	
	    	$(".Company").css("display","none");
	    	$(".Client").css("display", "none");
	    	$(".client_form").css("display","none");
	    }
	    else if(<?= CLIENT_TYPE_ID ?> == '31')
	    {
	    	$(".Company").css("display","none");
	    	$(".Client").css("display", "table-row");
	    	$(".client_form").css("display", "table-row");
	    }
	    else
	    {
	    	$(".Company").css("display", "table-row");
	    	$(".Client").css("display","none");
	    	$(".client_form").css("display", "table-row");
	    };
		$(".tdOrganizer").css("display","none");
		$("select.TaxForm option:selected").each(function ()
		{
			if($(this).text() == '1040')
			{
				$(".tdOrganizer").css("display","table-cell");
			}
		});
		if($(".tdOrganizer").css("display") == "none")
		{
			$(".cbOrganizer").attr('checked', false);
		};
		jQuery(function($)
		{
			$("#tbNameCompany").Watermark("<?= JS_COMPANY_NAME ?>");
			$("#tbClientNameLast").Watermark("<?= JS_CLIENT_LAST_NAME ?>");
			$("#tbClientNameFirst").Watermark("<?= JS_CLIENT_FIRST_NAME ?>");
			$("#tbClientSSN_EIN").Watermark("<?= JS_CLIENT_SSN_EIN ?>");
			$("#tbSpouseNameLast").Watermark("<?= JS_SPOUSE_LAST_NAME ?>");
			$("#tbSpouseNameFirst").Watermark("<?= JS_SPOUSE_FIRST_NAME ?>");
			$("#tbSpouseSSN").Watermark("<?= JS_SPOUSE_SSN ?>");
			$("#tbBillingID").Watermark("<?= JS_CLIENT_BILLING_ID ?>");
			$("#tbDocumentID").Watermark("<?= JS_CLIENT_DOCUMENT_ID ?>");
			$("#tbPrincipal").Watermark("<?= JS_CLIENT_PRINCIPAL ?>");
			$("#tbClientNotes").Watermark("<?= JS_CLIENT_NOTES ?>");
		});
		$("select.TaxForm").change(function ()
		{
			$(".tdOrganizer").css("display","none");
			$("select.TaxForm option:selected").each(function ()
			{
				if($(this).text() == '1040')
				{
					$(".tdOrganizer").css("display","table-cell");
				}
			});
			if($(".tdOrganizer").css("display") == "none")
			{
				$(".cbOrganizer").attr('checked', false);
			};
		})
	});
</script>