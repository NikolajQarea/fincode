<div id="content" class="clearfix">
	<fieldset style="width: 650px;">
		<legend>
			Remove Client:
		</legend>
		<div>&nbsp;</div>
		<?php echo form_open(base_url() . 'client/remove');  ?>
		<table style="width: 95%;">
			<colgroup>
				<col style="width: 25%;" />
				<col style="width: 60%;" />
				<col style="width: 15%;" />
			</colgroup>
                        <?php if ($success == 'success') { ?>
                                    <div id="remove_success" style="display: none; padding: 7px; border: 1px dotted green; background-color: greenyellow; opacity: 0.5; text-align: center;  width: 97%; margin-bottom: 25px;">
                                        <span>Client is deleted.</span>
                                    </div>
                        <?php } if ($success == 'error') { ?>
                                    <div id="remove_error" style="display: none; padding: 7px; border: 1px dotted red; background-color: lightpink; opacity: 0.5; text-align: center;  width: 97%; margin-bottom: 25px;">
                                        <span>This Client does not exist. Use the drop-down hint!</span>
                                    </div>
                        <?php } ?>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_CLIENT_ID ?>
				</td>
				<td>
					<input id="search" class="input client_select" name="remove_client[<?= STATEMENT_VIEW_CLIENT_NAME ?>]" type="text" value="<?= $this->session->flashdata(STATEMENT_VIEW_CLIENT_NAME) ? $this->session->flashdata(STATEMENT_VIEW_CLIENT_NAME) : null ?>"  />
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STATEMENT_CLIENT_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<input class ="submit" onClick="history.go(-1)" type="button" value ="Cancel" style="width: 95%; margin-top: 30px;">
				</td>
				<td>
					<input class="submit" type="submit" value="Remove" style="width: 100%; margin-top: 30px;">
				</td>
				<td>
					&nbsp;
				</td>
			</tr>
		</table>
		<?php echo form_close(); ?>
	</fieldset>
</div>
<script type="text/javascript">

        $(document).ready(function()
        {
    
        var getPartnerAndForm = function() {
                        $.ajax({
                                type: "POST",
                                url: "<?= $this->config->item('base_url') . TAX_RETURN_POST ?>/fill_select",
                                dataType: "json",
                                data: "<?= CLIENT_NAME ?>="+encodeURIComponent($('.client_select').val()),
                                success: function(response)
                                {
                                        if(response.found == '1') {
                                                $('#select_partner_data').html(response.<?= CLIENT_PARTNER_ID ?>);
                                                $('#select_form_data').html(response.<?= TAX_FORM_ID ?>);
                                        } else {
                                                $('#select_partner_data').html('First select a Client.');
                                                $('#select_form_data').html('First select a Client.');
                                        }
                                }
                        });
                }

                clientAutocomplete($('#search'), { 
                        select: function($search) { 
                                getPartnerAndForm();
                        }
                });
    
                jQuery(function($)
                {
                        $("#<?= STATEMENT_ACCOUNT_NUMBER ?>").Watermark("<?= JS_STATEMENT_ACCOUNT_NUMBER ?>");
                        $("#<?= STATEMENT_ABA_NUMBER ?>").Watermark("<?= JS_STATEMENT_ABA_NUMBER ?>");
                        $("#search").Watermark("<?= JS_CLIENT_NAME ?>");
                        $("#<?= STATEMENT_START_DATE ?>").mask("99/99/9999");
                        $("#<?= STATEMENT_START_DATE ?>").Watermark("<?= JS_STATEMENT_START_DATE ?>");
                        $("#<?= STATEMENT_END_DATE ?>").mask("99/99/9999");
                        $("#<?= STATEMENT_END_DATE ?>").Watermark("<?= JS_STATEMENT_END_DATE ?>");
                        $("#<?= STATEMENT_LAST_RECONCILIATION_DATE ?>").mask("99/99/9999");
                        $("#<?= STATEMENT_LAST_RECONCILIATION_DATE ?>").Watermark("<?= JS_STATEMENT_LAST_RECONCILIATION_DATE ?>");
                        $("#<?= STATEMENT_NOTES ?>").Watermark("<?= JS_STATEMENT_NOTES ?>");
                });


              $("#remove_success").delay(500).slideDown(300).delay(5000).slideUp(300);
              $("#remove_error").delay(500).slideDown(300).delay(5000).slideUp(300);
              
                $("form").submit(function() {
                  if (confirm('Are you sure you want to delete the client?')) {
                      return true;
                  }
                  return false;
                });            


         });
</script>
