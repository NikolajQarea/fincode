<div id="content" class="clearfix">
	<fieldset style="width: 725px;">
		<legend>
			<?php print stripslashes($data[CLIENT_VIEW_CLIENT_NAME]) ; ?>
		</legend>
		<table class="notes">
			<colgroup>
				<col class="label" style="white-space: nowrap; width: 22%;" />
				<col class="data" style="width: 78%;"/>
			</colgroup>
			<tr>
				<td>
					&nbsp;
				</td>
				<td align="right">
					<a style="text-decoration: none;" href="<?php print base_url() ?>client/edit/<?php print $data[CLIENT_VIEW_CLIENT_ID] ?>">
						Edit&nbsp;
						<img src="<?php print $this->config->item('base_url'); ?>application/views/css/images/edit.png" width="15px" />
					</a>
				</td>
			</tr>
			<?php if(isset($data[CLIENT_VIEW_CLIENT_NAME_LAST]) && $data[CLIENT_VIEW_CLIENT_NAME_LAST] != null && isset($data[CLIENT_VIEW_CLIENT_NAME_FIRST]) && $data[CLIENT_VIEW_CLIENT_NAME_FIRST] != null) : ?>
			<tr>
				<td class="label">
					<?= LABEL_CLIENT_NAME_LAST ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_CLIENT_NAME_LAST])) ?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_CLIENT_NAME_FIRST ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_CLIENT_NAME_FIRST])) ?>
				</td>
			</tr>
			<?php elseif(isset($data[CLIENT_VIEW_COMPANY_NAME]) && $data[CLIENT_VIEW_COMPANY_NAME] != NULL) : ?>
			<tr>
				<td class="label">
					<?= LABEL_COMPANY_NAME ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_COMPANY_NAME])); ?>
				</td>
			</tr>
			<?php endif; ?>
			<tr>
				<td class="label">
					<?= LABEL_CLIENT_TYPE_ID ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_CLIENT_TYPE])) ?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_CLIENT_RETAINER_STATUS_ID ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_RETAINER_STATUS])) ?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_CLIENT_RETAINER_YEAR ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_RETAINER_YEAR])) ?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_CLIENT_STATUS_ID ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_CLIENT_STATUS])) ?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= CLIENT_SSN_EIN ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_CLIENT_SSN_EIN])) ?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_CLIENT_OCCUPATION_ID ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_CLIENT_OCCUPATION])) ?>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<hr />
				</td>
			</tr>
			<?php if(isset($data[CLIENT_VIEW_CLIENT_NAME_LAST]) && $data[CLIENT_VIEW_CLIENT_NAME_LAST] != null && isset($data[CLIENT_VIEW_CLIENT_NAME_FIRST]) && $data[CLIENT_VIEW_CLIENT_NAME_FIRST] != null) : ?>
			<tr>
				<td class="label">
					<?= LABEL_SPOUSE_NAME_LAST ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_SPOUSE_NAME_LAST])) ?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_SPOUSE_NAME_FIRST ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_SPOUSE_NAME_FIRST])) ?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_SPOUSE_SSN_EIN ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_SPOUSE_SSN_EIN])) ?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_SPOUSE_OCCUPATION_ID ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_SPOUSE_OCCUPATION])) ?>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<hr />
				</td>
			</tr>
			<?php endif; ?>
			<tr>
				<td class="label">
					<?= LABEL_CLIENT_TAX_FORM_IDS ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_TAX_FORMS])) ?>
				</td>
			</tr>
			<?php $forms = explode(',', $data[CLIENT_VIEW_TAX_FORM_IDS]); ?>
			<?php foreach ($forms as $form) : ?>
			<?php if (trim($form) == 3) : ?>
			<tr>
				<td class="label">
					<?= LABEL_CLIENT_ORGANIZER_ID ?>
				</td>
				<td class="data">
					<?= $data[CLIENT_VIEW_ORGANIZER] ?>
				</td>
			</tr>
			<?php endif; ?>
			<?php endforeach; ?>
			<tr>
				<td class="label">
					<?= LABEL_STATE_ID ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_STATES])) ?>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<hr />
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_CLIENT_STAFF_ID ?>
				</td>
				<td class="data">
					<?php print ($data[CLIENT_VIEW_STAFF_NAME] != '' ? stripslashes(trim($data[CLIENT_VIEW_STAFF_NAME])) . ' (' . stripslashes(trim($data[CLIENT_VIEW_STAFF_LOGIN])) . ')' : 'N/A'); ?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_CLIENT_PARTNER_ID ?>
				</td>
				<td class="data">
					<?php print ($data[CLIENT_VIEW_PARTNER_NAME] != '' ? stripslashes(trim($data[CLIENT_VIEW_PARTNER_NAME])) . ' (' . trim(stripslashes($data[CLIENT_VIEW_PARTNER_LOGIN])) . ')' : 'N/A'); ?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_CLIENT_BILLING_ID ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_CLIENT_BILLING_ID])) ?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_CLIENT_DOCUMENT_ID ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_CLIENT_DOCUMENT_ID])) ?>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<hr />
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_TAX_MONTH_ID ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_MONTH])) ?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_CLIENT_INITIAL_YEAR_ID ?>
				</td>
				<td class="data">
					<?php print $data[CLIENT_VIEW_INITIAL_YEAR] ?>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<hr />
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_CLIENT_ACCOUNTING_SOFTWARE_ID ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_ACCOUNTING_SOFTWARE])) ?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_CLIENT_PRINCIPAL ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_CLIENT_PRINCIPAL])) ?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_CLIENT_REFERRED_BY_ID ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_REFERRED_BY])) ?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_CLIENT_NOTES ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_CLIENT_NOTES])) ?>
				</td>
			</tr>
		</table>
	</fieldset>
	<div class="clearfix">
		&nbsp;
	</div>
	<fieldset style="width: 725px; text-align: left;">
		<legend>
			Other Information
		</legend>
		<table class="notes">
			<colgroup>
				<col style="width: 100px;" />
				<col style="width: 225px;" />
				<col style="width: 100px;" />
				<col style="width: 225px;" />
			</colgroup>
			<tr>
				<td class="label">
					<?= LABEL_CLIENT_ID ?>
				</td>
				<td class="data">
					<?php print $data[CLIENT_VIEW_CLIENT_ID] ?>
				</td>
				<td colspan="2">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_STAMP_USER_ID ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_STAMP_USER])) ?>
				</td>
				<td class="label">
					<?= LABEL_LAST_EDIT_USER_ID ?>
				</td>
				<td class="data">
					<?php print stripslashes(trim($data[CLIENT_VIEW_LAST_EDIT_USER])) ?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_STAMP_DATE ?>
				</td>
				<td class="data">
					<?php print ($data[CLIENT_VIEW_STAMP_DATE] != null ? date('m/d/Y', $data[CLIENT_VIEW_STAMP_DATE]) :'' ) ?>
				</td>
				<td class="label">
					<?= LABEL_LAST_EDIT_DATE ?>
				</td>
				<td class="data">
					<?php print ($data[CLIENT_VIEW_LAST_EDIT_DATE] != 0 ? date('m/d/Y', $data[CLIENT_VIEW_LAST_EDIT_DATE]) : '') ?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_CLIENT_INACTIVE_DATE ?>
				</td>
				<td class="data">
					<? 	if ($data[CLIENT_VIEW_CLIENT_INACTIVE_DATE] > 0)
						{
							print (date('m/d/Y', $data[CLIENT_VIEW_CLIENT_INACTIVE_DATE]));
						}
						else
						{
							print ('&nbsp;');
						} 
					 ?>
				</td>
				<td colspan="2">
					&nbsp;
				</td>
			</tr>
		</table>
	</fieldset>
</div>
