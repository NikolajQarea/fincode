<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if(!($this->session->userdata(STAFF_ID)))
    echo '';
else{
 ?>
<div style="clear:both;"></div>
<div>
    <table width="100%">
    <tr>
        <th><a class ="view_link" href="<?= base_url() ?>report/generate_report/Client_Name/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>/order"> Client Name <?= ($order == 'Client_Name')&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == 'Client_Name')&&($as == 'DESC') ? '&darr;' : '' ?></a></th>
        
        
        <th><a class ="view_link" href="<?= base_url() ?>report/generate_report/form_name/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>/order"> Form <?= ($order == 'form_name')&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == 'form_name')&&($as == 'DESC') ? '&darr;' : '' ?></a></th>
        <th><a class ="view_link" href="<?= base_url() ?>report/generate_report/tax_year/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>/order"> Tax Year <?= ($order == 'tax_year')&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == 'tax_year')&&($as == 'DESC') ? '&darr;' : '' ?></a></th>
        <th><a class ="view_link" href="<?= base_url() ?>report/generate_report/tax_quarter/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>/order"> Tax Quarter <?= ($order == 'tax_quarter')&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == 'tax_quarter')&&($as == 'DESC') ? '&darr;' : '' ?></a></th>
        <th><a class ="view_link" href="<?= base_url() ?>report/generate_report/tax_month/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>/order"> Tax Month <?= ($order == 'tax_month')&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == 'tax_month')&&($as == 'DESC') ? '&darr;' : '' ?></a></th>
        <th><a class ="view_link" href="<?= base_url() ?>report/generate_report/state/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>/order"> States <?= ($order == 'state')&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == 'state')&&($as == 'DESC') ? '&darr;' : '' ?></a></th>
        <th><a class ="view_link" href="<?= base_url() ?>report/generate_report/partner/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>/order"> PTR <?= ($order == 'partner')&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == 'partner')&&($as == 'DESC') ? '&darr;' : '' ?></a></th>
        
        <th> Due date </th>
        <th><a class ="view_link" href="<?= base_url() ?>report/generate_report/staff/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>/order"> Staff <?= ($order == CLIENT_STAFF_ID)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == CLIENT_STAFF_ID)&&($as == 'DESC') ? '&darr;' : '' ?></a></th>
        <th><?= LABEL_CLIENT_STATUS_ID ?></th>
        <!--<th>Last Name</th>-->
        <!--<th>First Name</th>-->
        <!--<th>Spouse</th>-->
        <!--<th>Form</th>-->
        <!--<th>States</th>-->
        <!--<th>Partner</th>-->
        <!--<th>Month</th>-->
        <!--<th>Due date</th>-->
        <!--<th>Staff</th>-->
        <!--<th>Entity</th>-->
        <!--<th>Status</th>-->
        <!--<th> Notes </th>-->
    </tr>
    <?php foreach($reports as $report): ?>
        <?php
            if(count(explode(',', $report['state']))>1)
            {
                $states = explode(',', $report['state']);
                
                ?>
                    <tr align="center">
                        <td><a href="<?= $this->config->item('base_url'); ?>client/view_single/<?= $report[CLIENT_ID]; ?>"><?= stripslashes($report['Client_Name']); ?></a></td>
                       
                        
                        <td><?= isset($report['form_name']) ? $report['form_name'] : ''; ?></td>
                        <td><?= $report['tax_year']; ?></td>
                        <td><?= $report['tax_quarter'] == NOT_APPLICABLE_ID ? NOT_APPLICABLE : $report['tax_quarter']; ?></td>
                        <td><?= $report['tax_month'] == NOT_APPLICABLE_ID ? NOT_APPLICABLE : date('F', $report['tax_month']); ?></td>
                        <td><?= $states[0]; ?></td>
                        <td><?= $report['partner']; ?></td>
                        <td><?= $report['due_date']; ?></td>
                        <td><?= $report[CLIENT_STAFF_ID]; ?></td>
                        <td><?= $report[CLIENT_VIEW_CLIENT_STATUS]; ?></td>
                        
                    </tr>
                <?php
                for($i=1; $i<count($states); $i++)
                {
                    ?>
                    <tr align="center">
                        <td></td>
                        
                        <td></td>
                        <td></td>
                        <td></td>
                      
                        <td><?= $states[$i]; ?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        
                        
                        
                    </tr>
                <?php
                }
            }
            else
            {
                ?>
                <tr align="center">
                    <td><a href="<?= $this->config->item('base_url'); ?>client/view_single/<?= $report[CLIENT_ID]; ?>"><?= stripslashes($report['Client_Name']); ?></a></td>
                    
                    <td><?= isset($report['form_name']) ? $report['form_name'] : ''; ?></td>
                    <td><?= $report['tax_year']; ?></td>
                    <td><?= $report['tax_quarter'] == NOT_APPLICABLE_ID ? NOT_APPLICABLE : $report['tax_quarter']; ?></td>
                    <td><?= $report['tax_month'] == NOT_APPLICABLE_ID ? NOT_APPLICABLE : date('F', $report['tax_month']); ?></td>
                    <td><?= $report['state']; ?></td>
                    <td><?= $report['partner']; ?></td>
                    <td><?= $report['due_date']; ?></td>
                    <td><?= $report[CLIENT_STAFF_ID]; ?></td>
                    <td><?= $report[CLIENT_VIEW_CLIENT_STATUS]; ?></td>
                    
                </tr>
                <?php
            }
        ?>
    <?php endforeach; ?>
    </table>
    <div class="print_section">
        <a class="print_report" href="javascript:void(0);"><img width="32px" height="32px" src ="<?= $this->config->item('base_url'); ?>application/views/css/images/print.png"></a>
        <a href="<?= $this->config->item('base_url'); ?>report/convertToExcel"><img width="32px" height="32px" src ="<?= $this->config->item('base_url'); ?>application/views/css/images/excel.png"></a>
        <a href="<?= $this->config->item('base_url'); ?>report/convertToPdf"><img width="32px" height="32px" src ="<?= $this->config->item('base_url'); ?>application/views/css/images/pdf.png"></a>
    </div>
    <div class="for_iframe"> 
    </div>
</div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.print_report').bind("click", function(){
            var test_html = $('.for_iframe').html().replace(/(^\s+)|(\s+$)/g, "");
            if(test_html == '')
            {
                $('.for_iframe').html("<iframe class='print_iframe' name='print_iframe' src='<?= $this->config->item('base_url'); ?>report/printReport' />");
                $('.print_iframe').load( 
                    function(){
                        window.frames['print_iframe'].focus();
                        window.frames['print_iframe'].print();
                    }
                );
            }
            else
            {
                window.frames['print_iframe'].focus();
                window.frames['print_iframe'].print();
            }
        });
    });
</script>
<?php
}
 ?>