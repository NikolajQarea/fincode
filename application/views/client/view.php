<div id="content" class="clearfix">
	<div class="clearfix"></div>
	<?php echo form_open(base_url().'client/view'); ?>
	<div class="clearfix">
		<fieldset>
			<legend>
				Filters
			</legend>
			<div style='float: right;'>
				<a href="<?php print $this->config->item('base_url'); ?>client/convertToExcel">
					<img src="<?php print $this->config->item('base_url'); ?>application/views/css/images/excel.png" style="width: 25px; float: right;" />
				</a>
			</div>
			<table style="font-size: 8pt; display: inline-block; padding: 0px;">
				<tr>
					<td valign="middle">
						<input class="button" name="filter" type="submit" value="Apply Filter(s)" />
					</td>
					<td valign="middle">
						<input class="button" name="clear" type="submit" value="Clear Filter(s)" />
					</td>
					<td class="tdFilter" valign="middle">
						<label for="client[include_inactive]">
							Include Inactive/Terminated?
						</label>
						<input class="autoSubmit" id="client[include_inactive]" name="client[include_inactive]"  title="Include Inactive/Terminated?" type="checkbox" <?= isset($post['include_inactive']) && $post['include_inactive'] == 'on' ? ' checked' : ''; ?> />
		   		    </td>
				</tr>
			</table>
		</fieldset>
	</div>
	<div class="message clearfix">
		<span class="link">
			&nbsp;<?php print $link ?>
		</span>
	</div>
	<div class="clearfix">
		<table border="1" class="view">
			<colgroup>
				<col style="width: 23%;" />
				<col style="width: 13%;" />
				<col style="width: 6%;" />
				<col style="width: 6%;" />
				<col style="width: 6%;" />
				<col style="width: 6%;" />
				<col style="width: 7%;" />
				<col style="width: 6%;" />
				<col style="width: 12%;" />
				<col style="width: 9%;" />
				<col style="width: 6%;" />
			</colgroup>
			<tr>
				<th>
					<a class="view_link" href="<?php print base_url() ?>client/view/<?php print $page ?>/<?= CLIENT_NAME ?>/<?php print ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Client Name
						<?php print ($order == CLIENT_NAME)&&($as == 'ASC') ? '&uarr;' : '' ?><?php print ($order == CLIENT_NAME)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?php print base_url() ?>client/view/<?php print $page ?>/<?= TAX_FORM_ID ?>/<?php print ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Tax Form(s)
						<?php print ($order == TAX_FORM_ID)&&($as == 'ASC') ? '&uarr;' : '' ?><?php print ($order == TAX_FORM_ID)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?php print base_url() ?>client/view/<?php print $page ?>/<?= STATE_ID ?>/<?php print ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						State(s)
						<?php print ($order == STATE_ID)&&($as == 'ASC') ? '&uarr;' : '' ?><?php print ($order == STATE_ID)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?php print base_url() ?>client/view/<?php print $page ?>/<?= CLIENT_PARTNER_ID ?>/<?php print ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Partner
						<?php print ($order == CLIENT_PARTNER_ID)&&($as == 'ASC') ? '&uarr;' : '' ?><?php print ($order == CLIENT_PARTNER_ID)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?php print base_url() ?>client/view/<?php print $page ?>/<?= CLIENT_STAFF_ID ?>/<?php print ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Staff
						<?php print ($order == CLIENT_STAFF_ID)&&($as == 'ASC') ? '&uarr;' : '' ?><?php print ($order == CLIENT_STAFF_ID)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?php print base_url() ?>client/view/<?php print $page ?>/<?= CLIENT_INITIAL_YEAR_ID ?>/<?php print ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Year
						<?php print ($order == CLIENT_INITIAL_YEAR_ID)&&($as == 'ASC') ? '&uarr;' : '' ?><?php print ($order == CLIENT_INITIAL_YEAR_ID)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?php print base_url() ?>client/view/<?php print $page ?>/<?= CLIENT_MONTH_ID ?>/<?php print ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Begins
						<?php print ($order == CLIENT_MONTH_ID)&&($as == 'ASC') ? '&uarr;' : '' ?><?php print ($order == CLIENT_MONTH_ID)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?php print base_url() ?>client/view/<?php print $page ?>/<?= CLIENT_STATUS_ID ?>/<?php print ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Status
						<?php print ($order == CLIENT_STATUS_ID)&&($as == 'ASC') ? '&uarr;' : '' ?><?php print ($order == CLIENT_STATUS_ID)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?php print base_url() ?>client/view/<?php print $page ?>/<?= CLIENT_PRINCIPAL ?>/<?php print ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						<?= LABEL_CLIENT_PRINCIPAL ?>
						<?php print ($order == CLIENT_PRINCIPAL)&&($as == 'ASC') ? '&uarr;' : '' ?><?php print ($order == CLIENT_PRINCIPAL)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?php print base_url() ?>client/view/<?php print $page ?>/<?= CLIENT_SSN_EIN ?>/<?php print ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						SSN/EIN
						<?php print ($order == CLIENT_SSN_EIN)&&($as == 'ASC') ? '&uarr;' : '' ?><?php print ($order == CLIENT_SSN_EIN)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?php print base_url() ?>client/view/<?php print $page ?>/<?= CLIENT_VIEW_TAX_RETURN_IDS ?>/<?php print ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Returns
						<?php print ($order == CLIENT_VIEW_TAX_RETURN_IDS)&&($as == 'ASC') ? '&uarr;' : '' ?><?php print ($order == CLIENT_VIEW_TAX_RETURN_IDS)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
			</tr>
			<tr>
				<th>
					<input id="search" class="input" name="client[<?= CLIENT_NAME ?>]" type="text" value="<?php print stripslashes($post[CLIENT_NAME]) ?>"  />
				</th>
				<th>
					<?= $Tax_Forms ?>
				</th>
				<th>
					<?= $States ?>
				</th>
				<th>
					<?= $Partners ?>
				</th>
				<th>
					<?= $Staff ?>
				</th>
				<th>
					<?= $Initial_Years ?>
				</th>
				<th>
					<?= $Tax_Months ?>
				</th>
				<th>
					<?= $Client_Status ?>
				</th>
				<th>
					<input class="input" name="client[<?= CLIENT_PRINCIPAL ?>]" type="text" value="<?php print stripslashes($post[CLIENT_PRINCIPAL]) ?>"  />
				</th>
				<th>
					<input class="input" name="client[<?= CLIENT_SSN_EIN ?>]" type="text" value="<?php print stripslashes($post[CLIENT_SSN_EIN]) ?>"  />
				</th>
				<th>
					<?= $Tax_Returns ?>
				</th>
			</tr>
		<?php if(isset($list[0]['Client_Name'])) : ?>
		<?php foreach ($list as $key => $value) : ?>
		<?php if($this->session->userdata(ROLE_ADMIN)): ?>
		<tr class="clickRow" ondblclick="window.location='<?= base_url() ?>client/edit/<?= $value[CLIENT_ID] ?>';">
		<?php else: ?>
		<tr>
		<?php endif; ?>

				<?php if($this->session->userdata(ROLE_WRITE)) : ?>
				<td>
					<a href="<?php print base_url() ?>client/printer_friendly/<?php print ($value[CLIENT_ID]); ?>">
						<?php print (stripslashes($value[CLIENT_NAME])) ?>
					</a>
				</td>
				<?php else : ?>
				<td>
					<?php print stripslashes($value[CLIENT_NAME]) ?>
				</td>
				<?php endif; ?>
				<td>
					<?php print (isset($value[CLIENT_VIEW_TAX_FORMS]) && $value[CLIENT_VIEW_TAX_FORMS]) ? stripslashes($value[CLIENT_VIEW_TAX_FORMS]) : '&nbsp;' ?>
				</td>
				<td>
					<?php print $value[CLIENT_VIEW_STATES] ?>
				</td>
				<td>
					<?php print $value[CLIENT_VIEW_PARTNER_INITIALS] ? stripslashes($value[CLIENT_VIEW_PARTNER_INITIALS]) : '&nbsp;' ?>
				</td>
				<td>
					<?php print $value[CLIENT_VIEW_STAFF_INITIALS] ? $value[CLIENT_VIEW_STAFF_INITIALS] : '&nbsp;' ?>
				</td>
				<td>
					<?php print $value[CLIENT_VIEW_INITIAL_YEAR_ID] ?>
				</td>
				<td>
					<?php print $value[CLIENT_VIEW_MONTH] ?>
				</td>
				<td>
					<?php print $value[CLIENT_VIEW_CLIENT_STATUS] ?>
				</td>
				<td>
					<?php print stripslashes($value[CLIENT_VIEW_PRINCIPAL]) ?>
				</td>
				<td>
					<?php print stripslashes($value[CLIENT_VIEW_CLIENT_SSN_EIN]) ?>
				</td>
				<td>
					<?php print ($value['Tax_Returns'] != null ? '<a href ="'.base_url().'tax_return/filter/'.$value[CLIENT_ID].'">View</a>' : '&nbsp;') ?>
				</td>
			</tr>
		<?php endforeach; ?>
		<?php echo form_close(); ?>
		<?php else : ?>
			<tr>
				<td align="center" colspan="12">
					<h3>
						<?php print $list ?>
					</h3>
				</td>
			</tr>
		<?php endif; ?>
		</table>
	</div>
	<div class="message clearfix">
		<span class="link">
			&nbsp;<?php print $link ?>
		</span>
	</div>
	<script type="text/javascript">
		$(document).ready(function()
		{
			clientAutocomplete($('#search'), { 
				select: function($search) { 
					$search.closest('form').submit(); 
				} 
			});
		});
	</script>
</div>
