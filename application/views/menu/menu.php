<?php 	
	$querystr = $_SERVER['REQUEST_URI'];
	if($this->session->userdata(ROLE_ADMIN) || $this->session->userdata(ROLE_WRITE) || $this->session->userdata(ROLE_READ_ONLY)) : 
 ?>
<div id="menu" class="clearfix">
	<div id="col1">
		&nbsp;
	</div>
	<div id="col2">
		<table class="tblMenu">
			<tr>
				<td class="tdmenu<?php if(stristr($querystr, "/client/")) {echo " active";} ?>">
					<a class="menu" href="<?php echo base_url(); ?>client/view">
						Clients
					</a>
				</td>
				<td class="tdmenu<?php if(stristr($querystr, "/tax_return/")) {echo " active";} ?>">
					<a class="menu" href="<?php echo base_url(); ?>tax_return/view">
						Tax Returns
					</a>
				</td>
				<td class="tdmenu<?php if(stristr($querystr, "/statement/")) {echo " active";} ?>">
					<a class="menu" href="<?php echo base_url(); ?>statement/view">
						Bank/Broker Statements
					</a>
				</td>
				<td class="tdmenu<?php if(stristr($querystr, "/institution/")) {echo " active";} ?>">
					<a class="menu" href="<?php echo base_url(); ?>institution/view">
						Banks/Brokers
					</a>
				</td>
				<td class="tdmenu<?php if(stristr($querystr, "/report/")) {echo " active";} ?>">
					<a class="menu" href="<?php echo base_url(); ?>report/add">
						Reports
					</a>
				</td>
				<?php if ($this->session->userdata(ROLE_ADMIN) || $this->session->userdata(ROLE_WRITE)): ?>
				<td class="tdmenu<?php if(stristr($querystr, "/manage/")) {echo " active";} ?>">
					<a class="menu" href="<?php echo base_url(); ?>manage/view">
						List Management
					</a>
				</td>
				<?php endif; ?>
				<?php if ($this->session->userdata(ROLE_ADMIN)): ?>
				<td class="tdmenu<?php if(stristr($querystr, "/user/")) {echo " active";} ?>">
					<a class="menu" href="<?php echo base_url(); ?>user/view">
						Users
					</a>
				</td>
				<?php endif; ?>
			</tr>
		</table>
	</div>
	<div id="col3">
		<?php if ($this->session->userdata(ROLE_ADMIN) || $this->session->userdata(ROLE_WRITE)) : ?>
		<?php if (stristr($querystr, "/add") || stristr($querystr, "/edit") || stristr($querystr, "/printer_friendly")) : ?>
		<span>
			<img class="btnAddBack" align="left" onClick="history.go(-1)" src="<?php print $this->config->item('base_url'); ?>application/views/css/images/btn_Back_To_List.png" />
		</span>
		<?php elseif($this->session->userdata(ROLE_ADMIN) && stristr($querystr, "user/view")) : ?>
		<span>
			<a href="<?= base_url() ?>user/add">
				<img class="btnAddBack" align="left" src="<?= $this->config->item('base_url'); ?>application/views/css/images/btn_Add_User.png" />
			</a>
		</span>
		<?php elseif(stristr($querystr, "client/view")) : ?>
		<span>
			<a href="<?php print base_url() ?>client/add">
				<img class="btnAddBack" align="left" src="<?php print $this->config->item('base_url'); ?>application/views/css/images/btn_Add_Client.png" />
			</a>
		</span>
		<span>
			<a href="<?php print base_url() ?>client/remove">
				<img class="btnAddBack" align="left" src="<?php print $this->config->item('base_url'); ?>application/views/css/images/btn_Rem_Client.png" />
			</a>
		</span>
		<?php elseif(stristr($querystr, "tax_return/view")) : ?>
		<span>
			<a href="<?php print base_url() ?>tax_return/add">
				<img align="left" class="btnAddBack" src="<?php print $this->config->item('base_url'); ?>application/views/css/images/btn_Add_Tax_Form.png" />
			</a>
		</span>
                <span>
			<a href="<?php print base_url() ?>tax_return/add_multiple">
				<img align="left" class="btnAddBack" src="<?php print $this->config->item('base_url'); ?>application/views/css/images/btn_Add_Tax_Form_M.png" />
			</a>
		</span>
		<?php elseif(stristr($querystr, "institution/view")) : ?>
		<span>
			<a href="<?php print base_url() ?>institution/add">
				<img align="left" class="btnAddBack" src="<?php print $this->config->item('base_url'); ?>application/views/css/images/btn_Add_Institution.png" />
			</a>
		</span>
		<?php elseif(stristr($querystr, "statement/view")) : ?>
		<span>
			<a href="<?php print base_url() ?>statement/add">
				<img align="left" class="btnAddBack" src="<?php print $this->config->item('base_url'); ?>application/views/css/images/btn_Add_Statement.png" />
			</a>
		</span>
		<?php endif; ?>
		<?php endif; ?>
	</div>
</div>
<div class="message">
	<?= ($this->session->flashdata(STATUS_MESSAGE)) ? $this->session->flashdata(STATUS_MESSAGE) : '' ?>
	<span class="errors"><?= ($this->session->flashdata(ERROR_MESSAGE)) ? $this->session->flashdata(ERROR_MESSAGE) : '' ?></span>
</div><!--#menu-->
<?php else: ?>
<?php echo form_open(base_url().'main/load_menu'); ?>
<div id="content" style="width: 300px; float: left;">
	<fieldset>
		<legend>
			Log In
		</legend>
		<table align="center" style="border: none; padding: 10px; margin: 20px;">
			<tr>
				<td class="errors" colspan="2">
					<?= ($this->session->flashdata(STATUS_MESSAGE)) ? $this->session->flashdata(STATUS_MESSAGE) : '' ?>
					<?= ($this->session->flashdata(ERROR_MESSAGE)) ? $this->session->flashdata(ERROR_MESSAGE) : '' ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_STAFF_LOGIN ?>
				</td>
				<td>
					<input name="login[<?= STAFF_LOGIN ?>]" size="15" type="text" />
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_STAFF_PASSWORD ?>
				</td>
				<td>
					<input name="login[<?= STAFF_PASSWORD ?>]" size="15" type="password" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td align="center" colspan="2">
					<input class="submit" type="submit" value="Log In" style="width: 95%;">
				</td>
			</tr>
		</table>
	</fieldset>
</div>
<?php echo form_close(); ?>
<?php endif; ?>
