<div id="content" class="clearfix">
	<?php echo form_open(base_url().'statement/view'); ?>
	<div class="clearfix">
		<fieldset>
			<legend>
				Filters
			</legend>
			<div style='float: right;'>
				<a href="<?php print $this->config->item('base_url'); ?>statement/convertToExcel">
					<img src="<?php print $this->config->item('base_url'); ?>application/views/css/images/excel.png" style="width: 25px; float: right;" />
				</a>
			</div>
			<table style="font-size: 8pt; display: inline-block; padding: 0px;">
				<tr>
					<td valign="middle">
						<input class="button" name="filter" type="submit" value="Apply Filter(s)" />
					</td>
					<td valign="middle">
						<input class="button" name="clear" type="submit" value="Clear Filter(s)" />
					</td>
					<td class="tdFilter" valign="middle">
						<label for="statement[include_terminated]">
							Include Terminated?
						</label>
						<input class="autoSubmit" id="statement[include_terminated]" name="statement[include_terminated]"  title="Include Terminated?" type="checkbox" <?= isset($post['include_terminated']) && $post['include_terminated'] == 'on' ? ' checked' : ''; ?> />
		   		    </td>
				</tr>
			</table>
		</fieldset>
	</div>
	<div class="clearfix"></div>
	<div class="message clearfix">
		<span class="link">
			&nbsp;<?php print $link ?>
		</span>
	</div>
	<div class="clearfix">
		<table class="view">
		<colgroup>
			<col style="width: 23%;" />
			<col style="width: 8%;" />
			<col style="width: 8%;" />
			<col style="width: 7%;" />
			<col style="width: 8%;" />
			<col style="width: 7%;" />
			<col style="width: 10%;" />
			<col style="width: 9%;" />
			<col style="width: 9%;" />
			<col style="width: 12%;" />
		</colgroup>

			<tr>
				<th>
					<a class="view_link" href="<?= base_url() ?>statement/view/<?= $page ?>/<?= STATEMENT_VIEW_CLIENT_NAME ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Client&nbsp;Name
						<?= ($order == STATEMENT_VIEW_CLIENT_NAME)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == STATEMENT_VIEW_CLIENT_NAME)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>statement/view/<?= $page ?>/<?= STATEMENT_VIEW_CLIENT_PARTNER_NAME ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Partner
						<?= ($order == STATEMENT_VIEW_CLIENT_PARTNER_NAME)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == STATEMENT_VIEW_CLIENT_PARTNER_NAME)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>statement/view/<?= $page ?>/<?= STATEMENT_VIEW_CLIENT_STAFF_NAME ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Staff
						<?= ($order == STATEMENT_VIEW_CLIENT_STAFF_NAME)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == STATEMENT_VIEW_CLIENT_STAFF_NAME)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>statement/view/<?= $page ?>/<?= STATEMENT_VIEW_STATEMENT_TYPE ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Statement&nbsp;Type
						<?= ($order == STATEMENT_VIEW_STATEMENT_TYPE)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == STATEMENT_VIEW_STATEMENT_TYPE)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>statement/view/<?= $page ?>/<?= STATEMENT_VIEW_INSTITUTION_NAME ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Bank/Broker
						<?= ($order == STATEMENT_VIEW_INSTITUTION_NAME)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == STATEMENT_VIEW_INSTITUTION_NAME)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>statement/view/<?= $page ?>/<?= STATEMENT_VIEW_STATEMENT_ACCOUNT_NUMBER ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Account&nbsp;Number
						<?= ($order == STATEMENT_VIEW_STATEMENT_ACCOUNT_NUMBER)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == STATEMENT_VIEW_STATEMENT_ACCOUNT_NUMBER)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>statement/view/<?= $page ?>/<?= STATEMENT_VIEW_STATEMENT_END_DATE ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Archived&nbsp;Thru
						<?= ($order == STATEMENT_VIEW_STATEMENT_END_DATE)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == STATEMENT_VIEW_STATEMENT_END_DATE)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>statement/view/<?= $page ?>/<?= STATEMENT_VIEW_STATEMENT_LAST_RECONCILIATION_DATE ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Reconciled
						<?= ($order == STATEMENT_VIEW_STATEMENT_LAST_RECONCILIATION_DATE)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == STATEMENT_VIEW_STATEMENT_LAST_RECONCILIATION_DATE)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>statement/view/<?= $page ?>/<?= STATEMENT_VIEW_STATEMENT_STATUS ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Statement&nbsp;Status
						<?= ($order == STATEMENT_VIEW_STATEMENT_STATUS)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == STATEMENT_VIEW_STATEMENT_STATUS)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>statement/view/<?= $page ?>/<?= STATEMENT_VIEW_STATEMENT_NOTES ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Notes
						<?= ($order == STATEMENT_VIEW_STATEMENT_NOTES)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == STATEMENT_VIEW_STATEMENT_NOTES)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
			</tr>
			<tr>
				<th>
					<input id="search" class="input" name="statement[<?= STATEMENT_VIEW_CLIENT_NAME ?>]" type="text" value="<? print($post[STATEMENT_VIEW_CLIENT_NAME]); ?>" />
				</th>
				<th>
					<?= $Partners ?>
				</th>
				<th>
					<?= $Staff ?>
				</th>
				<th>
					<?= $Statement_Types ?>
				</th>
				<th>
					<?= $Institutions ?>
				</th>
				<th>
					<input class="input autoSubmit" name="statement[<?= STATEMENT_VIEW_STATEMENT_ACCOUNT_NUMBER ?>]" value="<?= $post[STATEMENT_VIEW_STATEMENT_ACCOUNT_NUMBER] ?>" />
				</th>
				<th>
					<input class="input datepicker autoSubmit" name="statement[<?= STATEMENT_VIEW_STATEMENT_END_DATE ?>]" size="7" value="<?= $post[STATEMENT_VIEW_STATEMENT_END_DATE] ?>" />
				</th>
				<th>
					<input class="input datepicker autoSubmit" name="statement[<?= STATEMENT_VIEW_STATEMENT_LAST_RECONCILIATION_DATE ?>]" size="7" value="<?= $post[STATEMENT_VIEW_STATEMENT_LAST_RECONCILIATION_DATE] ?>" />
				</th>
				<th>
					<?= $Statement_Status ?>
				</th>
				<th>
					<input class="input autoSubmit" name="statement[<?= STATEMENT_VIEW_STATEMENT_NOTES ?>]" type="text" value="<? print(stripslashes($post[STATEMENT_VIEW_STATEMENT_NOTES])); ?>" style="width: 90%;" />
				</th>
			</tr>
			<?php if(isset($list[0][STATEMENT_VIEW_STATEMENT_ID])): ?>
			<?php foreach ($list as $key => $value): ?>
			<?php if ($this->session->userdata(ROLE_ADMIN) || $this->session->userdata(ROLE_WRITE)): ?>
			<tr class="clickRow" ondblclick="window.location='<?= base_url() ?>statement/edit/<?= $value[STATEMENT_VIEW_STATEMENT_ID] ?>';">
			<?php else: ?>
			<tr>
			<?php endif; ?>
				<td>
					<?= stripslashes(trim($value[STATEMENT_VIEW_CLIENT_NAME])) ?>
				</td>
				<td>
					<?= stripslashes(trim($value[STATEMENT_VIEW_CLIENT_PARTNER_INITIALS])) ?>
				</td>
				<td>
					<?= stripslashes(trim($value[STATEMENT_VIEW_CLIENT_STAFF_INITIALS])) ?>
				</td>
				<td>
					<?= stripslashes(trim($value[STATEMENT_VIEW_STATEMENT_TYPE])) ?>
				</td>
				<td>
					<?= stripslashes(trim($value[STATEMENT_VIEW_INSTITUTION_NAME])) ?>
				</td>
				<td>
					<?= $value[STATEMENT_VIEW_STATEMENT_ACCOUNT_NUMBER] ?>
				</td>
				<td>
					<?= $value[STATEMENT_VIEW_STATEMENT_END_DATE] > 0 ? date(DATE_FORMAT, stripslashes(trim($value[STATEMENT_VIEW_STATEMENT_END_DATE]))) : null ?>
				</td>
				<td>
					<?= $value[STATEMENT_VIEW_STATEMENT_LAST_RECONCILIATION_DATE] > 0 ? date(DATE_FORMAT, stripslashes(trim($value[STATEMENT_VIEW_STATEMENT_LAST_RECONCILIATION_DATE]))) : null ?>
				</td>
				<td>
					<?= stripslashes(trim($value[STATEMENT_VIEW_STATEMENT_STATUS])) ?>
				</td>
				<td>
					<?= stripslashes($value[STATEMENT_VIEW_STATEMENT_NOTES]) ?>
				</td>
			</tr>
			<?php endforeach; ?><?php echo form_close(); ?>
			<?php else : ?>
			<tr>
				<td align="center" colspan="13">
					<h3>
						<?= $list ?>
					</h3>
				</td>
			</tr>
			<?php endif; ?>
		</table>
	</div>
	<div class="message clearfix">
		<span class="link">
			&nbsp;<?php print $link ?>
		</span>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function()
	{
		$('.datepicker').datepicker();
	
		clientAutocomplete($('#search'), { 
			select: function($search) { 
				$search.closest('form').submit(); 
			} 
		});
	});
</script>
