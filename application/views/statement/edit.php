<div id="content" class="clearfix">
	<fieldset style="width: 650px;">
		<legend>
			<?= stripslashes(trim($data[STATEMENT_VIEW_CLIENT_NAME])) ?>
		</legend>
		<div>&nbsp;</div>
		<?php echo form_open(base_url() . 'statement/insert_update_client_statement/'.$data[STATEMENT_ID]);  ?>
		<table style="width: 95%;">
			<colgroup>
				<col style="width: 25%;" />
				<col style="width: 60%;" />
				<col style="width: 15%;" />
			</colgroup>
			<tr style="display: none;">
				<td>
					<?= REQUIRED_FIELD . LABEL_CLIENT_ID ?>
				</td>
				<td>
					<input id="search" class="input client_select" type="hidden" name="statement[<?= STATEMENT_VIEW_CLIENT_NAME ?>]" type="text" value="<?= $this->session->flashdata(STATEMENT_VIEW_CLIENT_NAME) ? $this->session->flashdata(STATEMENT_VIEW_CLIENT_NAME) : $data[STATEMENT_VIEW_CLIENT_NAME] ? $data[STATEMENT_VIEW_CLIENT_NAME] : ANY_LABEL ?>"  />
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STATEMENT_CLIENT_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_STATEMENT_TYPE_ID ?>
				</td>
				<td>
					<?= $StatementTypes ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STATEMENT_TYPE_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_INSTITUTION_ID ?>
				</td>
				<td>
					<?= $Institutions ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_INSTITUTION_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_STATEMENT_ABA_NUMBER ?>
				</td>
				<td>
					<input class="input" id="<?= STATEMENT_ABA_NUMBER ?>" name="statement[<?= STATEMENT_ABA_NUMBER ?>]" type="text" value="<?= $this->session->flashdata(STATEMENT_ABA_NUMBER) ? stripslashes($this->session->flashdata(STATEMENT_ABA_NUMBER)) : $data[STATEMENT_ABA_NUMBER] ? stripslashes($data[STATEMENT_ABA_NUMBER]) : null ?>" />
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STATEMENT_ABA_NUMBER) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_STATEMENT_ACCOUNT_NUMBER ?>
				</td>
				<td>
					<input class="input" id="<?= STATEMENT_ACCOUNT_NUMBER ?>" name="statement[<?= STATEMENT_ACCOUNT_NUMBER ?>]" type="text" value="<?= $this->session->flashdata(STATEMENT_ACCOUNT_NUMBER) ? stripslashes($this->session->flashdata(STATEMENT_ACCOUNT_NUMBER)) : $data[STATEMENT_ACCOUNT_NUMBER] ? stripslashes($data[STATEMENT_ACCOUNT_NUMBER]) : null ?>" />
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STATEMENT_ACCOUNT_NUMBER) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_STATEMENT_START_DATE ?>
				</td>
				<td>
					<input class="input datepicker" id="<?= STATEMENT_START_DATE ?>" name="statement[<?= STATEMENT_START_DATE ?>]" type="text" value="<?= $this->session->flashdata(STATEMENT_START_DATE) ? date(DATE_FORMAT, $this->session->flashdata(STATEMENT_START_DATE)) : $data[STATEMENT_START_DATE] ? date(DATE_FORMAT, $data[STATEMENT_START_DATE]) : null  ?>" />
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STATEMENT_START_DATE) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_STATEMENT_END_DATE ?>
				</td>
				<td>
					<input class="input datepicker" id="<?= STATEMENT_END_DATE ?>" name="statement[<?= STATEMENT_END_DATE ?>]" type="text" value="<?= $this->session->flashdata(STATEMENT_END_DATE) ? date(DATE_FORMAT, $this->session->flashdata(STATEMENT_END_DATE)) : $data[STATEMENT_END_DATE] ? date(DATE_FORMAT, $data[STATEMENT_END_DATE]) : null  ?>" />
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STATEMENT_END_DATE) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_STATEMENT_LAST_RECONCILIATION_DATE ?>
				</td>
				<td>
					<input class="input datepicker" id="<?= STATEMENT_LAST_RECONCILIATION_DATE ?>" name="statement[<?= STATEMENT_LAST_RECONCILIATION_DATE ?>]" type="text" value="<?= $this->session->flashdata(STATEMENT_LAST_RECONCILIATION_DATE) ? date(DATE_FORMAT, $this->session->flashdata(STATEMENT_LAST_RECONCILIATION_DATE)) : $data[STATEMENT_LAST_RECONCILIATION_DATE] ? date(DATE_FORMAT, $data[STATEMENT_LAST_RECONCILIATION_DATE]) : null ?>" />
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STATEMENT_LAST_RECONCILIATION_DATE) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_STATEMENT_NOTES ?>
				</td>
				<td>
					<textarea class="input" id="<?= STATEMENT_NOTES ?>" name="statement[<?= STATEMENT_NOTES ?>]" style="width: 95%;" rows="5"><?= $this->session->flashdata(STATEMENT_NOTES) ? stripslashes(trim($this->session->flashdata(STATEMENT_NOTES))) : $data[STATEMENT_NOTES] ? stripslashes($data[STATEMENT_NOTES]) : null ?></textarea>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STATEMENT_NOTES) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_STATEMENT_STATEMENT_STATUS_ID ?>
				</td>
				<td>
					<?= $StatementStatuses ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STATEMENT_STATEMENT_STATUS_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= STATEMENT_VIEW_CLIENT_STAFF_NAME_LABEL ?>
				</td>
				<td>
					<?= $Staff ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STATEMENT_STATEMENT_STATUS_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<input class ="submit" onClick="history.go(-1)" type="button" value ="Cancel" style="width: 95%;">
				</td>
				<td>
					<input class="submit" type="submit" value="Save" style="width: 95%;">
				</td>
				<td>
					&nbsp;
				</td>
			</tr>
		</table>
		<?php echo form_close(); ?>
	</fieldset>
	<div>&nbsp;</div>
	<fieldset style="width: 650px;">
		<legend>
			Client Statement History
		</legend>
		<div>&nbsp;</div>
		<table class="view">
			<tr>
				<th>
					<?= LABEL_STATEMENT_HISTORY_STATEMENT_STATUS_ID ?>
				</th>
				<th>
					<?= LABEL_STATEMENT_HISTORY_STAMP_USER_ID ?>
				</th>
				<th>
					<?= LABEL_STATEMENT_HISTORY_STAMP_DATE ?>
				</th>
				<th>
					<?= LABEL_STATEMENT_HISTORY_LAST_EDIT_USER_ID ?>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>/statement/edit/<?= $data[STATEMENT_HISTORY_VIEW_STATEMENT_ID] ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						<?= LABEL_STATEMENT_HISTORY_LAST_EDIT_DATE ?><?= ($as == 'ASC') ? '&uarr;' : '' ?><?= ($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
			</tr>
			<?php foreach ($History as $his): ?>
			<tr>
				<td>
					<?= $his[STATEMENT_HISTORY_VIEW_STATEMENT_STATUS] ?>
				</td>
				<td>
					<?= $his[STATEMENT_HISTORY_VIEW_STATEMENT_HISTORY_STAMP_USER_NAME_FULL] ?>
				</td>
				<td>
					<?= date(DATE_FORMAT, $his[STATEMENT_HISTORY_VIEW_STATEMENT_HISTORY_STAMP_DATE]) ?>
				</td>
				<td>
					<?= $his[STATEMENT_HISTORY_VIEW_STATEMENT_HISTORY_LAST_EDIT_USER_NAME_FULL] ?>
				</td>
				<td>
					<?= date(DATE_FORMAT, $his[STATEMENT_HISTORY_VIEW_STATEMENT_HISTORY_LAST_EDIT_DATE]) ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</table>
	</fieldset>

</div>
<script type="text/javascript">

	$(document).ready(function()
	{
		jQuery(function($)
		{
			$("#<?= STATEMENT_ACCOUNT_NUMBER ?>").Watermark("<?= JS_STATEMENT_ACCOUNT_NUMBER ?>");
			$("#<?= STATEMENT_ABA_NUMBER ?>").Watermark("<?= JS_STATEMENT_ABA_NUMBER ?>");
			$("#search").Watermark("<?= JS_CLIENT_NAME ?>");
			$("#<?= STATEMENT_START_DATE ?>").mask("99/99/9999");
			$("#<?= STATEMENT_START_DATE ?>").Watermark("<?= JS_STATEMENT_START_DATE ?>");
			$("#<?= STATEMENT_END_DATE ?>").mask("99/99/9999");
			$("#<?= STATEMENT_END_DATE ?>").Watermark("<?= JS_STATEMENT_END_DATE ?>");
			$("#<?= STATEMENT_LAST_RECONCILIATION_DATE ?>").mask("99/99/9999");
			$("#<?= STATEMENT_LAST_RECONCILIATION_DATE ?>").Watermark("<?= JS_STATEMENT_LAST_RECONCILIATION_DATE ?>");
			$("#<?= STATEMENT_NOTES ?>").Watermark("<?= JS_STATEMENT_NOTES ?>");
		});
	});
</script>
