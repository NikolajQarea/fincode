<div id="content" class="clearfix">
<?php echo form_open(base_url().'user/add_user'); ?>
	<fieldset style="width: 725px;">
		<legend>
			Add New Staff Account
		</legend>
		<table>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_STAFF_NAME_LAST ?>
				</td>
				<td>
					<input class="input" id="tbNameLast" name="staff[<?= STAFF_NAME_LAST ?>]" type="text" value="<?= $this->session->flashdata(STAFF_NAME_LAST) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STAFF_NAME_LAST) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_STAFF_NAME_FIRST ?>
				</td>
				<td>
					<input class="input" id="tbNameFirst" name="staff[<?= STAFF_NAME_FIRST ?>]" type="text" value="<?= $this->session->flashdata(STAFF_NAME_FIRST) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STAFF_NAME_FIRST) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_STAFF_INITIALS ?>
				</td>
				<td>
					<input class="input" id="tbInitials" name="staff[<?= STAFF_INITIALS ?>]" type="text" value="<?= $this->session->flashdata(STAFF_INITIALS) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STAFF_INITIALS) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_STAFF_LOGIN ?>
				</td>
				<td>
					<input class="input" id="tbLogin" name="staff[<?= STAFF_LOGIN ?>]" type="text" value="<?= $this->session->flashdata(STAFF_LOGIN) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STAFF_LOGIN) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_STAFF_PASSWORD ?>
				</td>
				<td>
					<input class="input" id="tbPassword" name="staff[<?= STAFF_PASSWORD ?>]" type="password" value="<?= $this->session->flashdata(STAFF_PASSWORD) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STAFF_PASSWORD) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_STAFF_EMAIL ?>
				</td>
				<td>
					<input class="input" id="tbEmail" name="staff[<?= STAFF_EMAIL ?>]" type="text" value="<?= $this->session->flashdata(STAFF_EMAIL) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STAFF_EMAIL) ?>
				</td>
			</tr>
			<tr>
				<td style="vertical-align: top;">
					<?= REQUIRED_FIELD . LABEL_STAFF_STATUS_ID ?>
				</td>
				<td>
					<select class="input" name="staff[<?= STAFF_STAFF_STATUS_ID ?>]">
						<?= $this->User_model->get_user_status_options(0) ?>
					</select>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STAFF_STATUS_ID) ?>
				</td>
			</tr>
			<tr>
				<td style="vertical-align: top;">
					<?= REQUIRED_FIELD . LABEL_STAFF_ROLE_ID ?>
				</td>
				<td style="vertical-align: top;">
					<select class="input" multiple="multiple" name="<?= STAFF_ROLE_ID ?>[]" size="4">
						<?= $this->User_model->get_user_roles_options(0) ?>
					</select>
				</td>
				<td style="vertical-align: top;">
					<span class="error">
						<?= $this->session->flashdata(ERROR_STAFF_ROLE_ID) ?>
					</span>
					<br />
					<?= CONTROL_MULTI_SELECT_TEXT ?>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<hr />
				</td>
			</tr>
			<tr>
				<td>
					<input class ="submit" onClick="history.go(-1)" type="button" value ="Cancel" style="width: 95%;">
				</td>
				<td>
					<input class="submit" type="submit" value="Save" style="width: 95%;">
				</td>
				<td>
					&nbsp;
				</td>
			</tr>
		</table>
	</fieldset>
	<?php echo form_close(); ?>
</div>
<script>
	$(document).ready(function()
	{
		jQuery(function($)
		{
			$("#tbNameLast").Watermark("<?= JS_STAFF_NAME_LAST ?>");
			$("#tbNameFirst").Watermark("<?= JS_STAFF_NAME_FIRST ?>");
			$("#tbInitials").Watermark("<?= JS_STAFF_INITIALS ?>");
			$("#tbLogin").Watermark("<?= JS_STAFF_LOGIN_ID ?>");
			$("#tbEmail").Watermark("<?= JS_STAFF_EMAIL ?>");
		});
	});
</script>

