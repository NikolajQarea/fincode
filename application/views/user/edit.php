<div id="content" class="clearfix">
	<?php echo form_open(base_url().'user/edit_user/'.$data['Staff_ID']); ?>
	<fieldset style="width: 725px;">
		<legend>
			Edit Account Information
		</legend>
		<table class="editform">
			<tr>
				<td colspan="3">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_STAFF_NAME_LAST ?>
				</td>
				<td>
					<input class="input" id="tbNameLast" name="staff[<?= STAFF_NAME_LAST ?>]" type="text" value="<?= ($this->session->flashdata(STAFF_NAME_LAST)) ? $this->session->flashdata(STAFF_NAME_LAST) : stripslashes($data[STAFF_NAME_LAST]) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STAFF_NAME_LAST) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_STAFF_NAME_FIRST ?>
				</td>
				<td>
					<input class="input" id="tbNameFirst" name="staff[<?= STAFF_NAME_FIRST ?>]" type="text" value="<?= ($this->session->flashdata(STAFF_NAME_FIRST)) ? $this->session->flashdata(STAFF_NAME_FIRST) : stripslashes($data[STAFF_NAME_FIRST]) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STAFF_NAME_FIRST) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_STAFF_INITIALS ?>
				<td>
					<input class="input" id="Initials" name="staff[<?= STAFF_INITIALS ?>]" type="text" value="<?= ($this->session->flashdata(STAFF_INITIALS)) ? $this->session->flashdata(STAFF_INITIALS) : $data[STAFF_INITIALS] ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STAFF_INITIALS) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_STAFF_LOGIN ?>
				</td>
				<td>
					<input class="input" id="tbLogin" name="staff[<?= STAFF_LOGIN ?>]" type="text" value="<?= ($this->session->flashdata(STAFF_LOGIN)) ? $this->session->flashdata(STAFF_LOGIN) : $data[STAFF_LOGIN] ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STAFF_LOGIN) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_STAFF_PASSWORD_NEW ?>
				</td>
				<td>
					<input class="input" name="staff[<?= STAFF_PASSWORD_NEW ?>]" type="password" value="<?= ($this->session->flashdata(STAFF_PASSWORD_NEW)) ? ($this->session->flashdata(STAFF_PASSWORD_NEW)) : '' ?>">
				</td>
				<td class="errors">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_STAFF_PASSWORD_CONFIRM ?>
				</td>
				<td>
					<input class="input" name="staff[<?= STAFF_PASSWORD_CONFIRM ?>]" type="password" value="<?= ($this->session->flashdata(STAFF_PASSWORD_CONFIRM)) ? ($this->session->flashdata(STAFF_PASSWORD_CONFIRM)) : '' ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STAFF_PASSWORD_CONFIRM) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . STAFF_EMAIL ?>
				</td>
				<td>
					<input class="input" id="tbEmail" name="staff[<?= STAFF_EMAIL ?>]" type="text" value="<?= ($this->session->flashdata(STAFF_EMAIL)) ? ($this->session->flashdata(STAFF_EMAIL)) : $data[STAFF_EMAIL] ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STAFF_EMAIL) ?>
				</td>
			</tr>
			<tr>
				<td style="vertical-align: top;">
					<?= REQUIRED_FIELD . LABEL_STAFF_STATUS_ID ?>
				</td>
				<td>
					<select class="input" name="staff[<?= STAFF_STAFF_STATUS_ID ?>]">
						<?= $this->User_model->get_user_status_options($data[STAFF_ID]) ?>
					</select>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_STAFF_STATUS_ID) ?>
				</td>
			</tr>
			<tr>
				<td style="vertical-align: top;">
					<?= REQUIRED_FIELD . LABEL_STAFF_ROLE_ID ?>
				</td>
				<td style="vertical-align: top;">
					<select class="input" multiple="multiple" name="<?= STAFF_ROLE_ID ?>[]" size="4">
						<?= $this->User_model->get_user_roles_options($data[STAFF_ID]) ?>
					</select>
				</td>
				<td style="vertical-align: top;">
					<span class="error">
						<?= $this->session->flashdata(ERROR_STAFF_ROLE_ID) ?>
					</span>
					<br />
					<?= CONTROL_MULTI_SELECT_TEXT ?>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<hr />
				</td>
			</tr>
			<tr>
				<td>
					<input class ="submit" onClick="history.go(-1)" type="button" value ="Cancel" style="width: 95%;">
				</td>
				<td>
					<input class="submit" type="submit" value="Save" style="width: 95%;">
				</td>
			</tr>
		</table>
	</fieldset>
	<?php echo form_close(); ?>
	<div class="clearfix">
		&nbsp;
	</div>
	<fieldset style="width: 725px; text-align: left;">
		<legend>
			Other Information
		</legend>
		<table class="notes">
			<colgroup>
				<col style="width: 100px;" />
				<col style="width: 225px;" />
				<col style="width: 100px;" />
				<col style="width: 225px;" />
			</colgroup>
			<tr>
				<td class="label">
					Staff ID:
				</td>
				<td class="data">
					<?= $data['Staff_ID']; ?>
				</td>
				<td colspan="2">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_STAFF_STAMP_USER_ID ?>
				</td>
				<td class="data">
					<?= $data[LABEL_STAFF_VIEW_STAMP_USER_NAME_FULL]; ?>
				</td>
				<td class="label">
					<?= LABEL_STAFF_LAST_EDIT_USER_ID ?>
				</td>
				<td class="data">
					<?= $data[LABEL_STAFF_VIEW_LAST_EDIT_USER_NAME_FULL]; ?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_STAFF_STAMP_DATE ?>
				</td>
				<td class="data">
					<?= ($data[STAFF_STAMP_DATE] != null ? date(DATE_FORMAT, $data[STAFF_STAMP_DATE]) :'' ); ?>
				</td>
				<td class="label">
					<?= LABEL_STAFF_LAST_EDIT_DATE ?>
				</td>
				<td class="data">
					<?= ($data[STAFF_LAST_EDIT_DATE] != 0 ? date(DATE_FORMAT, $data[STAFF_LAST_EDIT_DATE]) : ''); ?>
				</td>
			</tr>
		</table>
	</fieldset>
</div>
<script>
	$(document).ready(function()
	{
		jQuery(function($)
		{
			$("#tbNameLast").Watermark("<?= JS_STAFF_NAME_LAST ?>");
			$("#tbNameFirst").Watermark("<?= JS_STAFF_NAME_FIRST ?>");
			$("#tbInitials").Watermark("<?= JS_STAFF_INITIALS ?>");
			$("#tbLogin").Watermark("<?= JS_STAFF_LOGIN_ID ?>");
			$("#tbEmail").Watermark("<?= JS_STAFF_EMAIL ?>");
		});
	});
</script>