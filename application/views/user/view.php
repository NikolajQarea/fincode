<?php 
//krumo($post);
?>
<div class="clearfix"></div>
<?php echo form_open(base_url() . 'user/view'); ?>
<div class="clearfix">
    <fieldset>
        <legend>
            Filters
        </legend>
        <table style="font-size: 8pt; display: inline-block; padding: 0px;">
            <tr>
                <td class="tdFilter" valign="middle">
                    <label for="user[include_terminated]">
                        Include Terminated?
                    </label>
                    <?php
                        $val = (($post == 'on') ? 'off' : 'on'); 
                        $checked = (($post == 'off') ? '' : 'checked="checked"'); 
                    ?>
                    <input class="autoSubmit" id="user[include_terminated]" name="user[include_terminated]"  title="Include Terminated?" value="<?= $val ?>" type="hidden"  />
                    <input <?= $checked ?> class="autoSubmit" id="user[include_terminated]" name="user[include_terminated]"  title="Include Terminated?" value="<?= $val ?>" type="checkbox"  />
                </td>
            </tr>
        </table>
    </fieldset>
</div>
<?php echo form_close(); ?>
	<div class="message clearfix" style="margin-bottom: 10px;">
		<span class="link">
			<?= $link ?>
		</span>
	</div>
	<?php if(isset($list[0]['Login_ID'])): ?>
	<table border="1" class="view">
		<tr>
			<th>
				<a class="view_link" href="<?= base_url() ?>user/view/<?= $page ?>/Login_ID/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Login ID
					<?= ($order == 'Login_ID')&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == 'Login_ID')&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>user/view/<?= $page ?>/Staff_Name_Full/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Name
					<?= ($order == 'Staff_Name_Full')&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == 'Staff_Name_Full')&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>user/view/<?= $page ?>/Initials/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Initials
					<?= ($order == 'Staff_ID')&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == 'Staff_ID')&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>user/view/<?= $page ?>/Email/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					E-mail 
					<?= ($order == 'Email')&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == 'Email')&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>user/view/<?= $page ?>/User_Roles/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Assigned User Roles
					<?= ($order == 'User_Roles')&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == 'User_Roles')&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>user/view/<?= $page ?>/Staff_Status_Label/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Status
					<?= ($order == 'Staff_Status_Label')&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == 'Staff_Status_Label')&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>user/view/<?= $page ?>/Stamp_Date/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Created On
					<?= ($order == 'Stamp_Date')&&($as == 'ASC') ? '&uarr;' : '' ?>
					<?= ($order == 'Stamp_Date')&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>user/view/<?= $page ?>/Stamp_User_Name_Full/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Created By
					<?= ($order == 'Stamp_User_Name_Full')&&($as == 'ASC') ? '&uarr;' : '' ?>
					<?= ($order == 'Stamp_User_Name_Full')&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>user/view/<?= $page ?>/Last_Edit_Date/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Last Edited On
					<?= ($order == 'Last_Edit_Date')&&($as == 'ASC') ? '&uarr;' : '' ?>
					<?= ($order == 'Last_Edit_Date')&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>user/view/<?= $page ?>/Last_Edit_User_Name_Full/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Last Edited By
					<?= ($order == 'Last_Edit_User_Name_Full')&&($as == 'ASC') ? '&uarr;' : '' ?>
					<?= ($order == 'Last_Edit_User_Name_Full')&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<!-- ?php if($this->session->userdata(ROLE_ADMIN)):?  ->
			<!-- th >
				Action
			</th -->
			<!-- ?php endif;? -->
		</tr>
		<?php foreach ($list as $key => $value): ?>
		<?php if ($this->session->userdata(ROLE_ADMIN)): ?>
		<tr class="clickRow" ondblclick="window.location='<?= base_url() ?>user/edit/<?= $value['Staff_ID'] ?>';">
		<?php else: ?>
		<tr>
		<?php endif; ?>
			<td>
				<?= $value['Login_ID'] ?>
			</td>
			<td>
				<?= $value['Staff_Name_Full'] ?>
			</td>
			<td>
				<?= $value['Initials'] ?>
			</td>
			<td>
				<?= $value['Email'] ?>
			</td>
			<td>
				<?= $value['User_Roles'] ?>
			</td>
			<td>
				<?= $value['Staff_Status_Label'] ?>
			</td>
			<td>
				<?= date('m/d/Y', $value['Stamp_Date']) ?> 
			</td>
			<td>
				<?= $value['Stamp_User_Name_Full'] ?>
			</td>
			<td>
				<?= date('m/d/Y', $value['Last_Edit_Date']) ?> 
			</td>
			<td>
				<?= $value['Last_Edit_User_Name_Full'] ?>
			</td>
			<!-- td -->
				<!-- a href="<?= base_url() ?>user/edit/<?= $value['Staff_ID'] ?>" -->
					<!-- img src="<?= $this->config->item('base_url'); ?>application/views/css/images/edit.png" width="15px" / -->
				<!-- /a -->
				<!-- a href="<?= base_url() ?>user/delete/<?= $value['Staff_ID'] ?>" onclick="if ( confirm( 'Are you sure you wish to delete this user? This action cannot be undone.\n Press \'Cancel\' to quit, or \'OK\' to continue with delete.' ) ) { return true;}return false;" -->
					<!-- img src="<?= $this->config->item('base_url'); ?>application/views/css/images/delete.png" width="15px" / -->
				<!-- /a -->
			<!-- /td -->
		</tr>
		<?php endforeach; ?>
	</table>
	<?php else : 
    echo '<h3 align = "center">'.$list.'</h3>'; ?><?php endif; ?>
	<div class="message clearfix" style="margin-top: 10px;">
		<span class="link">
			<?= $link ?>
		</span>
	</div>
</div>


