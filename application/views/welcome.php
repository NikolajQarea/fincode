<?php if ($this->session->userdata('Login_ID')): ?>
<div id="content" class="clearfix">
	<p>
		Welcome
		<? print($this->session->userdata('Staff_Name_First') . '&nbsp;' . $this->session->userdata('Staff_Name_Last')); ?>
		to the <span class="companyName">Ganer + Ganer, PLLC</span> intranet.
	</p>
</div>
<?php else: ?>
<div id="content" class="clearfix">
 	<p>
 		Welcome to the <span class="companyName">Ganer + Ganer, PLLC</span> intranet.
 	</p>
 	<p>
 		Please log in to begin using this site.
 	</p>
 </div>
<?php endif; ?>

