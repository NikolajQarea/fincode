<div id="content">
<?php echo form_open(base_url().'client/add_client'); ?>
<table>
	<tr>
		<td><label for="client[<?= CLIENT_NAME_LAST ?>]"><?= REQUIRED_FIELD . LABEL_CLIENT_NAME_LAST</label></td>
		<td><input type="text" name="client[<?= CLIENT_NAME_LAST ?>]" value ="<?= $this->session->flashdata(CLIENT_NAME_LAST) ?>"></td>
                <td class ="errors"><?= $this->session->flashdata(ERROR_CLIENT_NAME_LAST) ?></td>
	</tr>
	<tr>
		<td><label for="client[<?= CLIENT_NAME_LAST ?>]"><?= REQUIRED_FIELD . LABEL_CLIENT_NAME_FIRST</label></td>
		<td><input type="text" name="client[<?= CLIENT_NAME_FIRST ?>]" value ="<?= $this->session->flashdata(CLIENT_NAME_FIRST) ?>"></td>
                <td class ="errors"><?= $this->session->flashdata(ERROR_CLIENT_NAME_FIRST) ?></td>
	</tr>
	<tr>
		<td><label for="client[<?= SPOUSE_NAME_LAST ?>]"><?= LABEL_SPOUSE_NAME_LAST ?></label></td>
		<td><input type="text" name="client[<?= SPOUSE_NAME_LAST ?>]"></td>
	</tr>
	<tr>
		<td><label for="client[<?= SPOUSE_NAME_FIRST ?>]"><?= LABEL_SPOUSE_NAME_FIRST ?></label></td>
		<td><input type="text" name="client[<?= SPOUSE_NAME_FIRST ?>]"></td>
	</tr>

	<tr>
		<td><label for="client[form]">Choose form: </label></td>
		<td><select name="client[form][]" multiple="multiple">
		  <option value="709">709</option>
		  <option value="1040">1040</option>
		  <option value="1041">1041</option>
		  <option value="1120">1120</option>
		  <option value="Grantor Trust">Grantor Trust</option>
			</select>
		</td>
		<td valign="bottom">
			<?= CONTROL_MULTI_SELECT_TEXT ?>
		</td>
	</tr>
	<tr>
		<td><label for="client[state]">Choose state: </label></td>
		<td><select name="client[state][]" multiple="multiple">
		  <option value="AZ">AZ</option>
		  <option value="CT">CT</option>
		  <option value="KY">KY</option>
		  <option value="LA">LA</option>
		  <option value="MA">MA</option>
		  <option value="NJ">NJ</option>
		  <option value="NY">NY</option>
		  <option value="PA">PA</option>
		  <option value="RI">RI</option>
		  <option value="TX">TX</option>
		  <option value="VA/NY">VA/NY</option>
			</select>
		</td>
		<td valign="bottom">
			<?= CONTROL_MULTI_SELECT_TEXT ?>
		</td>
	</tr>
	<tr>
		<td><label for="client[staff]">Choose staff: </label></td>
		<td><select size="1" name="client[staff]">
		  <option value="ET">ET</option>
		  <option value="GG">GG</option>
		  <option value="KE">KE</option>
		  <option value="LZ">LZ</option>
		  <option value="MG">MG</option>
		  <option value="MJG">MJG</option>
		  <option value="RG">RG</option>
		  <option value="SP">SP</option>
		  <option value="TG">TG</option>
		  <option value="WG">WG</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><label for="client[entity]">Choose entity type: </label></td>
		<td><select size="1" name="client[entity]">
		  <option value="1065">1065</option>
		  <option value="C Corp">C Corp</option>
		  <option value="LLC">LLC</option>
		  <option value="S Corp">S Corp</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><label for="client[partner]">Choose client partner: </label></td>
		<td><select size="1" name="client[partner]">
		  <option value="RG">RG</option>
		  <option value="MG">MG</option>
		  <option value="MJG">MJG</option>
			</select>
		</td>
	</tr>
	<tr>
	<tr>
		<td><label for="client[<?= CLIENT_BILLING_ID ?>]"><?= LABEL_CLIENT_BILLING_ID ?></label></td>
		<td><select size="1" name="client[Billing_ID]">
		  <option value=""></option>
		  <option value="CG">CG</option>
		  <option value="TG">TG</option>
		  <option value="RG">RG</option>
		  <option value="etc">etc</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><label for="client[<?= CLIENT_DOCUMENT_ID ?>]"><?= LABEL_CLIENT_DOCUMENT_ID ?></label></td>
		<td><input type="text" name="client[<?= CLIENT_DOCUMENT_ID ?>]"></td>
	</tr>
	<tr>
		<td><label for="client[month]">Choose 1-st month of tax year: </label></td>
		<td><select size="1" name="client[month]">
		  <option value="1">January</option>
		  <option value="2">February</option>
		  <option value="3">March</option>
		  <option value="4">April</option>
		  <option value="5">May</option>
		  <option value="6">June</option>
		  <option value="7">July</option>
		  <option value="8">August</option>
		  <option value="9">September</option>
		  <option value="10">October</option>
		  <option value="11">November</option>
		  <option value="12">December</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><label for="client[<?= CLIENT_INITIAL_YEAR_ID ?>]"><?= LABEL_CLIENT_INITIAL_YEAR_ID ?></label></td>
		<td><select size="1" class="initial" name="client[<?= CLIENT_INITIAL_YEAR_ID ?>]">
		<?php for($i = 2000; $i <= 2013; $i++): ?>
		  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
		  <?php endfor; ?>
		  <option value="man_initial">Enter manually</option>
			</select>
		</td>
		<td><input class="manual_initial" name="" type="text" size="2" min="1980" max="2099" maxlength="4" style="display: none"></td>
	</tr>
	<tr>
		<td><label for="client[<?= CLIENT_RETAINER_YEAR ?>]"><?= LABEL_CLIENT_RETAINER_YEAR ?></label></td>
		<td><select size="1" class="retainer" name="client[<?= CLIENT_RETAINER_YEAR ?>]">
		<?php for($i = 2000; $i <= 2013; $i++): ?>
		  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
		  <?php endfor; ?>
		  <option value="man_retainer">Enter manually</option>
			</select>
		</td>
		<td><input class="manual_retainer" name="" type="text" size="2" min="1980" max="2099" maxlength="4" style="display: none"></td>
	</tr>
	<tr>
		<td><label for="client[outlook]">Outlook?: </label></td>
		<td><input name="client[outlook]" type="checkbox" value="1"></td>
	</tr>
	<tr>
		<td><label for="client[retainer]">Choose retainer: </label></td>
		<td><select size="1" name="client[retainer]">
		  <option value="individual">individual</option>
		  <option value="signed">signed</option>
		  <option value="trust">trust</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><label for="client[<?= CLIENT_ACCOUNTING_SOFTWARE_ID ?>]"><?= LABEL_CLIENT_ACCOUNTING_SOFTWARE_ID ?></label></td>
		<td><select size="1" name="client[<?= CLIENT_ACCOUNTING_SOFTWARE_ID ?>]">
		  <option value="Quickbooks">Quickbooks</option>
		  <option value="Quickbooks Enterprise">Quickbooks Enterprise</option>
		  <option value="Quicken">Quicken</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><label for="client[<?= CLIENT_PRINCIPAL ?>]"><?= REQUIRED_FILED . LABEL_CLIENT_PRINCIPAL ?></label></td>
		<td><input type="text" name="client[<?= CLIENT_PRINCIPAL ?>]" value ="<?= $this->session->flashdata(CLIENT_PRINCIPAL) ?>"></td>
                <td class ="errors"><?= $this->session->flashdata(ERROR_CLIENT_PRINCIPAL) ?></td>
	</tr>
	<tr>
		<td><label for="client[<?= CLIENT_SSN_EIN ?>]"><?= REQUIRED_FIELD . LABEL_CLIENT_SSN_EIN</label></td>
		<td><input type="text" name="client[<?= CLIENT_SSN_EIN ?>]" value ="<?= $this->session->flashdata(CLIENT_SSN_EIN) ?>"></td>
                <td class ="errors"><?= $this->session->flashdata(ERROR_CLIENT_SSN_EIN) ?></td>
	</tr>
	<tr>
		<td><label for="client[<?= SPOUSE_SSN_EIN ?>]"><?= LABEL_SPOUSE_SSN_EIN ?></label></td>
		<td><input type="text" name="client[<?= SPOUSE_SSN_EIN ?>]"></td>
	</tr>
	<tr>
		<td><label for="client[<?= CLIENT_REFERRED_BY_ID>?]"><?= LABEL_CLIENT_REFERRED_BY_ID ?></label></td>
		<td><input type="text" name="client[<?= CLIENT_REFERRED_BY_ID ?>]"></td>
	</tr>
	<tr>
		<td><label for="client[occupation]">Choose occupation: </label></td>
		<td><select size="1" name="client[occupation]">
		  <option value="Contractor/Construction">Contractor/Construction</option>
		  <option value="Dan Coughlan">Dan Coughlan</option>
		  <option value="indobox">indobox</option>
		  <option value="Information services">Information services</option>
		  <option value="MKM Capital Advisors">MKM Capital Advisors</option>
		  <option value="trust">trust</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><label for="client[occupation_spouse]">Enter spouse occupation: </label></td>
		<td><select size="1" name="client[occupation_spouse]">
			  <option value=""></option>
			  <option value="Teacher">Teacher</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><label for="client[notes]">Enter notes: </label></td>
		<td><textarea name="client[notes]"> </textarea></td>
	</tr>

	<tr>
		<td><label for="client[status]">Client status: </label></td>
		<td><select size="1" name="client[status]">
		  <option value="active">active</option>
		  <option value="inactive">inactive</option>
	</tr>
	<tr>
	<td colspan ="2" align ="center"><input type="submit" value="Send"></td>
	</tr>
</table>
<?php echo form_close(); ?>
</div>