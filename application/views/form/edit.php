<div id="content">
<?php echo form_open(base_url().'client/update_client/'.$data['id_c']); ?>
<table>
	<tr>
		<td><label for="client[<?= CLIENT_NAME_LAST ?>]"><?= REQUIRED_FIELD . LABEL_CLIENT_NAME_LAST</label></td>
		<td><input type="text" name="client[<?= CLIENT_NAME_LAST ?>]" value ="<?= ($this->session->flashdata(CLIENT_NAME_LAST)) ? $this->session->flashdata(CLIENT_NAME_LAST) : $data[CLIENT_NAME_LAST] ?>"></td>
                <td class ="errors"><?= $this->session->flashdata(ERROR_CLIENT_NAME_LAST) ?></td>
	</tr>
	<tr>
		<td><label for="client[<?= CLIENT_NAME_FIRST ?>]"><?= REQUIRED_FIELD . LABEL_CLIENT_NAME_LAST ?></label></td>
		<td><input type="text" name="client[<?= CLIENT_NAME_FIRST ?>]" value ="<?= ($this->session->flashdata(CLIENT_NAME_FIRST)) ? $this->session->flashdata(CLIENT_NAME_FIRST) : $data[CLIENT_NAME_FIRST] ?>"></td>
                <td class ="errors"><?= $this->session->flashdata(ERROR_CLIENT_NAME_FIRST) ?></td>
	</tr>
	<tr>
		<td><label for="client[<?= SPOUSE_NAME_LAST ?>]"><?= LABEL_SPOUSE_NAME_LAST ?></label></td>
		<td><input type="text" name="client[<?= SPOUSE_NAME_LAST ?>]" value="<?= $data[SPOUSE_NAME_LAST] ?>"></td>
	</tr>
	<tr>
		<td><label for="client[<?= SPOUSE_NAME_FIRST ?>]"><?= LABEL_SPOUSE_NAME_FIRST ?></label></td>
		<td><input type="text" name="client[<?= SPOUSE_NAME_FIRST ?>]" value="<?= $data[SPOUSE_NAME_FIRST]; ?>"></td>
	</tr>

	<tr>
		<td><label for="client[form]">Choose form: </label></td>
		<td><select name="client[form][]" multiple="multiple" size="3">
		  <option value="709" <?= (strstr($data['form'], '709')) ? 'selected="selected"' : '' ?>>709</option>
		  <option value="1040" <?= (strstr($data['form'], '1040')) ? 'selected="selected"' : '' ?>>1040</option>
		  <option value="1041" <?= (strstr($data['form'], '1041')) ? 'selected="selected"' : ''; ?>>1041</option>
		  <option value="1120" <?= (strstr($data['form'], '1120')) ? 'selected="selected"' : '' ?>>1120</option>
		  <option value="Grantor Trust" <?= (strstr($data['form'], 'Grantor Trust')) ? 'selected="selected"' : '' ?>>Grantor Trust</option>
			</select>
		</td>
		<td valign="bottom">
			<?= CONTROL_MULTI_SELECT_TEXT ?>
		</td>
	</tr>
	<tr>
		<td><label for="client[state]">Choose state: </label></td>
		<td><select name="client[state][]" multiple="multiple" size="3">
		  <option value="AZ" <?= (strstr($data['state'], 'AZ')) ? 'selected="selected"' : '' ?>>AZ</option>
		  <option value="CT" <?= (strstr($data['state'], 'CT')) ? 'selected="selected"' : '' ?>>CT</option>
		  <option value="KY" <?= (strstr($data['state'], 'KY')) ? 'selected="selected"' : '' ?>>KY</option>
		  <option value="LA" <?= (strstr($data['state'], 'LA')) ? 'selected="selected"' : '' ?>>LA</option>
		  <option value="MA" <?= (strstr($data['state'], 'MA')) ? 'selected="selected"' : '' ?>>MA</option>
		  <option value="NJ" <?= (strstr($data['state'], 'NJ')) ? 'selected="selected"' : '' ?>>NJ</option>
		  <option value="NY" <?= (strstr($data['state'], 'NY')) ? 'selected="selected"' : '' ?>>NY</option>
		  <option value="PA" <?= (strstr($data['state'], 'PA')) ? 'selected="selected"' : '' ?>>PA</option>
		  <option value="RI" <?= (strstr($data['state'], 'RI')) ? 'selected="selected"' : '' ?>>RI</option>
		  <option value="TX" <?= (strstr($data['state'], 'TX')) ? 'selected="selected"' : '' ?>>TX</option>
		  <option value="VA/NY" <?= (strstr($data['state'], 'VA/NY')) ? 'selected="selected"' : '' ?>>VA/NY</option>
			</select>
		</td>
		<td valign="bottom">
			<?= CONTROL_MULTI_SELECT_TEXT ?>
		</td>
	</tr>
	<tr>
		<td><label for="client[staff]">Choose staff: </label></td>
		<td><select name="client[staff]">
		  <option value="ET" <?= ('ET' == $data['staff']) ? 'selected="selected"' : '' ?>>ET</option>
		  <option value="GG" <?= ('GG' == $data['staff']) ? 'selected="selected"' : '' ?>>GG</option>
		  <option value="KE" <?= ('KE' == $data['staff']) ? 'selected="selected"' : '' ?>>KE</option>
		  <option value="LZ" <?= ('LZ' == $data['staff']) ? 'selected="selected"' : '' ?>>LZ</option>
		  <option value="MG" <?= ('MG' == $data['staff']) ? 'selected="selected"' : '' ?>>MG</option>
		  <option value="MJG" <?= ('MJG' == $data['staff']) ? 'selected="selected"' : '' ?>>MJG</option>
		  <option value="RG" <?= ('RG' == $data['staff']) ? 'selected="selected"' : '' ?>>RG</option>
		  <option value="SP" <?= ('SP' == $data['staff']) ? 'selected="selected"' : '' ?>>SP</option>
		  <option value="TG" <?= ('TG' == $data['staff']) ? 'selected="selected"' : '' ?>>TG</option>
		  <option value="WG" <?= ('WG' == $data['staff']) ? 'selected="selected"' : '' ?>>WG</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><label for="client[entity]">Choose entity type: </label></td>
		<td><select size="1" name="client[entity]">
		  <option value="1065"<?= ('1065' == $data['entity']) ? 'selected="selected"' : '' ?>>1065</option>
		  <option value="C Corp"<?= ('C Corp' == $data['entity']) ? 'selected="selected"' : '' ?>>C Corp</option>
		  <option value="LLC"<?= ('LLC' == $data['entity']) ? 'selected="selected"' : '' ?>>LLC</option>
		  <option value="S Corp"<?= ('S Corp' == $data['entity']) ? 'selected="selected"' : '' ?>>S Corp</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><label for="client[partner]">Choose client partner: </label></td>
		<td><select size="1" name="client[partner]">
		  <option value="RG" <?= ('RG' == $data['partner']) ? 'selected="selected"' : '' ?>>RG</option>
		  <option value="MG" <?= ('MG' == $data['partner']) ? 'selected="selected"' : '' ?>>MG</option>
		  <option value="MJG" <?= ('MJG' == $data['partner']) ? 'selected="selected"' : '' ?>>MJG</option>
			</select>
		</td>
	</tr>
	<tr>
	<tr>
		<td><label for="client[<?= CLIENT_BILLING_ID ?>]"><?= CLIENT_BILLING_ID ?></label></td>
		<td><input type="text" name="client[<?= CLIENT_BILLING_ID ?>]" value ="<?= $data[CLIENT_BILLING_ID] ?>"></td>
			</select>
		</td>
	</tr>
	<tr>
		<td><label for="client[<?= CLIENT_DOCUMENT_ID ?>]"><?= LABEL_CLIENT_DOCUMENT_ID ?></label></td>
		<td><input type="text" name="client[<?= CLIENT_DOCUMENT_ID ?>]" value ="<?= $data[CLIENT_DOCUMENT_ID] ?>"></td>
	</tr>
	<tr>
		<td><label for="client[month]">Choose 1-st month of tax year: </label></td>
		<td><select size="1" name="client[month]">
		  <option value="1" <?= ('1' == $data['month']) ? 'selected="selected"' : '' ?>>January</option>
		  <option value="2" <?= ('2' == $data['month']) ? 'selected="selected"' : '' ?>>February</option>
		  <option value="3" <?= ('3' == $data['month']) ? 'selected="selected"' : '' ?>>March</option>
		  <option value="4" <?= ('4' == $data['month']) ? 'selected="selected"' : '' ?>>April</option>
		  <option value="5" <?= ('5' == $data['month']) ? 'selected="selected"' : '' ?>>May</option>
		  <option value="6" <?= ('6' == $data['month']) ? 'selected="selected"' : '' ?>>June</option>
		  <option value="7" <?= ('7' == $data['month']) ? 'selected="selected"' : '' ?>>July</option>
		  <option value="8" <?= ('8' == $data['month']) ? 'selected="selected"' : '' ?>>August</option>
		  <option value="9" <?= ('9' == $data['month']) ? 'selected="selected"' : '' ?>>September</option>
		  <option value="10" <?= ('10' == $data['month']) ? 'selected="selected"' : '' ?>>October</option>
		  <option value="11" <?= ('11' == $data['month']) ? 'selected="selected"' : '' ?>>November</option>
		  <option value="12" <?= ('12' == $data['month']) ? 'selected="selected"' : '' ?>>December</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><label for="client[<?= CLIENT_INITIAL_YEAR_ID ?>]">Enter initial year: </label></td>
		<td><select size="1" class="initial" name="client[<?= CLIENT_INITIAL_YEAR_ID ?>]">
		<?php for($i = 2000; $i <= 2013; $i++): ?>
		  <option value="<?= $i ?>" <?= ($i == $data[CLIENT_INITIAL_YEAR_ID]) ? 'selected="selected"' : '' ?>><?= $i ?></option>
		  <?php endfor; ?>
		  <option value="man_initial">Enter manually</option>
			</select>
		</td>
		<td><input class="manual_initial" name="" type="text" size="2" min="1980" max="2099" maxlength="4" style="display: none"></td>
	</tr>
	<tr>
		<td><label for="client[<?= CLIENT_RETAINER_YEAR ?>]"><?= LABEL_CLIENT_RETAINER_YEAR ?></label></td>
		<td><select size="1" class="retainer" name="client[<?= CLIENT_RETAINER_YEAR ?>]">
		<?php for($i = 2000; $i <= 2013; $i++): ?>
		  <option value="<?= $i ?>" <?= ($i == $data[CLIENT_RETAINER_YEAR]) ? 'selected="selected"' : '' ?>><?= $i ?></option>
		  <?php endfor; ?>
		  <option value="man_retainer">Enter manually</option>
			</select>
		</td>
		<td><input class="manual_retainer" name="" type="text" size="2" min="1980" max="2099" maxlength="4" style="display: none"></td>
	</tr>
	<tr>
		<td><label for="client[outlook]">Outlook?: </label></td>
		<td><input name="client[outlook]" type="checkbox" value ="1" <?= $data['outlook'] ? 'checked="checked"' : '' ?>></td>
	</tr>
	<tr>
		<td><label for="client[retainer]">Choose retainer: </label></td>
		<td><select size="1" name="client[retainer]">
		  <option value="individual" <?= ('individual' == $data['retainer']) ? 'selected="selected"' : '' ?>>individual</option>
		  <option value="signed" <?= ('signed' == $data['retainer']) ? 'selected="selected"' : '' ?>>signed</option>
		  <option value="trust" <?= ('trust' == $data['retainer']) ? 'selected="selected"' : '' ?>>trust</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><label for="client[<?= CLIENT_ACCOUNTING_SOFTWARE_ID ?>]"><?= LABEL_CLIENT_ACCOUNTING_SOFTWARE_ID ?></label></td>
		<td><select size="1" name="client[<?= CLIENT_ACCOUNTING_SOFTWARE_ID ?>]">
		  <option value="Quicbooks" <?= ('Quicbooks' == $data[CLIENT_ACCOUNTING_SOFTWARE_ID]) ? 'selected="selected"' : '' ?>>Quickbooks</option>
		  <option value="Quicbooks Enterprise" <?= ('Quicbooks Enterprise' == $data[CLIENT_ACCOUNTING_SOFTWARE_ID]) ? 'selected="selected"' : '' ?>>Quickbooks Enterprise</option>
		  <option value="Quicken" <?= ('Quicken' == $data[CLIENT_ACCOUNTING_SOFTWARE_ID]) ? 'selected="selected"' : '' ?>>Quicken</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><label for="client[<?= CLIENT_PRINCIPAL ?>]"><?= REQUIRED_FIELD . LABEL_CLIENT_PRINCIPAL ?></label></td>
		<td><input type="text" name="client[<?= CLIENT_PRINCIPAL ?>]" value ="<?= $data[CLIENT_PRINCIPAL] ?>"></td>
	</tr>
	<tr>
		<td><label for="client[<?= CLIENT_SSN_EIN ?>]"><?= REQUIRED_FIELD . LABEL_CLIENT_SSN_EIN</label></td>
		<td><input type="text" name="client[<?= CLIENT_SSN_EIN ?>]" value ="<?= $data[CLIENT_SSN_EIN] ?>"></td>
	</tr>
	<tr>
		<td><label for="client[<?= SPOUSE_SSN_EIN ?>]"><?= LABEL_SPOUSE_SSN_EIN ?></label></td>
		<td><input type="text" name="client[<?= SPOUSE_SSN_EIN ?>]" value ="<?= $data[SPOUSE_SSN_EIN] ?>"></td>
	</tr>
	<tr>
		<td><label for="client[<?= CLIENT_REFERRED_BY_ID ?>]"><?= LABEL_REFERRED_BY_ID ?></label></td>
		<td><input type="text" name="client[<?= CLIENT_REFERRED_BY_ID ?>]" value ="<?= $data[CLIENT_REFERRED_BY_ID] ?>"></td>
	</tr>
	<tr>
		<td><label for="client[occupation]">Choose occupation: </label></td>
		<td><select name="client[occupation]">
		  <option value="Contractor/Construction" <?= ('Contractor/Construction' == $data['occupation']) ? 'selected="selected"' : '' ?>>Contractor/Construction</option>
		  <option value="Dan Coughlan" <?= ('Dan Coughlan' == $data['occupation']) ? 'selected="selected"' : '' ?>>Dan Coughlan</option>
		  <option value="indobox" <?= ('indobox' == $data['occupation']) ? 'selected="selected"' : '' ?>>indobox</option>
		  <option value="Information services" <?= ('Information services' == $data['occupation']) ? 'selected="selected"' : '' ?>>Information services</option>
		  <option value="MKM Capital Advisors" <?= ('MKM Capital Advisors' == $data['occupation']) ? 'selected="selected"' : '' ?>>MKM Capital Advisors</option>
		  <option value="trust" <?= ('trust' == $data['occupation']) ? 'selected="selected"' : '' ?>>trust</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><label for="client[occupation_spouse]">Enter spouse occupation: </label></td>
		<td><select name="client[occupation_spouse]">
			  <option value="" <?= ('' == $data['occupation_spouse']) ? 'selected="selected"' : '' ?>></option>
			  <option value="Teacher" <?= ('Teacher' == $data['occupation_spouse']) ? 'selected="selected"' : '' ?>>Teacher</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><label for="client[notes]">Enter notes: </label></td>
		<td><textarea name="client[notes]" value ="<?= $data['notes'] ?>"> </textarea></td>
	</tr>

	<tr>
		<td><label for="client[status]">Client status: </label></td>
		<td><select size="1" name="client[status]">
		  <option value="active" <?= ('active' == $data['status']) ? 'selected="selected"' : '' ?>>active</option>
		  <option value="inactive" <?= ('inactive' == $data['status']) ? 'selected="selected"' : '' ?>>inactive</option>
	</tr>
	<tr>
	<td colspan ="2" align ="center"><input type="submit" value="Send"></td>
	</tr>
</table>
<?php echo form_close(); ?>
</div>