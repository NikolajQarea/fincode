<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<script>
			BASE_URL = "<?php print $this->config->item('base_url'); ?>";
		</script>
		<script src="<?php print $this->config->item('base_url'); ?>application/views/js/jquery-1.6.1.min.js" type="text/javascript"></script>
		<script src="<?php print $this->config->item('base_url'); ?>application/views/js/jquery.ui.core.js" type="text/javascript"></script>
		<script src="<?php print $this->config->item('base_url'); ?>application/views/js/jquery-ui-1.8.13.custom.min.js" type="text/javascript"></script>
		<script src="<?php print $this->config->item('base_url'); ?>application/views/js/jquery.ui.datepicker.js" type="text/javascript"></script>
		<script src="<?php print $this->config->item('base_url'); ?>application/views/js/scripts.js" type="text/javascript"></script>
		<script src="<?php print $this->config->item('base_url'); ?>application/views/js/script_css.js" type="text/javascript"></script>
		<script src="<?php print $this->config->item('base_url'); ?>application/views/js/autocomplete.js" type="text/javascript"></script>
		<script src="<?php print $this->config->item('base_url'); ?>application/views/js/jquery.min.js" type="text/javascript"></script>
		<script src="<?php print $this->config->item('base_url'); ?>application/views/js/jquery-ui.min.js" type="text/javascript"></script>
		<script src="<?php print $this->config->item('base_url'); ?>application/views/js/jquery.watermarkinput.js" type="text/javascript"></script>
		<script src="<?php print $this->config->item('base_url'); ?>application/views/js/jquery.maskedinput-1.3.js" type="text/javascript"></script>
		<title>Ganer + Ganer, PLLC</title>
		<meta content="text/html; charset=cp1251" http-equiv="content-type" />
		<link href="<?php echo base_url(); ?>application/views/css/css.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url(); ?>application/views/css/autocomplete.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<div id="all">
			<div id="header" class="clearfix">
				<a href="<?php print $this->config->item('base_url'); ?>" alt="Ganer + Ganer, PLLC">
					<img align="left" class="head" height="73" src="<?php print $this->config->item('base_url'); ?>application/views/css/images/logo_small.png" style="cursor: pointer;" />
				</a>
				<span class="headerCompanyName" style="line-height: 60pt; text-shadow: black 0.1em 0.1em 0.2em">
					Ganer + Ganer, PLLC
				</span>
				<?php if ($this->session->userdata('Login_ID')): ?>
				<span class="link_head">
					Welcome
					<? print ($this->session->userdata('Staff_Name_First') . '&nbsp;' . $this->session->userdata('Staff_Name_Last')); ?>
					<a href="<?php echo base_url() . 'main/exit_user'; ?>">
						<img alt="Log Out" src="<?php print $this->config->item('base_url'); ?>application/views/css/images/logout.png" width="65px" />
					</a>
				</span>
				<?php endif; ?>
			</div>
