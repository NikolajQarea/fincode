// Set up the client name autocomplete:
// clientAutocomplete($('#search'), { select: function($search) {...} });
function clientAutocomplete($clientName, options) {
    if(typeof(options) === 'undefined') { options = {}; }
    var selectCallback = options['select'] || $.noop;

    $clientName.focus(function() {
        if ($clientName.val() == '-- Any --') {
            $clientName.val("");
        }
    });

    $clientName.blur(function() {
        if ($clientName.val() == '') {
            $clientName.val("-- Any --");
        }
    });

    $clientName.autocomplete({
        minLength: 1, 
        source: function(request, response) {
            var pathArray = window.location.pathname.split('/');
            var filtered = '0';
            if (pathArray[pathArray.length-1] == 'add' && (pathArray[pathArray.length-2] == 'tax_return' || pathArray[pathArray.length-2] == 'statement')) {
               filtered = '1'; 
            }            
            var search_url = BASE_URL + '/client/autocompleted/' + escape(request.term) + '/' +filtered;
            // var search_url = document.location.href.replace(/\/(?:add|view)\b.*/, '')
            //     + '/autocompleted/' + request.term;
            $.getJSON(search_url, function(result) {
                response($.map(result, function(item) { return item.Client_Name; }));
            });
        }, 
        select: function(event, ui) {
            $clientName.val(ui.item.label);
            selectCallback($clientName);
        }
    });
}
