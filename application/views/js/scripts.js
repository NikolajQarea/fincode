(function ($)
{
	$(function ()
	{
		$('select.Tax_Year_ID').change(function ()
		{
			$('.manual').hide();
			$('.manual').removeAttr('name', 'tax_return[Tax_Year_ID]');
		});
		$('select.trigger option[value="man"]').click(function ()
		{
			$('.manual').show();
			$('.manual').attr('name', 'tax_return[Tax_Year_ID]');
		});
		$('select.initial').change(function ()
		{
			$('.manual_initial').hide();
			$('.manual_initial').removeAttr('name', 'client[Initial_Year]');
		});
		$('select.initial option[value="man_initial"]').click(function ()
		{
			$('.manual_initial').show();
			$('.manual_initial').attr('name', 'tax_return[Tax_Year_ID]');
		});
		$('select.retainer').change(function ()
		{
			$('.manual_retainer').hide();
			$('.manual_retainer').removeAttr('name', 'client[Retainer_Year]');
		});
		$('select.retainer option[value="man_retainer"]').click(function ()
		{
			$('.manual_retainer').show();
			$('.manual_retainer').attr('name', 'tax_return[Tax_Year_ID]');
		});
		$('select[name="tax_return[Client_ID]"]').change(function ()
		{
			var Client_ID = $(this).attr('value');
			$.ajax(
			{
				url: 'ajax_get_info',
				data: 'Client_ID=' + Client_ID,
				dataType: 'json',
				success: function (data)
				{
					if (data.length > 0)
					{
						$('select[name="tax_return[Partner_ID]"] option[value=' + data[0].Partner_ID + ']').attr('selected', 'selected');
						$('select[name="tax_return[Tax_Form_ID]"] option[value]').removeAttr('selected');
						for (var i in data[0].Tax_Form_ID)
						{
							$('select[name="tax_return[Tax_Form_ID]"] option[value=' + data[0].Tax_Form_ID[i] + ']').attr('selected', 'selected');
						}
					}
				}
			});
		});
		////////////////////////////////
		// Statement - ABA Number Functionality
		$('select[name="statement[Institution_ID]"]').change(function ()
		{
			var Institution_ID = $(this).attr('value');
			$.ajax(
			{
				url: 'ajax_get_info',
				data: 'Institution_ID=' + Institution_ID,
				dataType: 'json',
				success: function (data)
				{
					if (data.length > 0)
					{
						$('#ABA_Number').val(data[0].Institution_ABA);
						$('#ABA_Number').focus();
					}
				}
			});
		});
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Instructions Functionality
		$('#add').click(function ()
		{
			var left = $(window).width() / 2 - $('.add_instructions').width() / 2;
			var top = $(window).height() / 2 - $('.add_instructions').height() / 2;
			$('.add_instructions').css('left', left);
			$('.add_instructions').css('top', top);
			$('.add_instructions').show();
			$('.overlay_instructions').show().fadeTo(0, 0.5);
		});
		$('.cancel_instructions').click(function ()
		{
			$('.add_instructions').hide();
			$('.overlay_instructions').hide();
		});
		$('.ok_instructions').click(function ()
		{
			var url_edit = document.location.href;
			url_edit = url_edit.replace(/\/(?:add|edit)\b.*/, '');
			url_edit += '/add_instructions';
			$.ajax(
			{
				async: false,
				type: "POST",
				url: url_edit,
				dataType: "json",
				data: "add_instructions=" + $('#add_instructions').val(),
				success: function (response)
				{
					$('.message_success_instructions').html(response.success);
					if (response.success != '')
					{
						$('.add_instructions').show(1000).delay(1000).hide(1);
						$('.overlay_instructions').show(1000).delay(1000).hide(1);
					}
					$('.error_instructions').html(response.error);
					$('.instructions').html('<option value="select">Please Select</option>');
					jQuery.each(response.instructions, function (i, val)
					{
						$('.instructions').append($('<option>').attr('value', val.id_instructions).html(val.name_instructions));
					})
				}
			});
		});
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //$('.add_client_type').click(function () {
        //    var url_view = document.location.href;
        //    url_view = url_view.replace(/\/(?:view)\b.*/, '');
        //    url_view += '/add_client_type';
        //    document.location.href = url_view;
        //});
		$('.add_client_type').click(function ()
		{
			var url_view = document.location.href;
			url_view = url_view.replace(/\/(?:view)\b.*/, '');
			url_view += '/add_client_type';
			document.location.href = url_view;
		});
		$('.add_instruction').click(function ()
		{
			var url_view = document.location.href;
			url_view = url_view.replace(/\/(?:view)\b.*/, '');
			url_view += '/add_instructions';
			document.location.href = url_view;
		});
		$('.add_retainer_status').click(function ()
		{
			var url_view = document.location.href;
			url_view = url_view.replace(/\/(?:view)\b.*/, '');
			url_view += '/add_retainer_status';
			document.location.href = url_view;
		});
		$('.add_statement_status').click(function ()
		{
			var url_view = document.location.href;
			url_view = url_view.replace(/\/(?:view)\b.*/, '');
			url_view += '/add_statement_status';
			document.location.href = url_view;
		});
		$('.add_statement_type').click(function ()
		{
			var url_view = document.location.href;
			url_view = url_view.replace(/\/(?:view)\b.*/, '');
			url_view += '/add_statement_type';
			document.location.href = url_view;
		});
		$('.add_tax_form').click(function ()
		{
			var url_view = document.location.href;
			url_view = url_view.replace(/\/(?:view)\b.*/, '');
			url_view += '/add_tax_form';
			document.location.href = url_view;
		});
		$('.add_tax_return_status').click(function ()
		{
			var url_view = document.location.href;
			url_view = url_view.replace(/\/(?:view)\b.*/, '');
			url_view += '/add_tax_return_status';
			document.location.href = url_view;
		});
		$('.add_referred_by').click(function ()
		{
			var url_view = document.location.href;
			url_view = url_view.replace(/\/(?:view)\b.*/, '');
			url_view += '/add_referred_by';
			document.location.href = url_view;
		});
		$('.add_occupation').click(function ()
		{
			var url_view = document.location.href;
			url_view = url_view.replace(/\/(?:view)\b.*/, '');
			url_view += '/add_occupation';
			document.location.href = url_view;
		});
		$('.cancel_add_occupation').click(function ()
		{
			var url_view = document.location.href;
			url_view = url_view.replace(/\/(?:view)\b.*/, '');
			url_view += '/view';
			document.location.href = url_view;
		});
		$('.add_client, .add_tax_return, .add_user').click(function ()
		{
			var url_view = document.location.href;
			url_view = url_view.replace(/\/(?:view)\b.*/, '');
			url_view += '/add';
			document.location.href = url_view;
		});
		$('.delete_client, .delete_tax_return, .lock, .unlock').click(function ()
		{
			document.location.href = $(this).attr('href');
		});
		$('.back_button_link').click(function ()
		{
			var url_view = document.location.href;
			url_view = url_view.replace(/\/(?:add|edit|edit_occupation|add_occupation|add_client_type|edit_client_type|add_instructions|edit_instructions|add_referred_by|edit_tax_form|autoadd|edit_referred_by|add_tax_form|add_tax_return_status|edit_tax_return_status)\b.*/, '');
			url_view += '/view';
			document.location.href = url_view;
		});
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//// Client Type Functionality
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Client Type: Add Button Click
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$('.quick_add_client_type').click(function ()
		{
			var left = $(window).width() / 2 - $('.add_client_type').width() / 2;
			var top = $(window).height() / 2 - $('.add_client_type').height() / 2;
			$('.add_new_client_type').css('left', left);
			$('.add_new_client_type').css('top', top);
			$('.message_success_client_type').html('');
			$('.error').html('');
			$('#add_client_type').val('');
			$('.add_new_client_type').show();
			$('.overlay').show().fadeTo(0, 0.5);
		});
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Add Client Type Cancel Button Click
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$('.cancel_client_type').click(function ()
		{
			$('.add_new_client_type').hide();
			$('.overlay').hide();
		});
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Add Client Type OK Button Click
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$('.ok_client_type').click(function ()
		{
			var url_edit = document.location.href;
			url_edit = url_edit.replace(/\/(?:add|edit)\b.*/, '');
			url_edit += '/add_client_type';
			$.ajax(
			{
				async: false,
				type: "POST",
				url: url_edit,
				dataType: "json",
				data: "add_client_type=" + $('#add_client_type').val(),
				success: function (response)
				{
					$('.message_success_client_type').html(response.success);
					if (response.success != '')
					{
						$('.add_new_client_type').show(1).delay(5000).hide(1);
						$('.overlay').show(1).delay(5000).hide(1);
					}
					$('.error').html(response.error);
					$('.client_type').html('<option value="0" selected="selected"> - Please Select - </option>');
					jQuery.each(response.client_type, function (i, val)
					{
						$('.client_type').append($('<option>').attr('value', val.Client_Type_ID).html(val.Client_Type_Label));
					})
				}
			});
		});
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//// Occupation Functionality
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Occupation: Add Button Click
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$('.quick_add_occupation').click(function()
		{
			var left = $(window).width() / 2 - $('.add_new_occupation').width() / 2;
			var top = $(window).height() / 2 - $('.add_new_occupation').height() / 2;
			$('.add_new_occupation').css('left', left);
			$('.add_new_occupation').css('top', top);
			$('.message_success_occupation').html('');
			$('.error').html('');
			$('#add_occupation').val('');
			$('.add_new_occupation').show();
			$('.overlay').show().fadeTo(0, 0.5);
		});
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Occupation: Cancel Button Click
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$('.cancel_occupation').click(function ()
		{
			$('.add_new_occupation').hide();
			$('.overlay').hide();
		});
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Occupation: OK Button Click
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$('.ok_occupation').click(function ()
		{
			var url_edit = document.location.href;
			url_edit = url_edit.replace(/\/(?:add|edit)\b.*/, '');
			url_edit += '/add_occupation';
			$.ajax(
			{
				async: false,
				type: "POST",
				url: url_edit,
				dataType: "json",
				data: "add_occupation=" + $('#add_occupation').val(),
				success: function (response)
				{
					$('.message_success_occupation').html(response.success);
					if (response.success != '')
					{
						$('.add_new_occupation').show(1).delay(5000).hide(1);
						$('.overlay').show(1).delay(5000).hide(1);
					}
					$('.error').html(response.error);
					$('.occupation').html('<option value="0"> - Please Select - </option>');
					jQuery.each(response.occupation, function (i, val)
					{
						$('.occupation').append($('<option>').attr('value', val.Occupation_ID).html(val.Occupation_Name));
					})
				}
			});
		});
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Referred By: Functionality
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Referred By: Add Button Click
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$('.quick_add_referred_by').click(function ()
		{
			var left = $(window).width() / 2 - $('.add_new_referred_by').width() / 2;
			var top = $(window).height() / 2 - $('.add_new_referred_by').height() / 2;
			$('.add_new_referred_by').css('left', left);
			$('.add_new_referred_by').css('top', top);
			$('.message_success_referred_by').html('');
			$('.error').html('');
			$('#add_referred_by').val('');
			$('.add_new_referred_by').show();
			$('.overlay').show().fadeTo(0, 0.5);
		});
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Referred By: Cancel Button Click
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$('.cancel_referred_by').click(function ()
		{
			$('.add_new_referred_by').hide();
			$('.overlay').hide();
		});
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Ok Button Click
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$('.ok_referred_by').click(function ()
		{
			var url_edit = document.location.href;
			url_edit = url_edit.replace(/\/(?:add|edit)\b.*/, '');
			url_edit += '/add_referred_by';
			$.ajax(
			{
				async: false,
				type: "POST",
				url: url_edit,
				dataType: "json",
				data: "add_referred_by=" + $('#add_referred_by').val(),
				success: function (response)
				{
					$('.message_success_referred_by').html(response.success);
					if (response.success != '')
					{
						$('.add_new_referred_by').show(1).delay(5000).hide(1);
						$('.overlay').show(1).delay(5000).hide(1);
					}
					$('.error').html(response.error);
					$('.referred_by').html('<option value="0">- Please Select -</option>');
					jQuery.each(response.referred_by, function (i, val)
					{
						$('.referred_by').append($('<option>').attr('value', val.Referred_By_ID).html(val.Referred_By_Name));
					})
					jQuery.each(response.occupation, function (i, val)
					{
						$('.occupation').append($('<option>').attr('value', val.Occupation_ID).html(val.Occupation_Name));
					})
				}
			});
		});
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Client Type ID Drop Down Menu OnChange Event Handler
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$("select[name='client[Client_Type_ID]']").change(function()
		{
			var Client_Type_ID = $(this).attr('value');
			if(Client_Type_ID == 'select')
			{
				$(".Company").css("display","none");
				$(".Client").css("display","none");
				$(".client_form").css("display","none");
			}
			else if(Client_Type_ID == '31')
			{
				$(".Company").css("display","none");
				$(".Client").css("display", "table-row");
				$(".client_form").css("display","table-row");
			}
			else
			{
				$(".Company").css("display", "table-row");
				$(".Client").css("display","none");
				$(".client_form").css("display","table-row");
			}
		});
    });
   	function wireUpAutoSubmit()
	{
		$(".autoSubmit").each(function (index)
		{
			$(this).change(function ()
			{
				$(this).closest('form').submit();
			})
		});
	}
	$(document).ready(function ()
	{
		wireUpAutoSubmit();
	});
})(jQuery);