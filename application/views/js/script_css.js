(function ($) {
    $(function () {
        $("li").hover(function () {
            $(this).css('background', '#e7eaf2');
        }).mouseleave(function () {
            $(this).css('background', '#ffffff');
        })
    });
})(jQuery);