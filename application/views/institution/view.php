<div id="content" class="clearfix">
	<div class="clearfix">
		<a href="<?php print $this->config->item('base_url'); ?>institution/convertToExcel">
			<img src="<?php print $this->config->item('base_url'); ?>application/views/css/images/excel.png" style="width: 25px; float: right;" />
		</a>
	</div>

	<div class="message clearfix" style="margin-bottom: 10px;">
		<span class="link">
			<?= $link ?>
		</span>
	</div>
	<?php if(isset($list[0][INSTITUTION_VIEW_ID])): ?>
	<table border="1" class="view">
		<tr>
			<th>
				<a class="view_link" href="<?= base_url() ?>institution/view/<?= $page ?>/<?= INSTITUTION_VIEW_NAME ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Institution
					<?= ($order == INSTITUTION_VIEW_NAME)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == INSTITUTION_VIEW_NAME)&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>institution/view/<?= $page ?>/<?= INSTITUTION_VIEW_ADDRESS_1 ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Address (1)
					<?= ($order == INSTITUTION_VIEW_ADDRESS_1)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == INSTITUTION_VIEW_ADDRESS_1)&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>institution/view/<?= $page ?>/<?= INSTITUTION_VIEW_ADDRESS_2 ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Address (2)
					<?= ($order == INSTITUTION_VIEW_ADDRESS_2)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == INSTITUTION_VIEW_ADDRESS_2)&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>institution/view/<?= $page ?>/<?= INSTITUTION_VIEW_CITY ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					City
					<?= ($order == INSTITUTION_VIEW_CITY)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == INSTITUTION_VIEW_CITY)&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>institution/view/<?= $page ?>/<?= INSTITUTION_VIEW_STATE_ID ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					State
					<?= ($order == INSTITUTION_VIEW_STATE_ID)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == INSTITUTION_VIEW_STATE_ID)&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>institution/view/<?= $page ?>/<?= INSTITUTION_VIEW_POSTAL_CODE ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Postal Code
					<?= ($order == INSTITUTION_VIEW_POSTAL_CODE)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == INSTITUTION_VIEW_POSTAL_CODE)&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>institution/view/<?= $page ?>/<?= INSTITUTION_VIEW_ABA ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					ABA
					<?= ($order == INSTITUTION_VIEW_ABA)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == INSTITUTION_VIEW_ABA)&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>institution/view/<?= $page ?>/<?= INSTITUTION_VIEW_STAMP_DATE ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Created On
					<?= ($order == INSTITUTION_VIEW_STAMP_DATE)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == INSTITUTION_VIEW_STAMP_DATE)&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>institution/view/<?= $page ?>/<?= INSTITUTION_VIEW_STAMP_USER_INITIALS ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Created By
					<?= ($order == INSTITUTION_VIEW_STAMP_USER_INITIALS)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == INSTITUTION_VIEW_STAMP_USER_INITIALS)&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>institution/view/<?= $page ?>/<?= INSTITUTION_VIEW_LAST_EDIT_DATE ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Last Edited On
					<?= ($order == INSTITUTION_VIEW_LAST_EDIT_DATE)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == INSTITUTION_VIEW_LAST_EDIT_DATE)&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>institution/view/<?= $page ?>/<?= INSTITUTION_VIEW_LAST_EDIT_USER_INITIALS ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Last Edited By
					<?= ($order == INSTITUTION_VIEW_LAST_EDIT_USER_INITIALS)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == INSTITUTION_VIEW_LAST_EDIT_USER_INITIALS)&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<?php if($this->session->userdata(ROLE_ADMIN) ||$this->session->userdata(ROLE_WRITE)) : ?>
			<th>
				<a class="view_link" href="<?= base_url() ?>institution/view/<?= $page ?>/<?= INSTITUTION_TOTAL_TIMES_USED ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					In Use/Delete
					<?= ($order == INSTITUTION_TOTAL_TIMES_USED)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == INSTITUTION_TOTAL_TIMES_USED)&&($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<?php endif; ?>
		</tr>
		<?php foreach ($list as $key => $value): ?>
		<?php if($this->session->userdata(ROLE_ADMIN) || $this->session->userdata(ROLE_WRITE)): ?>
		<tr class="clickRow" ondblclick="window.location='<?= base_url() ?>institution/edit/<?= $value[INSTITUTION_VIEW_ID] ?>';">
		<?php else: ?>
		<tr>
		<?php endif; ?>
			<td>
				<?= stripslashes($value[INSTITUTION_VIEW_NAME]) ?>
			</td>
			<td>
				<?= stripslashes($value[INSTITUTION_VIEW_ADDRESS_1]) ?>
			</td>
			<td>
				<?= stripslashes($value[INSTITUTION_VIEW_ADDRESS_2]) ?>
			</td>
			<td>
				<?= stripslashes($value[INSTITUTION_VIEW_CITY]) ?>
			</td>
			<td>
				<?= $value[INSTITUTION_VIEW_STATE_ID] ?>
			</td>
			<td>
				<?= stripslashes($value[INSTITUTION_VIEW_POSTAL_CODE]) ?>
			</td>
			<td>
				<?= stripslashes($value[INSTITUTION_VIEW_ABA]) ?>
			</td>
			<td>
				<?= date(DATE_FORMAT, $value[INSTITUTION_VIEW_STAMP_DATE]) ?> 
			</td>
			<td>
				<?= stripslashes($value[INSTITUTION_VIEW_STAMP_USER_INITIALS]) ?>
			</td>
			<td>
				<?= date(DATE_FORMAT, $value[INSTITUTION_VIEW_LAST_EDIT_DATE]) ?> 
			</td>
			<td>
				<?= stripslashes($value[INSTITUTION_VIEW_LAST_EDIT_USER_INITIALS]) ?>
			</td>
			<?php if($this->session->userdata(ROLE_ADMIN) ||$this->session->userdata(ROLE_WRITE)) : ?>
			<td>
				<?= $value[INSTITUTION_TOTAL_TIMES_USED] > 0 ? 
					'<span style="color: #999999;">'.$value[INSTITUTION_TOTAL_TIMES_USED].'</span>&nbsp;&nbsp;<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/no_delete.png" style="height: 10px;" title="Cannot delete"/>' : 
					'<span style="color: #990000;">'.$value[INSTITUTION_TOTAL_TIMES_USED].'</span>&nbsp;&nbsp;<a href="' . $this->config->item('base_url') . '/institution/delete_institution/' . $value[INSTITUTION_ID]. '" title="Delete this Institution"><img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/delete.png" style="height: 10px;" /></a></span>'
				?>
			</td>
			<?php endif; ?>
		</tr>
		<?php endforeach; ?>
	</table>
	<?php else : 
    echo '<h3 align = "center">'.$list.'</h3>'; ?><?php endif; ?>
	<div class="message clearfix" style="margin-top: 10px;">
		<span class="link">
			<?= $link ?>
		</span>
	</div>
</div>
