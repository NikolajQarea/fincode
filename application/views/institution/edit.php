<div id="content" class="clearfix">
	<?php echo form_open(base_url().'institution/update_institution/'.$data[INSTITUTION_ID]); ?>
	<fieldset style="width: 725px;">
		<legend>
			Edit Bank/Broker Information for <?= stripslashes($data[INSTITUTION_NAME]) ?>
		</legend>
		<table class="editform" style="width: 650px;">
			<tr>
				<td colspan="3">
					<div class="manage_delete" style="width: 110px; text-align: right;">
						<?= $data[INSTITUTION_TOTAL_TIMES_USED] > 0 ? 
							'<span style="color: #999999;">[In Use '.$data[INSTITUTION_TOTAL_TIMES_USED].' Time(s)]</span>&nbsp;&nbsp;<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/no_delete.png" style="height: 10px;" />' : 
							'<a href="' . $this->config->item('base_url') . '/institution/delete_institution/' . $data[INSTITUTION_ID]. '" style="color: #990000;" title="Delete this Institution">[Delete]<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/delete.png" style="height: 10px;" /></a>'
						?>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_INSTITUTION_NAME ?>
				</td>
				<td>
					<input class="input" id="<?= INSTITUTION_NAME ?>" name="institution[<?= INSTITUTION_NAME ?>]" type="text" value="<?= $this->session->flashdata(INSTITUTION_NAME) ? stripslashes($this->session->flashdata(INSTITUTION_NAME)) : stripslashes($data[INSTITUTION_NAME]) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_INSTITUTION_NAME) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_INSTITUTION_ADDRESS_1 ?>
				</td>
				<td>
					<input class="input" id="<?= INSTITUTION_ADDRESS_1 ?>" name="institution[<?= INSTITUTION_ADDRESS_1 ?>]" type="text" value="<?= $this->session->flashdata(INSTITUTION_ADDRESS_1) ? stripslashes($this->session->flashdata(INSTITUTION_ADDRESS_1)) : stripslashes($data[INSTITUTION_ADDRESS_1]) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_INSTITUTION_ADDRESS_1) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_INSTITUTION_ADDRESS_2 ?>
				</td>
				<td>
					<input class="input" id="<?= INSTITUTION_ADDRESS_1 ?>" name="institution[<?= INSTITUTION_ADDRESS_2 ?>]" type="text" value="<?= $this->session->flashdata(INSTITUTION_ADDRESS_2) ? stripslashes($this->session->flashdata(INSTITUTION_ADDRESS_2)) : stripslashes($data[INSTITUTION_ADDRESS_2]) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_INSTITUTION_ADDRESS_2) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_INSTITUTION_CITY ?>
				</td>
				<td>
					<input class="input" id="<?= INSTITUTION_CITY ?>" name="institution[<?= INSTITUTION_CITY ?>]" type="text" value="<?= $this->session->flashdata(INSTITUTION_CITY) ? stripslashes($this->session->flashdata(INSTITUTION_CITY)) : stripslashes($data[INSTITUTION_CITY]) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_INSTITUTION_CITY) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_INSTITUTION_STATE_ID ?>
				</td>
				<td>
					<?= $States ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_INSTITUTION_STATE_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_INSTITUTION_POSTAL_CODE ?>
				</td>
				<td>
					<input class="input" id="<?= INSTITUTION_POSTAL_CODE ?>" name="institution[<?= INSTITUTION_POSTAL_CODE ?>]" type="text" value="<?= $this->session->flashdata(INSTITUTION_POSTAL_CODE) ? stripslashes($this->session->flashdata(INSTITUTION_POSTAL_CODE)) : stripslashes($data[INSTITUTION_POSTAL_CODE]) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_INSTITUTION_POSTAL_CODE) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_INSTITUTION_CONTACT ?>
				</td>
				<td>
					<input class="input" id="<?= INSTITUTION_CONTACT ?>" name="institution[<?= INSTITUTION_CONTACT ?>]" type="text" value="<?= $this->session->flashdata(INSTITUTION_CONTACT) ? stripslashes($this->session->flashdata(INSTITUTION_CONTACT)) : stripslashes($data[INSTITUTION_CONTACT]) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_INSTITUTION_CONTACT) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_INSTITUTION_TELEPHONE ?>
				</td>
				<td>
					<input class="input" id="<?= INSTITUTION_TELEPHONE ?>" name="institution[<?= INSTITUTION_TELEPHONE ?>]" type="text" value="<?= $this->session->flashdata(INSTITUTION_TELEPHONE) ? stripslashes($this->session->flashdata(INSTITUTION_TELEPHONE)) :  stripslashes($data[INSTITUTION_TELEPHONE]) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_INSTITUTION_TELEPHONE) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_INSTITUTION_ABA ?>
				</td>
				<td>
					<input class="input" id="<?= INSTITUTION_ABA ?>" name="institution[<?= INSTITUTION_ABA ?>]" type="text" value="<?= $this->session->flashdata(INSTITUTION_ABA) ? stripslashes($this->session->flashdata(INSTITUTION_ABA)) : stripslashes($data[INSTITUTION_ABA]) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_INSTITUTION_ABA) ?>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<hr />
				</td>
			</tr>
			<tr>
				<td>
					<input class ="submit" onClick="history.go(-1)" type="button" value ="Cancel" style="width: 95%;">
				</td>
				<td>
					<input class="submit" type="submit" value="Save" style="width: 95%;">
				</td>
			</tr>
		</table>
	</fieldset>
	<?php echo form_close(); ?>
	<div class="clearfix">
		&nbsp;
	</div>
	<fieldset style="width: 725px; text-align: left;">
		<legend>
			Other Information
		</legend>
		<table class="notes">
			<colgroup>
				<col style="width: 100px;" />
				<col style="width: 225px;" />
				<col style="width: 100px;" />
				<col style="width: 225px;" />
			</colgroup>
			<tr>
				<td class="label">
					Instutition ID:
				</td>
				<td class="data">
					<?= $data[INSTITUTION_ID] ?>
				</td>
				<td colspan="2">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_INSTITUTION_STAMP_USER_ID ?>
				</td>
				<td class="data">
					<?= $data[INSTITUTION_VIEW_STAMP_USER_LOGIN_ID] ?>
				</td>
				<td class="label">
					<?= LABEL_INSTITUTION_LAST_EDIT_USER_ID ?>
				</td>
				<td class="data">
					<?= $data[INSTITUTION_VIEW_LASST_EDIT_USER_LOGIN_ID] ?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<?= LABEL_INSTITUTION_STAMP_DATE ?>
				</td>
				<td class="data">
					<?= ($data[INSTITUTION_VIEW_STAMP_DATE] != null ? date(DATE_FORMAT, $data[INSTITUTION_VIEW_STAMP_DATE]) :'' ) ?>
				</td>
				<td class="label">
					<?= LABEL_INSTITUTION_LAST_EDIT_DATE ?>
				</td>
				<td class="data">
					<?= ($data[INSTITUTION_VIEW_LAST_EDIT_DATE] != 0 ? date(DATE_FORMAT, $data[INSTITUTION_VIEW_LAST_EDIT_DATE]) : '') ?>
				</td>
			</tr>
		</table>
	</fieldset>
</div>
<script>
	$(document).ready(function()
	{
		jQuery(function($)
		{
			$("#<?= INSTITUTION_NAME ?>").Watermark("<?= JS_INSTITUTION_NAME ?>");
			$("#<?= INSTITUTION_ADDRESS_1 ?>").Watermark("<?= JS_INSTITUTION_ADDRESS_1 ?>");
			$("#<?= INSTITUTION_ADDRESS_2 ?>").Watermark("<?= JS_INSTITUTION_ADDRESS_2 ?>");
			$("#<?= INSTITUTION_CITY ?>").Watermark("<?= JS_INSTITUTION_CITY ?>");
			$("#<?= INSTITUTION_POSTAL_CODE ?>").mask("99999?-9999");
			$("#<?= INSTITUTION_POSTAL_CODE ?>").Watermark("<?= JS_INSTITUTION_POSTAL_CODE ?>");
			$("#<?= INSTITUTION_CONTACT ?>").Watermark("<?= JS_INSTITUTION_CONTACT ?>");
			$("#<?= INSTITUTION_TELEPHONE ?>").mask("(999) 999-9999");
			$("#<?= INSTITUTION_TELEPHONE ?>").Watermark("<?= JS_INSTITUTION_TELEPHONE ?>");
			$("#<?= INSTITUTION_ABA ?>").Watermark("<?= JS_INSTITUTION_ABA ?>");
		});
	});
</script>
