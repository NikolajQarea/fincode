<div id="content" class="clearfix">
<?php echo form_open(base_url().'institution/insert_institution'); ?>
	<fieldset style="width: 725px;">
		<legend>
			Add New Bank/Broker
		</legend>
		<table class="editform" style="width: 650px;">
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_INSTITUTION_NAME ?>
				</td>
				<td>
					<input class="input" id="<?= INSTITUTION_NAME ?>" name="institution[<?= INSTITUTION_NAME ?>]" type="text" value="<?= $this->session->flashdata(INSTITUTION_NAME) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_INSTITUTION_NAME) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_INSTITUTION_ADDRESS_1 ?>
				</td>
				<td>
					<input class="input" id="<?= INSTITUTION_ADDRESS_1 ?>" name="institution[<?= INSTITUTION_ADDRESS_1 ?>]" type="text" value="<?= $this->session->flashdata(INSTITUTION_ADDRESS_1) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_INSTITUTION_ADDRESS_1) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_INSTITUTION_ADDRESS_2 ?>
				</td>
				<td>
					<input class="input" id="<?= INSTITUTION_ADDRESS_2 ?>" name="institution[<?= INSTITUTION_ADDRESS_2 ?>]" type="text" value="<?= $this->session->flashdata(INSTITUTION_ADDRESS_2) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_INSTITUTION_ADDRESS_2) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_INSTITUTION_CITY ?>
				</td>
				<td>
					<input class="input" id="<?= INSTITUTION_CITY ?>" name="institution[<?= INSTITUTION_CITY ?>]" type="text" value="<?= $this->session->flashdata(INSTITUTION_CITY) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_INSTITUTION_CITY) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_INSTITUTION_STATE_ID ?>
				</td>
				<td>
					<?= $States ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_INSTITUTION_STATE_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_INSTITUTION_POSTAL_CODE ?>
				</td>
				<td>
					<input class="input" id="<?= INSTITUTION_POSTAL_CODE ?>" name="institution[<?= INSTITUTION_POSTAL_CODE ?>]" type="text" value="<?= $this->session->flashdata(INSTITUTION_POSTAL_CODE) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_INSTITUTION_POSTAL_CODE) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_INSTITUTION_CONTACT ?>
				</td>
				<td>
					<input class="input" id="<?= INSTITUTION_CONTACT ?>" name="institution[<?= INSTITUTION_CONTACT ?>]" type="text" value="<?= $this->session->flashdata(INSTITUTION_CONTACT) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_INSTITUTION_CONTACT) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_INSTITUTION_TELEPHONE ?>
				</td>
				<td>
					<input class="input" id="<?= INSTITUTION_TELEPHONE ?>" name="institution[<?= INSTITUTION_TELEPHONE ?>]" type="text" value="<?= $this->session->flashdata(INSTITUTION_TELEPHONE) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_INSTITUTION_TELEPHONE) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_INSTITUTION_ABA ?>
				</td>
				<td>
					<input class="input" id="<?= INSTITUTION_ABA ?>" name="institution[<?= INSTITUTION_ABA ?>]" type="text" value="<?= $this->session->flashdata(INSTITUTION_ABA) ?>">
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_INSTITUTION_ABA) ?>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<hr />
				</td>
			</tr>
			<tr>
				<td>
					<input class ="submit" onClick="history.go(-1)" type="button" value ="Cancel" style="width: 95%;">
				</td>
				<td>
					<input class="submit" type="submit" value="Save" style="width: 95%;">
				</td>
				<td>
					&nbsp;
				</td>
			</tr>
		</table>
	</fieldset>
	<?php echo form_close(); ?>
</div>
<script>
	$(document).ready(function()
	{
		jQuery(function($)
		{
			$("#<?= INSTITUTION_NAME ?>").Watermark("<?= JS_INSTITUTION_NAME ?>");
			$("#<?= INSTITUTION_ADDRESS_1 ?>").Watermark("<?= JS_INSTITUTION_ADDRESS_1 ?>");
			$("#<?= INSTITUTION_ADDRESS_2 ?>").Watermark("<?= JS_INSTITUTION_ADDRESS_2 ?>");
			$("#<?= INSTITUTION_CITY ?>").Watermark("<?= JS_INSTITUTION_CITY ?>");
			$("#<?= INSTITUTION_POSTAL_CODE ?>").mask("99999?-9999");
			$("#<?= INSTITUTION_POSTAL_CODE ?>").Watermark("<?= JS_INSTITUTION_POSTAL_CODE ?>");
			$("#<?= INSTITUTION_CONTACT ?>").Watermark("<?= JS_INSTITUTION_CONTACT ?>");
			$("#<?= INSTITUTION_TELEPHONE ?>").mask("(999) 999-9999");
			$("#<?= INSTITUTION_TELEPHONE ?>").Watermark("<?= JS_INSTITUTION_TELEPHONE ?>");
			$("#<?= INSTITUTION_ABA ?>").Watermark("<?= JS_INSTITUTION_ABA ?>");
		});
	});
</script>
