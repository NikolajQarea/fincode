<div id="content" class="clearfix">
	<fieldset style="width: 650px;">
		<legend>
			Add a Tax Return
		</legend>
		<div>&nbsp;</div>
		<?php echo form_open(base_url() . TAX_RETURN_POST . '/add_tax_return_multiple');  ?>
		<table style="width: 95%;">
			<colgroup>
				<col style="width: 25%;" />
				<col style="width: 60%;" />
				<col style="width: 15%;" />
			</colgroup>

			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_CLIENT_PARTNER_ID ?>
				</td>
				<td id="select_partner_data">
					<?= $Partners ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_TAX_FORM_ID ?>
				</td>
				<td id="select_form_data">
					<?= $Tax_Forms ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_FORM_ID) ?>
				</td>
			</tr>
			<tr>
				<td id="td_form_date"></td>
				<td><div id="form_date"></div></td>
				<td class="errors"><?= $this->session->flashdata(ERROR_TAX_FORM_DATE) ?></td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_TAX_YEAR_ID ?>
				</td>
				<td>
					<?= $Tax_Years ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_YEAR_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<input class ="submit" onClick="history.go(-1)" type="button" value ="Cancel" style="width: 95%;">
				</td>
				<td>
					<input class="submit" type="submit" value="ADD" style="width: 95%;">
				</td>
				<td>
					&nbsp;
				</td>
			</tr>
		</table>
		<input id="status_name" name="<?= TAX_RETURN_MULTIPLE_POST ?>[status_name]" type="hidden" />
		<?php echo form_close(); ?>
	</fieldset>

</div>
<script type="text/javascript">
	$(document).ready(function()
	{
		var getPartnerAndForm = function() {
			$.ajax({
				type: "POST",
				url: "<?= $this->config->item('base_url') . TAX_RETURN_MULTIPLE_POST ?>/fill_select",
				dataType: "json",
				data: "<?= CLIENT_NAME ?>="+encodeURIComponent($('.client_select').val()),
				success: function(response)
				{
					if(response.found == '1') {
						$('#select_partner_data').html(response.<?= CLIENT_PARTNER_ID ?>);
						$('#select_form_data').html(response.<?= TAX_FORM_ID ?>);
					} else {
						$('#select_partner_data').html('First select a Client.');
						$('#select_form_data').html('First select a Client.');
					}
				}
			});
		}

		clientAutocomplete($('#search'), { 
			select: function($search) { 
				getPartnerAndForm();
			}
		});

		// This form doesn't exist any more. [MSD 2/6/2013]
		// if($('.form_select').val() == '16')
		// {
		// 	$('#td_form_date').html('<?= REQUIRED_FIELD . LABEL_TAX_RETURN_990PF_DATE ?>');
		// 	$('#form_date').html('<input id="tbFormDate" type="text" class="input datepicker" name="<?= TAX_RETURN_VIEW_990PF_DATE ?>" value = "<?= $this->session->flashdata(TAX_RETURN_VIEW_990PF_DATE) ?>"/>');
		// 	$("#tbFormDate").Watermark("<?= JS_990PF_DATE ?>");
		// 	$(".datepicker").live("click", function()
		// 	{
		// 		$(this).datepicker({showOn:'focus'}).focus();
		// 		$(this).datepicker();
		// 	});
		// }

		// This form doesn't exist any more. [MSD 2/6/2013]
		// $('.form_select').live('change', function()
		// {
		// 	if($(this).val() == '16')
		// 	{
		// 		$('#td_form_date').html('<?= REQUIRED_FIELD . LABEL_TAX_RETURN_990PF_DATE?>');
		// 		$('#form_date').html('<input id="tbFormDate" type="text" class="input datepicker" name="<?= TAX_RETURN_VIEW_990PF_DATE ?>" />');
		// 		$("#tbFormDate").Watermark("<?= JS_990PF_DATE ?>");
		// 		$(".datepicker").live("click", function()
		// 		{
		// 			$(this).datepicker({showOn:'focus'}).focus();
		// 			$(this).datepicker();
		// 		});
		// 	}
		// 	else
		// 	{
		// 		$('#td_form_date').html('');
		// 		$('#form_date').html('');
		// 	}
		// });

		$("#<?= TAX_RETURN_DATE_RELEASED ?>").mask("99/99/9999");
		$("#<?= TAX_RETURN_DATE_RELEASED ?>").Watermark("<?= JS_DATE_RELEASED ?>");
		$("#<?= TAX_RETURN_NOTES ?>").Watermark("<?= JS_TAX_RETURN_NOTES ?>");
		$("#<?= TAX_RETURN_DATE_SENT ?>").mask("99/99/9999");
		$("#<?= TAX_RETURN_DATE_SENT ?>").Watermark("<?= JS_DATE_SENT ?>");
	});
</script>
