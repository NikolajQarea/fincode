<div id="content" class="clearfix">
	<?php echo form_open(base_url().'tax_return/view'); ?>
	<div class="clearfix">
		<fieldset>
			<legend>
				Filters
			</legend>
			<table style="font-size: 8pt; display: inline-block; width: auto; padding: 0px;">
				<tr>
					<td valign="middle">
						<input class="button" name="filter" type="submit" value="Apply Filter(s)" />
					</td>
					<td valign="middle">
						<input class="button" name="clear" type="submit" value="Clear Filter(s)" />
					</td>
					<td class="tdFilter" valign="middle">
						<label for="tax_return[sent_returns]">
							Exclude Sent To Client?
						</label>
						<input class="autoSubmit" id="tax_return[sent_returns]" name="tax_return[sent_returns]"  title="Exclude Sent To Client?" type="checkbox" <?= isset($post['sent_returns']) && $post['sent_returns'] == 'on' ? ' checked' : ''; ?> />
		   		    </td>
					<td class="tdFilter" valign="middle">
						<label for="tax_return[released_returns]">
							Exclude Released?
						</label>
						<input class="autoSubmit" id="tax_return[released_returns]" name="tax_return[released_returns]"  title="Exclude Released?" type="checkbox" <?= isset($post['released_returns']) && $post['released_returns'] == 'on' ? ' checked' : ''; ?> />
		   		    </td>
					<td class="tdFilter" valign="middle">
						<label for="tax_return[exclude_prepared_by_client]">
							Exclude Prepared &amp; Filed By Client?
						</label>
		   				<input class="autoSubmit" id="tax_return[exclude_prepared_by_client]" name="tax_return[exclude_prepared_by_client]" title="Exclude Prepared &amp; Filed By Client?" type="checkbox" <?= isset($post['exclude_prepared_by_client']) && $post['exclude_prepared_by_client'] == 'on' ? ' checked' : ''; ?> />
		   		    </td>
					<td class="tdFilter" valign="middle">
						<label for="tax_return[exclude_signature_pages]">
							Exclude Signature pgs/Auth sent to Client?
						</label>
						<input class="autoSubmit" id="tax_return[exclude_signature_pages]" name="tax_return[exclude_signature_pages]" title="Exclude Signature pgs/Auth sent to Client?" type="checkbox" <?= isset($post['exclude_signature_pages']) && $post['exclude_signature_pages'] == 'on' ? ' checked' : ''; ?> />
		   		    </td>
					<td class="tdFilter" valign="middle">
						<label for="tax_return[extension_filed]">
							Extension(s) Filed?
						</label>
						<input class="autoSubmit" id="tax_return[extension_filed]" name="tax_return[extension_filed]" title="Extension(s) Filed?" type="checkbox" <?= isset($post['extension_filed']) && $post['extension_filed'] == 'on' ? ' checked' : ''; ?> />
		   		    </td>
				</tr>
			</table>
		</fieldset>
	</div>
	<div class="clearfix"></div>
	<div class="message clearfix">
		<?= ($this->session->flashdata('auto_add')) ? $this->session->flashdata('auto_add') : '' ?>
		<span class="link">
			&nbsp;<?php print $link ?>
		</span>
	</div>
	<div class="clearfix">
		<table class="view">
		<colgroup>
			<col style="width: 19%;" />
			<col style="width: 5%;" />
			<col style="width: 5%;" />
			<col style="width: 6%;" />
			<col style="width: 6%;" />
			<col style="width: 6%;" />
			<col style="width: 10%;" />
			<col style="width: 9%;" />
			<col style="width: 9%;" />
			<col style="width: 7%;" />
			<col style="width: 6%;" />
			<col style="width: 6%;" />
			<col style="width: 6%;" />
		</colgroup>

			<tr>
				<th>
					<a class="view_link" href="<?= base_url() ?>tax_return/view/<?= $page ?>/<?= TAX_RETURN_VIEW_CLIENT_NAME ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Client&nbsp;Name
						<?= ($order == TAX_RETURN_VIEW_CLIENT_NAME)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_CLIENT_NAME)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>tax_return/view/<?= $page ?>/<?= TAX_RETURN_VIEW_PARTNER_ID ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Partner
						<?= ($order == TAX_RETURN_VIEW_PARTNER_ID)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_PARTNER_ID)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>tax_return/view/<?= $page ?>/<?= TAX_RETURN_VIEW_TAX_FORM_ID ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Form
						<?= ($order == TAX_RETURN_VIEW_TAX_FORM_ID)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_TAX_FORM_ID)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>tax_return/view/<?= $page ?>/<?= TAX_RETURN_VIEW_TAX_YEAR ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Year
						<?= ($order == TAX_RETURN_VIEW_TAX_YEAR)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_TAX_YEAR)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>tax_return/view/<?= $page ?>/<?= TAX_RETURN_VIEW_TAX_QUARTER ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Quarter
						<?= ($order == TAX_RETURN_VIEW_TAX_QUARTER)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_TAX_QUARTER)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>tax_return/view/<?= $page ?>/<?= TAX_RETURN_VIEW_TAX_MONTH_ID ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Month
						<?= ($order == TAX_RETURN_VIEW_TAX_MONTH_ID)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_TAX_MONTH_ID)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>tax_return/view/<?= $page ?>/<?= TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Return&nbsp;Status
						<?= ($order == TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>tax_return/view/<?= $page ?>/<?= TAX_RETURN_VIEW_DATE_SENT ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Sent
						<?= ($order == TAX_RETURN_VIEW_DATE_SENT)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_DATE_SENT)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>tax_return/view/<?= $page ?>/<?= TAX_RETURN_VIEW_DATE_RELEASED ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Released
						<?= ($order == TAX_RETURN_VIEW_DATE_RELEASED)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_DATE_RELEASED)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>tax_return/view/<?= $page ?>/<?= TAX_RETURN_VIEW_TAX_RETURN_NOTES ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Notes
						<?= ($order == TAX_RETURN_VIEW_TAX_RETURN_NOTES)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_TAX_RETURN_NOTES)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>tax_return/view/<?= $page ?>/<?= TAX_RETURN_STAFF_ID ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Preparer
						<?= ($order == TAX_RETURN_STAFF_ID)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_STAFF_ID)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>tax_return/view/<?= $page ?>/<?= TAX_RETURN_VIEW_REVIEWER_1_ID ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						1st Rev.
						<?= ($order == TAX_RETURN_VIEW_REVIEWER_1_ID)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_REVIEWER_1_ID)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
				<th>
					<a class="view_link" href="<?= base_url() ?>tax_return/view/<?= $page ?>/<?= TAX_RETURN_VIEW_REVIEWER_2_ID ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						2nd Rev.
						<?= ($order == TAX_RETURN_VIEW_REVIEWER_2_ID)&&($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_REVIEWER_2_ID)&&($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
			</tr>
			<tr>
				<th>
					<input id="search" class="input" name="tax_return[<?= TAX_RETURN_VIEW_CLIENT_NAME ?>]" type="text" value="<? print($post[TAX_RETURN_VIEW_CLIENT_NAME]); ?>" />
				</th>
				<th>
					<?= $Partners ?>
				</th>
				<th>
					<?= $Tax_Forms ?>
				</th>
				<th>
					<?= $Tax_Years ?>
				</th>
				<th>
					<?= $Tax_Quarters ?>
				</th>
				<th>
					<?= $Tax_Months ?>
				</th>
				<th>
					<?= $Tax_Status ?>
				</th>
				<th>
					<input class="input datepicker autoSubmit" name="tax_return[<?= TAX_RETURN_VIEW_DATE_SENT ?>]" size="7" value="<?= $post[TAX_RETURN_VIEW_DATE_SENT] ?>" />
				</th>
				<th>
					<input class="input datepicker autoSubmit" name="tax_return[<?= TAX_RETURN_VIEW_DATE_RELEASED ?>]" size="7" value="<?= $post[TAX_RETURN_VIEW_DATE_RELEASED] ?>" />
				</th>
				<th>
					<input class="input autoSubmit" name="tax_return[<?= TAX_RETURN_VIEW_TAX_RETURN_NOTES ?>]" type="text" value="<? print($post[TAX_RETURN_VIEW_TAX_RETURN_NOTES]); ?>" />
				</th>
				<th>
					<?= $Staff ?>
				</th>
				<th>
					<?= $Reviewer1 ?>
				</th>
				<th>
					<?= $Reviewer2 ?>
				</th>
			</tr>
			<?php if(isset($list[0][TAX_RETURN_VIEW_TAX_RETURN_ID])): ?>
			<?php foreach ($list as $key => $value): ?>
			<?php if ($this->session->userdata(ROLE_ADMIN) || $this->session->userdata(ROLE_WRITE)): ?>
			<tr class="clickRow" ondblclick="window.location='<?= base_url() ?>tax_return/edit/<?= $value[TAX_RETURN_VIEW_TAX_RETURN_ID] ?>';">
			<?php else: ?>
			<tr>
			<?php endif; ?>
				<td>
					<?= stripslashes(trim($value[TAX_RETURN_VIEW_CLIENT_NAME])) ?>
				</td>
				<td>
					<?= stripslashes(trim($value[TAX_RETURN_VIEW_PARTNER_INITIALS])) ?>
				</td>
				<td>
					<?= stripslashes(trim($value[TAX_RETURN_VIEW_TAX_FORM])) ?>
				</td>
				<td>
					<?= stripslashes(trim($value[TAX_RETURN_VIEW_TAX_YEAR])) == 1970 ? NOT_AVAILABLE : stripslashes(trim($value[TAX_RETURN_VIEW_TAX_YEAR])) ?>
				</td>
				<td>
					<?= $value[TAX_RETURN_VIEW_TAX_QUARTER] == NOT_APPLICABLE_ID ? NOT_APPLICABLE : $value[TAX_RETURN_VIEW_TAX_QUARTER] ?>
				</td>
				<td>
					<?= $value[TAX_RETURN_VIEW_TAX_MONTH_ID] == NOT_APPLICABLE_ID ? NOT_APPLICABLE : date('F', ((intval($value[TAX_RETURN_VIEW_TAX_MONTH_ID])-1)*30+15)*3600*24) ?>
				</td>
				<td>
					<?= $value[TAX_RETURN_VIEW_TAX_RETURN_STATUS] ?>
				</td>
				<td>
					<?= ($value[TAX_RETURN_VIEW_DATE_SENT] == 0) ? NOT_SENT : date(DATE_FORMAT, $value[TAX_RETURN_VIEW_DATE_SENT]) ?>
				</td>
				<td>
					<?= ($value[TAX_RETURN_VIEW_DATE_RELEASED] == 0) ? NOT_SENT : date(DATE_FORMAT, $value[TAX_RETURN_VIEW_DATE_RELEASED]) ?>
				</td>
				<td>
					<?= stripslashes($value[TAX_RETURN_VIEW_TAX_RETURN_NOTES]) ?>
				</td>
				<td>
					<?= stripslashes(trim($value[TAX_RETURN_VIEW_STAFF_INITIALS])) ?>
				</td>
				<td>
					<?= stripslashes(trim($value[TAX_RETURN_VIEW_REVIEWER_1_INITIALS])) ?>
				</td>
				<td>
					<?= stripslashes(trim($value[TAX_RETURN_VIEW_REVIEWER_2_INITIALS])) ?>
				</td>
			</tr>
			<?php endforeach; ?><?php echo form_close(); ?><?php else : ?>
			<tr>
				<td align="center" colspan="13">
					<h3>
						<?= $list ?>
					</h3>
				</td>
			</tr>
			<?php endif; ?>
		</table>
	</div>
	<div class="message clearfix">
		<span class="link">
			&nbsp;<?php print $link ?>
		</span>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function()
	{
		$('.datepicker').datepicker();
		clientAutocomplete($('#search'), { 
			select: function($search) { 
				$search.closest('form').submit(); 
			} 
		});
	});
</script>
