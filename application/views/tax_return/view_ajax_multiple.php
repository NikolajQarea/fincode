    <table class="view">
        <colgroup>
            <col style="width: 19%;" />
            <col style="width: 5%;" />
            <col style="width: 5%;" />
            <col style="width: 6%;" />
            <col style="width: 6%;" />
            <col style="width: 6%;" />
            <col style="width: 10%;" />
            <col style="width: 9%;" />
            <col style="width: 9%;" />
            <col style="width: 5%;" />
            <col style="width: 3%;" />
            <col style="width: 3%;" />
            <col style="width: 3%;" />
            <col style="width: 3%;" />
        </colgroup>
        <tr>
            <th>
                <a class="view_link" href="<?= base_url() ?>tax_return/view_multiple/0/<?= $page ?>/<?= TAX_RETURN_VIEW_CLIENT_NAME ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
                    Client&nbsp;Name
                    <?= ($order == TAX_RETURN_VIEW_CLIENT_NAME) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_CLIENT_NAME) && ($as == 'DESC') ? '&darr;' : '' ?>
                </a>
            </th>
            <th>
                <a class="view_link" href="<?= base_url() ?>tax_return/view_multiple/0/<?= $page ?>/<?= TAX_RETURN_VIEW_PARTNER_ID ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
                    Partner
                    <?= ($order == TAX_RETURN_VIEW_PARTNER_ID) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_PARTNER_ID) && ($as == 'DESC') ? '&darr;' : '' ?>
                </a>
            </th>
            <th>
                <a class="view_link" href="<?= base_url() ?>tax_return/view_multiple/0/<?= $page ?>/<?= TAX_RETURN_VIEW_TAX_FORM_ID ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
                    Form
                    <?= ($order == TAX_RETURN_VIEW_TAX_FORM_ID) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_TAX_FORM_ID) && ($as == 'DESC') ? '&darr;' : '' ?>
                </a>
            </th>
            <th>
                <a class="view_link" href="<?= base_url() ?>tax_return/view_multiple/0/<?= $page ?>/<?= TAX_RETURN_VIEW_TAX_YEAR ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
                    Year
                    <?= ($order == TAX_RETURN_VIEW_TAX_YEAR) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_TAX_YEAR) && ($as == 'DESC') ? '&darr;' : '' ?>
                </a>
            </th>
            <th>
            Year (Will be)
            </th>
            <th>
                <a class="view_link" href="<?= base_url() ?>tax_return/view_multiple/0/<?= $page ?>/<?= TAX_RETURN_VIEW_TAX_QUARTER ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
                    Quarter
                    <?= ($order == TAX_RETURN_VIEW_TAX_QUARTER) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_TAX_QUARTER) && ($as == 'DESC') ? '&darr;' : '' ?>
                </a>
            </th>
            <th>
                <a class="view_link" href="<?= base_url() ?>tax_return/view_multiple/0/<?= $page ?>/<?= TAX_RETURN_VIEW_TAX_MONTH_ID ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
                    Month
                    <?= ($order == TAX_RETURN_VIEW_TAX_MONTH_ID) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_TAX_MONTH_ID) && ($as == 'DESC') ? '&darr;' : '' ?>
                </a>
            </th>
            <th>
                <a class="view_link" href="<?= base_url() ?>tax_return/view_multiple/0/<?= $page ?>/<?= TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
                    Return&nbsp;Status
                    <?= ($order == TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_TAX_RETURN_STATUS_ID) && ($as == 'DESC') ? '&darr;' : '' ?>
                </a>
            </th>
            <th>
                <a class="view_link" href="<?= base_url() ?>tax_return/view_multiple/0/<?= $page ?>/<?= TAX_RETURN_VIEW_DATE_SENT ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
                    Sent
                    <?= ($order == TAX_RETURN_VIEW_DATE_SENT) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_DATE_SENT) && ($as == 'DESC') ? '&darr;' : '' ?>
                </a>
            </th>
            <th>
                <a class="view_link" href="<?= base_url() ?>tax_return/view_multiple/0/<?= $page ?>/<?= TAX_RETURN_VIEW_DATE_RELEASED ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
                    Released
                    <?= ($order == TAX_RETURN_VIEW_DATE_RELEASED) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_DATE_RELEASED) && ($as == 'DESC') ? '&darr;' : '' ?>
                </a>
            </th>
            <th>
                <a class="view_link" href="<?= base_url() ?>tax_return/view_multiple/0/<?= $page ?>/<?= TAX_RETURN_VIEW_TAX_RETURN_NOTES ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
                    Notes
                    <?= ($order == TAX_RETURN_VIEW_TAX_RETURN_NOTES) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_TAX_RETURN_NOTES) && ($as == 'DESC') ? '&darr;' : '' ?>
                </a>
            </th>
            <th>
                <a class="view_link" href="<?= base_url() ?>tax_return/view_multiple/0/<?= $page ?>/<?= TAX_RETURN_STAFF_ID ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
                    Preparer
                    <?= ($order == TAX_RETURN_STAFF_ID) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_STAFF_ID) && ($as == 'DESC') ? '&darr;' : '' ?>
                </a>
            </th>
            <th>
                <a class="view_link" href="<?= base_url() ?>tax_return/view_multiple/0/<?= $page ?>/<?= TAX_RETURN_VIEW_REVIEWER_1_ID ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
                    1st Rev.
                    <?= ($order == TAX_RETURN_VIEW_REVIEWER_1_ID) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_REVIEWER_1_ID) && ($as == 'DESC') ? '&darr;' : '' ?>
                </a>
            </th>
            <th>
                <a class="view_link" href="<?= base_url() ?>tax_return/view_multiple/0/<?= $page ?>/<?= TAX_RETURN_VIEW_REVIEWER_2_ID ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
                    2nd Rev.
                    <?= ($order == TAX_RETURN_VIEW_REVIEWER_2_ID) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_REVIEWER_2_ID) && ($as == 'DESC') ? '&darr;' : '' ?>
                </a>
            </th>
            <th>
                Delete Link
            </th>
        </tr>
        <?php if (isset($list) && !empty($list)) {
           ?>      
            <?php foreach ($list as $key => $value): ?>
                <?php if ($this->session->userdata(ROLE_ADMIN) || $this->session->userdata(ROLE_WRITE)): ?>
                <?php else: ?>
                    <tr>
                    <?php endif; ?>
                    <td>
                        <?= stripslashes(trim($value[TAX_RETURN_VIEW_CLIENT_NAME])) ?>
                    </td>
                    <td>
                        <?= stripslashes(trim($value[TAX_RETURN_VIEW_PARTNER_INITIALS])) ?>
                    </td>
                    <td>
                        <?= stripslashes(trim($value[TAX_RETURN_VIEW_TAX_FORM])) ?>
                    </td>
                    <td>
                        <?= stripslashes(trim($value[TAX_RETURN_VIEW_TAX_YEAR])) == 1970 ? NOT_AVAILABLE : stripslashes(trim($value[TAX_RETURN_VIEW_TAX_YEAR])) ?>
                    </td>
                    <td>
                        <?= stripslashes(trim($value[TAX_RETURN_VIEW_TAX_YEAR_MULTIPLE])) ?>
                    </td>
                    <td>
                       <?= $Tax_Month_Drop[$value['Tax_Return_ID']] ?>
                    </td>
                        
                    <td>
                        <?= $Tax_Quarter_Drop[$value['Tax_Return_ID']] ?>
                    </td>                    
                        
                    <td>
                        <?= $Tax_Status_Drop[$value['Tax_Return_ID']] ?>
                    </td>
                    <td>
                        <?= ($value[TAX_RETURN_VIEW_DATE_SENT] == 0) ? NOT_SENT : date(DATE_FORMAT, $value[TAX_RETURN_VIEW_DATE_SENT]) ?>
                    </td>
                    <td>
                        <?= ($value[TAX_RETURN_VIEW_DATE_RELEASED] == 0) ? NOT_SENT : date(DATE_FORMAT, $value[TAX_RETURN_VIEW_DATE_RELEASED]) ?>
                    </td>
                    <td>
                        <?= stripslashes($value[TAX_RETURN_VIEW_TAX_RETURN_NOTES]) ?>
                    </td>
                    <td>
                        <?= stripslashes(trim($value[TAX_RETURN_VIEW_STAFF_INITIALS])) ?>
                    </td>
                    <td>
                        <?= stripslashes(trim($value[TAX_RETURN_VIEW_REVIEWER_1_INITIALS])) ?>
                    </td>
                    <td>
                        <?= stripslashes(trim($value[TAX_RETURN_VIEW_REVIEWER_2_INITIALS])) ?>
                    </td>
                    <td>                                  
                        <a class="delete_tax_multiple" rel="<?php echo $value[TAX_RETURN_ID]; ?>" href="<?php echo base_url() . 'tax_return/delete_row/' . $value[TAX_RETURN_ID]; ?>">Delete</a>
                    </td>
                </tr>
            <?php
            endforeach;
        }
        else {
            header("Location: " . base_url() . "tax_return/add_multiple");
        }             
        ?> 
    </table>
<?php echo form_open(base_url() . 'tax_return/add_tax_multiple'); ?>  
    <div id="multiple_submit">
    <input class="submit" type="submit" value="Accept">
    <input class="submit cancel_tax_multiple" type="button" value="Cancel">
    </div>
<?php echo form_close(); ?>