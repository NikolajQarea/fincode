<div id="content" class="clearfix">
	<fieldset style="width: 650px;">
		<legend>
			Add a Tax Return
		</legend>
		<div>&nbsp;</div>
		<?php echo form_open(base_url() . TAX_RETURN_POST . '/add_tax_return');  ?>
		<table style="width: 95%;">
			<colgroup>
				<col style="width: 25%;" />
				<col style="width: 60%;" />
				<col style="width: 15%;" />
			</colgroup>
			<tr>
				<td> 
					<?= REQUIRED_FIELD . LABEL_CLIENT_ID ?>
				</td>
				<td>
					<input id="search" class="input client_select" name="<?= TAX_RETURN_POST ?>[<?= CLIENT_NAME ?>]" type="text" value="<?= $this->session->flashdata(TAX_RETURN_VIEW_CLIENT_NAME) ? $this->session->flashdata(TAX_RETURN_VIEW_CLIENT_NAME) : ANY_LABEL ?>"  />
				</td>
				<td class="errors">
					<?= $this->session->flashdata('<?= ERROR_CLIENT_ID ?>') ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_CLIENT_PARTNER_ID ?>
				</td>
				<td id="select_partner_data">
					<?= $Partners ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_TAX_FORM_ID ?>
				</td>
				<td id="select_form_data">
					<?= $Tax_Forms ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_FORM_ID) ?>
				</td>
			</tr>
			<tr>
				<td id="td_form_date"></td>
				<td><div id="form_date"></div></td>
				<td class="errors"><?= $this->session->flashdata(ERROR_TAX_FORM_DATE) ?></td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_TAX_YEAR_ID ?>
				</td>
				<td>
					<?= $Tax_Years ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_YEAR_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_TAX_QUARTER_ID ?>
				</td>
				<td>
					<?= $Tax_Quarters ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_QUARTER_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_TAX_MONTH_ID ?>
				</td>
				<td>
					<?= $Tax_Months ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_MONTH_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_TAX_RETURN_STATUS_ID ?>
				</td>
				<td>
					<?= str_replace('"input"', '"input status_input"', $Tax_Return_Status) ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_RETURN_STATUS_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
				
					<?= LABEL_TAX_RETURN_STAFF_ID ?>
				</td>
				<td>
					<?= $Staff ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_RETURN_STAFF_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_TAX_RETURN_REVIEWER_1_ID ?>
				</td>
				<td>
					<?= $Reviewer_1 ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_RETURN_REVIEWER_1_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_TAX_RETURN_REVIEWER_2_ID ?>
				</td>
				<td>
					<?= $Reviewer_2 ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_RETURN_REVIEWER_2_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_TAX_RETURN_NOTES ?>
				</td>
				<td>
					<textarea class="input" id="<?= TAX_RETURN_NOTES ?>" name="<?= TAX_RETURN_POST ?>[<?= TAX_RETURN_NOTES ?>]" style="width: 95%;" rows="5"><?= stripslashes(trim($this->session->flashdata(TAX_RETURN_NOTES))) ?></textarea>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_RETURN_NOTES) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_INSTRUCTIONS_ID ?>
				</td>
				<td>
					<?= str_replace('"input"', '"input instructions"', $Instructions) ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_INSTRUCTIONS_ID) ?>
					<?php if ($this->session->userdata(ROLE_ADMIN) || $this->session->userdata(ROLE_WRITE)): ?>
					<div id="add">
						<img src="<?= $this->config->item('base_url'); ?>application/views/css/images/add.png" /></div>
					<?php endif; ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_TAX_RETURN_DATE_SENT ?>
				</td>
				<td>
					<input class="input datepicker" id="<?= TAX_RETURN_DATE_SENT ?>" name="<?= TAX_RETURN_POST ?>[<?= TAX_RETURN_DATE_SENT ?>]" type="text" value="<?= $this->session->flashdata(TAX_RETURN_DATE_SENT) ?>" />
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_RETURN_DATE_SENT) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_TAX_RETURN_DATE_RELEASED ?>
				</td>
				<td>
					<input class="input datepicker" id="<?= TAX_RETURN_DATE_RELEASED ?>" name="<?= TAX_RETURN_POST ?>[<?= TAX_RETURN_DATE_RELEASED ?>]" type="text" value="<?= $this->session->flashdata(TAX_RETURN_DATE_RELEASED) ?>" />
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_RETURN_DATE_RELEASED) ?>
				</td>
			</tr>
			<tr>
				<td>
					<input class ="submit" onClick="history.go(-1)" type="button" value ="Cancel" style="width: 95%;">
				</td>
				<td>
					<input class="submit" type="submit" value="Save" style="width: 95%;">
				</td>
				<td>
					&nbsp;
				</td>
			</tr>
		</table>
		<input id="status_name" name="<?= TAX_RETURN_POST ?>[status_name]" type="hidden" />
		<?php echo form_close(); ?>
	</fieldset>
	<div class="overlay_instructions">
	</div>
	<div class="add_instructions">
		<img align="right" class="cancel_instructions" src="<?= $this->config->item('base_url'); ?>application/views/css/images/exit_add_occupation.png" />
		<?= form_open(base_url() . TAX_RETURN_POST.'/add_instructions'); ?>
		<table cellpadding="5px" cellspasing="5px">
			<tr>
				<td align="center"><span class="message_success_instructions">
				</span><span class="error_instructions"></span>&nbsp;</td>
			</tr>
			<tr>
				<td>Enter new Instruction</td>
			</tr>
			<tr>
				<td>
				<input id="add_instructions" class="input" name="add_instructions" type="text" value=""></td>
			</tr>
			<tr>
				<td align="center">
				<input class="submit ok_instructions" style="display: inherit" type="button" value="Add">
				<input class="submit cancel_instructions" type="button" value="Cancel"></td>
			</tr>
		</table>
	</fieldset>
	<input id="status_name" name="tax_return[status_name]" type="hidden" />
	<?= form_close(); ?></div>
</div>
<script type="text/javascript">
	$(document).ready(function()
	{
		var getPartnerAndForm = function() {
			$.ajax({
				type: "POST",
				url: "<?= $this->config->item('base_url') . TAX_RETURN_POST ?>/fill_select",
				dataType: "json",
				data: "<?= CLIENT_NAME ?>="+encodeURIComponent($('.client_select').val()),
				success: function(response)
				{
					if(response.found == '1') {
						$('#select_partner_data').html(response.<?= CLIENT_PARTNER_ID ?>);
						$('#select_form_data').html(response.<?= TAX_FORM_ID ?>);
					} else {
						$('#select_partner_data').html('First select a Client.');
						$('#select_form_data').html('First select a Client.');
					}
				}
			});
		}

		clientAutocomplete($('#search'), { 
			select: function($search) { 
				getPartnerAndForm();
			}
		});

		// This form doesn't exist any more. [MSD 2/6/2013]
		// if($('.form_select').val() == '16')
		// {
		// 	$('#td_form_date').html('<?= REQUIRED_FIELD . LABEL_TAX_RETURN_990PF_DATE ?>');
		// 	$('#form_date').html('<input id="tbFormDate" type="text" class="input datepicker" name="<?= TAX_RETURN_VIEW_990PF_DATE ?>" value = "<?= $this->session->flashdata(TAX_RETURN_VIEW_990PF_DATE) ?>"/>');
		// 	$("#tbFormDate").Watermark("<?= JS_990PF_DATE ?>");
		// 	$(".datepicker").live("click", function()
		// 	{
		// 		$(this).datepicker({showOn:'focus'}).focus();
		// 		$(this).datepicker();
		// 	});
		// }

		// This form doesn't exist any more. [MSD 2/6/2013]
		// $('.form_select').live('change', function()
		// {
		// 	if($(this).val() == '16')
		// 	{
		// 		$('#td_form_date').html('<?= REQUIRED_FIELD . LABEL_TAX_RETURN_990PF_DATE?>');
		// 		$('#form_date').html('<input id="tbFormDate" type="text" class="input datepicker" name="<?= TAX_RETURN_VIEW_990PF_DATE ?>" />');
		// 		$("#tbFormDate").Watermark("<?= JS_990PF_DATE ?>");
		// 		$(".datepicker").live("click", function()
		// 		{
		// 			$(this).datepicker({showOn:'focus'}).focus();
		// 			$(this).datepicker();
		// 		});
		// 	}
		// 	else
		// 	{
		// 		$('#td_form_date').html('');
		// 		$('#form_date').html('');
		// 	}
		// });

		$("#<?= TAX_RETURN_DATE_RELEASED ?>").mask("99/99/9999");
		$("#<?= TAX_RETURN_DATE_RELEASED ?>").Watermark("<?= JS_DATE_RELEASED ?>");
		$("#<?= TAX_RETURN_NOTES ?>").Watermark("<?= JS_TAX_RETURN_NOTES ?>");
		$("#<?= TAX_RETURN_DATE_SENT ?>").mask("99/99/9999");
		$("#<?= TAX_RETURN_DATE_SENT ?>").Watermark("<?= JS_DATE_SENT ?>");
	});
</script>
