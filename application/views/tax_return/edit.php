<div id="content" class="clearfix">
	<fieldset style="width: 650px;">
		<legend>
			<?= stripslashes(trim($data[TAX_RETURN_VIEW_CLIENT_NAME])) ?>
		</legend>
		<div>&nbsp;</div>
		<?php echo form_open(base_url().TAX_RETURN_POST.'/update_tax_return/'.$data[TAX_RETURN_VIEW_TAX_RETURN_ID]); ?>
		<table>
			<colgroup>
				<col style="width: 15%;" />
				<col style="width: 20%;" />
				<col style="width: 20%;" />
				<col style="width: 15%;" />
			</colgroup>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_TAX_FORM_ID ?>
				</td>
				<td colspan="2">
					<?= $Tax_Forms ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_FORM_ID) ?>
				</td>
			</tr>
			<tr>
				<td id="td_form_date"></td>
				<td colspan="2">
					<div id="form_date"></div>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_FORM_DATE) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_TAX_YEAR_ID ?>
				</td>
				<td colspan="2">
					<?= $Tax_Years ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_YEAR_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_TAX_QUARTER_ID ?>
				</td>
				<td colspan="2">
					<?= $Tax_Quarters ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_QUARTER_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_TAX_MONTH_ID ?>
				</td>
				<td colspan="2">
					<?= $Tax_Months ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_MONTH_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= REQUIRED_FIELD . LABEL_TAX_RETURN_STATUS_ID ?>
				</td>
				<td colspan="2">
					<?= $Tax_Return_Status ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_RETURN_STATUS_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_TAX_RETURN_STAFF_ID ?>
				<td colspan="2">
					<?= $Staff ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_RETURN_STAFF_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_TAX_RETURN_REVIEWER_1_ID ?>
				</td>
				<td colspan="2">
					<?= $Reviewer_1 ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_REVIEWER_1_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_TAX_RETURN_REVIEWER_2_ID ?>
				</td>
				<td colspan="2">
					<?= $Reviewer_2 ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_REVIEWER_2_ID) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_TAX_RETURN_NOTES ?>	
				</td>
				<td colspan="2">
					<textarea class="input" id="<?= TAX_RETURN_VIEW_TAX_RETURN_NOTES ?>" name="<?= TAX_RETURN_POST ?>[<?= TAX_RETURN_VIEW_TAX_RETURN_NOTES ?>]" rows="5" style="width: 95%;"  value="<?= $data[TAX_RETURN_VIEW_TAX_RETURN_NOTES] ?>"><?= $data[TAX_RETURN_VIEW_TAX_RETURN_NOTES] ?></textarea>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_CLIENT_NOTES) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_INSTRUCTIONS_ID ?>
				</td>
				<td colspan="2">
					<?= $Instructions ?>
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_INSTRUCTIONS_ID) ?>
					<?php if ($this->session->userdata(ROLE_ADMIN) || $this->session->userdata(ROLE_WRITE)): ?>
					<div id="add">
						<img src="<?= $this->config->item('base_url'); ?>application/views/css/images/add.png" />
					</div>
					<?php endif; ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_TAX_RETURN_DATE_SENT ?>
				</td>
				<td colspan="2">
					<input class="input datepicker" id="<?= TAX_RETURN_VIEW_DATE_SENT ?>" name="<?= TAX_RETURN_POST ?>[<?= TAX_RETURN_VIEW_DATE_SENT ?>]" type="text" value="<?= ($data[TAX_RETURN_VIEW_DATE_SENT] != 0) ? date(DATE_FORMAT, $data[TAX_RETURN_VIEW_DATE_SENT]) : ''; ?>" />
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_RETURN_DATE_SENT) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_TAX_RETURN_DATE_RELEASED ?>
				</td>
				<td colspan="2">
					<input class="input datepicker" id="<?= TAX_RETURN_VIEW_DATE_RELEASED ?>" name="<?= TAX_RETURN_POST ?>[<?= TAX_RETURN_VIEW_DATE_RELEASED ?>]" type="text" value="<?= ($data[TAX_RETURN_VIEW_DATE_RELEASED] != 0) ? date(DATE_FORMAT, $data[TAX_RETURN_VIEW_DATE_RELEASED]) : ''; ?>" />
				</td>
				<td class="errors">
					<?= $this->session->flashdata(ERROR_TAX_RETURN_DATE_RELEASED) ?>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td>
					<input class="submit back_button_link" type="button" value="Cancel" style="width: 95%; text-align: center;">
				</td>
				<td colspan="2" style="text-align: center;">
					<input class="submit" type="submit" value="Save" style="width: 40%; text-align: center;">
					<?php if ($this->session->userdata(ROLE_ADMIN) || $this->session->userdata(ROLE_WRITE)): ?>
					<a class="menu delete_tax_return" href="<?php echo base_url() . TAX_RETURN_POST ?>/delete/<?= $data[TAX_RETURN_VIEW_TAX_RETURN_ID] ?>" onclick="if(confirm('Are you sure you wish to delete this tax return?\n \'Cancel\' to stop, \'OK\' to delete.')){return true;} return false;">
						<input class="submit" type="button" value="Delete" style="float: right; width: 40%; text-align: center;">
					</a>
					<?php endif; ?>
				</td>
				<td>
					<?php if(($this->session->userdata(ROLE_ADMIN) || $this->session->userdata(ROLE_WRITE)) && $data['Is_Locked'] > 0) : ?>
					<a class="menu unlock" href="<?php echo base_url() . TAX_RETURN_POST ?>/unlock/<?= $data[TAX_RETURN_VIEW_TAX_RETURN_ID] ?>" onclick="if(confirm('Are you sure you want to unlock this record?\n \'Cancel\' to stop, \'OK\' to unlock.')){return true;}return false;">
						<input class="submit" type="button" value="UnLock" style="width: 95%; text-align: center;">
					</a>
					<?php elseif(($this->session->userdata(ROLE_ADMIN) || $this->session->userdata(ROLE_WRITE)) && $data['Is_Locked'] == 0) : ?>
					<a class="menu lock" href="<?php echo base_url() . TAX_RETURN_POST ?>/lock/<?= $data[TAX_RETURN_VIEW_TAX_RETURN_ID] ?>" onclick="if(confirm('Are you sure you want to lock this record?\n \'Cancel\' to stop, \'OK\' to lock.')){return true;}return false;">
						<input class="submit" type="button" value="Lock" style="width: 95%; text-align: center;">
					</a>
					<?php elseif($this->session->userdata(ROLE_WRITE) && $data['Is_Locked'] > 0) : ?>
					<a class="menu lock" href="<?php echo base_url() . TAX_RETURN_POST ?>/lock/<?= $data[TAX_RETURN_VIEW_TAX_RETURN_ID] ?>" onclick="if(confirm('Are you sure you want to lock this record?\n \'Cancel\' to stop, \'OK\' to lock.')){return true;}return false;">
						<input class="submit" type="button" value="Lock" style="width: 7095text-align: center;">
					</a>
					<?php endif; ?>
				</td>
			</tr>
		</table>
		<input name="<?= TAX_RETURN_POST ?>[<?= TAX_RETURN_VIEW_CLIENT_NAME ?>]" type="hidden" value="<?= $data[TAX_RETURN_VIEW_CLIENT_NAME] ?>" />
		<?php echo form_close(); ?>
	</fieldset>
	<div>&nbsp;</div>
	<fieldset style="width: 650px;">
		<legend>
			Status History
		</legend>
		<div>&nbsp;</div>
		<table class="view">
			<tr>
				<th>
					Tax Return Status
				</th>
				<th>
					Prepared By
				</th>
				<th>
					1st Reviewer
				</th>
				<th>
					2nd Reviewer
				</th>
				<th>
					Last Edited By
				</th>
				<th>
					<a class="view_link" href="<?= base_url() . TAX_RETURN_POST ?>/edit/<?= $data[TAX_RETURN_VIEW_TAX_RETURN_ID] ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
						Last Edited On<?= ($as == 'ASC') ? '&uarr;' : '' ?><?= ($as == 'DESC') ? '&darr;' : '' ?>
					</a>
				</th>
			</tr>
			<?php foreach ($History as $his): ?>
			<tr>
				<td>
					<?= $his[TAX_RETURN_HISTORY_VIEW_TAX_RETURN_STATUS] ?>
				</td>
				<td>
					<?= $his[TAX_RETURN_HISTORY_VIEW_STAFF_NAME] ? $his[TAX_RETURN_HISTORY_VIEW_STAFF_NAME] : '&nbsp;' ?>
				</td>
				<td>
					<?= $his[TAX_RETURN_HISTORY_VIEW_REVIEWER_1_NAME] ? $his[TAX_RETURN_HISTORY_VIEW_REVIEWER_1_NAME] : '&nbsp;' ?>
				</td>
				<td>
					<?= $his[TAX_RETURN_HISTORY_VIEW_REVIEWER_2_NAME] ? $his[TAX_RETURN_HISTORY_VIEW_REVIEWER_2_NAME] : '&nbsp;' ?>
				</td>
				<td>
					<?= $his[TAX_RETURN_HISTORY_VIEW_LAST_EDIT_USER_NAME] ?>
				</td>
				<td>
					<?= date('m/d/Y', $his[TAX_RETURN_HISTORY_VIEW_LAST_EDIT_DATE]) ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</table>
	</fieldset>
	<input id="status_name" name="<?= TAX_RETURN_POST ?>[status_name]" type="hidden" />
	<div class="overlay_instructions">
	</div>
	<div class="add_instructions">
		<img align="right" class="cancel_instructions" src="<?= $this->config->item('base_url'); ?>application/views/css/images/exit_add_occupation.png" />
		<?= form_open(base_url().TAX_RETURN_POST.'/add_instructions'); ?>
		<table cellpadding="5px" cellspasing="5px">
			<tr>
				<td align="center">
				<span class="message_success_instructions"></span>
				<span class="error_instructions"></span>
			</td>
			</tr>
			<tr>
				<td>
					Enter new Instruction
				</td>
			</tr>
			<tr>
				<td>
					<input id="add_instructions" class="input" name="add_instructions" type="text" value="">
				</td>
			</tr>
			<tr>
				<td align="center">
					<input class="submit ok_instructions" type="button" value="Add">
					<input class="submit cancel_instructions" type="button" value="Cancel">
				</td>
			</tr>
		</table>
		<?= form_close(); ?>
	</div>
	<br />
</div>
<script type="text/javascript">
	$(document).ready(function()
	{
		$("#<?= TAX_RETURN_DATE_RELEASED ?>").mask("99/99/9999");
		$("#<?= TAX_RETURN_DATE_RELEASED ?>").Watermark("<?= JS_DATE_RELEASED ?>");
		$("#<?= TAX_RETURN_NOTES ?>").Watermark("<?= JS_TAX_RETURN_NOTES ?>");
		$("#search_tex").Watermark("<?= JS_CLIENT_NAME ?>");
		$("#<?= TAX_RETURN_DATE_SENT ?>").mask("99/99/9999");
		$("#<?= TAX_RETURN_DATE_SENT ?>").Watermark("<?= JS_DATE_SENT ?>");

		// This form doesn't exist any more. [MSD 2/6/2013]
		// if($('.form_select').val() == '16')
		// {
		// 	$('#form_date').html('<input type="text" class="input datepicker" name="form_990PF" value="<?= $data[TAX_FORM_ID] == 16 && $tax_return_date ? date('m/d/Y',$tax_return_date) : ''; ?>" />');
		// 	$(".datepicker").live("click", function()
		// 	{
		// 		$(this).datepicker({showOn:'focus'}).focus();
		// 		$(this).datepicker();
		// 	});
		// };

		// This control doesn't exist. [MSD 2/6/2013]
		// $('.select_status').change(function()
		// {
		// 	$('#status_name').val($(this).find('option:selected').text());
		// 	$.ajax(
		// 	{
		// 		type: "POST",
		// 		url: "<?= $this->config->item('base_url') . TAX_RETURN_POST ?>/get_input",
		// 		dataType: "html",
		// 		data: "id="+$(this).val()+"&name="+$(this).find('option:selected').text(),
		// 		success: function(response)
		// 		{
		// 			$('#required_input').html(response);
		// 			$(".datepicker").live("click", function()
		// 			{
		// 				$(this).datepicker({showOn:'focus'}).focus();
		// 				$(this).datepicker();
		// 			});
		// 		}
		// 	});
		// });

		// This form doesn't exist any more. [MSD 2/6/2013]
		// $('.form_select').change(function()
		// {
		// 	if($(this).val() == '16')
		// 	{
		// 		$('#form_date').html('<input type="text" class="input datepicker" name="form_990PF" />');
		// 		$(".datepicker").live("click", function()
		// 		{
		// 			$(this).datepicker({showOn:'focus'}).focus();
		// 			$(this).datepicker();
		// 		});
		// 	}
		// 	else
		// 	{
		// 		$('#form_date').html('');
		// 	}
		// });
	});
</script>
