<div id="content">
  <input class ="submit back_button_link" type="button" value ="Back to List" />
  <?php echo form_open(base_url() . 'tax_return/autoadd_set');
  ?>
  <table >
<tr>
      <td><label for="tax_return[entity]">Entity type: </label></td>
      <td colspan="2">
        <select class ="input entity_type" name="tax_return[entity]">
          <?php foreach ($entity_type as $entity): ?>
            <option value="<?= $entity['name_entity_type']; ?>"><?= $entity['name_entity_type']; ?></option>
          <?php endforeach; ?>
        </select>
      </td>
      <td class ="errors"><?= $this->session->flashdata('error_entity') ?></td>
    </tr>
    <tr>
      <td valign="top"><label for="tax_return[state]">State: </label></td>
      <td colspan="2">
        <select class ="input" name="tax_return[state][]">

          <option value="AL" <?= (array_search('AL', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>AL</option>
          <option value="AK" <?= (array_search('AK', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>AK</option>
          <option value="AS" <?= (array_search('AS', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>AS</option>
          <option value="AZ" <?= (array_search('AZ', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>AZ</option>
          <option value="AR" <?= (array_search('AR', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>AR</option>
          <option value="CA" <?= (array_search('CA', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>CA</option>
          <option value="CO" <?= (array_search('CO', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>CO</option>
          <option value="CT" <?= (array_search('CT', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>CT</option>
          <option value="DE" <?= (array_search('DE', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>DE</option>
          <option value="DC" <?= (array_search('DC', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>DC</option>
          <option value="FM" <?= (array_search('FM', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>FM</option>
          <option value="FL" <?= (array_search('FL', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>FL</option>
          <option value="GA" <?= (array_search('GA', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>GA</option>
          <option value="GU" <?= (array_search('GU', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>GU</option>
          <option value="HI" <?= (array_search('HI', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>HI</option>
          <option value="ID" <?= (array_search('ID', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>ID</option>
          <option value="IL" <?= (array_search('IL', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>IL</option>
          <option value="IN" <?= (array_search('IN', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>IN</option>
          <option value="IA" <?= (array_search('IA', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>IA</option>
          <option value="KS" <?= (array_search('KS', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>KS</option>
          <option value="KY" <?= (array_search('KY', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>KY</option>
          <option value="LA" <?= (array_search('LA', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>LA</option>
          <option value="ME" <?= (array_search('ME', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>ME</option>
          <option value="MH" <?= (array_search('MH', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>MH</option>
          <option value="MD" <?= (array_search('MD', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>MD</option>
          <option value="MA" <?= (array_search('MA', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>MA</option>
          <option value="MI" <?= (array_search('MI', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>MI</option>
          <option value="MN" <?= (array_search('MN', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>MN</option>
          <option value="MS" <?= (array_search('MS', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>MS</option>
          <option value="MO" <?= (array_search('MO', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>MO</option>
          <option value="MT" <?= (array_search('MT', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>MT</option>
          <option value="NE" <?= (array_search('NE', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>NE</option>
          <option value="NV" <?= (array_search('NV', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>NV</option>
          <option value="NH" <?= (array_search('NH', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>NH</option>
          <option value="NJ" <?= (array_search('NJ', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>NJ</option>
          <option value="NM" <?= (array_search('NM', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>NM</option>
          <option value="NY" <?= (($this->session->flashdata('state') && array_search('NY', explode(',', $this->session->flashdata('state'))) !== FALSE) || !$this->session->flashdata('state')) ? 'selected="selected"' : '' ?>>NY</option>
          <option value="NC" <?= (array_search('NC', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>NC</option>
          <option value="ND" <?= (array_search('ND', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>ND</option>
          <option value="MP" <?= (array_search('MP', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>MP</option>
          <option value="OH" <?= (array_search('OH', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>OH</option>
          <option value="OK" <?= (array_search('OK', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>OK</option>
          <option value="OR" <?= (array_search('OR', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>OR</option>
          <option value="PW" <?= (array_search('PW', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>PW</option>
          <option value="PA" <?= (array_search('PA', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>PA</option>
          <option value="PR" <?= (array_search('PR', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>PR</option>
          <option value="RI" <?= (array_search('RI', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>RI</option>
          <option value="SC" <?= (array_search('SC', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>SC</option>
          <option value="SD" <?= (array_search('SD', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>SD</option>
          <option value="TN" <?= (array_search('TN', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>TN</option>
          <option value="TX" <?= (array_search('TX', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>TX</option>
          <option value="UT" <?= (array_search('UT', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>UT</option>
          <option value="VT" <?= (array_search('VT', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>VT</option>
          <option value="VI" <?= (array_search('VI', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>VI</option>
          <option value="VA" <?= (array_search('VA', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>VA</option>
          <option value="WA" <?= (array_search('WA', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>WA</option>
          <option value="WV" <?= (array_search('WV', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>WV</option>
          <option value="WI" <?= (array_search('WI', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>WI</option>
          <option value="WY" <?= (array_search('WY', explode(',', $this->session->flashdata('state'))) !== FALSE) ? 'selected="selected"' : '' ?>>WY</option>

        </select>
      </td>
    <tr>
      <td><label for="tax_return[form]">Form<span class="errors">*</span>: </label></td>
      <td><div id="select_form_section">

          <select class ="input form_select" name="tax_return[form][]">
            <?php
            if (is_array($forms)) {
              ?>
              <?php
              foreach ($forms as $form) {
                ?>
                <option class="select_form" value="<?= $form['form_id']; ?>" <?= $form['form_id'] == $this->session->flashdata('form_id') ? 'selected="selected"' : ''; ?>><?= $form['form_name'] ?></option>
                <?php
              }
            } else {
              ?>
              <option class="select_form" value="0">Select client first</option>-->
              <?php
            }
            ?>
            <!--< ?php if($this->session->flashdata('form')): ?>-->
            <!--<option class="select_form" value="< ?=$this->session->flashdata('form_id') ?>" selected ="selected">< ?=$this->session->flashdata('form') ?></option>-->
            <!--< ?php else: ?>-->
            <!--<option class="select_form" value="0">Select client first</option>-->
            <!--< ?php endif; ?>-->
            <!--< ?php foreach($forms as $form): ?>-->
            <!--	<option class="select_form" value="< ?=$form['form_id']; ?>">< ?=$form['form_name']; ?></option>-->
            <!--< ?php endforeach; ?>-->
          </select>
          <div>
            </td>
            <td></td>
            <td class ="errors"><?= $this->session->flashdata('error_form') ?></td>
            </tr>

            <tr><td></td><td><div id="form_date"></div></td>
              <td class ="errors"><?= $this->session->flashdata('error_form_date') ?></td></tr>
            <tr>
              <td><label for="tax_return[tax_year]">Tax year<span class="errors">*</span>: </label></td>
              <td><select class ="input" class="tax_year" name="tax_return[tax_year]">
                  <option value="1970">N/A</option>
                  <?php for ($i = 2013; $i >= 2000; $i--): ?>
                    <option value="<?php echo $i; ?>" <?= ($i == $this->session->flashdata('tax_year')) ? 'selected="selected"' : '' ?>><?php echo $i; ?></option>
                  <?php endfor; ?>

                </select>
              </td>
              <td></td>
              <td class ="errors"><?= $this->session->flashdata('error_tax_year') ?></td>

            </tr>
            <tr>
              <td><input class ="submit back_button_link" type="button" value ="Cancel"></td>
              <td><input class ="submit" type="submit" value="Save"></td>

            </tr>

            </table>
            <input type="hidden" name="tax_return[status_name]" id="status_name"/>
            <?php echo form_close(); ?>
            