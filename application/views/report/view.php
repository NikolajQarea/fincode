<div id="content" class="clearfix">
	<div class="clearfix">
	</div>
	<?php echo form_open(base_url() . 'report/generate_report'); ?>
	<fieldset id="report_filters" style="width: 900px;">
		<legend>
			Generate a Report
		</legend>
		<table class="report">
			<colgroup>
				<col style="width: 15%;">
				<col style="width: 35%;">
				<col style="width: 15%;">
				<col style="width: 35%;">
			</colgroup>
			<tr>
				<td>
					Report Date:
				</td>
				<td>
					<input class="datepicker input date_input" name="report[<?= REPORT_DATE ?>]" type="text" value="<?= $this->session->flashdata(REPORT_DATE) ? $this->session->flashdata(REPORT_DATE) : date(DATE_FORMAT); ?>" />
				</td>
				<td>
					<?= LABEL_TAX_FORM_ID ?>
				</td>
				<td>
					<?= str_replace("input", "input select_form_input", $TaxForms) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_CLIENT_ID ?>
				</td>
				<td>
					<?= str_replace("input", "input client_select", $ClientNames) ?>
				</td>
				<td>
					<?= LABEL_TAX_RETURN_STATUS_ID ?>
				</td>
				<td>
					<?= str_replace("input", "input select_status", $ReturnStatus) ?>
				</td>
			</tr>
			
			<tr>
				<td>
					<?= LABEL_CLIENT_STATUS_ID ?>
				</td>
				<td>
					<?= $ClientStatus ?>
				</td>
				<td>
					<?= LABEL_TAX_MONTH_ID ?>
				</td>
				<td>
					<?= $TaxMonths ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_CLIENT_PARTNER_ID ?>
				</td>
				<td>
					<?= str_replace("input", "input select_partner", $Partners) ?>
				</td>
				<td>
					<?= LABEL_TAX_QUARTER_ID ?>
				</td>
				<td>
					<?= $TaxQuarters ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_STAFF_ID ?>
				</td>
				<td>
					<?= $Staff ?>
				</td>
				<td>
					<?= LABEL_TAX_YEAR_ID ?>
				</td>
				<td>
					<?= $TaxYears ?>
				</td>
			</tr>
			<tr>
				<td class="tdFilter" colspan="2" valign="middle">
					<label for="report[sent_returns]">
						Exclude Sent To Client?
					</label>
					<input id="report[Exclude_Sent]" name="report[Exclude_Sent]"  title="Exclude Sent To Client?" type="checkbox" <?= $this->session->flashdata('Exclude_Sent') == 1 ? ' checked' : ''; ?> />
	   		    </td>
				<td>
					&nbsp;
				</td>
				<td>
					&nbsp;
				</td>
			</tr>
			<tr>
				<td>
					&nbsp;
				</td>
				<td align="right" colspan="2">
					<input class="submit" style="width: 55%;" type="submit" value="Generate" />
				</td>
				<td colspan="2">
					&nbsp;
				</td>
			</tr>
		</table>
	</fieldset>
	<?php echo form_close(); ?>
	<div class="print_section">
		<a href="<?= $this->config->item('base_url'); ?>report/convertToExcel">
			<img src="<?php print $this->config->item('base_url'); ?>application/views/css/images/excel.png" style="width: 25px; float: right; margin: 5px;" />
		</a>
		<a href="<?= $this->config->item('base_url'); ?>report/convertToPdf">
			<img height="32px" src="<?= $this->config->item('base_url'); ?>application/views/css/images/pdf.png" style="width: 25px; float: right; margin: 5px;" />
		</a>
		<a class="print_report" href="javascript:window.print();">
			<img height="32px" src="<?= $this->config->item('base_url'); ?>application/views/css/images/print.png" style="width: 25px; float: right; margin: 5px;" />
		</a>
	</div>
	<table class="view">
		<tr>
			<th>
				<a class="view_link" href="<?= base_url() ?>report/generate_report/<?= CLIENT_VIEW_CLIENT_NAME ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Client Name<?= ($order == CLIENT_VIEW_CLIENT_NAME) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == CLIENT_VIEW_CLIENT_NAME) && ($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>report/generate_report/<?= TAX_RETURN_VIEW_TAX_FORM ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Tax Form<?= ($order == TAX_RETURN_VIEW_TAX_FORM) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_TAX_FORM) && ($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>report/generate_report/<?= TAX_RETURN_VIEW_TAX_YEAR ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Tax Year<?= ($order == TAX_RETURN_VIEW_TAX_YEAR) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_TAX_YEAR) && ($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>report/generate_report/<?= TAX_RETURN_VIEW_TAX_QUARTER ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Tax Quarter<?= ($order == TAX_RETURN_VIEW_TAX_QUARTER) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_TAX_QUARTER) && ($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>report/generate_report/<?= TAX_RETURN_VIEW_TAX_MONTH_ID ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Begins<?= ($order == TAX_RETURN_VIEW_TAX_MONTH_ID) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_TAX_MONTH_ID) && ($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>report/generate_report/<?= TAX_RETURN_VIEW_CLIENT_STATES ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					States<?= ($order == TAX_RETURN_VIEW_CLIENT_STATES) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_CLIENT_STATES) && ($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>report/generate_report/<?= TAX_RETURN_VIEW_PARTNER ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
					Partner<?= ($order == TAX_RETURN_VIEW_PARTNER) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_PARTNER) && ($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				Due Date
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>report/generate_report/<?= TAX_RETURN_VIEW_STAFF ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
				Prepared By<?= ($order == TAX_RETURN_VIEW_STAFF) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_STAFF) && ($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>report/generate_report/<?= TAX_RETURN_VIEW_TAX_RETURN_STATUS ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
				Tax Return Status<?= ($order == TAX_RETURN_VIEW_TAX_RETURN_STATUS) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_TAX_RETURN_STATUS) && ($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
			<th>
				<a class="view_link" href="<?= base_url() ?>report/generate_report/<?= TAX_RETURN_VIEW_CLIENT_STATUS ?>/<?= ($as == 'ASC') ? 'DESC' : 'ASC' ?>">
				Client Status<?= ($order == TAX_RETURN_VIEW_CLIENT_STATUS) && ($as == 'ASC') ? '&uarr;' : '' ?><?= ($order == TAX_RETURN_VIEW_CLIENT_STATUS) && ($as == 'DESC') ? '&darr;' : '' ?>
				</a>
			</th>
		</tr>
		<?php foreach ($reports as $report): ?>
		<tr align="center">
			<td>
				<a href="<?= $this->config->item('base_url'); ?>tax_return/edit/<?= $report[TAX_RETURN_VIEW_TAX_RETURN_ID]; ?>" target="_blank">
					<?= stripslashes(trim($report[TAX_RETURN_VIEW_CLIENT_NAME])) ?>
				</a>
			</td>
			<td>
				<?= isset($report[TAX_RETURN_VIEW_TAX_FORM]) ? $report[TAX_RETURN_VIEW_TAX_FORM] : '' ?>
			</td>
			<td>
				<?= $report[TAX_RETURN_VIEW_TAX_YEAR] == 1970 ? 'N/A' : $report[TAX_RETURN_VIEW_TAX_YEAR] ?>
			</td>
			<td>
				<?= $report[TAX_RETURN_VIEW_TAX_QUARTER_ID] < 1 ? NOT_APPLICABLE : $report[TAX_RETURN_VIEW_TAX_QUARTER] ?>
			</td>
			<td>
				<?= $report[TAX_RETURN_VIEW_TAX_MONTH_ID] < 0 ? NOT_APPLICABLE : $report[TAX_RETURN_VIEW_TAX_MONTH] ?>
			</td>
			<td>
				<?= $report[TAX_RETURN_VIEW_CLIENT_STATES] ?>
			</td>
			<td>
				<?= $report[TAX_RETURN_VIEW_PARTNER_INITIALS] ?>
			</td>
			<td>
				<?= $report['Due_Date'] ?>
			</td>
			<td>
				<?= $report[TAX_RETURN_VIEW_STAFF_INITIALS] ?>
			</td>
			<td>
				<?= $report[TAX_RETURN_VIEW_TAX_RETURN_STATUS] ?>
			</td>
			<td>
				<?= $report[TAX_RETURN_VIEW_CLIENT_STATUS] ?>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
	<div class="print_section">
		<a href="<?= $this->config->item('base_url'); ?>report/convertToExcel">
			<img src="<?php print $this->config->item('base_url'); ?>application/views/css/images/excel.png" style="width: 25px; float: right; margin: 5px;" />
		</a>
		<a href="<?= $this->config->item('base_url'); ?>report/convertToPdf">
			<img height="32px" src="<?= $this->config->item('base_url'); ?>application/views/css/images/pdf.png" style="width: 25px; float: right; margin: 5px;" />
		</a>
		<a class="print_report" href="javascript:window.print();">
			<img height="32px" src="<?= $this->config->item('base_url'); ?>application/views/css/images/print.png" style="width: 25px; float: right; margin: 5px;" />
		</a>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function()
	{
		$('.print_report').bind("click", function()
		{
			var test_html = $('.for_iframe').html().replace(/(^\s+)|(\s+$)/g, "");
			if(test_html == '')
			{
				$('.for_iframe').html("<iframe class='print_iframe' name='print_iframe' src='<?= $this->config->item('base_url'); ?>report/printReport' />");
				$('.print_iframe').load(function()
				{
					window.frames['print_iframe'].focus();
					window.frames['print_iframe'].print();
				});
			}
			else
			{
				window.frames['print_iframe'].focus();
				window.frames['print_iframe'].print();
			}
		});
	});
</script>
