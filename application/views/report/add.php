<div id="content" class="clearfix">
	<div class="clearfix">
	</div>
	<?php echo form_open(base_url() . 'report/generate_report'); ?>
	<fieldset style="width: 900px;">
		<legend>
			Generate a Report
		</legend>
		<table class="report">
			<colgroup>
				<col style="width: 15%;">
				<col style="width: 35%;">
				<col style="width: 15%;">
				<col style="width: 35%;">
			</colgroup>
			<tr>
				<td>
					Report Date:
				</td>
				<td>
					<input class="datepicker input date_input" name="report[<?= REPORT_DATE ?>]" type="text" value="<?= $this->session->flashdata(REPORT_DATE) ? $this->session->flashdata(REPORT_DATE) : date(DATE_FORMAT); ?>" />
				</td>
				<td>
					<?= LABEL_TAX_FORM_ID ?>
				</td>
				<td>
					<?= str_replace("input", "input select_form_input", $TaxForms) ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_CLIENT_ID ?>
				</td>
				<td>
					<?= str_replace("input", "input client_select", $ClientNames) ?>
				</td>
				<td>
					<?= LABEL_TAX_RETURN_STATUS_ID ?>
				</td>
				<td>
					<?= str_replace("input", "input select_status", $ReturnStatus) ?>
				</td>
			</tr>
			
			<tr>
				<td>
					<?= LABEL_CLIENT_STATUS_ID ?>
				</td>
				<td>
					<?= $ClientStatus ?>
				</td>
				<td>
					<?= LABEL_TAX_MONTH_ID ?>
				</td>
				<td>
					<?= $TaxMonths ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_CLIENT_PARTNER_ID ?>
				</td>
				<td>
					<?= str_replace("input", "input select_partner", $Partners) ?>
				</td>
				<td>
					<?= LABEL_TAX_QUARTER_ID ?>
				</td>
				<td>
					<?= $TaxQuarters ?>
				</td>
			</tr>
			<tr>
				<td>
					<?= LABEL_STAFF_ID ?>
				</td>
				<td>
					<?= $Staff ?>
				</td>
				<td>
					<?= LABEL_TAX_YEAR_ID ?>
				</td>
				<td>
					<?= $TaxYears ?>
				</td>
			</tr>
			<tr>
				<td class="tdFilter" colspan="2" valign="middle">
					<label for="report[sent_returns]">
						Exclude Sent To Client?
					</label>
					<input id="report[Exclude_Sent]" name="report[Exclude_Sent]"  title="Exclude Sent To Client?" type="checkbox" <?= isset($post['Exclude_Sent']) && $post['Exclude_Sent'] == 'on' ? ' checked' : ''; ?> />
	   		    </td>
				<td>
					&nbsp;
				</td>
				<td>
					&nbsp;
				</td>
			</tr>
			<tr>
				<td>
					&nbsp;
				</td>
				<td align="right" colspan="2">
					<input class="submit" style="width: 55%;" type="submit" value="Generate" />
				</td>
				<td colspan="2">
					&nbsp;
				</td>
			</tr>
		</table>
	</fieldset>
	<?php echo form_close(); ?>
</div>
