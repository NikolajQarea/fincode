<h3>Tax Control Report</h3>
<!--Client: < ?=$report_data['client']; ?><br />-->
Form: <?= $report_data['form']; ?><br />
Year: <?= $report_data['year']; ?><br />
Quarter: <?= $report_data['quarter'] == NOT_APPLICABLE_ID ? NOT_APPLICABLE : $report_data['quarter']; ?><br />
Month: <?= $report_data['month'] == NOT_APPLICABLE_ID ? NOT_APPLICABLE : $report_data['month']; ?><br />
PTR: <?= $report_data['partner']; ?><br />
Staff: <?= $report_data['staff']; ?><br />


Report date: <?= $report_data['date']; ?><br />
<br />
<table width="100%">
<tr>
    <th>Client Name</th>
    <th>Form</th>
    <th>Year</th>
    <th>Quarter</th>
    <th>Month</th>
    <th>States</th>
    <th>PTR</th>
    <th>Due date</th>
    <th>Staff</th>
    <th>Status</th>
</tr>
<?php foreach($reports as $report): ?>
    <?php
        if(count(explode(',', $report['state']))>1)
        {
            $states = explode(',', $report['state']);
            
            ?>
                <tr align="center">
                    <td><?= stripslashes($report['Client_Name']); ?></td>
                    <td><?= isset($report['form_name']) ? $report['form_name'] : ''; ?></td>
                    <td><?= $report['tax_year']; ?></td>
                    <td><?= $report['tax_quarter'] == NOT_APPLICABLE_ID ? NOT_APPLICABLE : $report['tax_quarter']; ?></td>
                    <td><?= $report['tax_month'] == NOT_APPLICABLE_ID ? NOT_APPLICABLE : date('F', $report['tax_month']); ?></td>
                    <td><?= $states[0]; ?></td>
                    <td><?= $report['partner']; ?></td>
                    <td><?= $report['due_date']; ?></td>
                    <td><?= $report['staff']; ?></td>
                    <td><?= $report['status_name']; ?></td>
                </tr>
            <?php
            for($i=1; $i<count($states); $i++)
            {
                ?>
                <tr align="center">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><?= $states[$i]; ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    
                </tr>
            <?php
            }
        }
        else
        {
            ?>
            <tr align="center">
                <td><?= stripslashes($report['Client_Name']); ?></td>
                <td><?= isset($report['form_name']) ? $report['form_name'] : ''; ?></td>
                <td><?= $report['tax_year']; ?></td>
                <td><?= $report['tax_quarter'] == NOT_APPLICABLE_ID ? NOT_APPLICABLE : $report['tax_quarter']; ?></td>
                <td><?= $report['tax_month'] == NOT_APPLICABLE_ID ? NOT_APPLICABLE : date('F', $report['tax_month']); ?></td>
                <td><?= $report['state']; ?></td>
                <td><?= $report['partner']; ?></td>
                <td><?= $report['due_date']; ?></td>
                <td><?= $report['staff']; ?></td>
                <td><?= $report['status_name']; ?></td>
            </tr>
            <?php
        }
    ?>
    <!--<tr>-->
    <!--    <td>< ?=stripslashes($report[COMPANY_NAME]); ?></td>-->
    <!--    <td>< ?=stripslashes($report[CLIENT_NAME_LAST]).' '.stripslashes($report[CLIENT_NAME_FIRST]); ?></td>-->
    <!--    <td>< ?=stripslashes($report[SPOUSE_NAME_LAST]); ?></td>-->
    <!--    <td>< ?=$report['form_name']; ?></td>-->
    <!--    <td>< ?=$report['state']; ?></td>-->
    <!--    <td>< ?=$report['partner']; ?></td>-->
    <!--    <td>< ?=$report['month']; ?></td>-->
    <!--    <td>< ?=$report['due_date']; ?></td>-->
    <!--    <td>< ?=$report['staff']; ?></td>-->
    <!--    <td>< ?=$report['entity']; ?></td>-->
    <!--    <td>< ?=$report['status']; ?></td>-->
    <!--    <td>< ?=stripslashes($report['notes']); ?></td>-->
    <!--</tr>-->
<?php endforeach; ?>
</table>
<br />
<h4>Report date: <?= date('H:i:s m/d/Y'); ?></h4>