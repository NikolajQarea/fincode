<h3>Tax Control Report</h3>
Form: <?= $data[TAX_FORM] ?><br />
Year: <?= $data[TAX_YEAR_ID] ?><br />
Quarter: <?= $data[TAX_QUARTER_ID] == NOT_APPLICABLE_ID ? NOT_APPLICABLE : $data[TAX_QUARTER_ID] ?><br />
Month: <?= $data[TAX_MONTH_ID] == NOT_APPLICABLE_ID ? NOT_APPLICABLE : $data[TAX_MONTH_ID] ?><br />
Partner: <?= $data[CLIENT_VIEW_PARTNER_INITIALS] ?><br />
Prepared By: <?= $data[CLIENT_VIEW_STAFF_INITIALS] ?><br />
Report date: <?= $data[REPORT_DATE] ?><br />
<br />
<table width="100%">
<tr>
    <th>Client Name</th>
    <th>Form</th>
    <th>Year</th>
    <th>Quarter</th>
    <th>Month</th>
    <th>States</th>
    <th>PTR</th>
    <th>Due date</th>
    <th>Staff</th>
    <th>Status</th>
</tr>
<?php
	foreach($reports as $report) 
	{
		if(count(explode(',', $report[TAX_RETURN_VIEW_CLIENT_STATES])) > 1)
		{
			$states = explode(',', $report[TAX_RETURN_VIEW_CLIENT_STATES]);
			echo '<tr>';
			echo '<td><center>' . stripslashes($report[TAX_RETURN_VIEW_CLIENT_NAME]) . '</center></td>';
			echo '<td><center>' . $report[TAX_RETURN_VIEW_TAX_FORM] . '</center></td>';
			echo '<td><center>' . $report[TAX_RETURN_VIEW_TAX_YEAR] . '</center></td>';
			echo '<td><center>' . $report[TAX_RETURN_VIEW_TAX_QUARTER] . '</center></td>';
			echo '<td><center>' . $report[TAX_RETURN_VIEW_TAX_MONTH] . '</center></td>';
			echo '<td><center>' . $states[0] . '</center></td>';
			echo '<td><center>' . $report[TAX_RETURN_VIEW_PARTNER_INITIALS] . '</center></td>';
			echo '<td><center>' . date(DATE_FORMAT, strtotime($report['Due_Date'])) . '</center></td>';
			echo '<td><center>' . $report[TAX_RETURN_VIEW_STAFF_INITIALS] . '</center></td>';
			echo '<td><center>' . $report[TAX_RETURN_VIEW_TAX_RETURN_STATUS] . '</center></td>';
			echo '</tr>';
			for($i=1; $i<count($states); $i++)
			{
				echo '<tr><td></td><td></td><td></td><td></td><td></td><td><center>' . $states[$i] . '</center></td><td></td><td></td><td></td><td></td></tr>';
            }
        }
        else
        {
			echo '<tr>';
			echo '<td><center>' . stripslashes($report[TAX_RETURN_VIEW_CLIENT_NAME]) . '</center></td>';
			echo '<td><center>' . $report[TAX_RETURN_VIEW_TAX_FORM] . '</center></td>';
			echo '<td><center>' . $report[TAX_RETURN_VIEW_TAX_YEAR] . '</center></td>';
			echo '<td><center>' . $report[TAX_RETURN_VIEW_TAX_QUARTER] . '</center></td>';
			echo '<td><center>' . $report[TAX_RETURN_VIEW_TAX_MONTH] . '</center></td>';
			echo '<td><center>' . $report[TAX_RETURN_VIEW_CLIENT_STATES] . '</center></td>';
			echo '<td><center>' . $report[TAX_RETURN_VIEW_PARTNER_INITIALS] . '</center></td>';
			echo '<td><center>' . date(DATE_FORMAT, strtotime($report['Due_Date'])) . '</center></td>';
			echo '<td><center>' . $report[TAX_RETURN_VIEW_STAFF_INITIALS] . '</center></td>';
			echo '<td><center>' . $report[TAX_RETURN_VIEW_TAX_RETURN_STATUS] . '</center></td>';
			echo '</tr>';
		}
	}
?>
<!--< ?php foreach($reports as $report): ?>-->
    <!--<tr>-->
    <!--    <td>< ?=stripslashes($report[COMPANY_NAME]); ?></td>-->
    <!--    <td>< ?=stripslashes($report[CLIENT_NAME_LAST]).' '.stripslashes($report[CLIENT_NAME_FIRST]); ?></td>-->
    <!--    <td>< ?=$report[SPOUSE_NAME_LAST]; ?></td>-->
    <!--    <td>< ?=$report['form_name']; ?></td>-->
    <!--    <td>< ?=$report['state']; ?></td>-->
    <!--    <td>< ?=$report['partner']; ?></td>-->
    <!--    <td>< ?=$report['month']; ?></td>-->
    <!--    <td>< ?=$report['due_date']; ?></td>-->
    <!--    <td>< ?=$report['staff']; ?></td>-->
    <!--    <td>< ?=$report['entity']; ?></td>-->
    <!--    <td>< ?=$report['status']; ?></td>-->
    <!--    <td>< ?=$report['notes']; ?></td>-->
    <!--</tr>-->
<!--< ?php endforeach; ?>-->
</table>
<br />
<h4>Report date: <?= date('H:i:s m/d/Y'); ?></h4>