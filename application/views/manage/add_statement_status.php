<div id="content" class="clearfix">
	<div class="clearfix">
		&nbsp;
	</div>
	<?php echo form_open(base_url().'manage/insert_statement_status'); ?>
	<fieldset style="width: 300px;">
		<legend>
			Add an Statement Status
		</legend>
		<table>
			<tr>
				<td colspan="2">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="text" id="tbStatus" name="manage[<?= STATEMENT_STATUS?>]" value="<?= $this->session->flashdata(STATEMENT_STATUS) ?>" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td>
					<input class ="submit" onClick="history.go(-1)" type="button" value ="Cancel" style="width: 95%;">
				</td>
				<td>
					<input class="submit" type="submit" value="Save" style="width: 95%;">
				</td>
			</tr>
		</table>
	</fieldset>
	<?php echo form_close(); ?>
</div>
<script>
	$(document).ready(function()
	{
		jQuery(function($)
		{
			$("#tbStatus").Watermark("<?= JS_STATEMENT_STATUS ?>");
		});
	});
</script>


