<?
	$init = '';
	$ext1 = '';
	$ext2 = '';
	$inittype = '';
	$ext1type = '';
	$ext2type = '';
?>
<div id="content" class="clearfix">
	<div class="clearfix">
		&nbsp;
	</div>
	<?php echo form_open(base_url() . 'manage/insert_tax_form') ?>
	<fieldset style="width: 400px;">
		<legend>
			Add a Tax Form
		</legend>
		<div class="clearfix">
			&nbsp;
		</div>
		<table>
			<tr>
				<td>
					<label for="tform">
						<?= LABEL_TAX_FORM_ID ?>
					</label>
				</td>
				<td>
					<input id="tform" type="text" name="manager[<?= TAX_FORM ?>]" value="<?= $this->session->flashdata(TAX_FORM_ID) ?>" />
					<span class="errors">
						<?= $this->session->flashdata(ERROR_TAX_FORM_ID) ?>
					</span>
				</td>
			</tr>
			<tr>
				<td class="tdFilter" style="text-align: left;">
					<input name="manager[<?= TAX_FORM_INITIAL_DUE_DATE_TYPE_INITIAL_DATE ?>]" id="due" type="checkbox" <?= ($inittype == TAX_FORM_INITIAL_DUE_DATE_TYPE_INITIAL_DATE) ? 'checked' : '' ?> />
					<label for="due">
						<?= LABEL_TAX_FORM_INITIAL_DUE_DATE_ID ?>
					</label>
				</td>
				<td>
					<input type="text" id="duei" name="manager[<?= TAX_FORM_INITIAL_DUE_DATE ?>]" style="display: none" value="<?= $init; ?>" />
					<span class="errors">
						<?= $this->session->flashdata(ERROR_TAX_FORM_INITIAL_DUE_DATE) ?>
					</span>
				</td>
			</tr>
			<tr>
				<td class="tdFilter" style="text-align: left;">
					<input id="ex1" name="manager[<?= TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_1 ?>]" type="checkbox" <?= ($ext1type == TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_1) ? 'checked' : '' ?> />
					<label for="ex1">
						<?= LABEL_TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_1 ?>
					</label>
				</td>
				<td>
					<input id="ex1i" type="text"  style="display: none" name="manager[<?= TAX_FORM_EXTENTION_1 ?>]" value="<?= $ext1; ?>" />
					<span class="errors">
						<?= $this->session->flashdata(ERROR_TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_1) ?>
					</span>
				</td>
			</tr>
			<tr>
				<td class="tdFilter" style="text-align: left;">
					<input id="ex2"  type="checkbox" name="manager[<?= TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_2 ?>]" <?= ($ext2type == TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_2) ? 'checked' : '' ?>/>
					<label for="ex2">
						<?= LABEL_TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_2 ?>
					</label>
				</td>
				<td>
					<input id="ex2i" type="text"  style="display: none" name="manager[<?= TAX_FORM_EXTENTION_2 ?>]" value="<?= $ext2; ?>" />
					<span class="errors">
						<?= $this->session->flashdata(ERROR_TAX_FORM_INITIAL_DUE_DATE_TYPE_EXTENSION_2) ?>
					</span>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td>
					<input class ="submit" onClick="history.go(-1)" type="button" value ="Cancel" style="width: 95%;">
				</td>
				<td>
					<input class="submit" type="submit" value="Save" style="width: 95%;">
				</td>
			</tr>
		</table>
	</fieldset>
	<?php echo form_close(); ?>
<script>

	$(document).ready(function()
	{
		if($('#due').is(':checked'))
		{
			$("#duei").css("display","inherit");
		}
		if($("#ex1").is(':checked'))
		{
			$("#ex1i").css("display","inherit");
		}
		if($("#ex2").is(':checked'))
		{
			$("#ex2i").css("display","inherit");
		}
		$("#due").change(function ()
		{
			if(this.checked)
			{
				$("#duei").css("display","inherit");
			}
			else
			{
				$("#duei").css("display","none");
			}
		});
		$("#ex1").change(function ()
		{
			if(this.checked)
			{
				$("#ex1i").css("display","inherit");
			}
			else
			{
				$("#ex1i").css("display","none");
			}
		});
		$("#ex2").change(function ()
		{
			if(this.checked)
			{
				$("#ex2i").css("display","inherit");
			}
			else
			{
				$("#ex2i").css("display","none");
			}
		});
		jQuery(function($)
		{
			$("#ex1i").mask("99/99");
			$("#ex2i").mask("99/99");
			$("#duei").mask("99/99");
			$("#tform").Watermark("<?= JS_TAX_FORM_NAME ?>");
			$("#ex1i").Watermark("<?= JS_TAX_FORM_DATE_FORMAT ?>");
			$("#ex2i").Watermark("<?= JS_TAX_FORM_DATE_FORMAT ?>");
			$("#duei").Watermark("<?= JS_TAX_FORM_DATE_FORMAT ?>");
		});
	});
</script>

