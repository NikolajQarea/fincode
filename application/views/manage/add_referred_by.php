<div id="content" class="clearfix">
	<div class="clearfix">
		&nbsp;
	</div>
	<?php echo form_open(base_url() . 'manage/insert_referred_by'); ?>
	<fieldset style="width: 300px;">
		<legend>
			Add a Referred By
		</legend>
		<div class="clearfix">
			&nbsp;
		</div>
		<table style="width: 95%;">
			<tr>
				<td colspan="2">
					<input id="tbReferredBy" type="text" name="manager[<?= REFERRED_BY ?>]" value="" style="width: 95%;"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td>
					<input class ="submit" onClick="history.go(-1)" type="button" value ="Cancel" style="width: 95%;">
				</td>
				<td>
					<input class="submit" type="submit" value="Save" style="width: 95%;">
				</td>
			</tr>
		</table>
	</fieldset>
<?php echo form_close(); ?>
<script>
	$(document).ready(function()
	{
		jQuery(function($)
		{
			$("#tbReferredBy").Watermark("<?= JS_REFERRED_BY ?>");
		});
	});
</script>
