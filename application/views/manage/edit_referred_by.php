<div id="content" class="clearfix">
	<div class="clearfix">
		&nbsp;
	</div>
	<?php echo form_open(base_url() . 'manage/update_referred_by/' . $data[0][REFERRED_BY_ID]); ?>
	<fieldset style="width: 300px;">
		<legend>
			Edit Referred By
		</legend>
		<div class="manage_delete">
			<?= $delete ? 
				'<span style="color: #999999;">[In Use]</span> <img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/no_delete.png" style="height: 10px;" />' : 
				'<a href="' . $this->config->item('base_url') . '/manage/delete_referred_by/' . $data[0][REFERRED_BY_ID]. '" style="color: #990000;" title="Delete this Referred By record">[Delete]<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/delete.png" style="height: 10px;" /></a>' 
			?>
		</div>
		<table style="width: 95%;">
			<tr>
				<td colspan="2">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input id="tbReferredBy" type="text" name="manager[<?= REFERRED_BY ?>]" style="width: 95%;" value="<?= $data[0][REFERRED_BY]; ?>" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td>
					<input class ="submit" onClick="history.go(-1)" type="button" value ="Cancel" style="width: 95%;">
				</td>
				<td>
					<input class="submit" type="submit" value="Save" style="width: 95%;">
				</td>
			</tr>
		</table> 
 <?php echo form_close(); ?>
 <script>
	$(document).ready(function()
	{
		jQuery(function($)
		{
			$("#tbReferredBy").Watermark(<?= JS_REFERRED_BY ?>);
		});
	});
</script>


