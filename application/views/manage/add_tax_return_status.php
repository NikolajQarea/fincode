<div id="content" class="clearfix">
	<div class="clearfix">
		&nbsp;
	</div>
	<?php echo form_open(base_url() . 'manage/insert_tax_return_status'); ?>
	<fieldset style="width: 300px;">
		<legend>
			Add a Tax Return Status
		</legend>
		<div class="clearfix">
			&nbsp;
		</div>
		<table style="width: 95%">
			<tr>
				<td colspan="2">
					<input id="tbTaxReturnStatus" type="text" name="manager[<?= TAX_RETURN_STATUS ?>]" style="width: 95%" value="" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td>
					<input class ="submit" onClick="history.go(-1)" type="button" value ="Cancel" style="width: 95%;">
				</td>
				<td>
					<input class="submit" type="submit" value="Save" style="width: 95%;">
				</td>
			</tr>
		</table>
	</fieldset>
</div>
<?php echo form_close(); ?>
<script>
	$(document).ready(function()
	{
		jQuery(function($)
		{
			$("#tbTaxReturnStatus").Watermark("<?= JS_TAX_RETURN_STATUS ?>");
		});
	});
</script>
