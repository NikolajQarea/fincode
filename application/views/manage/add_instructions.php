<div id="content" class="clearfix">
	<div class="clearfix">
		&nbsp;
	</div>
	<?php echo form_open(base_url() . 'manage/insert_instructions'); ?>
	<fieldset style="width: 300px;">
		<legend>
			Add an Instruction
		</legend>
		<div class="clearfix">
			&nbsp;
		</div>
		<table style="width: 95%;">
			<tr>
				<td colspan="2">
					<input id="tbInstruction" type="text" name="manage[<?= INSTRUCTIONS ?>]" style="width: 95%;" value="" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td>
					<input class ="submit" onClick="history.go(-1)" type="button" value ="Cancel" style="width: 95%;">
				</td>
				<td>
					<input class="submit" type="submit" value="Save" style="width: 95%;">
				</td>
			</tr>
		</table>
	</fieldset>
	<?php echo form_close(); ?>
</div>
<script>
	$(document).ready(function()
	{
		jQuery(function($)
		{
			$("#tbInstruction").Watermark("<?= JS_INSTRUCTIONS ?>");
		});
	});
</script>

