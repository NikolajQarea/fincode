<div id="content" class="clearfix" style="margin: 5px auto 5px auto; width: 800px;">
	<div class="clearfix"></div>
	<fieldset style=" margin: auto; width: 750px;">
		<legend>
			List Management
		</legend>
		<div style="width: 50%; display: inline; float: left;">
			<select class="input" name="manage[view]" id="viewselect" OnChange="window.location='<?= base_url() ?>manage/view/' + document.getElementById('viewselect').options[selectedIndex].value">
				<option value="0"<?= $this->session->flashdata('cat') == 0 ? ' selected="select" ' : '' ?>>Please Select</option>
				<option value="<?= CATEGORY_CLIENT_TYPE ?>"<?= $Category_ID == CATEGORY_CLIENT_TYPE ? ' selected="select" ' : '' ?>>Client Type</option>
				<option value="<?= CATEGORY_INSTRUCTIONS ?>"<?= $Category_ID == CATEGORY_INSTRUCTIONS ? ' selected="select" ' : '' ?>>Instructions</option>
				<option value="<?= CATEGORY_OCCUPATIONS ?>"<?= $Category_ID == CATEGORY_OCCUPATIONS ? ' selected="select" ' : '' ?>>Occupation</option>
				<option value="<?= CATEGORY_REFERRED_BY ?>"<?= $Category_ID == CATEGORY_REFERRED_BY ? ' selected="select" ' : '' ?>>Referred By</option>
				<option value="<?= CATEGORY_RETAINER_STATUS ?>"<?= $Category_ID == CATEGORY_RETAINER_STATUS ? ' selected="select" ' : '' ?>>Retainer Status</option>
				<option value="<?= CATEGORY_STATEMENT_STATUS ?>"<?= $Category_ID == CATEGORY_STATEMENT_STATUS ? ' selected="select" ' : '' ?>>Statement Status</option>
				<option value="<?= CATEGORY_STATEMENT_TYPE ?>"<?= $Category_ID == CATEGORY_STATEMENT_TYPE ? ' selected="select" ' : '' ?>>Statement Type</option>
				<option value="<?= CATEGORY_TAX_FORMS ?>"<?= $Category_ID == CATEGORY_TAX_FORMS ? ' selected="select" ' : '' ?>>Tax Form</option>
				<option value="<?= CATEGORY_TAX_RETURN_STATUS ?>"<?= $Category_ID == CATEGORY_TAX_RETURN_STATUS ? ' selected="select" ' : '' ?>>Tax Return Status</option>
			</select>
		</div>
		<?php if($Category_ID == CATEGORY_OCCUPATIONS) : ?>
		<?php echo form_open(base_url() . 'manage/add_occupation') ?>
		<div style="width: 50%; display: inline; float: right;">
			<input name="add_occupation" class ="button add_occupation" type="button" style="float: right;" value ="Add Occupation" margin: 10px;" />
		</div>
		<table style="border: none; width: auto;" id="view_occupations">
			<tr>
				<td style="vertical-align: middle;">
					<fieldset style="width: 100%; margin: auto;">
						<legend >
							Manage Occupation
						</legend>
						<div class="clearfix">
							&nbsp;
						</div>
						<table class="manage view">
							<tr>
								<th style="vertical-align: middle; width: 90%;">
									Occupation
								</th>
								<th>
									Times Used
								</th>
							</tr>
							<?php foreach ($Occupations as $v) : ?>
							<tr class="clickRow" ondblclick="window.location='<?= base_url() ?>manage/edit_occupation/<?= $v[OCCUPATION_ID] ?>';">
								<td>
									<? print($v[OCCUPATION]); ?>
								</td>
								<td>
								<?= $v['Status'] > 0 ? 
									$v['Status'] . '<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/no_delete.png" style="height: 10px;" />' : 
									'<span style="color: #990000;">' . $v['Status'] . '</span> 
									<a href="' . $this->config->item('base_url') . '/manage/delete_occupation/' . $v[OCCUPATION_ID]. '" style="color: #990000; float: right;" title="Delete this Occupation">
									<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/delete.png" style="height: 10px;" />
									</a>' 
								?>
								</td>
							</tr>
						    <?php endforeach; ?>
						</table>
					</fieldset>
				</td>
			</tr>
		</table>
		<?php echo form_close(); ?>
		<?php elseif($Category_ID == CATEGORY_CLIENT_TYPE) : ?>
		<?php echo form_open(base_url() . 'manage/add_client_type'); ?>
		<div style="width: 50%; display: inline; float: right;">
			<input name="add_client_type" class ="button add_client_type" type="button" value ="Add Client Type" style="float: right; margin: 10px;" />
		</div>
		<table style="border: none; width: auto;" id="view_client_types">
			<tr>
				<td>
					<fieldset style="width: 100%; margin: auto;">
						<legend >
							Manage Client Type
						</legend>
						<div class="clearfix">
							&nbsp;
						</div>
						<table class="view" style="border: none; width: 525px;">
							<tr>
								<th style="vertical-align: middle; width: 90%;">
									Client Type
								</th>
								<th>
									Times Used
								</th>
							</tr>
							<?php foreach ($Client_Types as $v) : ?>
							<tr class="clickRow" ondblclick="window.location='<?= base_url() ?>manage/edit_client_type/<?= $v[CLIENT_TYPE_ID] ?>';">
								<td>
									<? print(stripslashes(trim($v[CLIENT_TYPE]))); ?>
								</td>
								<td>
								<?= $v['Status'] > 0 ? 
									$v['Status'] . '<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/no_delete.png" style="height: 10px;" />' : 
									'<span style="color: #990000;">' . $v['Status'] . '</span> 
										<a href="' . $this->config->item('base_url') . '/manage/delete_client_type/' . $v[CLIENT_TYPE_ID]. '" style="color: #990000; float: right;" title="Delete this Client Type">
											<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/delete.png" style="height: 10px;" />
										</a>' 
								?>
								</td>
							</tr>
						    <?php endforeach; ?>
						</table>
					</fieldset>
				</td>
			</tr>
		</table>
		<?php echo form_close(); ?>

		<?php elseif($Category_ID == CATEGORY_INSTRUCTIONS) : ?>
		<?php echo form_open(base_url() . 'manage/add_instructions'); ?>
		<div style="width: 50%; display: inline; float: right;">
			<input name="add_instruction" class ="button add_instruction" type="button" value ="Add Instruction" style="float: right; margin: 10px;" />
		</div>
		<table style="border: none; width: auto;" id="view_instructions">
			<tr>
				<td>
					<fieldset style="width: 100%; margin: auto;">
						<legend >
							Manage Instructions
						</legend>
						<div class="clearfix">
							&nbsp;
						</div>
						<table class="view" style="border: none; width: 525px;">
							<tr>
								<th style="vertical-align: middle; width: 90%;">
									Instructions
								</th>
								<th>
									Times Used
								</th>
							</tr>
							<?php foreach ($Instructions as $v) : ?>
							<tr class="clickRow" ondblclick="window.location='<?= base_url() ?>manage/edit_instructions/<?= $v[INSTRUCTIONS_ID] ?>';">
								<td>
									<? print(stripslashes(trim($v[INSTRUCTIONS]))); ?>
								</td>
								<td>
								<?= $v['Status'] > 0 ? 
									$v['Status'] . '<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/no_delete.png" style="height: 10px;" />' : 
									'<span style="color: #990000;">' . $v['Status'] . '</span> 
										<a href="' . $this->config->item('base_url') . '/manage/delete_instructions/' . $v[INSTRUCTIONS_ID]. '" style="color: #990000; float: right;" title="Delete this Instruction">
											<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/delete.png" style="height: 10px;" />
										</a>' 
								?>
								</td>
							</tr>
						    <?php endforeach; ?>
						</table>
					</fieldset>
				</td>
			</tr>
		</table>
		<?php echo form_close(); ?>

		<?php elseif($Category_ID == CATEGORY_REFERRED_BY) : ?>
		<?php echo form_open(base_url() . 'manage/add_referred_by'); ?>
		<div style="width: 50%; display: inline; float: right;">
			<input name="add_referred_by" class ="button add_referred_by" type="button" value ="Add Referred by" style="float: right; margin: 10px;" />
		</div>
		<table style="border: none; width: auto;" id="view_referred_bys">
			<tr>
				<td>
					<fieldset style="width: 100%; margin: auto;">
						<legend >
							Manage Referred By
						</legend>
						<div class="clearfix">
							&nbsp;
						</div>
						<table class="view" style="border: none; width: 525px;">
							<tr>
								<th style="vertical-align: middle; width: 90%;">
									Referred By
								</th>
								<th>
									Times Used
								</th>
							</tr>
							<?php foreach ($Referred_Bys as $v) : ?>
							<tr class="clickRow" ondblclick="window.location='<?= base_url() ?>manage/edit_referred_by/<?= $v[REFERRED_BY_ID] ?>';">
								<td>
									<? print($v[REFERRED_BY]); ?>
								</td>
								<td>
								<?= $v['Status'] > 0 ? 
									$v['Status'] . '<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/no_delete.png" style="height: 10px;" />' : 
									'<span style="color: #990000;">' . $v['Status'] . '</span> 
										<a href="' . $this->config->item('base_url') . '/manage/delete_referred_by/' . $v[REFERRED_BY_ID]. '" style="color: #990000; float: right;" title="Delete this Referred By record">
											<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/delete.png" style="height: 10px;" />
										</a>' 
								?>
								</td>
							</tr>
						    <?php endforeach; ?>
						</table>
					</fieldset>
				</td>
			</tr>
		</table>
		<?php echo form_close(); ?>
		<?php elseif($Category_ID == CATEGORY_TAX_FORMS) : ?>
		<!-- Begin table Tax Forms -->
		<?php echo form_open(base_url() . 'manage/add_tax_forms'); ?>
		<div style="width: 50%; display: inline; float: right;">
			<input name="add_tax_form" class ="button add_tax_form" type="button" value ="Add Tax Form" style="float: right; margin: 10px;" />
		</div>
		<table style="border: none; width: auto;" id="view_tax_forms">
			<tr>
				<td>
					<fieldset style="width: 100%; margin: auto;">
						<legend >
							Manage Tax Form
						</legend>
						<div class="clearfix">
							&nbsp;
						</div>
						<table class="view" style="border: none; width: 525px;">
							<tr>
								<th style="vertical-align: middle; width: 90%;">
									Tax Form
								</th>
								<th>
									Times Used
								</th>
							</tr>
							<?php foreach ($Tax_Forms as $v) : ?>
							<tr class="clickRow" ondblclick="window.location='<?= base_url() ?>manage/edit_tax_form/<?= $v[TAX_FORM_ID] ?>';">
								<td>
									<? print($v['Tax_Form_Name']); ?>
								</td>
								<td>
								<?= $v['Status'] > 0 ? 
									$v['Status'] . '<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/no_delete.png" style="height: 10px;" />' : 
									'<span style="color: #990000;">' . $v['Status'] . '</span> 
										<a href="' . $this->config->item('base_url') . '/manage/delete_tax_form/' . $v[TAX_FORM_ID]. '" style="color: #990000; float: right;" title="Delete this Tax Form">
											<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/delete.png" style="height: 10px;" />
										</a>' 
								?>
								</td>
							</tr>
						    <?php endforeach; ?>
						</table>
					</fieldset>
				</td>
			</tr>
		</table>
		<?php echo form_close(); ?>
		<?php elseif($Category_ID == CATEGORY_TAX_RETURN_STATUS) : ?>
		<!-- Begin table Tax Return Status  -->
		<?php echo form_open(base_url() . 'manage/add_tax_return_status'); ?>
		<div style="width: 50%; display: inline; float: right;">
			<input name="add_tax_return_status" class ="button add_tax_return_status" type="button" value ="Add Tax Return Status" style="float: right; margin: 10px;" />
		</div>
		<table style="border: none; width: auto;" id="view_tax_return_statuses">
			<tr>
				<td>
					<fieldset style="width: 100%; margin: auto;">
						<legend >
							Manage Tax Return Status
						</legend>
						<div class="clearfix">
							&nbsp;
						</div>
						<table class="view" style="border: none; width: 525px;">
							<tr>
								<th style="vertical-align: middle; width: 90%;">
									Tax Return Status
								</th>
								<th>
									Times Used
								</th>
							</tr>
							<?php foreach ($Tax_Return_Statuses as $v) : ?>
							<tr class="clickRow" ondblclick="window.location='<?= base_url() ?>manage/edit_tax_return_status/<?= $v[TAX_RETURN_STATUS_ID] ?>';">
								<td>
									<? print($v[TAX_RETURN_STATUS]); ?>
								</td>
								<td>
								<?= $v['Status'] > 0 ? 
									$v['Status'] . '<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/no_delete.png" style="height: 10px;" />' : 
									'<span style="color: #990000;">' . $v['Status'] . '</span> 
										<a href="' . $this->config->item('base_url') . '/manage/delete_tax_return_status/' . $v[TAX_RETURN_STATUS_ID]. '" style="color: #990000; float: right;" title="Delete this Tax Return Status">
											<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/delete.png" style="height: 10px;" />
										</a>' 
								?>
								</td>
							</tr>
						    <?php endforeach; ?>
						</table>
					</fieldset>
				</td>
			</tr>
		</table>
		<?php echo form_close(); ?>
		<?php elseif($Category_ID == CATEGORY_RETAINER_STATUS) : ?>
		<!-- Begin table Retainer Status  -->
		<?php echo form_open(base_url() . 'manage/add_return_status'); ?>
		<div style="width: 50%; display: inline; float: right;">
			<input name="add_retainer_status" class ="button add_retainer_status" type="button" value ="Add Retainer Status" style="float: right; margin: 10px;" />
		</div>
		<table style="border: none; width: auto;" id="view_retainer_statuses">
			<tr>
				<td>
					<fieldset style="width: 100%; margin: auto;">
						<legend >
							Manage Retainer Status
						</legend>
						<div class="clearfix">
							&nbsp;
						</div>
						<table class="view" style="border: none; width: 525px;">
							<tr>
								<th style="vertical-align: middle; width: 90%;">
									Retainer Status
								</th>
								<th>
									Times Used
								</th>
							</tr>
							<?php foreach ($Retainer_Statuses as $v) : ?>
							<tr class="clickRow" ondblclick="window.location='<?= base_url() ?>manage/edit_retainer_status/<?= $v[RETAINER_STATUS_ID] ?>';">
								<td>
									<? print($v[RETAINER_STATUS]); ?>
								</td>
								<td>
								<?= $v['Status'] > 0 ? 
									$v['Status'] . '<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/no_delete.png" style="height: 10px;" />' : 
									'<span style="color: #990000;">' . $v['Status'] . '</span> 
										<a href="' . $this->config->item('base_url') . '/manage/delete_retainer_status/' . $v[RETAINER_STATUS_ID]. '" style="color: #990000; float: right;" title="Delete this Retainer Status">
											<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/delete.png" style="height: 10px;" />
										</a>' 
								?>
								</td>
							</tr>
						    <?php endforeach; ?>
						</table>
					</fieldset>
				</td>
			</tr>
		</table>
		<?php echo form_close(); ?>
		<?php elseif($Category_ID == CATEGORY_STATEMENT_STATUS) : ?>
		<!-- Begin table Statement Status  -->
		<?php echo form_open(base_url() . 'manage/add_statement_status'); ?>
		<div style="width: 50%; display: inline; float: right;">
			<input name="add_statement_status" class ="button add_statement_status" type="button" value ="Add Statement Status" style="float: right; margin: 10px;" />
		</div>
		<table style="border: none; width: auto;" id="view_statement_statuses">
			<tr>
				<td>
					<fieldset style="width: 100%; margin: auto;">
						<legend >
							Manage Statement Status
						</legend>
						<div class="clearfix">
							&nbsp;
						</div>
						<table class="view" style="border: none; width: 525px;">
							<tr>
								<th style="vertical-align: middle; width: 90%;">
									Statement Status
								</th>
								<th>
									Times Used
								</th>
							</tr>
							<?php foreach ($Statement_Statuses as $v) : ?>
							<tr class="clickRow" ondblclick="window.location='<?= base_url() ?>manage/edit_statement_status/<?= $v[STATEMENT_STATUS_ID] ?>';">
								<td>
									<? print($v[STATEMENT_STATUS]); ?>
								</td>
								<td>
								<?= $v['Status'] > 0 ? 
									$v['Status'] . '<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/no_delete.png" style="height: 10px;" />' : 
									'<span style="color: #990000;">' . $v['Status'] . '</span> 
										<a href="' . $this->config->item('base_url') . '/manage/delete_statement_status/' . $v[STATEMENT_STATUS_ID]. '" style="color: #990000; float: right;" title="Delete this Statement Status">
											<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/delete.png" style="height: 10px;" />
										</a>' 
								?>
								</td>
							</tr>
						    <?php endforeach; ?>
						</table>
					</fieldset>
				</td>
			</tr>
		</table>
		<?php echo form_close(); ?>
		<?php elseif($Category_ID == CATEGORY_STATEMENT_TYPE) : ?>
		<!-- Begin table Statement Type  -->
		<?php echo form_open(base_url() . 'manage/add_statement_type'); ?>
		<div style="width: 50%; display: inline; float: right;">
			<input name="add_statement_type" class ="button add_statement_type" type="button" value ="Add Statement Type" style="float: right; margin: 10px;" />
		</div>
		<table style="border: none; width: auto;" id="view_statement_types">
			<tr>
				<td>
					<fieldset style="width: 100%; margin: auto;">
						<legend >
							Manage Statement Type
						</legend>
						<div class="clearfix">
							&nbsp;
						</div>
						<table class="view" style="border: none; width: 525px;">
							<tr>
								<th style="vertical-align: middle; width: 90%;">
									Statement Type
								</th>
								<th>
									Times Used
								</th>
							</tr>
							<?php foreach ($Statement_Types as $v) : ?>
							<tr class="clickRow" ondblclick="window.location='<?= base_url() ?>manage/edit_statement_type/<?= $v[STATEMENT_TYPE_ID] ?>';">
								<td>
									<? print($v[STATEMENT_TYPE]); ?>
								</td>
								<td>
								<?= $v['Status'] > 0 ? 
									$v['Status'] . '<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/no_delete.png" style="height: 10px;" />' : 
									'<span style="color: #990000;">' . $v['Status'] . '</span> 
										<a href="' . $this->config->item('base_url') . '/manage/delete_statement_type/' . $v[STATEMENT_TYPE_ID]. '" style="color: #990000; float: right;" title="Delete this Statement Type">
											<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/delete.png" style="height: 10px;" />
										</a>' 
								?>
								</td>
							</tr>
						    <?php endforeach; ?>
						</table>
					</fieldset>
				</td>
			</tr>
		</table>
		<?php echo form_close(); ?>
		<?php endif; ?>
	</fieldset>
</div>