<div id="content" class="clearfix">
	<div class="clearfix">
		&nbsp;
	</div>
	<?php echo form_open(base_url() . 'manage/update_statement_type/' . $data[0][STATEMENT_TYPE_ID]); ?>
	<fieldset style="width: 300px;">
		<legend>
			Edit Statement Type
		</legend>
		<div class="manage_delete">
			<?= $delete ? 
				'<span style="color: #999999;">[In Use]</span> <img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/no_delete.png" style="height: 10px;" />' : 
				'<a href="' . $this->config->item('base_url') . '/manage/delete_statement_type/' . $data[0][STATEMENT_TYPE_ID]. '" style="color: #990000;" title="Delete this Statement Type">[Delete]<img align="right" src="' . $this->config->item('base_url') . 'application/views/css/images/delete.png" style="height: 10px;" /></a>' 
			?>
		</div>
		<div class="clearfix"></div>
		<table style="width: 95%;">
			<tr>
				<td colspan="2">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="text" id="tbStatementType" name="manage[<?= STATEMENT_TYPE ?>]" value="<?= $data[0][STATEMENT_TYPE]; ?>" style="width: 99%;" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td>
					<input class ="submit" onClick="history.go(-1)" type="button" value ="Cancel" style="width: 95%;">
				</td>
				<td>
					<input class="submit" type="submit" value="Save" style="width: 95%;">
				</td>
			</tr>
		</table>
	</fieldset>
	<?php echo form_close(); ?>
</div>
<script>
	$(document).ready(function()
	{
		jQuery(function($)
		{
			$("#tbStatementType").Watermark("<?= JS_STATEMENT_TYPE ?>");
		});
	});
</script>
