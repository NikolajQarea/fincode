-- phpMyAdmin SQL Dump
-- version 3.2.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 23, 2011 at 12:02 PM
-- Server version: 5.1.40
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `ggg`
--

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `id_client` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `input_date` int(11) NOT NULL,
  `last_update_date` int(11) NOT NULL,
  `entered_by` int(11) NOT NULL,
  `last_update_user` int(11) NOT NULL,
  `last_spouse` varchar(100) NOT NULL,
  `first_spouse` varchar(100) NOT NULL,
  `form_id` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `staff` varchar(100) NOT NULL,
  `entity` varchar(100) NOT NULL,
  `partner` enum('MG','RG','MJG') NOT NULL,
  `billing_client` varchar(100) NOT NULL,
  `doc_id` varchar(255) NOT NULL,
  `month` varchar(2) NOT NULL,
  `initial_year` int(4) NOT NULL,
  `retainer_year` int(4) NOT NULL,
  `retainer` varchar(100) NOT NULL,
  `accounting` varchar(100) NOT NULL,
  `principal` varchar(100) NOT NULL,
  `ssn_ein` varchar(11) NOT NULL,
  `ssn_spouse` varchar(11) NOT NULL,
  `id_referred_by` int(11) NOT NULL,
  `id_occupation` int(11) NOT NULL,
  `id_occupation_spouse` int(11) NOT NULL,
  `inactive_date` int(11) NOT NULL,
  `notes` text NOT NULL,
  `removed` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_client`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=125 ;

--
-- Dumping data for table `client`
--



--
-- Table structure for table `form`
--

DROP TABLE IF EXISTS `form`;
CREATE TABLE IF NOT EXISTS `form` (
  `form_id` int(11) NOT NULL AUTO_INCREMENT,
  `form_name` varchar(50) NOT NULL,
  `initial_due_date` tinyint(1) NOT NULL,
  `extension1` tinyint(1) NOT NULL,
  `extension2` tinyint(1) NOT NULL,
  PRIMARY KEY (`form_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=151 ;

--
-- Dumping data for table `form`
--

INSERT INTO `form` (`form_id`, `form_name`, `initial_due_date`, `extension1`, `extension2`) VALUES
(1, '990', 1, 1, 1),
(2, '1041', 1, 1, 0),
(3, '1040', 1, 1, 0),
(4, '1065', 1, 1, 0),
(5, '1120', 1, 1, 0),
(6, '1120S', 1, 1, 0),
(7, '941', 1, 0, 0),
(8, '709', 1, 1, 0),
(9, '940', 1, 0, 0),
(10, '942', 1, 0, 0),
(11, '1098', 0, 1, 0),
(12, '1099', 0, 1, 0),
(13, '1040NR', 1, 1, 0),
(14, '5500', 1, 1, 0),
(15, '8027', 0, 1, 0),
(16, '990PF', 1, 0, 0),
(17, 'IT-201', 1, 1, 0),
(18, 'IT-204LL', 1, 0, 0),
(19, 'IT-205', 1, 1, 0),
(20, 'NYCCRA', 1, 0, 0),
(21, 'NYS45', 1, 0, 0),
(22, 'ST100', 1, 0, 0),
(23, 'ST101', 1, 0, 0),
(24, 'ST809', 1, 0, 0),
(25, 'ST810', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `form_initial_date`
--

DROP TABLE IF EXISTS `form_initial_date`;
CREATE TABLE IF NOT EXISTS `form_initial_date` (
  `form_id` int(11) NOT NULL,
  `initial_date` text NOT NULL,
  `type` enum('init','ext1','ext2') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `form_initial_date`
--

INSERT INTO `form_initial_date` (`form_id`, `initial_date`, `type`) VALUES
(1, '5/15', 'init'),
(1, '8/15', 'ext1'),
(1, '11/15', 'ext2'),
(2, '4/15', 'init'),
(2, '9/15', 'ext1'),
(3, '4/15', 'init'),
(3, '10/15', 'ext1'),
(4, '4/15', 'init'),
(4, '9/15', 'ext1'),
(5, '3/15', 'init'),
(5, '9/15', 'ext1'),
(6, '3/15', 'init'),
(6, '9/15', 'ext1'),
(7, '4/30', 'init'),
(7, '7/31', 'init'),
(7, '10/31', 'init'),
(7, '1/31', 'init'),
(8, '4/15', 'init'),
(8, '10/15', 'ext1'),
(9, '1/15', 'init'),
(10, '1/31', 'init'),
(10, '4/31', 'init'),
(10, '7/31', 'init'),
(10, '10/31', 'init'),
(11, '2/28', 'ext1'),
(11, '3/31', 'ext1'),
(12, '2/28', 'ext1'),
(12, '3/31', 'ext1'),
(13, '4/15', 'init'),
(13, '10/15', 'ext1'),
(14, '7/15', 'init'),
(14, '10/15', 'ext1'),
(15, '2/28', 'ext1'),
(15, '3/31', 'ext1'),
(17, '4/15', 'init'),
(17, '10/15', 'ext1'),
(18, '1/31', 'init'),
(19, '7/15', 'init'),
(19, '10/15', 'ext1'),
(20, '6/20', 'init'),
(21, '1/31', 'init'),
(21, '4/31', 'init'),
(21, '7/31', 'init'),
(21, '10/31', 'init'),
(22, '3/20', 'init'),
(22, '6/20', 'init'),
(22, '9/20', 'init'),
(22, '12/20', 'init'),
(23, '3/20', 'init'),
(24, '1/20', 'init'),
(24, '2/20', 'init'),
(24, '3/20', 'init'),
(24, '4/20', 'init'),
(24, '5/20', 'init'),
(24, '6/20', 'init'),
(24, '7/20', 'init'),
(24, '8/20', 'init'),
(24, '9/20', 'init'),
(24, '10/20', 'init'),
(24, '11/20', 'init'),
(24, '12/20', 'init'),
(25, '3/20', 'init'),
(25, '6/20', 'init'),
(25, '9/20', 'init'),
(25, '12/20', 'init'),
(1, '5/15', 'init'),
(1, '8/15', 'ext1'),
(1, '11/15', 'ext2'),
(2, '4/15', 'init'),
(2, '9/15', 'ext1'),
(3, '4/15', 'init'),
(3, '10/15', 'ext1'),
(4, '4/15', 'init'),
(4, '9/15', 'ext1'),
(5, '3/15', 'init'),
(5, '9/15', 'ext1'),
(6, '3/15', 'init'),
(6, '9/15', 'ext1'),
(7, '4/30', 'init'),
(7, '7/31', 'init'),
(7, '10/31', 'init'),
(7, '1/31', 'init'),
(8, '4/15', 'init'),
(8, '10/15', 'ext1'),
(9, '1/15', 'init'),
(10, '1/31', 'init'),
(10, '4/31', 'init'),
(10, '7/31', 'init'),
(10, '10/31', 'init'),
(11, '2/28', 'ext1'),
(11, '3/31', 'ext1'),
(12, '2/28', 'ext1'),
(12, '3/31', 'ext1'),
(13, '4/15', 'init'),
(13, '10/15', 'ext1'),
(14, '7/15', 'init'),
(14, '10/15', 'ext1'),
(15, '2/28', 'ext1'),
(15, '3/31', 'ext1'),
(17, '4/15', 'init'),
(17, '10/15', 'ext1'),
(18, '1/31', 'init'),
(19, '7/15', 'init'),
(19, '10/15', 'ext1'),
(20, '6/20', 'init'),
(21, '1/31', 'init'),
(21, '4/31', 'init'),
(21, '7/31', 'init'),
(21, '10/31', 'init'),
(22, '3/20', 'init'),
(22, '6/20', 'init'),
(22, '9/20', 'init'),
(22, '12/20', 'init'),
(23, '3/20', 'init'),
(24, '1/20', 'init'),
(24, '2/20', 'init'),
(24, '3/20', 'init'),
(24, '4/20', 'init'),
(24, '5/20', 'init'),
(24, '6/20', 'init'),
(24, '7/20', 'init'),
(24, '8/20', 'init'),
(24, '9/20', 'init'),
(24, '10/20', 'init'),
(24, '11/20', 'init'),
(24, '12/20', 'init'),
(25, '3/20', 'init'),
(25, '6/20', 'init'),
(25, '9/20', 'init'),
(25, '12/20', 'init'),
(1, '5/15', 'init'),
(1, '8/15', 'ext1'),
(1, '11/15', 'ext2'),
(2, '4/15', 'init'),
(2, '9/15', 'ext1'),
(3, '4/15', 'init'),
(3, '10/15', 'ext1'),
(4, '4/15', 'init'),
(4, '9/15', 'ext1'),
(5, '3/15', 'init'),
(5, '9/15', 'ext1'),
(6, '3/15', 'init'),
(6, '9/15', 'ext1'),
(7, '4/30', 'init'),
(7, '7/31', 'init'),
(7, '10/31', 'init'),
(7, '1/31', 'init'),
(8, '4/15', 'init'),
(8, '10/15', 'ext1'),
(9, '1/15', 'init'),
(10, '1/31', 'init'),
(10, '4/31', 'init'),
(10, '7/31', 'init'),
(10, '10/31', 'init'),
(11, '2/28', 'ext1'),
(11, '3/31', 'ext1'),
(12, '2/28', 'ext1'),
(12, '3/31', 'ext1'),
(13, '4/15', 'init'),
(13, '10/15', 'ext1'),
(14, '7/15', 'init'),
(14, '10/15', 'ext1'),
(15, '2/28', 'ext1'),
(15, '3/31', 'ext1'),
(17, '4/15', 'init'),
(17, '10/15', 'ext1'),
(18, '1/31', 'init'),
(19, '7/15', 'init'),
(19, '10/15', 'ext1'),
(20, '6/20', 'init'),
(21, '1/31', 'init'),
(21, '4/31', 'init'),
(21, '7/31', 'init'),
(21, '10/31', 'init'),
(22, '3/20', 'init'),
(22, '6/20', 'init'),
(22, '9/20', 'init'),
(22, '12/20', 'init'),
(23, '3/20', 'init'),
(24, '1/20', 'init'),
(24, '2/20', 'init'),
(24, '3/20', 'init'),
(24, '4/20', 'init'),
(24, '5/20', 'init'),
(24, '6/20', 'init'),
(24, '7/20', 'init'),
(24, '8/20', 'init'),
(24, '9/20', 'init'),
(24, '10/20', 'init'),
(24, '11/20', 'init'),
(24, '12/20', 'init'),
(25, '3/20', 'init'),
(25, '6/20', 'init'),
(25, '9/20', 'init'),
(25, '12/20', 'init'),
(1, '5/15', 'init'),
(1, '8/15', 'ext1'),
(1, '11/15', 'ext2'),
(2, '4/15', 'init'),
(2, '9/15', 'ext1'),
(3, '4/15', 'init'),
(3, '10/15', 'ext1'),
(4, '4/15', 'init'),
(4, '9/15', 'ext1'),
(5, '3/15', 'init'),
(5, '9/15', 'ext1'),
(6, '3/15', 'init'),
(6, '9/15', 'ext1'),
(7, '4/30', 'init'),
(7, '7/31', 'init'),
(7, '10/31', 'init'),
(7, '1/31', 'init'),
(8, '4/15', 'init'),
(8, '10/15', 'ext1'),
(9, '1/15', 'init'),
(10, '1/31', 'init'),
(10, '4/31', 'init'),
(10, '7/31', 'init'),
(10, '10/31', 'init'),
(11, '2/28', 'ext1'),
(11, '3/31', 'ext1'),
(12, '2/28', 'ext1'),
(12, '3/31', 'ext1'),
(13, '4/15', 'init'),
(13, '10/15', 'ext1'),
(14, '7/15', 'init'),
(14, '10/15', 'ext1'),
(15, '2/28', 'ext1'),
(15, '3/31', 'ext1'),
(17, '4/15', 'init'),
(17, '10/15', 'ext1'),
(18, '1/31', 'init'),
(19, '7/15', 'init'),
(19, '10/15', 'ext1'),
(20, '6/20', 'init'),
(21, '1/31', 'init'),
(21, '4/31', 'init'),
(21, '7/31', 'init'),
(21, '10/31', 'init'),
(22, '3/20', 'init'),
(22, '6/20', 'init'),
(22, '9/20', 'init'),
(22, '12/20', 'init'),
(23, '3/20', 'init'),
(24, '1/20', 'init'),
(24, '2/20', 'init'),
(24, '3/20', 'init'),
(24, '4/20', 'init'),
(24, '5/20', 'init'),
(24, '6/20', 'init'),
(24, '7/20', 'init'),
(24, '8/20', 'init'),
(24, '9/20', 'init'),
(24, '10/20', 'init'),
(24, '11/20', 'init'),
(24, '12/20', 'init'),
(25, '3/20', 'init'),
(25, '6/20', 'init'),
(25, '9/20', 'init'),
(25, '12/20', 'init'),
(1, '5/15', 'init'),
(1, '8/15', 'ext1'),
(1, '11/15', 'ext2'),
(2, '4/15', 'init'),
(2, '9/15', 'ext1'),
(3, '4/15', 'init'),
(3, '10/15', 'ext1'),
(4, '4/15', 'init'),
(4, '9/15', 'ext1'),
(5, '3/15', 'init'),
(5, '9/15', 'ext1'),
(6, '3/15', 'init'),
(6, '9/15', 'ext1'),
(7, '4/30', 'init'),
(7, '7/31', 'init'),
(7, '10/31', 'init'),
(7, '1/31', 'init'),
(8, '4/15', 'init'),
(8, '10/15', 'ext1'),
(9, '1/15', 'init'),
(10, '1/31', 'init'),
(10, '4/31', 'init'),
(10, '7/31', 'init'),
(10, '10/31', 'init'),
(11, '2/28', 'ext1'),
(11, '3/31', 'ext1'),
(12, '2/28', 'ext1'),
(12, '3/31', 'ext1'),
(13, '4/15', 'init'),
(13, '10/15', 'ext1'),
(14, '7/15', 'init'),
(14, '10/15', 'ext1'),
(15, '2/28', 'ext1'),
(15, '3/31', 'ext1'),
(17, '4/15', 'init'),
(17, '10/15', 'ext1'),
(18, '1/31', 'init'),
(19, '7/15', 'init'),
(19, '10/15', 'ext1'),
(20, '6/20', 'init'),
(21, '1/31', 'init'),
(21, '4/31', 'init'),
(21, '7/31', 'init'),
(21, '10/31', 'init'),
(22, '3/20', 'init'),
(22, '6/20', 'init'),
(22, '9/20', 'init'),
(22, '12/20', 'init'),
(23, '3/20', 'init'),
(24, '1/20', 'init'),
(24, '2/20', 'init'),
(24, '3/20', 'init'),
(24, '4/20', 'init'),
(24, '5/20', 'init'),
(24, '6/20', 'init'),
(24, '7/20', 'init'),
(24, '8/20', 'init'),
(24, '9/20', 'init'),
(24, '10/20', 'init'),
(24, '11/20', 'init'),
(24, '12/20', 'init'),
(25, '3/20', 'init'),
(25, '6/20', 'init'),
(25, '9/20', 'init'),
(25, '12/20', 'init'),
(1, '5/15', 'init'),
(1, '8/15', 'ext1'),
(1, '11/15', 'ext2'),
(2, '4/15', 'init'),
(2, '9/15', 'ext1'),
(3, '4/15', 'init'),
(3, '10/15', 'ext1'),
(4, '4/15', 'init'),
(4, '9/15', 'ext1'),
(5, '3/15', 'init'),
(5, '9/15', 'ext1'),
(6, '3/15', 'init'),
(6, '9/15', 'ext1'),
(7, '4/30', 'init'),
(7, '7/31', 'init'),
(7, '10/31', 'init'),
(7, '1/31', 'init'),
(8, '4/15', 'init'),
(8, '10/15', 'ext1'),
(9, '1/15', 'init'),
(10, '1/31', 'init'),
(10, '4/31', 'init'),
(10, '7/31', 'init'),
(10, '10/31', 'init'),
(11, '2/28', 'ext1'),
(11, '3/31', 'ext1'),
(12, '2/28', 'ext1'),
(12, '3/31', 'ext1'),
(13, '4/15', 'init'),
(13, '10/15', 'ext1'),
(14, '7/15', 'init'),
(14, '10/15', 'ext1'),
(15, '2/28', 'ext1'),
(15, '3/31', 'ext1'),
(17, '4/15', 'init'),
(17, '10/15', 'ext1'),
(18, '1/31', 'init'),
(19, '7/15', 'init'),
(19, '10/15', 'ext1'),
(20, '6/20', 'init'),
(21, '1/31', 'init'),
(21, '4/31', 'init'),
(21, '7/31', 'init'),
(21, '10/31', 'init'),
(22, '3/20', 'init'),
(22, '6/20', 'init'),
(22, '9/20', 'init'),
(22, '12/20', 'init'),
(23, '3/20', 'init'),
(24, '1/20', 'init'),
(24, '2/20', 'init'),
(24, '3/20', 'init'),
(24, '4/20', 'init'),
(24, '5/20', 'init'),
(24, '6/20', 'init'),
(24, '7/20', 'init'),
(24, '8/20', 'init'),
(24, '9/20', 'init'),
(24, '10/20', 'init'),
(24, '11/20', 'init'),
(24, '12/20', 'init'),
(25, '3/20', 'init'),
(25, '6/20', 'init'),
(25, '9/20', 'init'),
(25, '12/20', 'init');

-- --------------------------------------------------------

--
-- Table structure for table `instructions`
--

DROP TABLE IF EXISTS `instructions`;
CREATE TABLE IF NOT EXISTS `instructions` (
  `id_instructions` int(11) NOT NULL AUTO_INCREMENT,
  `name_instructions` varchar(100) NOT NULL,
  PRIMARY KEY (`id_instructions`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `instructions`
--



-- --------------------------------------------------------

--
-- Table structure for table `occupation`
--

DROP TABLE IF EXISTS `occupation`;
CREATE TABLE IF NOT EXISTS `occupation` (
  `id_occupation` int(11) NOT NULL AUTO_INCREMENT,
  `name_occupation` varchar(100) NOT NULL,
  PRIMARY KEY (`id_occupation`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `occupation`
--


-- --------------------------------------------------------

--
-- Table structure for table `referred_by`
--

DROP TABLE IF EXISTS `referred_by`;
CREATE TABLE IF NOT EXISTS `referred_by` (
  `id_referred_by` int(11) NOT NULL AUTO_INCREMENT,
  `name_referred_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id_referred_by`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `referred_by`
--


-- --------------------------------------------------------

--
-- Table structure for table `return_status_history`
--

DROP TABLE IF EXISTS `return_status_history`;
CREATE TABLE IF NOT EXISTS `return_status_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_return_id` int(11) NOT NULL,
  `status_name` varchar(100) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `date` int(11) DEFAULT NULL,
  `staff` varchar(10) DEFAULT NULL,
  `transmitted` varchar(10) DEFAULT NULL,
  `review_status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=189 ;

--
-- Dumping data for table `return_status_history`
--


--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
CREATE TABLE IF NOT EXISTS `status` (
  `status_id` int(11) NOT NULL,
  `field_required` varchar(50) NOT NULL,
  `values` text
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`status_id`, `field_required`, `values`) VALUES
(3, 'date', ''),
(7, 'select', 'RG,MG,MJG,ET,WG,TG,LZ,SP,GG,KE'),
(9, 'date', ''),
(10, 'date', ''),
(11, 'date', ''),
(12, 'date', ''),
(12, 'select', 'projection final,holding,ready to collate'),
(13, 'date', ''),
(14, 'select', 'client, ef');

-- --------------------------------------------------------

--
-- Table structure for table `status_name`
--

DROP TABLE IF EXISTS `status_name`;
CREATE TABLE IF NOT EXISTS `status_name` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(100) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `status_name`
--

INSERT INTO `status_name` (`status_id`, `status_name`) VALUES
(1, 'Active'),
(2, 'Inactive'),
(3, 'Info received'),
(4, 'In doc'),
(5, 'Bookmarked'),
(6, 'Date assigned'),
(7, 'Staff'),
(8, 'Missing info'),
(9, 'Ready for review'),
(10, 'Returned for revision'),
(11, 'Reviewed'),
(12, 'Review status'),
(13, 'Collated'),
(14, 'Transmitted'),
(15, 'To Be Determined');

-- --------------------------------------------------------

--
-- Table structure for table `tax_return`
--

DROP TABLE IF EXISTS `tax_return`;
CREATE TABLE IF NOT EXISTS `tax_return` (
  `id_tax_return` int(11) NOT NULL AUTO_INCREMENT,
  `id_client` int(11) NOT NULL,
  `partner` varchar(100) NOT NULL,
  `form_id` varchar(100) NOT NULL,
  `tax_year` int(4) NOT NULL,
  `tax_quarter` int(1) NOT NULL,
  `tax_month` varchar(2) NOT NULL,
  `status` varchar(255) NOT NULL,
  `notes` text NOT NULL,
  `id_instructions` int(11) NOT NULL,
  `sent_date` int(11) NOT NULL,
  `released_date` int(11) NOT NULL,
  `date` int(11) DEFAULT NULL,
  `lock` tinyint(4) NOT NULL,
  `last_update_user` int(4) NOT NULL,
  `last_update_date` int(4) NOT NULL,
  `removed` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_tax_return`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=119 ;

--
-- Dumping data for table `tax_return`
--


--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `last_name` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `staff` varchar(10) NOT NULL,
  `login` varchar(100) NOT NULL,
  `pwd` varchar(100) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `type` enum('admin','editor','reader') NOT NULL,
  `date_registered` int(11) NOT NULL,
  `removed` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `last_name`, `first_name`, `staff`, `login`, `pwd`, `mail`, `type`, `date_registered`, `removed`) VALUES
(1, 'admin', 'admin', 'AD', 'admin', '96e79218965eb72c92a549dd5a330112', 'admin@ggg.com', 'admin', 1308824914, 0);
