DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `id_client` int(11) NOT NULL AUTO_INCREMENT,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `input_date` int(11) NOT NULL,
  `last_update_date` int(11) NOT NULL,
  `entered_by` int(11) NOT NULL,
  `last_update_user` int(11) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_spouse` varchar(100) NOT NULL,
  `first_spouse` varchar(100) NOT NULL,
  `form_id` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `staff` varchar(100) NOT NULL,
  `entity` varchar(100) NOT NULL,
  `partner` enum('MG','RG','MJG') NOT NULL,
  `billing_client` varchar(100) NOT NULL,
  `doc_id` varchar(255) NOT NULL,
  `month` varchar(2) NOT NULL,
  `initial_year` int(4) NOT NULL,
  `retainer_year` int(4) NOT NULL,
  `outlook` tinyint(1) NOT NULL,
  `retainer` varchar(100) NOT NULL,
  `accounting` varchar(100) NOT NULL,
  `principal` varchar(100) NOT NULL,
  `ssn_ein` varchar(11) NOT NULL,
  `ssn_spouse` varchar(11) NOT NULL,
  `referred_by` varchar(100) NOT NULL,
  `occupation` varchar(100) NOT NULL,
  `occupation_spouse` varchar(100) NOT NULL,
  `inactive_date` int(11) NOT NULL,
  `notes` text NOT NULL,
  `removed` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_client`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=59 ;

DROP TABLE IF EXISTS `form`;
CREATE TABLE IF NOT EXISTS `form` (
  `form_id` int(11) NOT NULL AUTO_INCREMENT,
  `form_name` varchar(50) NOT NULL,
  `initial_due_date` tinyint(1) NOT NULL,
  `extension1` tinyint(1) NOT NULL,
  `extension2` tinyint(1) NOT NULL,
  PRIMARY KEY (`form_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=151 ;

DROP TABLE IF EXISTS `form_initial_date`;
CREATE TABLE IF NOT EXISTS `form_initial_date` (
  `form_id` int(11) NOT NULL,
  `initial_date` text NOT NULL,
  `type` enum('init','ext1','ext2') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

DROP TABLE IF EXISTS `return_status_history`;
CREATE TABLE IF NOT EXISTS `return_status_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_return_id` int(11) NOT NULL,
  `status_name` varchar(100) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_date` int(11) NOT NULL,
  `date` int(11) DEFAULT NULL,
  `staff` varchar(10) DEFAULT NULL,
  `transmitted` varchar(10) DEFAULT NULL,
  `review_status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=116 ;

DROP TABLE IF EXISTS `status`;
CREATE TABLE IF NOT EXISTS `status` (
  `status_id` int(11) NOT NULL,
  `field_required` varchar(50) NOT NULL,
  `values` text
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

DROP TABLE IF EXISTS `status_name`;
CREATE TABLE IF NOT EXISTS `status_name` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(100) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=15 ;


DROP TABLE IF EXISTS `tax_return`;
CREATE TABLE IF NOT EXISTS `tax_return` (
  `id_tax_return` int(11) NOT NULL AUTO_INCREMENT,
  `id_client` int(11) NOT NULL,
  `partner` varchar(100) NOT NULL,
  `form_id` varchar(100) NOT NULL,
  `tax_year` int(4) NOT NULL,
  `tax_quarter` int(1) NOT NULL,
  `tax_month` varchar(2) NOT NULL,
  `status` varchar(255) NOT NULL,
  `notes` text NOT NULL,
  `instructions` text NOT NULL,
  `sent_date` int(11) NOT NULL,
  `released_date` int(11) NOT NULL,
  `date` int(11) DEFAULT NULL,
  `lock` tinyint(4) NOT NULL,
  `last_update_user` int(4) NOT NULL,
  `last_update_date` int(4) NOT NULL,
  `removed` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_tax_return`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=77 ;

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `last_name` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `staff` varchar(10) NOT NULL,
  `login` varchar(100) NOT NULL,
  `pwd` varchar(100) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `type` enum('admin','editor','reader') NOT NULL,
  `date_registered` int(11) NOT NULL,
  `removed` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=27 ;