/*
SQLyog Ultimate v10.42 
MySQL - 5.1.67-community-log : Database - ggg
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `Accounting_Software` */

CREATE TABLE `Accounting_Software` (
  `Accounting_Software_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Accounting_Software_Name` varchar(50) NOT NULL,
  PRIMARY KEY (`Accounting_Software_ID`),
  UNIQUE KEY `IDX_Accounting_Software-Accounting_Software_ID` (`Accounting_Software_ID`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

/*Table structure for table `Client` */

CREATE TABLE `Client` (
  `Client_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Client_Type_ID` int(11) DEFAULT NULL,
  `Company_Name` varchar(100) DEFAULT NULL,
  `Client_Name_Last` varchar(25) DEFAULT NULL,
  `Client_Name_First` varchar(25) DEFAULT NULL,
  `Client_SSN_EIN` varchar(11) DEFAULT NULL,
  `Client_Occupation_ID` int(11) DEFAULT NULL,
  `Spouse_Name_Last` varchar(25) DEFAULT NULL,
  `Spouse_Name_First` varchar(25) DEFAULT NULL,
  `Spouse_SSN_EIN` varchar(11) DEFAULT NULL,
  `Spouse_Occupation_ID` int(11) DEFAULT NULL,
  `Staff_ID` int(11) DEFAULT NULL,
  `Partner_ID` int(11) DEFAULT NULL,
  `Stamp_Date` int(11) DEFAULT '0',
  `Stamp_User` int(11) DEFAULT NULL,
  `Last_Edit_Date` int(11) DEFAULT '0',
  `Last_Edit_User` int(11) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `form_id` varchar(100) DEFAULT NULL,
  `Staff` varchar(100) DEFAULT NULL,
  `entity` varchar(100) DEFAULT NULL,
  `partner` enum('MG','RG','MJG') DEFAULT NULL,
  `retainer` varchar(100) DEFAULT NULL,
  `Billing_ID` varchar(50) DEFAULT NULL,
  `Document_ID` varchar(50) DEFAULT NULL,
  `Month_ID` int(11) DEFAULT '0',
  `Initial_Year` int(11) DEFAULT '0',
  `Retainer_Status_ID` int(11) DEFAULT NULL,
  `Retainer_Year` int(11) DEFAULT NULL,
  `Accounting_Software_ID` int(11) DEFAULT NULL,
  `accounting` varchar(100) DEFAULT NULL,
  `removed` tinyint(4) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `Principal_Name` varchar(100) DEFAULT NULL,
  `Referred_By_ID` int(11) DEFAULT NULL,
  `Inactive_Date` int(11) DEFAULT NULL,
  `Client_Notes` text,
  `Client_Status_ID` int(11) NOT NULL DEFAULT '4',
  `Organizer` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`Client_ID`),
  UNIQUE KEY `IDX_Client-Client_ID` (`Client_ID`) USING BTREE,
  KEY `IDX_Client-Company_Name` (`Company_Name`) USING BTREE,
  KEY `IDX_Client-Client_Name` (`Client_Name_Last`,`Client_Name_First`) USING BTREE,
  KEY `FK_Client-Client_Occupation_ID` (`Client_Occupation_ID`),
  KEY `FK_Client-Spouse_Occupation_ID` (`Spouse_Occupation_ID`),
  KEY `FK_Client-Staff_ID` (`Staff_ID`),
  KEY `FK_Client-Client_Type_ID` (`Client_Type_ID`),
  KEY `FK_Client-Partner_ID` (`Partner_ID`),
  KEY `FK_Client-Account_Software_ID` (`Accounting_Software_ID`),
  KEY `FK_Client-Stamp_User` (`Stamp_User`),
  KEY `FK_Client-Last_Edit_User` (`Last_Edit_User`),
  KEY `FK_Client-Retainer_Status_ID` (`Retainer_Status_ID`),
  KEY `FK_Client-Client_Status_ID` (`Client_Status_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=1620 DEFAULT CHARSET=cp1251;

/*Table structure for table `Client_State_Link` */

CREATE TABLE `Client_State_Link` (
  `Client_State_Link_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Client_ID` int(11) NOT NULL,
  `State_ID` varchar(2) NOT NULL,
  PRIMARY KEY (`Client_State_Link_ID`),
  UNIQUE KEY `IDX_Client_State_Link-Client_State_Link_ID` (`Client_State_Link_ID`) USING BTREE,
  UNIQUE KEY `IDX_Client_State_Link-Client_ID_State_ID` (`Client_ID`,`State_ID`) USING BTREE,
  KEY `FK_Client_State_Link-State_ID` (`State_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=1528 DEFAULT CHARSET=utf8;

/*Table structure for table `Client_Status` */

CREATE TABLE `Client_Status` (
  `Client_Status_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Client_Status_Label` varchar(25) NOT NULL,
  PRIMARY KEY (`Client_Status_ID`),
  UNIQUE KEY `IDX_Client_Status-Client_Status_ID` (`Client_Status_ID`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Table structure for table `Client_Tax_Form_Link` */

CREATE TABLE `Client_Tax_Form_Link` (
  `Client_Tax_Form_Link_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Client_ID` int(11) DEFAULT NULL,
  `Tax_Form_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Client_Tax_Form_Link_ID`),
  UNIQUE KEY `IDX_Client_Tax_Form_Link-Client_Tax_Form_Link_ID` (`Client_Tax_Form_Link_ID`) USING BTREE,
  UNIQUE KEY `IDX_Client_Tax_Form_Link-Client_ID-Form_ID` (`Client_ID`,`Tax_Form_ID`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3067 DEFAULT CHARSET=utf8;

/*Table structure for table `Client_Type` */

CREATE TABLE `Client_Type` (
  `Client_Type_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Client_Type_Label` varchar(50) NOT NULL,
  PRIMARY KEY (`Client_Type_ID`),
  UNIQUE KEY `IDX_Client_Type-Client_Type_ID` (`Client_Type_ID`) USING BTREE,
  UNIQUE KEY `IDX_Client_Type-Client_Type_Label` (`Client_Type_Label`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=cp1251;

/*Table structure for table `Institution` */

CREATE TABLE `Institution` (
  `Institution_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Institution_Name` varchar(50) NOT NULL,
  `Institution_Address_1` varchar(50) DEFAULT NULL,
  `Institution_Address_2` varchar(50) DEFAULT NULL,
  `Institution_City` varchar(25) DEFAULT NULL,
  `Institution_State` varchar(2) DEFAULT NULL,
  `Institution_Postal_Code` varchar(10) DEFAULT NULL,
  `Institution_Contact_Name` varchar(50) DEFAULT NULL,
  `Institution_Telephone` varchar(15) DEFAULT NULL,
  `Institution_ABA` varchar(25) DEFAULT NULL,
  `Stamp_Date` int(11) NOT NULL,
  `Stamp_User` int(11) NOT NULL,
  `Last_Edit_Date` int(11) NOT NULL,
  `Last_Edit_User` int(11) NOT NULL,
  PRIMARY KEY (`Institution_ID`),
  UNIQUE KEY `IDX_Institutions-Institution_ID` (`Institution_ID`) USING BTREE,
  KEY `FK_Institution-Stamp_User` (`Stamp_User`),
  KEY `FK_Institution-Last_Edit_User` (`Last_Edit_User`)
) ENGINE=MyISAM AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;

/*Table structure for table `Instructions` */

CREATE TABLE `Instructions` (
  `Instructions_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Instructions_Label` varchar(100) NOT NULL,
  PRIMARY KEY (`Instructions_ID`),
  UNIQUE KEY `IDX_Instructions-Instructions_ID` (`Instructions_ID`) USING BTREE,
  UNIQUE KEY `IDX_Instructions-Instructions_Label` (`Instructions_Label`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=cp1251;

/*Table structure for table `Occupation` */

CREATE TABLE `Occupation` (
  `Occupation_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Occupation_Name` varchar(50) NOT NULL,
  PRIMARY KEY (`Occupation_ID`),
  UNIQUE KEY `IDX_Occupation-Occupation_ID` (`Occupation_ID`) USING BTREE,
  UNIQUE KEY `IDX_Occupation-Occupation_Name` (`Occupation_Name`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=351 DEFAULT CHARSET=cp1251;

/*Table structure for table `Referred_By` */

CREATE TABLE `Referred_By` (
  `Referred_By_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Referred_By_Name` varchar(100) NOT NULL,
  PRIMARY KEY (`Referred_By_ID`),
  UNIQUE KEY `IDX_Referred_By_ID` (`Referred_By_ID`) USING BTREE,
  UNIQUE KEY `IDX_Referred_By_Name` (`Referred_By_Name`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=337 DEFAULT CHARSET=cp1251;

/*Table structure for table `Retainer_Status` */

CREATE TABLE `Retainer_Status` (
  `Retainer_Status_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Retainer_Status_Label` varchar(25) NOT NULL,
  PRIMARY KEY (`Retainer_Status_ID`),
  UNIQUE KEY `IDX_Retainer_Status-Retainer_Status_ID` (`Retainer_Status_ID`) USING BTREE,
  UNIQUE KEY `IDX_Retainer_Status-Retainer_Status_Label` (`Retainer_Status_Label`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Table structure for table `Role` */

CREATE TABLE `Role` (
  `Role_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Role_Name` varchar(25) NOT NULL,
  PRIMARY KEY (`Role_ID`),
  UNIQUE KEY `IDX_Role-Role_ID` (`Role_ID`) USING BTREE,
  UNIQUE KEY `IDS_Role-Role_Name` (`Role_Name`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Table structure for table `Staff` */

CREATE TABLE `Staff` (
  `Staff_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Staff_Name_Last` varchar(30) NOT NULL,
  `Staff_Name_First` varchar(30) NOT NULL,
  `Initials` varchar(3) NOT NULL,
  `Login_ID` varchar(25) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Stamp_Date` int(11) DEFAULT '0',
  `Stamp_User` int(11) DEFAULT '1',
  `Last_Edit_Date` int(11) DEFAULT '0',
  `Last_Edit_User` int(11) DEFAULT '1',
  `Staff_Status_ID` int(11) NOT NULL DEFAULT '1',
  `type` enum('admin','editor','reader') DEFAULT NULL,
  `removed` tinyint(4) DEFAULT NULL,
  `is_partner` bit(1) DEFAULT NULL,
  PRIMARY KEY (`Staff_ID`),
  UNIQUE KEY `IDX_Staff-Staff_ID` (`Staff_ID`) USING BTREE,
  UNIQUE KEY `IDX_Staff-Login_ID` (`Login_ID`) USING BTREE,
  UNIQUE KEY `IDX_Staff-Email` (`Email`) USING BTREE,
  UNIQUE KEY `IDX_Staff-Initials` (`Initials`) USING BTREE,
  KEY `FK_Staff_Status-Staff_Status_ID` (`Staff_Status_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=cp1251;

/*Table structure for table `Staff_Role_Link` */

CREATE TABLE `Staff_Role_Link` (
  `Staff_Role_Link_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Staff_ID` int(11) NOT NULL,
  `Role_ID` int(11) NOT NULL,
  PRIMARY KEY (`Staff_Role_Link_ID`),
  UNIQUE KEY `Staff_Role_Link-Staff_Role_Link_ID` (`Staff_Role_Link_ID`) USING BTREE,
  UNIQUE KEY `Staff_Role_Link-Staff_ID-Role_ID` (`Staff_ID`,`Role_ID`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=134 DEFAULT CHARSET=utf8;

/*Table structure for table `Staff_Status` */

CREATE TABLE `Staff_Status` (
  `Staff_Status_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Staff_Status_Label` varchar(25) NOT NULL,
  PRIMARY KEY (`Staff_Status_ID`),
  UNIQUE KEY `IDX_Staff-Staff_Status_ID` (`Staff_Status_ID`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Table structure for table `State` */

CREATE TABLE `State` (
  `State_ID` varchar(2) NOT NULL,
  `State_Name` varchar(50) NOT NULL,
  PRIMARY KEY (`State_ID`),
  UNIQUE KEY `IDX_State-State_ID` (`State_ID`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `Statement` */

CREATE TABLE `Statement` (
  `Statement_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Client_ID` int(11) NOT NULL,
  `Institution_ID` int(11) NOT NULL,
  `Statement_Type_ID` int(11) NOT NULL,
  `Account_Number` varchar(25) DEFAULT NULL,
  `ABA_Number` varchar(25) DEFAULT NULL,
  `Statement_Start_Date` int(11) NOT NULL DEFAULT '0',
  `Statement_End_Date` int(11) NOT NULL DEFAULT '0',
  `Last_Reconciliation_Date` int(11) NOT NULL DEFAULT '0',
  `Contact_Access_Notes` text,
  `Statement_Status_ID` int(11) NOT NULL,
  `Inactive_Date` int(11) NOT NULL DEFAULT '0',
  `Stamp_User` int(11) NOT NULL,
  `Stamp_Date` int(11) NOT NULL,
  `Last_Edit_User` int(11) NOT NULL,
  `Last_Edit_Date` int(11) NOT NULL,
  PRIMARY KEY (`Statement_ID`),
  UNIQUE KEY `IDX_Statement-Statement_ID` (`Statement_ID`) USING BTREE,
  KEY `FK_Statement-Statement_Type_ID` (`Statement_Type_ID`) USING BTREE,
  KEY `FK_Statement-Institution_ID` (`Institution_ID`) USING BTREE,
  KEY `FK_Statement-Statement_Status_ID` (`Statement_Status_ID`) USING BTREE,
  KEY `FK_Statement-Stamp_User` (`Stamp_User`),
  KEY `FK_Statement-Last_Edit_User` (`Last_Edit_User`),
  KEY `FK_Statement-Client_ID` (`Client_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=612 DEFAULT CHARSET=utf8;

/*Table structure for table `Statement_History` */

CREATE TABLE `Statement_History` (
  `Statement_History_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Statement_ID` int(11) NOT NULL,
  `Statement_Status_ID` int(11) NOT NULL,
  `Stamp_Date` int(11) NOT NULL,
  `Stamp_User` int(11) NOT NULL,
  `Last_Edit_Date` int(11) NOT NULL,
  `Last_Edit_User` int(11) NOT NULL,
  PRIMARY KEY (`Statement_History_ID`),
  UNIQUE KEY `IDX_Statement_History-Statement_History_ID` (`Statement_History_ID`) USING BTREE,
  KEY `FK_Statement_History-Statement_ID` (`Statement_ID`),
  KEY `FK_Statement_History-Statement_Status_ID` (`Statement_Status_ID`),
  KEY `FK_Statement_History-Stamp_User` (`Stamp_User`),
  KEY `FK_Statement_History-Last_Edit_User` (`Last_Edit_User`)
) ENGINE=MyISAM AUTO_INCREMENT=1851 DEFAULT CHARSET=utf8;

/*Table structure for table `Statement_Status` */

CREATE TABLE `Statement_Status` (
  `Statement_Status_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Statement_Status_Label` varchar(50) NOT NULL,
  PRIMARY KEY (`Statement_Status_ID`),
  UNIQUE KEY `IDX_Statement_Status-Statement_Status_ID` (`Statement_Status_ID`) USING BTREE,
  UNIQUE KEY `IDS_Statement_Status-Statement_Status_Label` (`Statement_Status_Label`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Table structure for table `Statement_Type` */

CREATE TABLE `Statement_Type` (
  `Statement_Type_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Statement_Type_Label` varchar(25) NOT NULL,
  PRIMARY KEY (`Statement_Type_ID`),
  UNIQUE KEY `IDX_Statement_Type-Statement_Type_ID` (`Statement_Type_ID`) USING BTREE,
  UNIQUE KEY `IDX_Statement_Type-Statment_Type_Label` (`Statement_Type_Label`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Table structure for table `Tax_Form` */

CREATE TABLE `Tax_Form` (
  `Tax_Form_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Tax_Form_Name` varchar(50) NOT NULL,
  `Initial_Due_Date` tinyint(1) NOT NULL,
  `Extension_1` tinyint(1) NOT NULL,
  `Extension_2` tinyint(1) NOT NULL,
  PRIMARY KEY (`Tax_Form_ID`),
  UNIQUE KEY `IDX_Tax_Form-Tax_Form_ID` (`Tax_Form_ID`) USING BTREE,
  UNIQUE KEY `IDX_Tax_Form-Tax_Form_Name` (`Tax_Form_Name`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=183 DEFAULT CHARSET=cp1251;

/*Table structure for table `Tax_Form_Initial_Date` */

CREATE TABLE `Tax_Form_Initial_Date` (
  `Tax_Form_ID` int(11) NOT NULL,
  `Initial_Date` text NOT NULL,
  `Type` enum('init','ext1','ext2') NOT NULL,
  KEY `IDX_Tax_Form_Initial_Date-Tax_Form_ID` (`Tax_Form_ID`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

/*Table structure for table `Tax_Return` */

CREATE TABLE `Tax_Return` (
  `Tax_Return_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Client_ID` int(11) NOT NULL,
  `Partner_ID` int(11) DEFAULT NULL,
  `Staff_ID` int(11) DEFAULT NULL,
  `Tax_Form_ID` int(11) NOT NULL,
  `Tax_Year` int(4) NOT NULL,
  `Tax_Quarter` int(1) NOT NULL,
  `Tax_Month` int(2) NOT NULL,
  `Tax_Return_Status_ID` int(255) NOT NULL,
  `Instructions_ID` int(11) NOT NULL,
  `Tax_Return_Notes` text NOT NULL,
  `Reviewer_1_ID` int(11) DEFAULT NULL,
  `Reviewer_2_ID` int(11) DEFAULT NULL,
  `Date_Sent` int(11) NOT NULL,
  `Date_Released` int(11) NOT NULL,
  `Is_Locked` bit(1) NOT NULL DEFAULT b'0',
  `Stamp_Date` int(11) DEFAULT NULL,
  `Stamp_User` int(11) DEFAULT NULL,
  `990PF_Date` int(11) DEFAULT NULL,
  `Last_Edit_Date` int(4) NOT NULL,
  `Last_Edit_User` int(4) NOT NULL,
  `partner` varchar(100) DEFAULT NULL,
  `lock` tinyint(4) DEFAULT NULL,
  `removed` tinyint(4) DEFAULT NULL,
  `staff_tax` varchar(100) DEFAULT NULL,
  `reviewer1_tax` varchar(100) DEFAULT NULL,
  `reviewer2_tax` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Tax_Return_ID`),
  UNIQUE KEY `IDX_Tax_Return-Tax_Return_ID` (`Tax_Return_ID`) USING BTREE,
  KEY `IDX_Tax_Return-Tax_Year` (`Tax_Year`) USING BTREE,
  KEY `IDX_Tax_Return-Tax_Month` (`Tax_Month`) USING BTREE,
  KEY `IDX_Tax_Return-Tax_Quarter` (`Tax_Quarter`) USING BTREE,
  KEY `FK_Tax_Return-Client_ID` (`Client_ID`),
  KEY `FK_Tax_Return-Partner_ID` (`Partner_ID`),
  KEY `FK_Tax_Return-Staff_ID` (`Staff_ID`),
  KEY `FK_Tax_Return-Stamp_User` (`Stamp_User`),
  KEY `FK_Tax_Return-Last_Edit_User` (`Last_Edit_User`),
  KEY `FK_Tax_Return-Tax_Form_ID` (`Tax_Form_ID`),
  KEY `FK_Tax_Return-Tax_Return_Status_ID` (`Tax_Return_Status_ID`),
  KEY `FK_Tax_Return-Instructions_ID` (`Instructions_ID`),
  KEY `FK_Tax_Return-Reviewer_1_ID` (`Reviewer_1_ID`),
  KEY `FK_Tax_Return-Reviewer_2_ID` (`Reviewer_2_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3730 DEFAULT CHARSET=cp1251;

/*Table structure for table `Tax_Return_Status` */

CREATE TABLE `Tax_Return_Status` (
  `Tax_Return_Status_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Tax_Return_Status_Label` varchar(100) NOT NULL,
  PRIMARY KEY (`Tax_Return_Status_ID`),
  UNIQUE KEY `IDX_Tax_Return_Status-Tax_Return_Status_ID` (`Tax_Return_Status_ID`) USING BTREE,
  UNIQUE KEY `IDX_Tax_Return_Status-Tax_Return_Status_Label` (`Tax_Return_Status_Label`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=cp1251;

/*Table structure for table `Tax_Return_Status_History` */

CREATE TABLE `Tax_Return_Status_History` (
  `Tax_Return_Status_History_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Tax_Return_ID` int(11) NOT NULL,
  `Tax_Return_Status_ID` int(11) DEFAULT NULL,
  `Staff_ID` int(11) DEFAULT NULL,
  `Last_Edit_User` int(11) NOT NULL,
  `Last_Edit_Date` int(11) NOT NULL,
  `Staff` varchar(10) DEFAULT NULL,
  `review_status` varchar(100) DEFAULT NULL,
  `Stamp_Date` int(11) DEFAULT NULL,
  `Stamp_User` int(11) DEFAULT NULL,
  `reviewer1` varchar(10) DEFAULT NULL,
  `reviewer2` varchar(10) DEFAULT NULL,
  `status_name` varchar(100) DEFAULT NULL,
  `Transmitted` varchar(10) DEFAULT NULL,
  `Reviewer_1_ID` int(11) DEFAULT NULL,
  `Reviewer_2_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Tax_Return_Status_History_ID`),
  UNIQUE KEY `IDX_Tax_Return_Status_History-Tax_Return_Status_History_ID` (`Tax_Return_Status_History_ID`) USING BTREE,
  KEY `FK_Tax_Return_Status_History-Tax_Return_ID` (`Tax_Return_ID`),
  KEY `FK_Tax_Return_Status_History-Staff_ID` (`Staff_ID`),
  KEY `FK_Tax_Return_Status_History-Reviewer_1_ID` (`Reviewer_1_ID`),
  KEY `FK_Tax_Return_Status_History-Reviewer_2_ID` (`Reviewer_2_ID`),
  KEY `FK_Tax_Return_Status_History-Last_Edit_User` (`Last_Edit_User`),
  KEY `FK_Tax_Return_Status_History-Stamp_User` (`Stamp_User`),
  KEY `FK_Tax_Return_Status_History-Tax_Return_Status_ID` (`Tax_Return_Status_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=16307 DEFAULT CHARSET=cp1251;

/*Table structure for table `ci_sessions` */

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `status` */

CREATE TABLE `status` (
  `status_id` int(11) NOT NULL,
  `field_required` varchar(50) NOT NULL,
  `values` text
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

/* Function  structure for function  `fn_Utility_CleanNames` */

CREATE DEFINER=`root`@`localhost` FUNCTION `fn_Utility_CleanNames`(oldName VARCHAR(255), delim VARCHAR(1), trimSpaces BOOL) RETURNS varchar(255) CHARSET utf8
BEGIN
  SET @oldString := oldName;
  SET @newString := "";
 
  tokenLoop: LOOP
    IF trimSpaces THEN SET @oldString := TRIM(BOTH " " FROM @oldString); END IF;
 
    SET @splitPoint := LOCATE(delim, @oldString);
 
    IF @splitPoint = 0 THEN
      SET @newString := CONCAT(@newString, fn_Utility_UpperCaseFirst(@oldString));
      LEAVE tokenLoop;
    END IF;
 
    SET @newString := CONCAT(@newString, fn_Utility_UpperCaseFirst(SUBSTRING(@oldString, 1, @splitPoint)));
    SET @oldString := SUBSTRING(@oldString, @splitPoint+1);
		
  END LOOP tokenLoop;
 
  RETURN @newString;
END;

/* Function  structure for function  `fn_Utility_UpperCaseFirst` */


CREATE DEFINER=`root`@`localhost` FUNCTION `fn_Utility_UpperCaseFirst`(oldWord VARCHAR(255)) RETURNS varchar(255) CHARSET utf8
RETURN CONCAT(UCASE(SUBSTRING(oldWord, 1, 1)),SUBSTRING(oldWord, 2));

/* Procedure structure for procedure `sp_00_MassUpdate` */


CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_00_MassUpdate`()
BEGIN
	call sp_01_CreateNewTables();
	call sp_02_ModifyExistingTables();
	call sp_03_UpdateStaffInformation();
	call sp_04_UpdateClientInformation();
	call sp_05_UpdateTaxReturnInformation();
	call sp_06_UpdateTaxReturnStatusHistoryInformation();
END;

/* Procedure structure for procedure `sp_01_CreateNewTables` */

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_01_CreateNewTables`()
BEGIN
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `Role`;
CREATE TABLE `Role` (
  `Role_ID` INT NOT NULL AUTO_INCREMENT,
  `Role_Name` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`Role_ID`),
  UNIQUE KEY `IDX_Role-Role_ID` (`Role_ID`) USING BTREE,
  UNIQUE KEY `IDS_Role-Role_Name` (`Role_Name`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
INSERT INTO `Role` VALUES ('3', 'Editor');
INSERT INTO `Role` VALUES ('2', 'Partner');
INSERT INTO `Role` VALUES ('4', 'Read-Only');
INSERT INTO `Role` VALUES ('1', 'System Administrator');
DROP TABLE IF EXISTS `Staff_Status`;
CREATE TABLE `Staff_Status` (
  `Staff_Status_ID` INT NOT NULL AUTO_INCREMENT,
  `Staff_Status_Label` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`Staff_Status_ID`),
  UNIQUE KEY `IDX_Staff-Staff_Status_ID` (`Staff_Status_ID`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
INSERT INTO `Staff_Status` VALUES ('1', 'Active');
INSERT INTO `Staff_Status` VALUES ('2', 'Inactive');
INSERT INTO `Staff_Status` VALUES ('3', 'Terminated');
INSERT INTO `Staff_Status` VALUES ('4', 'Pending');
DROP TABLE IF EXISTS `Staff_Role_Link`;
CREATE TABLE `Staff_Role_Link` (
  `Staff_Role_Link_ID` INT NOT NULL AUTO_INCREMENT,
  `Staff_ID` INT NOT NULL,
  `Role_ID` INT NOT NULL,
  PRIMARY KEY (`Staff_Role_Link_ID`),
  UNIQUE KEY `Staff_Role_Link-Staff_Role_Link_ID` (`Staff_Role_Link_ID`) USING BTREE,
  UNIQUE KEY `Staff_Role_Link-Staff_ID-Role_ID` (`Staff_ID`,`Role_ID`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `Client_Tax_Form_Link`;
CREATE TABLE `Client_Tax_Form_Link`
(
  `Client_Tax_Form_Link_ID` INT NOT NULL AUTO_INCREMENT,
  `Client_ID` INT DEFAULT NULL,
  `Tax_Form_ID` INT DEFAULT NULL,
  PRIMARY KEY (`Client_Tax_Form_Link_ID`),
  UNIQUE KEY `IDX_Client_Tax_Form_Link-Client_Tax_Form_Link_ID` (`Client_Tax_Form_Link_ID`) USING BTREE,
  UNIQUE KEY `IDX_Client_Tax_Form_Link-Client_ID-Form_ID` (`Client_ID`,`Tax_Form_ID`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `Client_Status`;
CREATE TABLE `Client_Status` (
  `Client_Status_ID` INT NOT NULL AUTO_INCREMENT,
  `Client_Status_Label` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`Client_Status_ID`),
  UNIQUE KEY `IDX_Client_Status-Client_Status_ID` (`Client_Status_ID`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
INSERT INTO `Client_Status` VALUES ('1', 'Active');
INSERT INTO `Client_Status` VALUES ('2', 'Inactive');
INSERT INTO `Client_Status` VALUES ('3', 'Terminated');
INSERT INTO `Client_Status` VALUES ('4', 'Pending');
	-- Table structure for `State`
	-- ----------------------------
	DROP TABLE IF EXISTS `State`;
	CREATE TABLE `State` (
		`State_ID` VARCHAR(2) NOT NULL,
		`State_Name` VARCHAR(50) NOT NULL,
		PRIMARY KEY (`State_ID`),
		UNIQUE KEY `IDX_State-State_ID` (`State_ID`) USING BTREE
	) ENGINE=MyISAM DEFAULT CHARSET=utf8;
	-- ----------------------------
	-- Records of State
	-- ----------------------------
	INSERT INTO `State` VALUES ('AK', 'Alaska');
	INSERT INTO `State` VALUES ('AL', 'Alabama');
	INSERT INTO `State` VALUES ('AR', 'Arkansas');
	INSERT INTO `State` VALUES ('AZ', 'Arizona');
	INSERT INTO `State` VALUES ('CA', 'California');
	INSERT INTO `State` VALUES ('CO', 'Colorado');
	INSERT INTO `State` VALUES ('CT', 'Connecticut');
	INSERT INTO `State` VALUES ('DC', 'District of Columbia');
	INSERT INTO `State` VALUES ('DE', 'Delaware');
	INSERT INTO `State` VALUES ('FL', 'Florida');
	INSERT INTO `State` VALUES ('GA', 'Georgia');
	INSERT INTO `State` VALUES ('HI', 'Hawaii');
	INSERT INTO `State` VALUES ('IA', 'Iowa');
	INSERT INTO `State` VALUES ('ID', 'Idaho');
	INSERT INTO `State` VALUES ('IL', 'Illinois');
	INSERT INTO `State` VALUES ('IN', 'Indiana');
	INSERT INTO `State` VALUES ('KS', 'Kansas');
	INSERT INTO `State` VALUES ('KY', 'Kentucky');
	INSERT INTO `State` VALUES ('LA', 'Louisiana');
	INSERT INTO `State` VALUES ('MA', 'Massachusetts');
	INSERT INTO `State` VALUES ('MD', 'Maryland');
	INSERT INTO `State` VALUES ('ME', 'Maine');
	INSERT INTO `State` VALUES ('MI', 'Michigan');
	INSERT INTO `State` VALUES ('MN', 'Minnesota');
	INSERT INTO `State` VALUES ('MO', 'Missouri');
	INSERT INTO `State` VALUES ('MS', 'Mississippi');
	INSERT INTO `State` VALUES ('MT', 'Montana');
	INSERT INTO `State` VALUES ('NC', 'North Carolina');
	INSERT INTO `State` VALUES ('ND', 'North Dakota');
	INSERT INTO `State` VALUES ('NE', 'Nebraska');
	INSERT INTO `State` VALUES ('NH', 'New Hampshire');
	INSERT INTO `State` VALUES ('NJ', 'New Jersey');
	INSERT INTO `State` VALUES ('NM', 'New Mexico');
	INSERT INTO `State` VALUES ('NV', 'Nevada');
	INSERT INTO `State` VALUES ('NY', 'New York');
	INSERT INTO `State` VALUES ('OH', 'Ohio');
	INSERT INTO `State` VALUES ('OK', 'Oklahoma');
	INSERT INTO `State` VALUES ('OR', 'Oregon');
	INSERT INTO `State` VALUES ('PA', 'Pennsylvania');
	INSERT INTO `State` VALUES ('RI', 'Rhode Island');
	INSERT INTO `State` VALUES ('SC', 'South Carolina');
	INSERT INTO `State` VALUES ('SD', 'South Dakota');
	INSERT INTO `State` VALUES ('TN', 'Tennessee');
	INSERT INTO `State` VALUES ('TX', 'Texas');
	INSERT INTO `State` VALUES ('UT', 'Utah');
	INSERT INTO `State` VALUES ('VA', 'Virginia');
	INSERT INTO `State` VALUES ('VT', 'Vermont');
	INSERT INTO `State` VALUES ('WA', 'Washington');
	INSERT INTO `State` VALUES ('WI', 'Wisconsin');
	INSERT INTO `State` VALUES ('WV', 'West Virginia');
	INSERT INTO `State` VALUES ('WY', 'Wyoming');
DROP TABLE IF EXISTS `Client_State_Link`;
CREATE TABLE `Client_State_Link` (
  `Client_State_Link_ID` INT NOT NULL AUTO_INCREMENT ,
  `Client_ID` INT NOT NULL,
  `State_ID` VARCHAR(2) NOT NULL,
  PRIMARY KEY (`Client_State_Link_ID`),
  UNIQUE KEY `IDX_Client_State_Link-Client_State_Link_ID` (`Client_State_Link_ID`) USING BTREE,
  UNIQUE KEY `IDX_Client_State_Link-Client_ID_State_ID` (`Client_ID`,`State_ID`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
ALTER TABLE `Client_State_Link` ADD CONSTRAINT `FK_Client_State_Link-State_ID` FOREIGN KEY (`State_ID`) REFERENCES `State` (`State_ID`) ON DELETE RESTRICT;
DROP TABLE IF EXISTS `Accounting_Software`;
CREATE TABLE `Accounting_Software` (
  `Accounting_Software_ID` INT NOT NULL AUTO_INCREMENT,
  `Accounting_Software_Name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`Accounting_Software_ID`),
  UNIQUE KEY `IDX_Accounting_Software-Accounting_Software_ID` (`Accounting_Software_ID`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
INSERT INTO `Accounting_Software` VALUES ('1', 'Unknown');
INSERT INTO `Accounting_Software` VALUES ('2', 'Quicken');
INSERT INTO `Accounting_Software` VALUES ('3', 'Quickbooks');
INSERT INTO `Accounting_Software` VALUES ('4', 'Quicbooks Enterprise');
INSERT INTO `Accounting_Software` VALUES ('5', 'Sage 50');
INSERT INTO `Accounting_Software` VALUES ('6', 'Bookkeeper');
INSERT INTO `Accounting_Software` VALUES ('7', 'AccountEdge');
INSERT INTO `Accounting_Software` VALUES ('8', 'DacEasy');
INSERT INTO `Accounting_Software` VALUES ('9', 'Simply Accounting');
INSERT INTO `Accounting_Software` VALUES ('10', 'CYMA');
INSERT INTO `Accounting_Software` VALUES ('11', 'NetSuite');
INSERT INTO `Accounting_Software` VALUES ('12', 'Microsoft Money');
INSERT INTO `Accounting_Software` VALUES ('13', 'Open Systems Accouting Software');
INSERT INTO `Accounting_Software` VALUES ('14', 'PeachTree Accounting');
INSERT INTO `Accounting_Software` VALUES ('15', 'SAP Business One');
INSERT INTO `Accounting_Software` VALUES ('16', 'Traverse');
INSERT INTO `Accounting_Software` VALUES ('17', 'Xero');
INSERT INTO `Accounting_Software` VALUES ('18', 'Pastel Accounting');
INSERT INTO `Accounting_Software` VALUES ('19', 'NOSA XP');
INSERT INTO `Accounting_Software` VALUES ('20', 'NewViews');
INSERT INTO `Accounting_Software` VALUES ('21', 'Moneydance');
INSERT INTO `Accounting_Software` VALUES ('22', 'IRIS Software');
INSERT INTO `Accounting_Software` VALUES ('23', 'Intacct');
INSERT INTO `Accounting_Software` VALUES ('24', 'iBank');
INSERT INTO `Accounting_Software` VALUES ('25', 'Fortora Fresh Finance');
INSERT INTO `Accounting_Software` VALUES ('26', 'FinancialForce');
INSERT INTO `Accounting_Software` VALUES ('27', 'Coda Financials');
INSERT INTO `Accounting_Software` VALUES ('28', 'CGram Software');
INSERT INTO `Accounting_Software` VALUES ('29', 'AME Accounting Software');
INSERT INTO `Accounting_Software` VALUES ('30', 'Agresso');
INSERT INTO `Accounting_Software` VALUES ('31', 'Advanced Business Solutions');
INSERT INTO `Accounting_Software` VALUES ('32', 'Accpac');
INSERT INTO `Accounting_Software` VALUES ('33', 'BIG4Books');
INSERT INTO `Accounting_Software` VALUES ('34', 'Outright');
INSERT INTO `Accounting_Software` VALUES ('35', 'Unknown');
DROP TABLE IF EXISTS `Retainer_Status`;
CREATE TABLE `Retainer_Status` (
  `Retainer_Status_ID` INT NOT NULL AUTO_INCREMENT,
  `Retainer_Status_Label` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`Retainer_Status_ID`),
  UNIQUE KEY `IDX_Retainer_Status-Retainer_Status_ID` (`Retainer_Status_ID`) USING BTREE,
  UNIQUE KEY `IDX_Retainer_Status-Retainer_Status_Label` (`Retainer_Status_Label`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
INSERT INTO `Retainer_Status` VALUES ('1', 'None');
INSERT INTO `Retainer_Status` VALUES ('2', 'On File');
DROP TABLE IF EXISTS `Institution`;
CREATE TABLE `Institution` (
  `Institution_ID` INT NOT NULL AUTO_INCREMENT,
  `Institution_Name` VARCHAR(50) NOT NULL,
  `Institution_Address_1` VARCHAR(50) DEFAULT NULL,
  `Institution_Address_2` VARCHAR(50) DEFAULT NULL,
  `Institution_City` VARCHAR(25) DEFAULT NULL,
  `Institution_State` VARCHAR(2) DEFAULT NULL,
  `Institution_Postal_Code` VARCHAR(10) DEFAULT NULL,
  `Institution_Contact_Name` VARCHAR(50) DEFAULT NULL,
  `Institution_Telephone` VARCHAR(15) DEFAULT NULL,
  `Stamp_Date` INT NOT NULL,
  `Stamp_User` INT NOT NULL,
  `Last_Edit_Date` INT NOT NULL,
  `Last_Edit_User` INT NOT NULL,
  PRIMARY KEY (`Institution_ID`),
  UNIQUE KEY `IDX_Institutions-Institution_ID` (`Institution_ID`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `Statement_Type`;
CREATE TABLE `Statement_Type` (
  `Statement_Type_ID` INT NOT NULL AUTO_INCREMENT,
  `Statement_Type_Label` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`Statement_Type_ID`),
  UNIQUE KEY `IDX_Statement_Type-Statement_Type_ID` (`Statement_Type_ID`) USING BTREE,
  UNIQUE KEY `IDX_Statement_Type-Statment_Type_Label` (`Statement_Type_Label`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
INSERT INTO `Statement_Type` VALUES ('1', 'Auto Download');
INSERT INTO `Statement_Type` VALUES ('2', 'Manual Download');
INSERT INTO `Statement_Type` VALUES ('3', 'Direct Mail');
INSERT INTO `Statement_Type` VALUES ('4', 'Statement Online');
INSERT INTO `Statement_Type` VALUES ('5', 'Client Supplied');
DROP TABLE IF EXISTS `Statement_Status`;
CREATE TABLE `Statement_Status` (
  `Statement_Status_ID` INT NOT NULL AUTO_INCREMENT,
  `Statement_Status_Label` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`Statement_Status_ID`),
  UNIQUE KEY `IDX_Statement_Status-Statement_Status_ID` (`Statement_Status_ID`) USING BTREE,
  UNIQUE KEY `IDS_Statement_Status-Statement_Status_Label` (`Statement_Status_Label`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
INSERT INTO `Statement_Status` VALUES ('1', 'Reconciled through Prior Month End');
INSERT INTO `Statement_Status` VALUES ('2', 'Missing Statement');
INSERT INTO `Statement_Status` VALUES ('3', 'Ready for Review');
INSERT INTO `Statement_Status` VALUES ('4', 'Review in Process');
INSERT INTO `statement_Status` VALUES ('5', 'Review Completed');
DROP TABLE IF EXISTS `Statement`;
CREATE TABLE `Statement` (
  `Statement_ID` INT NOT NULL AUTO_INCREMENT,
  `Client_ID` INT NOT NULL,
  `Institution_ID` INT NOT NULL,
  `Statement_Type_ID` INT NOT NULL,
  `Account_Number` VARCHAR(25) DEFAULT NULL,
  `ABA_Number` VARCHAR(25) DEFAULT NULL,
  `Statement_Start_Date` INT NOT NULL DEFAULT '0',
  `Statement_End_Date` INT NOT NULL DEFAULT '0',
  `Last_Reconciliation_Date` INT NOT NULL DEFAULT '0',
  `Contact_Access_Notes` TEXT,
  `Statement_Status_ID` INT NOT NULL,
  `Inactive_Date` INT NOT NULL DEFAULT '0',
  `Stamp_User` INT NOT NULL,
  `Stamp_Date` INT NOT NULL,
  `Last_Edit_User` INT NOT NULL,
  `Last_Edit_Date` INT NOT NULL,
  PRIMARY KEY (`Statement_ID`),
  UNIQUE KEY `IDX_Statement-Statement_ID` (`Statement_ID`) USING BTREE,
  KEY `FK_Statement-Statement_Type_ID` (`Statement_Type_ID`) USING BTREE,
  KEY `FK_Statement-Institution_ID` (`Institution_ID`) USING BTREE,
  KEY `FK_Statement-Statement_Status_ID` (`Statement_Status_ID`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
END;

/* Procedure structure for procedure `sp_02_ModifyExistingTables` */

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_02_ModifyExistingTables`()
BEGIN
ALTER TABLE `user`
CHANGE COLUMN `id_user` `Staff_ID`  INT NOT NULL AUTO_INCREMENT FIRST ,
CHANGE COLUMN `last_name` `Staff_Name_Last`  VARCHAR(30) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Staff_ID`,
CHANGE COLUMN `first_name` `Staff_Name_First`  VARCHAR(30) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Staff_Name_Last`,
CHANGE COLUMN `staff` `Initials`  VARCHAR(3) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Staff_Name_First`,
CHANGE COLUMN `login` `Login_ID`  VARCHAR(25) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Initials`,
CHANGE COLUMN `pwd` `Password`  VARCHAR(50) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Login_ID`,
CHANGE COLUMN `mail` `Email`  VARCHAR(50) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Password`,
CHANGE COLUMN `date_registered` `Stamp_Date`  INT NULL DEFAULT 0 AFTER `Email`,
ADD COLUMN `Stamp_User`  INT NULL DEFAULT 1 AFTER `Stamp_Date`,
ADD COLUMN `Last_Edit_Date`  INT NULL DEFAULT 0 AFTER `Stamp_User`,
ADD COLUMN `Last_Edit_User`  INT NULL DEFAULT 1 AFTER `Last_Edit_Date`,
ADD COLUMN `Staff_Status_ID`  INT NOT NULL DEFAULT 1 AFTER `Last_Edit_User`,
MODIFY COLUMN `type`  enum('admin','editor','reader') CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `Staff_Status_ID`,
MODIFY COLUMN `removed`  tinyint(4) NULL AFTER `type`,
MODIFY COLUMN `is_partner`  bit(1) NULL AFTER `removed`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`Staff_ID`),
ADD UNIQUE INDEX `IDX_Staff-Staff_ID` (`Staff_ID`) USING BTREE ,
ADD UNIQUE INDEX `IDX_Staff-Login_ID` (`Login_ID`) USING BTREE ,
ADD UNIQUE INDEX `IDX_Staff-Email` (`Email`) USING BTREE ,
ADD UNIQUE INDEX `IDX_Staff-Initials` (`Initials`) USING BTREE ;
ALTER TABLE `user` ADD CONSTRAINT `FK_Staff_Status-Staff_Status_ID` FOREIGN KEY (`Staff_Status_ID`) REFERENCES `Staff_Status` (`Staff_Status_ID`) ON DELETE RESTRICT;
ALTER TABLE `user`
RENAME AS `Staff`;
ALTER TABLE `Institution` ADD CONSTRAINT `FK_Institution-Stamp_User` FOREIGN KEY (`Stamp_User`) REFERENCES `Staff` (`Staff_ID`) ON DELETE RESTRICT;
ALTER TABLE `Institution` ADD CONSTRAINT `FK_Institution-Last_Edit_User` FOREIGN KEY (`Last_Edit_User`) REFERENCES `Staff` (`Staff_ID`) ON DELETE RESTRICT;
ALTER TABLE `Statement` ADD CONSTRAINT `FK_Statement-Stamp_User` FOREIGN KEY (`Stamp_User`) REFERENCES `Staff` (`Staff_ID`) ON DELETE RESTRICT;
ALTER TABLE `Statement` ADD CONSTRAINT `FK_Statement-Last_Edit_User` FOREIGN KEY (`Last_Edit_User`) REFERENCES `Staff` (`Staff_ID`) ON DELETE RESTRICT;
ALTER TABLE `entity_type`
CHANGE COLUMN `id_entity_type` `Client_Type_ID`  INT NOT NULL AUTO_INCREMENT FIRST ,
CHANGE COLUMN `name_entity_type` `Client_Type_Label`  VARCHAR(50) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Client_Type_ID`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`Client_Type_ID`),
ADD UNIQUE INDEX `IDX_Client_Type-Client_Type_ID` (`Client_Type_ID`) USING BTREE ,
ADD UNIQUE INDEX `IDX_Client_Type-Client_Type_Label` (`Client_Type_Label`) USING BTREE ;
ALTER TABLE `entity_type`
RENAME AS `Client_Type`;
	UPDATE Client
	SET ID_Occupation = 193
	WHERE id_Occupation = 194;
	UPDATE Client
	SET ID_Occupation_spouse = 193
	WHERE id_Occupation_spouse = 194;
	DELETE from Occupation
	WHERE id_Occupation = 194;
	UPDATE Client
	SET ID_Occupation = 64
	WHERE id_Occupation = 300;
	UPDATE Client
	SET ID_Occupation_spouse = 64
	WHERE id_Occupation_spouse = 300;
	DELETE from Occupation
	WHERE id_Occupation = 300;
	UPDATE Client
	SET ID_Occupation = 65
	WHERE id_Occupation = 141;
	UPDATE Client
	SET ID_Occupation_spouse = 65
	WHERE id_Occupation_spouse = 141;
	DELETE from Occupation
	WHERE id_Occupation = 141;
	UPDATE Client_Type AS t
	SET t.Client_Type_Label = fn_Utility_CleanNames(t.Client_Type_Label, ' ', true)
	WHERE t.Client_Type_Label IS NOT NULL;
	UPDATE Client_Type AS t
	SET t.Client_Type_Label = fn_Utility_CleanNames(t.Client_Type_Label, ',', false)
	WHERE t.Client_Type_Label IS NOT NULL;
	UPDATE Client_Type AS t
	SET t.Client_Type_Label = fn_Utility_CleanNames(t.Client_Type_Label, '/', true)
	WHERE t.Client_Type_Label IS NOT NULL;
	UPDATE Client_Type AS t
	SET t.Client_Type_Label = fn_Utility_CleanNames(t.Client_Type_Label, '-', true)
	WHERE t.Client_Type_Label IS NOT NULL;
ALTER TABLE `Occupation`
CHANGE COLUMN `id_Occupation` `Occupation_ID`  INT NOT NULL AUTO_INCREMENT FIRST ,
CHANGE COLUMN `name_Occupation` `Occupation_Name`  VARCHAR(50) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Occupation_ID`,
DROP PRIMARY KEY,
ADD PRIMARY KEY(`Occupation_ID`),
ADD UNIQUE INDEX `IDX_Occupation-Occupation_ID` (`Occupation_ID`) USING BTREE ,
ADD UNIQUE INDEX `IDX_Occupation-Occupation_Name` (`Occupation_Name`) USING BTREE ;
UPDATE Occupation
set Occupation_Name = LCASE(Occupation_Name);
UPDATE Occupation
set Occupation_Name = fn_Utility_CleanNames(Occupation_Name, ' ', true);
UPDATE Occupation
set Occupation_Name = fn_Utility_CleanNames(Occupation_Name, '/', true);
UPDATE Occupation
set Occupation_Name = fn_Utility_CleanNames(Occupation_Name, ',', true);
UPDATE Occupation
set Occupation_Name = fn_Utility_CleanNames(Occupation_Name, '-', true);
ALTER TABLE `Client`
CHANGE COLUMN `id_client` `Client_ID`  INT NOT NULL AUTO_INCREMENT FIRST ,
CHANGE COLUMN `company_name` `Company_Name`  VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL ,
CHANGE COLUMN `last_name` `Client_Name_Last`  VARCHAR(25) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Company_Name`,
CHANGE COLUMN `first_name` `Client_Name_First`  VARCHAR(25) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Client_Name_Last`,
CHANGE COLUMN `ssn_ein` `Client_SSN_EIN`  VARCHAR(11) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Client_Name_First`,
CHANGE COLUMN `id_Occupation` `Client_Occupation_ID`  INT NOT NULL AFTER `Client_SSN_EIN`,
CHANGE COLUMN `last_spouse` `Spouse_Name_Last`  VARCHAR(25) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Client_Occupation_ID`,
CHANGE COLUMN `first_spouse` `Spouse_Name_First`  VARCHAR(25) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Spouse_Name_Last`,
CHANGE COLUMN `ssn_spouse` `Spouse_SSN_EIN`  VARCHAR(11) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Spouse_Name_First`,
CHANGE COLUMN `id_Occupation_spouse` `Spouse_Occupation_ID`  INT NOT NULL AFTER `Spouse_SSN_EIN`,
CHANGE COLUMN `billing_client` `Billing_ID`  VARCHAR(50) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL ,
CHANGE COLUMN `doc_id` `Document_ID`  VARCHAR(50) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Billing_ID`,
CHANGE COLUMN `month` `Month_ID`  INT NULL DEFAULT 0 AFTER `Document_ID`,
CHANGE COLUMN `initial_year` `Initial_Year`  INT NULL DEFAULT 0 AFTER `Month_ID`,
CHANGE COLUMN `retainer_year` `Retainer_Year`  INT NULL ,
CHANGE COLUMN `principal` `Principal_Name`  VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL ,
CHANGE COLUMN `id_referred_by` `Referred_By_ID`  INT NOT NULL AFTER `Principal_Name`,
CHANGE COLUMN `inactive_date` `Inactive_Date`  INT NOT NULL AFTER `Referred_By_ID`,
CHANGE COLUMN `notes` `Client_Notes`  text CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Inactive_Date`,
CHANGE COLUMN `input_date` `Stamp_Date`  INT NULL DEFAULT 0 ,
CHANGE COLUMN `entered_by` `Stamp_User`  INT NULL AFTER `Stamp_Date`,
CHANGE COLUMN `last_update_date` `Last_Edit_Date`  INT NULL DEFAULT 0 AFTER `Stamp_User`,
CHANGE COLUMN `last_update_user` `Last_Edit_User`  INT NULL AFTER `Last_Edit_Date`,
MODIFY COLUMN `status`  enum('active','inactive') CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL DEFAULT 'active' AFTER `Last_Edit_User`,
MODIFY COLUMN `form_id`  VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `status`,
MODIFY COLUMN `Staff`  VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `form_id`,
MODIFY COLUMN `retainer`  VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `partner`,
MODIFY COLUMN `removed`  tinyint(4) NOT NULL AFTER `accounting`,
MODIFY COLUMN `state`  VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `removed`,
ADD COLUMN `Client_Type_ID`  INT NULL AFTER `Client_ID`,
ADD COLUMN `Staff_ID`  INT NULL AFTER `Spouse_Occupation_ID`,
ADD COLUMN `Partner_ID`  INT NULL AFTER `Staff_ID`,
ADD COLUMN `Retainer_Status_ID`  INT NULL AFTER `Initial_Year`,
ADD COLUMN `Accounting_Software_ID`  INT NULL AFTER `Retainer_Year`,
ADD COLUMN `Client_Status_ID`  INT NOT NULL DEFAULT 4 AFTER `Client_Notes`,
ADD COLUMN `Organizer`  bit NOT NULL DEFAULT b'0' AFTER `Client_Status_ID`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`Client_ID`),
ADD UNIQUE INDEX `IDX_Client-Client_ID` (`Client_ID`) USING BTREE ,
ADD INDEX `IDX_Client-Company_Name` (`Company_Name`) USING BTREE ,
ADD INDEX `IDX_Client-Client_Name` (`Client_Name_Last`, `Client_Name_First`) USING BTREE ;
ALTER TABLE `Client` ADD CONSTRAINT `FK_Client-Client_Occupation_ID` FOREIGN KEY (`Client_Occupation_ID`) REFERENCES `Occupation` (`Occupation_ID`) ON DELETE RESTRICT;
ALTER TABLE `Client` ADD CONSTRAINT `FK_Client-Spouse_Occupation_ID` FOREIGN KEY (`Spouse_Occupation_ID`) REFERENCES `Occupation` (`Occupation_ID`) ON DELETE RESTRICT;
ALTER TABLE `Client` ADD CONSTRAINT `FK_Client-Staff_ID` FOREIGN KEY (`Staff_ID`) REFERENCES `Staff` (`Staff_ID`) ON DELETE RESTRICT;
ALTER TABLE `Client` ADD CONSTRAINT `FK_Client-Client_Type_ID` FOREIGN KEY (`Client_Type_ID`) REFERENCES `Client_Type` (`Client_Type_ID`) ON DELETE RESTRICT;
ALTER TABLE `Client` ADD CONSTRAINT `FK_Client-Partner_ID` FOREIGN KEY (`Partner_ID`) REFERENCES `Staff` (`Staff_ID`) ON DELETE RESTRICT;
ALTER TABLE `Client` ADD CONSTRAINT `FK_Client-Account_Software_ID` FOREIGN KEY (`Accounting_Software_ID`) REFERENCES `accounting_software` (`Accounting_Software_ID`) ON DELETE RESTRICT;
ALTER TABLE `Client` ADD CONSTRAINT `FK_Client-Status_ID` FOREIGN KEY (`Client_Status_ID`) REFERENCES `client_status` (`Client_Status_ID`) ON DELETE RESTRICT;
ALTER TABLE `Client` ADD CONSTRAINT `FK_Client-Stamp_User` FOREIGN KEY (`Stamp_User`) REFERENCES `Staff` (`Staff_ID`) ON DELETE RESTRICT;
ALTER TABLE `Client` ADD CONSTRAINT `FK_Client-Last_Edit_User` FOREIGN KEY (`Last_Edit_User`) REFERENCES `Staff` (`Staff_ID`) ON DELETE RESTRICT;
ALTER TABLE `Client`
MODIFY COLUMN `Company_Name`  VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `Client_Type_ID`,
MODIFY COLUMN `Client_Name_Last`  VARCHAR(25) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `Company_Name`,
MODIFY COLUMN `Client_Name_First`  VARCHAR(25) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `Client_Name_Last`,
MODIFY COLUMN `Client_SSN_EIN`  VARCHAR(11) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `Client_Name_First`,
MODIFY COLUMN `Client_Occupation_ID`  INT NULL AFTER `Client_SSN_EIN`,
MODIFY COLUMN `Spouse_Name_Last`  VARCHAR(25) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `Client_Occupation_ID`,
MODIFY COLUMN `Spouse_Name_First`  VARCHAR(25) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `Spouse_Name_Last`,
MODIFY COLUMN `Spouse_SSN_EIN`  VARCHAR(11) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `Spouse_Name_First`,
MODIFY COLUMN `Spouse_Occupation_ID`  INT NULL AFTER `Spouse_SSN_EIN`,
MODIFY COLUMN `status`  enum('active','inactive') CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL DEFAULT 'active' AFTER `Last_Edit_User`,
MODIFY COLUMN `form_id`  VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `status`,
MODIFY COLUMN `Staff`  VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `form_id`,
MODIFY COLUMN `entity`  VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `Staff`,
MODIFY COLUMN `partner`  enum('MG','RG','MJG') CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `entity`,
MODIFY COLUMN `retainer`  VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `partner`,
MODIFY COLUMN `Billing_ID`  VARCHAR(50) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `retainer`,
MODIFY COLUMN `Document_ID`  VARCHAR(50) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `Billing_ID`,
MODIFY COLUMN `accounting`  VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `Accounting_Software_ID`,
MODIFY COLUMN `removed`  tinyint(4) NULL AFTER `accounting`,
MODIFY COLUMN `state`  VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `removed`,
MODIFY COLUMN `Principal_Name`  VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `state`,
MODIFY COLUMN `Referred_By_ID`  INT NULL AFTER `Principal_Name`,
MODIFY COLUMN `Inactive_Date`  INT NULL AFTER `Referred_By_ID`,
MODIFY COLUMN `Client_Notes`  text CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `Inactive_Date`;
ALTER TABLE `Client` ADD CONSTRAINT `FK_Client-Retainer_Status_ID` FOREIGN KEY (`Retainer_Status_ID`) REFERENCES `Retainer_Status` (`Retainer_Status_ID`) ON DELETE RESTRICT;
ALTER TABLE `Client` ADD CONSTRAINT `FK_Client-Client_Status_ID` FOREIGN KEY (`Client_Status_ID`) REFERENCES `Client_Status` (`Client_Status_ID`) ON DELETE RESTRICT;
ALTER TABLE `Statement` ADD CONSTRAINT `FK_Statement-Client_ID` FOREIGN KEY (`Client_ID`) REFERENCES `Client` (`Client_ID`) ON DELETE RESTRICT;
ALTER TABLE `form`
CHANGE COLUMN `form_id` `Tax_Form_ID`  INT NOT NULL AUTO_INCREMENT FIRST ,
CHANGE COLUMN `form_name` `Tax_Form_Name`  VARCHAR(50) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Tax_Form_ID`,
CHANGE COLUMN `initial_due_date` `Initial_Due_Date`  tinyint(1) NOT NULL AFTER `Tax_Form_Name`,
CHANGE COLUMN `extension1` `Extension_1`  tinyint(1) NOT NULL AFTER `Initial_Due_Date`,
CHANGE COLUMN `extension2` `Extension_2`  tinyint(1) NOT NULL AFTER `Extension_1`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`Tax_Form_ID`),
ADD UNIQUE INDEX `IDX_Tax_Form-Tax_Form_ID` (`Tax_Form_ID`) USING BTREE ,
ADD UNIQUE INDEX `IDX_Tax_Form-Tax_Form_Name` (`Tax_Form_Name`) USING BTREE ;
ALTER TABLE `form`
RENAME AS `Tax_Form`;
UPDATE Tax_Form
SET Tax_Form_Name = UCASE(Tax_Form_Name);
ALTER TABLE `status_name`
CHANGE COLUMN `status_id` `Tax_Return_Status_ID`  INT NOT NULL AUTO_INCREMENT FIRST ,
CHANGE COLUMN `status_name` `Tax_Return_Status_Label`  VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Tax_Return_Status_ID`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`Tax_Return_Status_ID`),
ADD UNIQUE INDEX `IDX_Tax_Return_Status-Tax_Return_Status_ID` (`Tax_Return_Status_ID`) USING BTREE ,
ADD UNIQUE INDEX `IDX_Tax_Return_Status-Tax_Return_Status_Label` (`Tax_Return_Status_Label`) USING BTREE ;
ALTER TABLE `status_name`
RENAME AS `Tax_Return_Status`;
UPDATE Tax_Return_Status
SET Tax_Return_Status_Label = LCASE(Tax_Return_Status_Label);
UPDATE Tax_Return_Status
SET Tax_Return_Status_Label = fn_Utility_CleanNames(Tax_Return_Status_Label, ' ', true);
UPDATE Tax_Return_Status
SET Tax_Return_Status_Label = fn_Utility_CleanNames(Tax_Return_Status_Label, '/', true);
UPDATE Tax_Return_Status
SET Tax_Return_Status_Label = fn_Utility_CleanNames(Tax_Return_Status_Label, '-', true);
UPDATE Tax_Return_Status
SET Tax_Return_Status_Label = fn_Utility_CleanNames(Tax_Return_Status_Label, '\'', true);
UPDATE Tax_Return_Status
SET Tax_Return_Status_Label = fn_Utility_CleanNames(Tax_Return_Status_Label, ',', true);
ALTER TABLE `Instructions`
CHANGE COLUMN `id_instructions` `Instructions_ID`  INT NOT NULL AUTO_INCREMENT FIRST ,
CHANGE COLUMN `name_instructions` `Instructions_Label`  VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Instructions_ID`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`Instructions_ID`),
ADD UNIQUE INDEX `IDX_Instructions-Instructions_ID` (`Instructions_ID`) USING BTREE ,
ADD UNIQUE INDEX `IDX_Instructions-Instructions_Label` (`Instructions_Label`) USING BTREE;
UPDATE Instructions
SET Instructions_Label = LCASE(Instructions_Label);
UPDATE Instructions
SET Instructions_Label = fn_Utility_CleanNames(Instructions_Label, ' ', true);
UPDATE Instructions
SET Instructions_Label = fn_Utility_CleanNames(Instructions_Label, '/', true);
UPDATE Instructions
SET Instructions_Label = fn_Utility_CleanNames(Instructions_Label, '-', true);
UPDATE Instructions
SET Instructions_Label = fn_Utility_CleanNames(Instructions_Label, '\'', true);
UPDATE Instructions
SET Instructions_Label = fn_Utility_CleanNames(Instructions_Label, ',', true);
ALTER TABLE `Tax_Return`
CHANGE COLUMN `id_tax_return` `Tax_Return_ID`  INT NOT NULL AUTO_INCREMENT FIRST ,
CHANGE COLUMN `id_client` `Client_ID`  INT NOT NULL AFTER `Tax_Return_ID`,
CHANGE COLUMN `form_id` `Tax_Form_ID`  INT NOT NULL ,
CHANGE COLUMN `tax_year` `Tax_Year`  int(4) NOT NULL AFTER `Tax_Form_ID`,
CHANGE COLUMN `tax_quarter` `Tax_Quarter`  int(1) NOT NULL AFTER `Tax_Year`,
CHANGE COLUMN `tax_month` `Tax_Month`  int(2) NOT NULL AFTER `Tax_Quarter`,
CHANGE COLUMN `status` `Tax_Return_Status_ID`  int(255) NOT NULL AFTER `Tax_Month`,
CHANGE COLUMN `id_instructions` `Instructions_ID`  INT NOT NULL AFTER `Tax_Return_Status_ID`,
CHANGE COLUMN `notes` `Tax_Return_Notes`  text CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Instructions_ID`,
CHANGE COLUMN `sent_date` `Date_Sent`  INT NOT NULL ,
CHANGE COLUMN `released_date` `Date_Released`  INT NOT NULL AFTER `Date_Sent`,
CHANGE COLUMN `date` `990PF_Date`  INT NULL DEFAULT NULL ,
CHANGE COLUMN `last_update_date` `Last_Edit_Date`  int(4) NOT NULL ,
CHANGE COLUMN `last_update_user` `Last_Edit_User`  int(4) NOT NULL AFTER `Last_Edit_Date`,
MODIFY COLUMN `partner`  VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `Last_Edit_User`,
MODIFY COLUMN `lock`  tinyint(4) NULL AFTER `partner`,
MODIFY COLUMN `removed`  tinyint(4) NULL AFTER `lock`,
ADD COLUMN `Partner_ID`  INT NULL AFTER `Client_ID`,
ADD COLUMN `Staff_ID`  INT NULL AFTER `Partner_ID`,
ADD COLUMN `Reviewer_1_ID`  INT NULL AFTER `Tax_Return_Notes`,
ADD COLUMN `Reviewer_2_ID`  INT NULL AFTER `Reviewer_1_ID`,
ADD COLUMN `Is_Locked`  bit NOT NULL DEFAULT b'0' AFTER `Date_Released`,
ADD COLUMN `Stamp_Date`  INT NULL AFTER `Is_Locked`,
ADD COLUMN `Stamp_User`  INT NULL AFTER `Stamp_Date`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`Tax_Return_ID`),
ADD UNIQUE INDEX `IDX_Tax_Return-Tax_Return_ID` (`Tax_Return_ID`) USING BTREE ,
ADD INDEX `IDX_Tax_Return-Tax_Year` (`Tax_Year`) USING BTREE ,
ADD INDEX `IDX_Tax_Return-Tax_Month` (`Tax_Month`) USING BTREE ,
ADD INDEX `IDX_Tax_Return-Tax_Quarter` (`Tax_Quarter`) USING BTREE ;
ALTER TABLE `Tax_Return` ADD CONSTRAINT `FK_Tax_Return-Client_ID` FOREIGN KEY (`Client_ID`) REFERENCES `Client` (`Client_ID`) ON DELETE RESTRICT;
ALTER TABLE `Tax_Return` ADD CONSTRAINT `FK_Tax_Return-Partner_ID` FOREIGN KEY (`Partner_ID`) REFERENCES `Staff` (`Staff_ID`) ON DELETE RESTRICT;
ALTER TABLE `Tax_Return` ADD CONSTRAINT `FK_Tax_Return-Staff_ID` FOREIGN KEY (`Staff_ID`) REFERENCES `Staff` (`Staff_ID`) ON DELETE RESTRICT;
ALTER TABLE `Tax_Return` ADD CONSTRAINT `FK_Tax_Return-Stamp_User` FOREIGN KEY (`Stamp_User`) REFERENCES `Staff` (`Staff_ID`) ON DELETE RESTRICT;
ALTER TABLE `Tax_Return` ADD CONSTRAINT `FK_Tax_Return-Last_Edit_User` FOREIGN KEY (`Last_Edit_User`) REFERENCES `Staff` (`Staff_ID`) ON DELETE RESTRICT;
ALTER TABLE `Tax_Return` ADD CONSTRAINT `FK_Tax_Return-Tax_Form_ID` FOREIGN KEY (`Tax_Form_ID`) REFERENCES `tax_form` (`Tax_Form_ID`) ON DELETE RESTRICT;
ALTER TABLE `Tax_Return` ADD CONSTRAINT `FK_Tax_Return-Tax_Return_Status_ID` FOREIGN KEY (`Tax_Return_Status_ID`) REFERENCES `Tax_Return_Status` (`Tax_Return_Status_ID`) ON DELETE RESTRICT;
ALTER TABLE `Tax_Return` ADD CONSTRAINT `FK_Tax_Return-Instructions_ID` FOREIGN KEY (`Instructions_ID`) REFERENCES `instructions` (`Instructions_ID`) ON DELETE RESTRICT;
ALTER TABLE `Tax_Return` ADD CONSTRAINT `FK_Tax_Return-Reviewer_1_ID` FOREIGN KEY (`Reviewer_1_ID`) REFERENCES `Staff` (`Staff_ID`) ON DELETE RESTRICT;
ALTER TABLE `Tax_Return` ADD CONSTRAINT `FK_Tax_Return-Reviewer_2_ID` FOREIGN KEY (`Reviewer_2_ID`) REFERENCES `Staff` (`Staff_ID`) ON DELETE RESTRICT;
ALTER TABLE `form_initial_date`
CHANGE COLUMN `form_id` `Tax_Form_ID`  INT NOT NULL FIRST ,
CHANGE COLUMN `initial_date` `Initial_Date`  text CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Tax_Form_ID`,
CHANGE COLUMN `type` `Type`  enum('init','ext1','ext2') CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL AFTER `Initial_Date`,
ADD INDEX `IDX_Tax_Form_Initial_Date-Tax_Form_ID` (`Tax_Form_ID`) USING BTREE ,
RENAME AS `Tax_Form_Initial_Date`;
ALTER TABLE `Tax_Form_Initial_Date` ADD CONSTRAINT `FK_Tax_Form_Initial_Date-Tax_Form_ID` FOREIGN KEY (`Tax_Form_ID`) REFERENCES `Tax_Form` (`Tax_Form_ID`) ON DELETE RESTRICT;
ALTER TABLE `return_status_history`
CHANGE COLUMN `id` `Tax_Return_Status_History_ID`  INT NOT NULL AUTO_INCREMENT FIRST ,
CHANGE COLUMN `tax_return_id` `Tax_Return_ID`  INT NOT NULL AFTER `Tax_Return_Status_History_ID`,
CHANGE COLUMN `transmitted` `Transmitted`  VARCHAR(10) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL DEFAULT NULL ,
CHANGE COLUMN `date` `Stamp_Date`  INT NULL DEFAULT NULL ,
CHANGE COLUMN `modified_by` `Last_Edit_User`  INT NOT NULL ,
CHANGE COLUMN `modified_date` `Last_Edit_Date`  INT NOT NULL AFTER `Last_Edit_User`,
MODIFY COLUMN `Staff`  VARCHAR(10) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL DEFAULT NULL AFTER `Last_Edit_Date`,
MODIFY COLUMN `review_status`  VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL DEFAULT NULL AFTER `Staff`,
MODIFY COLUMN `status_name`  VARCHAR(100) CHARACTER SET cp1251 COLLATE cp1251_general_ci NULL AFTER `reviewer2`,
ADD COLUMN `Tax_Return_Status_ID`  INT NULL AFTER `Tax_Return_ID`,
ADD COLUMN `Staff_ID`  INT NULL AFTER `Tax_Return_Status_ID`,
ADD COLUMN `Reviewer_1_ID`  INT NULL AFTER `Transmitted`,
ADD COLUMN `Reviewer_2_ID`  INT NULL AFTER `Reviewer_1_ID`,
ADD COLUMN `Stamp_User`  INT NULL AFTER `Stamp_Date`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`Tax_Return_Status_History_ID`),
ADD UNIQUE INDEX `IDX_Tax_Return_Status_History-Tax_Return_Status_History_ID` (`Tax_Return_Status_History_ID`) USING BTREE ,
RENAME AS `Tax_Return_Status_History`;
ALTER TABLE `Tax_Return_Status_History` ADD CONSTRAINT `FK_Tax_Return_Status_History-Tax_Return_ID` FOREIGN KEY (`Tax_Return_ID`) REFERENCES `Tax_Return` (`Tax_Return_ID`) ON DELETE RESTRICT;
ALTER TABLE `Tax_Return_Status_History` ADD CONSTRAINT `FK_Tax_Return_Status_History-Staff_ID` FOREIGN KEY (`Staff_ID`) REFERENCES `Staff` (`Staff_ID`) ON DELETE RESTRICT;
ALTER TABLE `Tax_Return_Status_History` ADD CONSTRAINT `FK_Tax_Return_Status_History-Reviewer_1_ID` FOREIGN KEY (`Reviewer_1_ID`) REFERENCES `Staff` (`Staff_ID`) ON DELETE RESTRICT;
ALTER TABLE `Tax_Return_Status_History` ADD CONSTRAINT `FK_Tax_Return_Status_History-Reviewer_2_ID` FOREIGN KEY (`Reviewer_2_ID`) REFERENCES `Staff` (`Staff_ID`) ON DELETE RESTRICT;
ALTER TABLE `Tax_Return_Status_History` ADD CONSTRAINT `FK_Tax_Return_Status_History-Last_Edit_User` FOREIGN KEY (`Last_Edit_User`) REFERENCES `Staff` (`Staff_ID`) ON DELETE RESTRICT;
ALTER TABLE `Tax_Return_Status_History` ADD CONSTRAINT `FK_Tax_Return_Status_History-Stamp_User` FOREIGN KEY (`Stamp_User`) REFERENCES `Staff` (`Staff_ID`) ON DELETE RESTRICT;
ALTER TABLE `Tax_Return_Status_History` ADD CONSTRAINT `FK_Tax_Return_Status_History-Tax_Return_Status_ID` FOREIGN KEY (`Tax_Return_Status_ID`) REFERENCES `Tax_Return_Status` (`Tax_Return_Status_ID`) ON DELETE RESTRICT;
END;

/* Procedure structure for procedure `sp_03_UpdateStaffInformation` */
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_03_UpdateStaffInformation`()
BEGIN
INSERT INTO Staff_Role_Link
(
	Staff_ID,
	Role_ID
)
SELECT	Staff_ID,
				1
FROM		Staff
WHERE		type = 'admin';
INSERT INTO Staff_Role_Link
(
	Staff_ID,
	Role_ID
)
SELECT	Staff_ID,
				2
FROM		Staff
WHERE		is_partner = 1;
INSERT INTO Staff_Role_Link
(
	Staff_ID,
	Role_ID
)
SELECT	Staff_ID,
				3
FROM		Staff
WHERE		type = 'editor';
INSERT INTO Staff_Role_Link
(
	Staff_ID,
	Role_ID
)
SELECT	Staff_ID,
				4
FROM		Staff
WHERE		type = 'reader';
UPDATE	Staff
SET			Stamp_Date = 1320768253
WHERE		Stamp_Date = 0;
UPDATE 	Staff
SET 		Last_Edit_Date = Stamp_Date,
				Stamp_User = 1,
				Last_Edit_User = 1;
END;

/* Procedure structure for procedure `sp_04_UpdateClientInformation` */
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_04_UpdateClientInformation`()
BEGIN
	-- Set all other status to 'Active' WHERE empty
	UPDATE Client 
	SET status = 'active' 
	WHERE status = '';
	-- Set all to Unknown first
	UPDATE Client 
	SET Client_Status_ID = 4;
	-- Change Active Clients only
	UPDATE Client
	SET Client_Status_ID = 1
	WHERE status = 'active';
	-- change Inactive Clients only
	UPDATE Client
	SET Client_Status_ID = 2
	WHERE status = 'inactive';
	-- change Deleted Clients Only
	UPDATE Client
	SET Client_Status_ID = 3
	WHERE removed = 1;
	-- change Inactive_Date for all active clients
	UPDATE Client 
	SET Inactive_Date = 0
	WHERE status = 'active';
	-- UPDATE newly created Last_Edit_User field
	UPDATE Client
	SET Last_Edit_User = Stamp_User
	WHERE Last_Edit_User = 0;
	-- Set a Default Parter for those missing
	UPDATE Client 
	set partner = 'RG' 
	WHERE partner = '';
	-- Set all other status to 'Active' WHERE empty
	UPDATE Client 
	set status = 'Active' 
	WHERE status = '';
	-- insert necessary records into Client_Tax_Form_Link table
	INSERT INTO Client_Tax_Form_Link
	(
		Client_ID,
		Tax_Form_ID
	)	
	SELECT		c.Client_ID,
						tf.Tax_Form_ID
	FROM 			Tax_Form tf
	LEFT JOIN	Client c
		ON 			c.form_id like tf.Tax_Form_ID
		OR			c.form_id like CONCAT(tf.Tax_Form_ID, ',%')
		OR			c.form_id like CONCAT('%,', tf.Tax_Form_ID, ',%')
		OR			c.form_id like CONCAT('%,', tf.Tax_Form_ID)
	WHERE			c.Client_ID is not null;
	-- Insert necessary records into Client_State_Link table
	INSERT INTO Client_State_Link
	(
		Client_ID,
		State_ID
	)	
	SELECT	c.Client_ID,
					s.State_ID
	FROM		State s
	LEFT JOIN Client c
		ON			c.State like CONCAT('%', s.State_ID, '%')
	WHERE		c.Client_ID is not NULL;
	-- Se Staff_ID's instead of Initials
	UPDATE Client AS c, Staff AS s
	SET c.Staff_ID = s.Staff_ID
	WHERE TRIM(c.staff) = TRIM(s.Initials);
	-- Set Partner_ID's instead of initials
	UPDATE Client AS c, Staff AS s
	SET c.Partner_ID = s.Staff_ID
	WHERE TRIM(c.partner) = TRIM(s.Initials);
	-- Set Client Type ID's instead of entity names
	UPDATE Client AS c, Client_Type AS t
	SET c.Client_Type_ID = t.Client_Type_ID
	WHERE TRIM(c.entity) = TRIM(t.Client_Type_Label);
	-- fix mispelling of QuickBooks
	UPDATE Client
	SET accounting = 'Quickbooks'
	WHERE TRIM(accounting) = 'Quicbooks';
	-- UPDATE Accounting_Software_ID's instead of software names
	UPDATE Client
	SET Accounting_Software_ID = 1
	WHERE TRIM(accounting) = '-1' or TRIM(accounting) = '' or TRIM(accounting) is null;
	-- Update the Accounting Software IDs
	UPDATE Client c, Accounting_Software s
	set c.Accounting_Software_ID = s.Accounting_Software_ID
	WHERE c.accounting = s.Accounting_Software_Name;
	-- Set all to Not On Retainer
	UPDATE Client
	SET Retainer_Status_ID = 1;
	-- Changes those that are On Retainer only
	UPDATE Client
	SET Retainer_Status_ID = 2
	WHERE retainer = 'yes';
END;

/* Procedure structure for procedure `sp_05_UpdateTaxReturnInformation` */
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_05_UpdateTaxReturnInformation`()
BEGIN
	-- Populate Partner ID's
	UPDATE Tax_Return AS tr, Staff AS s
	SET tr.Partner_ID = s.Staff_ID
	WHERE tr.partner = s.Initials;
	-- Populate Staff ID's
	UPDATE Tax_Return AS tr, Staff AS s
	SET tr.Staff_ID = s.Staff_ID
	WHERE tr.staff_tax = s.Initials;
	-- Populate Reviewer 1 ID's
	UPDATE Tax_Return AS tr, Staff AS s
	SET tr.Reviewer_1_ID = s.Staff_ID
	WHERE tr.reviewer1_tax = s.Initials;
	-- Populate Reviewer 2 ID's
	UPDATE Tax_Return AS tr, Staff AS s
	SET tr.Reviewer_2_ID = s.Staff_ID
	WHERE tr.reviewer2_tax = s.Initials;
	-- Update Is_Locked
	UPDATE Tax_Return AS tr
	SET tr.Is_Locked = 1
	WHERE tr.lock <> 0;
	-- Update Tax_Return_Status_ID from 'removed'
	UPDATE Tax_Return AS tr
	SET tr.Tax_Return_Status_ID = 2
	WHERE tr.removed <> 0;
	-- Populate Stamp User if NULL
	UPDATE Tax_Return AS tr
	SET tr.Stamp_User = tr.Last_Edit_User
	where tr.Stamp_User IS NULL or tr.Stamp_User = 0;
	-- Populate Stamp Date if NULL
	UPDATE Tax_Return AS tr
	SET tr.Stamp_Date = tr.Last_Edit_Date
	where tr.Stamp_Date IS NULL or tr.Stamp_User = 0;
	-- Fix Date_Sent
	UPDATE Tax_Return AS tr
	SET tr.Date_Sent = 0
	WHERE tr.Date_Sent < 0;
	-- Fix Date Released
	UPDATE Tax_Return AS tr
	SET tr.Date_Released = 0
	WHERE tr.Date_Released < 0;
	-- Fix Tax_Form_ID
	UPDATE Tax_Return AS tr
	SET tr.Tax_Form_ID = 0
	WHERE tr.Tax_Form_ID < 0;
	-- Fix Tax YEAR
	UPDATE Tax_Return AS tr
	SET tr.Tax_Year = 0
	WHERE tr.Tax_Year < 0;
END;

/* Procedure structure for procedure `sp_06_UpdateTaxReturnStatusHistoryInformation` */

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_06_UpdateTaxReturnStatusHistoryInformation`()
BEGIN
	-- Populate Staff ID's
	UPDATE Tax_Return_Status_History AS h, Staff AS s
	SET h.Staff_ID = s.Staff_ID
	WHERE h.staff = s.Initials;
	-- Populate Reviewer 1 ID's
	UPDATE Tax_Return_Status_History AS h, Staff AS s
	SET h.Reviewer_1_ID = s.Staff_ID
	WHERE h.reviewer1 = s.Initials;
	-- Populate Reviewer 2 ID's
	UPDATE Tax_Return_Status_History AS h, Staff AS s
	SET h.Reviewer_2_ID = s.Staff_ID
	WHERE h.reviewer2 = s.Initials;
	-- Populate Tax Return Sstatus ID's
	UPDATE Tax_Return_Status_History AS h
	SET h.Tax_Return_Status_ID = h.status_name;
	-- Populate Stamp User
	UPDATE Tax_Return_Status_History AS h
	SET h.Stamp_User = h.Last_Edit_User
	WHERE h.Stamp_User IS NULL
	OR	h.Stamp_User < 1;
	-- Populate Stamp Date
	UPDATE Tax_Return_Status_History AS h
	SET h.Stamp_Date = h.Last_Edit_Date
	WHERE h.Stamp_Date IS NULL
	OR	h.Stamp_Date < 1;
	-- Populate Last Edit User
	UPDATE Tax_Return_Status_History AS h
	SET h.Last_Edit_User = h.Stamp_User
	WHERE h.Last_Edit_User IS NULL
	OR	h.Last_Edit_User < 1;
	-- Populate Stamp Date
	UPDATE Tax_Return_Status_History AS h
	SET h.Last_Edit_Date = h.Stamp_Date
	WHERE h.Last_Edit_Date IS NULL
	OR	h.Last_Edit_Date < 1;
	-- Clean-up Transmitted records
	UPDATE Tax_Return_Status_History AS h
	SET h.Transmitted = fn_Utility_CleanNames(h.Transmitted, ' ', true)
	WHERE h.Transmitted IS NOT NULL;
	UPDATE Tax_Return_Status_History AS h
	SET h.Transmitted = fn_Utility_CleanNames(h.Transmitted, ',', true)
	WHERE h.Transmitted IS NOT NULL;
	UPDATE Tax_Return_Status_History AS h
	SET h.Transmitted = fn_Utility_CleanNames(h.Transmitted, '-', true)
	WHERE h.Transmitted IS NOT NULL;
	UPDATE Tax_Return_Status_History AS h
	SET h.Transmitted = fn_Utility_CleanNames(h.Transmitted, '/', true)
	WHERE h.Transmitted IS NOT NULL;
END;

/*Table structure for table `client_info` */

DROP TABLE IF EXISTS `client_info`;

CREATE TABLE  `client_info`(
 `Client_ID` int(11) ,
 `Client_Name` varchar(100) ,
 `Company_Name` varchar(100) ,
 `Client_Name_Last` varchar(25) ,
 `Client_Name_First` varchar(25) ,
 `Client_Name_Full` varchar(52) ,
 `Client_Type_ID` int(11) ,
 `Client_Type` varchar(50) ,
 `Client_SSN_EIN` varchar(11) ,
 `Client_Occupation_ID` int(11) ,
 `Client_Occupation` varchar(50) ,
 `Spouse_Name_Full` varchar(52) ,
 `Spouse_Name_Last` varchar(25) ,
 `Spouse_Name_First` varchar(25) ,
 `Spouse_SSN_EIN` varchar(11) ,
 `Spouse_Occupation_ID` int(11) ,
 `Spouse_Occupation` varchar(50) ,
 `Tax_Form_IDs` longblob ,
 `Tax_Forms` longtext ,
 `States` varchar(341) ,
 `Partner_ID` bigint(11) ,
 `Partner_Name_Last` varchar(30) ,
 `Partner_Name_First` varchar(30) ,
 `Partner_Name_Full` varchar(62) ,
 `Partner_Initials` varchar(3) ,
 `Partner_Login_ID` varchar(25) ,
 `Staff_ID` bigint(11) ,
 `Staff_Name_Last` varchar(30) ,
 `Staff_Name_First` varchar(30) ,
 `Staff_Name_Full` varchar(62) ,
 `Staff_Initials` varchar(3) ,
 `Staff_Login_ID` varchar(25) ,
 `Client_Status_ID` int(11) ,
 `Client_Status` varchar(25) ,
 `Client_Notes` longtext ,
 `Month_ID` int(11) ,
 `Organizer` int(1) ,
 `Organizer_Label` varchar(3) ,
 `Month` varchar(64) ,
 `Initial_Year` bigint(11) ,
 `Stamp_Date` bigint(11) ,
 `Stamp_User` int(11) ,
 `Stamp_User_Name_Last` varchar(30) ,
 `Stamp_User_Name_First` varchar(30) ,
 `Stamp_User_Name_Full` varchar(62) ,
 `Stamp_User_Login_ID` varchar(25) ,
 `Stamp_User_Initials` varchar(3) ,
 `Last_Edit_Date` bigint(11) ,
 `Last_Edit_User` int(11) ,
 `Last_Edit_User_Name_Last` varchar(30) ,
 `Last_Edit_User_Name_First` varchar(30) ,
 `Last_Edit_User_Name_Full` varchar(62) ,
 `Last_Edit_User_Login_ID` varchar(25) ,
 `Last_Edit_User_Initials` varchar(3) ,
 `Billing_ID` varchar(50) ,
 `Document_ID` varchar(50) ,
 `Retainer_Status_ID` int(11) ,
 `Retainer_Status` varchar(25) ,
 `Retainer_Year` bigint(11) ,
 `Accounting_Software_ID` int(11) ,
 `Accounting_Software_Name` varchar(50) ,
 `Principal_Name` varchar(100) ,
 `Inactive_Date` bigint(11) ,
 `Referred_By_ID` bigint(11) ,
 `Referred_By_Name` varchar(100) ,
 `Tax_Returns` longtext ,
 `Tax_Return_IDs` longblob ,
 `Entity_Type` int(1) 
);

/*Table structure for table `client_names` */

DROP TABLE IF EXISTS `client_names`;

CREATE TABLE  `client_names`(
 `Company_Name` varchar(100) ,
 `Client_Name_Last` varchar(25) ,
 `Client_Name` varchar(100) 
);

/*Table structure for table `client_statement_info` */

DROP TABLE IF EXISTS `client_statement_info`;

CREATE TABLE  `client_statement_info`(
 `Statement_ID` int(11) ,
 `Client_ID` int(11) ,
 `Institution_ID` int(11) ,
 `Institution_Name` varchar(50) ,
 `Institution_Address_1` varchar(50) ,
 `Institution_Address_2` varchar(50) ,
 `Institution_City` varchar(25) ,
 `Institution_State` varchar(2) ,
 `Institution_Postal_Code` varchar(10) ,
 `Institution_Contact_Name` varchar(50) ,
 `Institution_Telephone` varchar(15) ,
 `Institution_Stamp_Date` int(11) ,
 `Institution_Last_Edit_Date` int(11) ,
 `Statement_Type_ID` int(11) ,
 `Statement_Type` varchar(25) ,
 `Account_Number` varchar(25) ,
 `ABA_Number` varchar(25) ,
 `Statement_Start_Date` int(11) ,
 `Statement_End_Date` int(11) ,
 `Last_Reconciliation_Date` int(11) ,
 `Contact_Access_Notes` text ,
 `Statement_Status_ID` int(11) ,
 `Statement_Status` varchar(50) ,
 `Inactive_Date` int(11) ,
 `Statement_Stamp_Date` int(11) ,
 `Statement_Last_Edit_Date` int(11) ,
 `Statement_Stamp_User_ID` int(11) ,
 `Statement_Stamp_User_Initials` varchar(3) ,
 `Statement_Stamp_User_Login_ID` varchar(25) ,
 `Statement_Last_Edit_User_ID` int(11) ,
 `Statement_Last_Edit_User_Login_ID` varchar(25) ,
 `Statement_Last_Edit_User_Initials` varchar(3) ,
 `Institution_Stamp_User_ID` int(11) ,
 `Institution_Stamp_User_Initials` varchar(3) ,
 `Institution_Stamp_User_Login_ID` varchar(25) ,
 `Institution_Last_Edit_User_ID` int(11) ,
 `Institution_Last_Edit_User_Initials` varchar(3) ,
 `Institution_Last_Edit_User_Login_ID` varchar(25) 
);

/*Table structure for table `institution_info` */

DROP TABLE IF EXISTS `institution_info`;

CREATE TABLE  `institution_info`(
 `Institution_ID` int(11) ,
 `Institution_Name` varchar(50) ,
 `Institution_Address_1` varchar(50) ,
 `Institution_Address_2` varchar(50) ,
 `Institution_City` varchar(25) ,
 `Institution_State` varchar(2) ,
 `Institution_Postal_Code` varchar(10) ,
 `Institution_Contact_Name` varchar(50) ,
 `Institution_Telephone` varchar(15) ,
 `Institution_ABA` varchar(25) ,
 `Stamp_Date` int(11) ,
 `Stamp_User` int(11) ,
 `Last_Edit_Date` int(11) ,
 `Last_Edit_User` int(11) ,
 `Stamp_User_Login_ID` varchar(25) ,
 `Stamp_User_Initials` varchar(3) ,
 `Stamp_User_ID` int(11) ,
 `Last_Edit_User_ID` int(11) ,
 `Last_Edit_User_Initials` varchar(3) ,
 `Last_Edit_User_Login_ID` varchar(25) ,
 `Total_In_Use` bigint(21) 
);

/*Table structure for table `staff_info` */

DROP TABLE IF EXISTS `staff_info`;

CREATE TABLE  `staff_info`(
 `Staff_ID` int(11) ,
 `Staff_Name_Last` varchar(30) ,
 `Staff_Name_First` varchar(30) ,
 `Staff_Name_Full` varchar(62) ,
 `Initials` varchar(3) ,
 `Login_ID` varchar(25) ,
 `Email` varchar(50) ,
 `User_Roles` varchar(341) ,
 `Stamp_Date` int(11) ,
 `Stamp_User` int(11) ,
 `Stamp_User_Name_Last` varchar(30) ,
 `Stamp_User_Name_First` varchar(30) ,
 `Stamp_User_Name_Full` varchar(62) ,
 `Stamp_User_Initials` varchar(3) ,
 `Stamp_User_Login_ID` varchar(25) ,
 `Last_Edit_Date` int(11) ,
 `Last_Edit_User` int(11) ,
 `Last_Edit_User_Name_Last` varchar(30) ,
 `Last_Edit_User_Name_First` varchar(30) ,
 `Last_Edit_User_Name_Full` varchar(62) ,
 `Last_Edit_User_Initials` varchar(3) ,
 `Last_Edit_User_Login_ID` varchar(25) ,
 `Staff_Status_ID` int(11) ,
 `Staff_Status_Label` varchar(25) 
);

/*Table structure for table `statement_history_info` */

DROP TABLE IF EXISTS `statement_history_info`;

CREATE TABLE  `statement_history_info`(
 `Statement_ID` int(11) ,
 `Institution_ID` int(11) ,
 `Account_Number` varchar(25) ,
 `ABA_Number` varchar(25) ,
 `Statement_Start_Date` bigint(11) ,
 `Statement_End_Date` bigint(11) ,
 `Last_Reconciliation_Date` bigint(11) ,
 `Contact_Access_Notes` longtext ,
 `Inactive_Date` bigint(11) ,
 `Statement_Stamp_Date` bigint(11) ,
 `Statement_Last_Edit_Date` bigint(11) ,
 `Statement_Stamp_User_ID` bigint(11) ,
 `Statement_Stamp_User_Name_Full` varchar(62) ,
 `Statement_Stamp_User_Inititials` varchar(3) ,
 `Statement_Stamp_User_Login_ID` varchar(25) ,
 `Statement_Last_Edit_User_ID` bigint(11) ,
 `Statement_Last_Edit_User_Name_Full` varchar(62) ,
 `Statement_Last_Edit_User_Initials` varchar(3) ,
 `Statement_Last_Edit_User_Login_ID` varchar(25) ,
 `Client_ID` bigint(11) ,
 `Company_Name` varchar(100) ,
 `Client_Name_Last` varchar(25) ,
 `Client_Name_First` varchar(25) ,
 `Client_Name` varchar(100) ,
 `Statement_Type_ID` bigint(11) ,
 `Statement_Type` varchar(25) ,
 `Statement_Status_ID` bigint(11) ,
 `Statement_Statement_Status` varchar(50) ,
 `Statement_History_Stamp_User_ID` bigint(11) ,
 `Statement_History_Stamp_User_Name_Full` varchar(62) ,
 `Statement_History_Stamp_User_Initials` varchar(3) ,
 `Statement_History_Stamp_User_Login_ID` varchar(25) ,
 `Statement_History_Last_Edit_User_ID` bigint(11) ,
 `Statement_History_Last_Edit_User_Name_Full` varchar(62) ,
 `Statement_History_Last_Edit_User_Login_ID` varchar(25) ,
 `Statement_History_Last_Edit_User_Initials` varchar(3) ,
 `Statement_History_Statement_Status_ID` bigint(11) ,
 `Statement_History_Statement_Status_Label` varchar(50) ,
 `Statement_History_ID` bigint(11) ,
 `Statement_History_Stamp_Date` bigint(11) ,
 `Statement_History_Last_Edit_Date` bigint(11) 
);

/*Table structure for table `statement_info` */

DROP TABLE IF EXISTS `statement_info`;

CREATE TABLE  `statement_info`(
 `Statement_ID` int(11) ,
 `Account_Number` varchar(25) ,
 `ABA_Number` varchar(25) ,
 `Statement_Start_Date` bigint(11) ,
 `Statement_End_Date` bigint(11) ,
 `Last_Reconciliation_Date` bigint(11) ,
 `Contact_Access_Notes` longtext ,
 `Inactive_Date` bigint(11) ,
 `Statement_Stamp_Date` bigint(11) ,
 `Statement_Last_Edit_Date` bigint(11) ,
 `Statement_Stamp_User_ID` bigint(11) ,
 `Statement_Stamp_User_Name_Full` varchar(62) ,
 `Statement_Stamp_User_Initials` varchar(3) ,
 `Statement_Stamp_User_Login_ID` varchar(25) ,
 `Statement_Last_Edit_User_ID` bigint(11) ,
 `Statement_Last_Edit_User_Name_Full` varchar(62) ,
 `Statement_Last_Edit_User_Initials` varchar(3) ,
 `Statement_Last_Edit_User_Login_ID` varchar(25) ,
 `Client_ID` bigint(11) ,
 `Company_Name` varchar(100) ,
 `Client_Name_Last` varchar(25) ,
 `Client_Name_First` varchar(25) ,
 `Client_Name` varchar(100) ,
 `Client_Status_ID` varbinary(11) ,
 `Statement_Type_ID` bigint(11) ,
 `Statement_Type` varchar(25) ,
 `Statement_Status_ID` bigint(11) ,
 `Statement_Status` varchar(50) ,
 `Institution_ID` bigint(11) ,
 `Institution_Name` varchar(50) ,
 `Institution_Address_1` varchar(50) ,
 `Institution_Address_2` varchar(50) ,
 `Institution_City` varchar(25) ,
 `Institution_State` varchar(2) ,
 `Institution_Postal_Code` varchar(10) ,
 `Institution_Contact_Name` varchar(50) ,
 `Institution_Telephone` varchar(15) ,
 `Client_Staff_ID` bigint(11) ,
 `Client_Staff_Initials` varchar(3) ,
 `Client_Staff_Login_ID` varchar(25) ,
 `Client_Staff_Name_Full` varchar(62) ,
 `Client_Partner_ID` bigint(11) ,
 `Client_Partner_Initials` varchar(3) ,
 `Client_Partner_Login_ID` varchar(25) ,
 `Client_Partner_Name_Full` varchar(62) 
);

/*Table structure for table `tax_return_history_info` */

DROP TABLE IF EXISTS `tax_return_history_info`;

CREATE TABLE  `tax_return_history_info`(
 `Tax_Return_Status_History_ID` int(11) ,
 `Tax_Return_Status_Label` varchar(100) ,
 `Client_ID` int(11) ,
 `Tax_Return_ID` int(11) ,
 `Tax_Return_Status_ID` int(11) ,
 `Transmitted` varchar(10) ,
 `Preparer_By_ID` int(11) ,
 `Prepared_By_Name_Last` varchar(30) ,
 `Prepared_By_Name_First` varchar(30) ,
 `Prepared_By_Name_Full` varchar(62) ,
 `Prepared_By_Initials` varchar(3) ,
 `Prepared_By_Login_ID` varchar(25) ,
 `Prepared_By_Email` varchar(50) ,
 `Reviewer_1_ID` int(11) ,
 `Reviewer_1_Name_First` varchar(30) ,
 `Reviewer_1_Name_Last` varchar(30) ,
 `Reviewer_1_Name_Full` varchar(62) ,
 `Reviewer_1_Initials` varchar(3) ,
 `Reviewer_1_Login_ID` varchar(25) ,
 `Reviewer_1_Email` varchar(50) ,
 `Reviewer_2_ID` int(11) ,
 `Reviewer_2_Name_First` varchar(30) ,
 `Reviewer_2_Name_Last` varchar(30) ,
 `Reviewer_2_Name_Full` varchar(62) ,
 `Reviewer_2_Initials` varchar(3) ,
 `Reviewer_2_Login_ID` varchar(25) ,
 `Reviewer_2_Email` varchar(50) ,
 `Last_Edit_Date` int(11) ,
 `Last_Edit_User` int(11) ,
 `Last_Edit_User_Name_First` varchar(30) ,
 `Last_Edit_User_Name_Last` varchar(30) ,
 `Last_Edit_User_Name_Full` varchar(62) ,
 `Last_Edit_User_Email` varchar(50) ,
 `Last_Edit_User_Initials` varchar(3) ,
 `Last_Edit_User_Login_ID` varchar(25) ,
 `Is_Locked` bit(1) 
);

/*Table structure for table `tax_return_info` */

DROP TABLE IF EXISTS `tax_return_info`;

CREATE TABLE  `tax_return_info`(
 `Tax_Return_ID` int(11) ,
 `Client_ID` int(11) ,
 `Client_Name` varchar(100) ,
 `Client_Status_ID` bigint(11) ,
 `Client_Status_Label` varchar(25) ,
 `Client_States` varchar(341) ,
 `Tax_Form_ID` bigint(11) ,
 `Tax_Form_Name` varchar(50) ,
 `Partner_ID` bigint(11) ,
 `Partner_Name_Last` varchar(30) ,
 `Partner_Name_First` varchar(30) ,
 `Partner_Name_Full` varchar(62) ,
 `Partner_Initials` varchar(3) ,
 `Partner_Email` varchar(50) ,
 `Partner_Login` varchar(25) ,
 `Date_Sent` bigint(11) ,
 `Tax_Year` bigint(11) ,
 `Tax_Quarter_ID` bigint(11) ,
 `Tax_Quarter` varchar(14) ,
 `Tax_Month_ID` bigint(11) ,
 `Tax_Month` varchar(64) ,
 `Is_Locked` bit(1) ,
 `Instructions_ID` bigint(11) ,
 `Instructions` varchar(100) ,
 `Tax_Return_Status_ID` bigint(11) ,
 `Tax_Return_Status` varchar(100) ,
 `990PF_Date` bigint(11) ,
 `Date_Released` bigint(11) ,
 `Tax_Return_Notes` longtext ,
 `Staff_ID` bigint(11) ,
 `Staff_Name_Last` varchar(30) ,
 `Staff_Name_First` varchar(30) ,
 `Staff_Name_Full` varchar(62) ,
 `Staff_Initials` varchar(3) ,
 `Staff_Email` varchar(50) ,
 `Staff_Login` varchar(25) ,
 `Reviewer_1_ID` bigint(11) ,
 `Reviewer_1_Name_Last` varchar(30) ,
 `Reviewer_1_Name_First` varchar(30) ,
 `Reviewer_1_Name_Full` varchar(62) ,
 `Reviewer_1_Initials` varchar(3) ,
 `Reviewer_1_Email` varchar(50) ,
 `Reviewer_1_Login` varchar(25) ,
 `Reviewer_2_ID` bigint(11) ,
 `Reviewer_2_Name_Last` varchar(30) ,
 `Reviewer_2_Name_First` varchar(30) ,
 `Reviewer_2_Name_Full` varchar(62) ,
 `Reviewer_2_Initials` varchar(3) ,
 `Reviewer_2_Email` varchar(50) ,
 `Reviewer_2_Login` varchar(25) ,
 `Stamp_Date` bigint(11) ,
 `Stamp_User` bigint(11) ,
 `Stamp_User_Name_Last` varchar(30) ,
 `Stamp_User_Name_First` varchar(30) ,
 `Stamp_User_Name_Full` varchar(62) ,
 `Stamp_User_Initials` varchar(3) ,
 `Stamp_User_Email` varchar(50) ,
 `Stamp_User_Login` varchar(25) ,
 `Last_Edit_Date` bigint(11) ,
 `Last_Edit_User` bigint(11) ,
 `Last_Edit_User_Name_Last` varchar(30) ,
 `Last_Edit_User_Name_First` varchar(30) ,
 `Last_Edit_User_Name_Full` varchar(62) ,
 `Last_Edit_User_Initials` varchar(3) ,
 `Last_Edit_User_Email` varchar(50) ,
 `Last_Edit_User_Login` varchar(25) 
);

/*View structure for view client_info */

/*!50001 DROP TABLE IF EXISTS `client_info` */;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `client_info` AS select `c`.`Client_ID` AS `Client_ID`,if((trim(`c`.`Company_Name`) = ''),nullif(concat(trim(`c`.`Client_Name_Last`),', ',trim(`c`.`Client_Name_First`)),', '),trim(`c`.`Company_Name`)) AS `Client_Name`,nullif(trim(`c`.`Company_Name`),'') AS `Company_Name`,nullif(trim(`c`.`Client_Name_Last`),'') AS `Client_Name_Last`,nullif(trim(`c`.`Client_Name_First`),'') AS `Client_Name_First`,nullif(concat(trim(`c`.`Client_Name_Last`),', ',trim(`c`.`Client_Name_First`)),', ') AS `Client_Name_Full`,`c`.`Client_Type_ID` AS `Client_Type_ID`,`ct`.`Client_Type_Label` AS `Client_Type`,nullif(trim(`c`.`Client_SSN_EIN`),'') AS `Client_SSN_EIN`,`c`.`Client_Occupation_ID` AS `Client_Occupation_ID`,`co`.`Occupation_Name` AS `Client_Occupation`,nullif(concat(trim(`c`.`Spouse_Name_Last`),', ',trim(`c`.`Spouse_Name_First`)),', ') AS `Spouse_Name_Full`,nullif(trim(`c`.`Spouse_Name_Last`),'') AS `Spouse_Name_Last`,nullif(trim(`c`.`Spouse_Name_First`),'') AS `Spouse_Name_First`,nullif(trim(`c`.`Spouse_SSN_EIN`),'') AS `Spouse_SSN_EIN`,`c`.`Spouse_Occupation_ID` AS `Spouse_Occupation_ID`,`so`.`Occupation_Name` AS `Spouse_Occupation`,group_concat(distinct `tfl`.`Tax_Form_ID` order by `tfl`.`Tax_Form_ID` ASC separator ', ') AS `Tax_Form_IDs`,group_concat(distinct `tf`.`Tax_Form_Name` order by `tf`.`Tax_Form_Name` ASC separator ', ') AS `Tax_Forms`,group_concat(distinct `csl`.`State_ID` order by `csl`.`State_ID` ASC separator ', ') AS `States`,nullif(`c`.`Partner_ID`,0) AS `Partner_ID`,trim(`pi`.`Staff_Name_Last`) AS `Partner_Name_Last`,trim(`pi`.`Staff_Name_First`) AS `Partner_Name_First`,concat(trim(`pi`.`Staff_Name_Last`),', ',trim(`pi`.`Staff_Name_First`)) AS `Partner_Name_Full`,nullif(trim(`pi`.`Initials`),'') AS `Partner_Initials`,trim(`pi`.`Login_ID`) AS `Partner_Login_ID`,nullif(`c`.`Staff_ID`,0) AS `Staff_ID`,trim(`si`.`Staff_Name_Last`) AS `Staff_Name_Last`,trim(`si`.`Staff_Name_First`) AS `Staff_Name_First`,concat(trim(`si`.`Staff_Name_Last`),', ',trim(`si`.`Staff_Name_First`)) AS `Staff_Name_Full`,nullif(trim(`si`.`Initials`),'') AS `Staff_Initials`,trim(`si`.`Login_ID`) AS `Staff_Login_ID`,`c`.`Client_Status_ID` AS `Client_Status_ID`,`cs`.`Client_Status_Label` AS `Client_Status`,nullif(trim(`c`.`Client_Notes`),'') AS `Client_Notes`,`c`.`Month_ID` AS `Month_ID`,if((`c`.`Organizer` = 1),1,0) AS `Organizer`,if((`c`.`Organizer` = 1),_utf8'Yes',_utf8'No') AS `Organizer_Label`,if((`c`.`Month_ID` > 0),date_format(concat(_utf8'2012/',`c`.`Month_ID`,_utf8'/01'),_utf8'%M'),_utf8'') AS `Month`,nullif(nullif(`c`.`Initial_Year`,0),-(1)) AS `Initial_Year`,nullif(`c`.`Stamp_Date`,0) AS `Stamp_Date`,`c`.`Stamp_User` AS `Stamp_User`,`su`.`Staff_Name_Last` AS `Stamp_User_Name_Last`,`su`.`Staff_Name_First` AS `Stamp_User_Name_First`,concat(trim(`su`.`Staff_Name_Last`),', ',trim(`su`.`Staff_Name_First`)) AS `Stamp_User_Name_Full`,`su`.`Login_ID` AS `Stamp_User_Login_ID`,nullif(trim(`su`.`Initials`),'') AS `Stamp_User_Initials`,nullif(`c`.`Last_Edit_Date`,0) AS `Last_Edit_Date`,`c`.`Last_Edit_User` AS `Last_Edit_User`,`eu`.`Staff_Name_Last` AS `Last_Edit_User_Name_Last`,`eu`.`Staff_Name_First` AS `Last_Edit_User_Name_First`,concat(trim(`eu`.`Staff_Name_Last`),', ',trim(`eu`.`Staff_Name_First`)) AS `Last_Edit_User_Name_Full`,`eu`.`Login_ID` AS `Last_Edit_User_Login_ID`,nullif(trim(`eu`.`Initials`),'') AS `Last_Edit_User_Initials`,nullif(trim(`c`.`Billing_ID`),'') AS `Billing_ID`,nullif(trim(`c`.`Document_ID`),'') AS `Document_ID`,`rs`.`Retainer_Status_ID` AS `Retainer_Status_ID`,`rs`.`Retainer_Status_Label` AS `Retainer_Status`,nullif(nullif(`c`.`Retainer_Year`,0),-(1)) AS `Retainer_Year`,`c`.`Accounting_Software_ID` AS `Accounting_Software_ID`,`a`.`Accounting_Software_Name` AS `Accounting_Software_Name`,nullif(trim(`c`.`Principal_Name`),'') AS `Principal_Name`,nullif(`c`.`Inactive_Date`,0) AS `Inactive_Date`,nullif(`c`.`Referred_By_ID`,0) AS `Referred_By_ID`,nullif(`rb`.`Referred_By_Name`,'') AS `Referred_By_Name`,group_concat(distinct `tf2`.`Tax_Form_Name` order by `tf2`.`Tax_Form_Name` ASC separator ', ') AS `Tax_Returns`,group_concat(distinct `tf2`.`Tax_Form_ID` order by `tf2`.`Tax_Form_Name` ASC separator ', ') AS `Tax_Return_IDs`,if((`c`.`Client_Type_ID` = 31),2,1) AS `Entity_Type` from ((((((((((((((((`client` `c` left join `client_tax_form_link` `tfl` on((`c`.`Client_ID` = `tfl`.`Client_ID`))) left join `tax_form` `tf` on((`tfl`.`Tax_Form_ID` = `tf`.`Tax_Form_ID`))) left join `client_state_link` `csl` on((`c`.`Client_ID` = `csl`.`Client_ID`))) left join `staff` `pi` on((`c`.`Partner_ID` = `pi`.`Staff_ID`))) left join `staff` `si` on((`c`.`Staff_ID` = `si`.`Staff_ID`))) left join `client_status` `cs` on((`c`.`Client_Status_ID` = `cs`.`Client_Status_ID`))) left join `client_type` `ct` on((`c`.`Client_Type_ID` = `ct`.`Client_Type_ID`))) left join `accounting_software` `a` on((`c`.`Accounting_Software_ID` = `a`.`Accounting_Software_ID`))) left join `occupation` `co` on((`c`.`Client_Occupation_ID` = `co`.`Occupation_ID`))) left join `occupation` `so` on((`c`.`Spouse_Occupation_ID` = `so`.`Occupation_ID`))) left join `tax_return` `tr` on((`c`.`Client_ID` = `tr`.`Client_ID`))) left join `tax_form` `tf2` on((`tr`.`Tax_Form_ID` = `tf2`.`Tax_Form_ID`))) left join `referred_by` `rb` on((`c`.`Referred_By_ID` = `rb`.`Referred_By_ID`))) left join `staff` `su` on((`c`.`Stamp_User` = `su`.`Staff_ID`))) left join `staff` `eu` on((`c`.`Last_Edit_User` = `eu`.`Staff_ID`))) left join `retainer_status` `rs` on((`c`.`Retainer_Status_ID` = `rs`.`Retainer_Status_ID`))) group by `c`.`Client_ID`;

/*View structure for view client_names */

/*!50001 DROP TABLE IF EXISTS `client_names` */;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `client_names` AS select `client`.`Company_Name` AS `Company_Name`,`client`.`Client_Name_Last` AS `Client_Name_Last`,if((trim(`client`.`Company_Name`) = _cp1251''),nullif(concat(trim(`client`.`Client_Name_Last`),_cp1251', ',trim(`client`.`Client_Name_First`)),_cp1251', '),trim(`client`.`Company_Name`)) AS `Client_Name` from `client`;

/*View structure for view client_statement_info */

/*!50001 DROP TABLE IF EXISTS `client_statement_info` */;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `client_statement_info` AS select `s`.`Statement_ID` AS `Statement_ID`,`c`.`Client_ID` AS `Client_ID`,`institution`.`Institution_ID` AS `Institution_ID`,`institution`.`Institution_Name` AS `Institution_Name`,`institution`.`Institution_Address_1` AS `Institution_Address_1`,`institution`.`Institution_Address_2` AS `Institution_Address_2`,`institution`.`Institution_City` AS `Institution_City`,`institution`.`Institution_State` AS `Institution_State`,`institution`.`Institution_Postal_Code` AS `Institution_Postal_Code`,`institution`.`Institution_Contact_Name` AS `Institution_Contact_Name`,`institution`.`Institution_Telephone` AS `Institution_Telephone`,`institution`.`Stamp_Date` AS `Institution_Stamp_Date`,`institution`.`Last_Edit_Date` AS `Institution_Last_Edit_Date`,`st`.`Statement_Type_ID` AS `Statement_Type_ID`,`st`.`Statement_Type_Label` AS `Statement_Type`,`s`.`Account_Number` AS `Account_Number`,`s`.`ABA_Number` AS `ABA_Number`,`s`.`Statement_Start_Date` AS `Statement_Start_Date`,`s`.`Statement_End_Date` AS `Statement_End_Date`,`s`.`Last_Reconciliation_Date` AS `Last_Reconciliation_Date`,`s`.`Contact_Access_Notes` AS `Contact_Access_Notes`,`ss`.`Statement_Status_ID` AS `Statement_Status_ID`,`ss`.`Statement_Status_Label` AS `Statement_Status`,`s`.`Inactive_Date` AS `Inactive_Date`,`s`.`Stamp_Date` AS `Statement_Stamp_Date`,`s`.`Last_Edit_Date` AS `Statement_Last_Edit_Date`,`s_su`.`Staff_ID` AS `Statement_Stamp_User_ID`,`s_su`.`Initials` AS `Statement_Stamp_User_Initials`,`s_su`.`Login_ID` AS `Statement_Stamp_User_Login_ID`,`s_leu`.`Staff_ID` AS `Statement_Last_Edit_User_ID`,`s_leu`.`Login_ID` AS `Statement_Last_Edit_User_Login_ID`,`s_leu`.`Initials` AS `Statement_Last_Edit_User_Initials`,`i_su`.`Staff_ID` AS `Institution_Stamp_User_ID`,`i_su`.`Initials` AS `Institution_Stamp_User_Initials`,`i_su`.`Login_ID` AS `Institution_Stamp_User_Login_ID`,`i_leu`.`Staff_ID` AS `Institution_Last_Edit_User_ID`,`i_leu`.`Initials` AS `Institution_Last_Edit_User_Initials`,`i_leu`.`Login_ID` AS `Institution_Last_Edit_User_Login_ID` from ((((((((`statement` `s` left join `statement_status` `ss` on((`s`.`Statement_Status_ID` = `ss`.`Statement_Status_ID`))) left join `statement_type` `st` on((`s`.`Statement_Type_ID` = `st`.`Statement_Type_ID`))) left join `client` `c` on((`s`.`Client_ID` = `c`.`Client_ID`))) left join `institution` on((`s`.`Institution_ID` = `institution`.`Institution_ID`))) left join `staff` `s_su` on((`s`.`Stamp_User` = `s_su`.`Staff_ID`))) left join `staff` `s_leu` on((`s`.`Last_Edit_User` = `s_leu`.`Staff_ID`))) left join `staff` `i_su` on((`institution`.`Stamp_User` = `i_su`.`Staff_ID`))) left join `staff` `i_leu` on((`institution`.`Last_Edit_User` = `i_leu`.`Staff_ID`)));

/*View structure for view institution_info */

/*!50001 DROP TABLE IF EXISTS `institution_info` */;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `institution_info` AS select `i`.`Institution_ID` AS `Institution_ID`,`i`.`Institution_Name` AS `Institution_Name`,`i`.`Institution_Address_1` AS `Institution_Address_1`,`i`.`Institution_Address_2` AS `Institution_Address_2`,`i`.`Institution_City` AS `Institution_City`,`i`.`Institution_State` AS `Institution_State`,`i`.`Institution_Postal_Code` AS `Institution_Postal_Code`,`i`.`Institution_Contact_Name` AS `Institution_Contact_Name`,`i`.`Institution_Telephone` AS `Institution_Telephone`,`i`.`Institution_ABA` AS `Institution_ABA`,`i`.`Stamp_Date` AS `Stamp_Date`,`i`.`Stamp_User` AS `Stamp_User`,`i`.`Last_Edit_Date` AS `Last_Edit_Date`,`i`.`Last_Edit_User` AS `Last_Edit_User`,`su`.`Login_ID` AS `Stamp_User_Login_ID`,`su`.`Initials` AS `Stamp_User_Initials`,`su`.`Staff_ID` AS `Stamp_User_ID`,`leu`.`Staff_ID` AS `Last_Edit_User_ID`,`leu`.`Initials` AS `Last_Edit_User_Initials`,`leu`.`Login_ID` AS `Last_Edit_User_Login_ID`,count(`s`.`Institution_ID`) AS `Total_In_Use` from (((`institution` `i` left join `staff` `su` on((`i`.`Stamp_User` = `su`.`Staff_ID`))) left join `staff` `leu` on((`i`.`Last_Edit_User` = `leu`.`Staff_ID`))) left join `statement` `s` on((`i`.`Institution_ID` = `s`.`Institution_ID`))) group by `i`.`Institution_ID`;

/*View structure for view staff_info */

/*!50001 DROP TABLE IF EXISTS `staff_info` */;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `staff_info` AS select `s`.`Staff_ID` AS `Staff_ID`,`s`.`Staff_Name_Last` AS `Staff_Name_Last`,`s`.`Staff_Name_First` AS `Staff_Name_First`,concat(trim(`s`.`Staff_Name_Last`),_cp1251', ',trim(`s`.`Staff_Name_First`)) AS `Staff_Name_Full`,`s`.`Initials` AS `Initials`,`s`.`Login_ID` AS `Login_ID`,`s`.`Email` AS `Email`,group_concat(distinct `r`.`Role_Name` order by `r`.`Role_Name` ASC separator ', ') AS `User_Roles`,`s`.`Stamp_Date` AS `Stamp_Date`,`s`.`Stamp_User` AS `Stamp_User`,`su`.`Staff_Name_Last` AS `Stamp_User_Name_Last`,`su`.`Staff_Name_First` AS `Stamp_User_Name_First`,nullif(concat(trim(`su`.`Staff_Name_Last`),_cp1251', ',trim(`su`.`Staff_Name_First`)),_cp1251', ') AS `Stamp_User_Name_Full`,`su`.`Initials` AS `Stamp_User_Initials`,`su`.`Login_ID` AS `Stamp_User_Login_ID`,`s`.`Last_Edit_Date` AS `Last_Edit_Date`,`s`.`Last_Edit_User` AS `Last_Edit_User`,`eu`.`Staff_Name_Last` AS `Last_Edit_User_Name_Last`,`eu`.`Staff_Name_First` AS `Last_Edit_User_Name_First`,nullif(concat(trim(`eu`.`Staff_Name_Last`),_cp1251', ',trim(`eu`.`Staff_Name_First`)),_cp1251', ') AS `Last_Edit_User_Name_Full`,`eu`.`Initials` AS `Last_Edit_User_Initials`,`eu`.`Login_ID` AS `Last_Edit_User_Login_ID`,`s`.`Staff_Status_ID` AS `Staff_Status_ID`,`ss`.`Staff_Status_Label` AS `Staff_Status_Label` from (((((`staff` `s` left join `staff` `su` on((`s`.`Stamp_User` = `su`.`Staff_ID`))) left join `staff` `eu` on((`s`.`Last_Edit_User` = `eu`.`Staff_ID`))) left join `staff_status` `ss` on((`s`.`Staff_Status_ID` = `ss`.`Staff_Status_ID`))) left join `staff_role_link` `srl` on((`s`.`Staff_ID` = `srl`.`Staff_ID`))) left join `role` `r` on((`srl`.`Role_ID` = `r`.`Role_ID`))) group by `s`.`Staff_ID`;

/*View structure for view statement_history_info */

/*!50001 DROP TABLE IF EXISTS `statement_history_info` */;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `statement_history_info` AS select `s`.`Statement_ID` AS `Statement_ID`,`s`.`Institution_ID` AS `Institution_ID`,nullif(trim(`s`.`Account_Number`),'') AS `Account_Number`,nullif(trim(`s`.`ABA_Number`),'') AS `ABA_Number`,nullif(`s`.`Statement_Start_Date`,0) AS `Statement_Start_Date`,nullif(`s`.`Statement_End_Date`,0) AS `Statement_End_Date`,nullif(`s`.`Last_Reconciliation_Date`,0) AS `Last_Reconciliation_Date`,nullif(trim(`s`.`Contact_Access_Notes`),'') AS `Contact_Access_Notes`,nullif(`s`.`Inactive_Date`,0) AS `Inactive_Date`,nullif(`s`.`Stamp_Date`,0) AS `Statement_Stamp_Date`,nullif(`s`.`Last_Edit_Date`,0) AS `Statement_Last_Edit_Date`,nullif(`s_su`.`Staff_ID`,0) AS `Statement_Stamp_User_ID`,nullif(trim(concat(trim(`s_su`.`Staff_Name_Last`),', ',trim(`s_su`.`Staff_Name_First`))),', ') AS `Statement_Stamp_User_Name_Full`,nullif(trim(`s_su`.`Initials`),'') AS `Statement_Stamp_User_Inititials`,nullif(trim(`s_su`.`Login_ID`),'') AS `Statement_Stamp_User_Login_ID`,nullif(`s_leu`.`Staff_ID`,0) AS `Statement_Last_Edit_User_ID`,nullif(trim(concat(trim(`s_leu`.`Staff_Name_Last`),', ',trim(`s_leu`.`Staff_Name_First`))),', ') AS `Statement_Last_Edit_User_Name_Full`,nullif(trim(`s_leu`.`Initials`),'') AS `Statement_Last_Edit_User_Initials`,nullif(trim(`s_leu`.`Login_ID`),'') AS `Statement_Last_Edit_User_Login_ID`,nullif(`c`.`Client_ID`,0) AS `Client_ID`,nullif(trim(`c`.`Company_Name`),'') AS `Company_Name`,nullif(trim(`c`.`Client_Name_Last`),'') AS `Client_Name_Last`,nullif(trim(`c`.`Client_Name_First`),'') AS `Client_Name_First`,if((trim(`c`.`Company_Name`) = ''),nullif(concat(trim(`c`.`Client_Name_Last`),', ',trim(`c`.`Client_Name_First`)),', '),trim(`c`.`Company_Name`)) AS `Client_Name`,nullif(`s_st`.`Statement_Type_ID`,0) AS `Statement_Type_ID`,nullif(trim(`s_st`.`Statement_Type_Label`),'') AS `Statement_Type`,nullif(`s_ss`.`Statement_Status_ID`,0) AS `Statement_Status_ID`,nullif(trim(`s_ss`.`Statement_Status_Label`),'') AS `Statement_Statement_Status`,nullif(`sh_su`.`Staff_ID`,0) AS `Statement_History_Stamp_User_ID`,nullif(trim(concat(trim(`sh_su`.`Staff_Name_Last`),', ',trim(`sh_su`.`Staff_Name_First`))),', ') AS `Statement_History_Stamp_User_Name_Full`,nullif(trim(`sh_su`.`Initials`),'') AS `Statement_History_Stamp_User_Initials`,nullif(trim(`sh_su`.`Login_ID`),'') AS `Statement_History_Stamp_User_Login_ID`,nullif(`sh_leu`.`Staff_ID`,0) AS `Statement_History_Last_Edit_User_ID`,nullif(trim(concat(trim(`sh_leu`.`Staff_Name_Last`),', ',trim(`sh_leu`.`Staff_Name_First`))),', ') AS `Statement_History_Last_Edit_User_Name_Full`,nullif(trim(`sh_leu`.`Login_ID`),'') AS `Statement_History_Last_Edit_User_Login_ID`,nullif(trim(`sh_leu`.`Initials`),'') AS `Statement_History_Last_Edit_User_Initials`,nullif(`sh_ss`.`Statement_Status_ID`,0) AS `Statement_History_Statement_Status_ID`,nullif(trim(`sh_ss`.`Statement_Status_Label`),'') AS `Statement_History_Statement_Status_Label`,nullif(`sh`.`Statement_History_ID`,0) AS `Statement_History_ID`,nullif(`sh`.`Stamp_Date`,0) AS `Statement_History_Stamp_Date`,nullif(`sh`.`Last_Edit_Date`,0) AS `Statement_History_Last_Edit_Date` from (((((((((`statement` `s` left join `staff` `s_su` on((`s`.`Stamp_User` = `s_su`.`Staff_ID`))) left join `staff` `s_leu` on((`s`.`Last_Edit_User` = `s_leu`.`Staff_ID`))) left join `client` `c` on((`s`.`Client_ID` = `c`.`Client_ID`))) left join `statement_type` `s_st` on((`s`.`Statement_Type_ID` = `s_st`.`Statement_Type_ID`))) left join `statement_status` `s_ss` on((`s`.`Statement_Status_ID` = `s_ss`.`Statement_Status_ID`))) left join `statement_history` `sh` on((`s`.`Statement_ID` = `sh`.`Statement_ID`))) left join `staff` `sh_su` on((`sh`.`Stamp_User` = `sh_su`.`Staff_ID`))) left join `staff` `sh_leu` on((`sh`.`Last_Edit_User` = `sh_leu`.`Staff_ID`))) left join `statement_status` `sh_ss` on((`sh`.`Statement_Status_ID` = `sh_ss`.`Statement_Status_ID`)));

/*View structure for view statement_info */

/*!50001 DROP TABLE IF EXISTS `statement_info` */;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `statement_info` AS select `s`.`Statement_ID` AS `Statement_ID`,nullif(trim(`s`.`Account_Number`),'') AS `Account_Number`,nullif(trim(`s`.`ABA_Number`),'') AS `ABA_Number`,nullif(`s`.`Statement_Start_Date`,0) AS `Statement_Start_Date`,nullif(`s`.`Statement_End_Date`,0) AS `Statement_End_Date`,nullif(`s`.`Last_Reconciliation_Date`,0) AS `Last_Reconciliation_Date`,nullif(trim(`s`.`Contact_Access_Notes`),'') AS `Contact_Access_Notes`,nullif(`s`.`Inactive_Date`,0) AS `Inactive_Date`,nullif(`s`.`Stamp_Date`,0) AS `Statement_Stamp_Date`,nullif(`s`.`Last_Edit_Date`,0) AS `Statement_Last_Edit_Date`,nullif(`s_su`.`Staff_ID`,0) AS `Statement_Stamp_User_ID`,nullif(trim(concat(trim(`s_su`.`Staff_Name_Last`),', ',trim(`s_su`.`Staff_Name_First`))),', ') AS `Statement_Stamp_User_Name_Full`,nullif(trim(`s_su`.`Initials`),'') AS `Statement_Stamp_User_Initials`,nullif(trim(`s_su`.`Login_ID`),'') AS `Statement_Stamp_User_Login_ID`,nullif(`s_leu`.`Staff_ID`,0) AS `Statement_Last_Edit_User_ID`,nullif(trim(concat(trim(`s_leu`.`Staff_Name_Last`),', ',trim(`s_leu`.`Staff_Name_First`))),', ') AS `Statement_Last_Edit_User_Name_Full`,nullif(trim(`s_leu`.`Initials`),'') AS `Statement_Last_Edit_User_Initials`,nullif(trim(`s_leu`.`Login_ID`),'') AS `Statement_Last_Edit_User_Login_ID`,nullif(`c`.`Client_ID`,0) AS `Client_ID`,nullif(trim(`c`.`Company_Name`),'') AS `Company_Name`,nullif(trim(`c`.`Client_Name_Last`),'') AS `Client_Name_Last`,nullif(trim(`c`.`Client_Name_First`),'') AS `Client_Name_First`,if((trim(`c`.`Company_Name`) = ''),nullif(concat(trim(`c`.`Client_Name_Last`),', ',trim(`c`.`Client_Name_First`)),', '),trim(`c`.`Company_Name`)) AS `Client_Name`,nullif(trim(`c`.`Client_Status_ID`),'') AS `Client_Status_ID`,nullif(`s_st`.`Statement_Type_ID`,0) AS `Statement_Type_ID`,nullif(trim(`s_st`.`Statement_Type_Label`),'') AS `Statement_Type`,nullif(`s_ss`.`Statement_Status_ID`,0) AS `Statement_Status_ID`,nullif(trim(`s_ss`.`Statement_Status_Label`),'') AS `Statement_Status`,nullif(`s`.`Institution_ID`,0) AS `Institution_ID`,nullif(trim(`i`.`Institution_Name`),'') AS `Institution_Name`,nullif(trim(`i`.`Institution_Address_1`),'') AS `Institution_Address_1`,nullif(trim(`i`.`Institution_Address_2`),'') AS `Institution_Address_2`,nullif(trim(`i`.`Institution_City`),'') AS `Institution_City`,nullif(trim(`i`.`Institution_State`),'') AS `Institution_State`,nullif(trim(`i`.`Institution_Postal_Code`),'') AS `Institution_Postal_Code`,nullif(trim(`i`.`Institution_Contact_Name`),'') AS `Institution_Contact_Name`,nullif(trim(`i`.`Institution_Telephone`),'') AS `Institution_Telephone`,nullif(`c`.`Staff_ID`,0) AS `Client_Staff_ID`,nullif(`c_s`.`Initials`,'') AS `Client_Staff_Initials`,nullif(`c_s`.`Login_ID`,'') AS `Client_Staff_Login_ID`,nullif(trim(concat(trim(`c_s`.`Staff_Name_Last`),', ',trim(`c_s`.`Staff_Name_First`))),', ') AS `Client_Staff_Name_Full`,nullif(`c`.`Partner_ID`,0) AS `Client_Partner_ID`,nullif(`c_p`.`Initials`,'') AS `Client_Partner_Initials`,nullif(`c_p`.`Login_ID`,'') AS `Client_Partner_Login_ID`,nullif(trim(concat(trim(`c_p`.`Staff_Name_Last`),', ',trim(`c_p`.`Staff_Name_First`))),', ') AS `Client_Partner_Name_Full` from ((((((((`statement` `s` left join `staff` `s_su` on((`s`.`Stamp_User` = `s_su`.`Staff_ID`))) left join `staff` `s_leu` on((`s`.`Last_Edit_User` = `s_leu`.`Staff_ID`))) left join `client` `c` on((`s`.`Client_ID` = `c`.`Client_ID`))) left join `statement_type` `s_st` on((`s`.`Statement_Type_ID` = `s_st`.`Statement_Type_ID`))) left join `statement_status` `s_ss` on((`s`.`Statement_Status_ID` = `s_ss`.`Statement_Status_ID`))) left join `institution` `i` on((`s`.`Institution_ID` = `i`.`Institution_ID`))) left join `staff` `c_s` on((`c`.`Staff_ID` = `c_s`.`Staff_ID`))) left join `staff` `c_p` on((`c`.`Partner_ID` = `c_p`.`Staff_ID`)));

/*View structure for view tax_return_history_info */

/*!50001 DROP TABLE IF EXISTS `tax_return_history_info` */;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tax_return_history_info` AS select `rh`.`Tax_Return_Status_History_ID` AS `Tax_Return_Status_History_ID`,`trs`.`Tax_Return_Status_Label` AS `Tax_Return_Status_Label`,`r`.`Client_ID` AS `Client_ID`,`r`.`Tax_Return_ID` AS `Tax_Return_ID`,`trs`.`Tax_Return_Status_ID` AS `Tax_Return_Status_ID`,`rh`.`Transmitted` AS `Transmitted`,`rh`.`Staff_ID` AS `Preparer_By_ID`,`rhs`.`Staff_Name_Last` AS `Prepared_By_Name_Last`,`rhs`.`Staff_Name_First` AS `Prepared_By_Name_First`,nullif(concat(`rhs`.`Staff_Name_Last`,_cp1251', ',`rhs`.`Staff_Name_First`),_cp1251', ') AS `Prepared_By_Name_Full`,`rhs`.`Initials` AS `Prepared_By_Initials`,`rhs`.`Login_ID` AS `Prepared_By_Login_ID`,`rhs`.`Email` AS `Prepared_By_Email`,`rh`.`Reviewer_1_ID` AS `Reviewer_1_ID`,`rhrr1`.`Staff_Name_First` AS `Reviewer_1_Name_First`,`rhrr1`.`Staff_Name_Last` AS `Reviewer_1_Name_Last`,nullif(concat(`rhrr1`.`Staff_Name_Last`,_cp1251', ',`rhrr1`.`Staff_Name_First`),_cp1251', ') AS `Reviewer_1_Name_Full`,`rhrr1`.`Initials` AS `Reviewer_1_Initials`,`rhrr1`.`Login_ID` AS `Reviewer_1_Login_ID`,`rhrr1`.`Email` AS `Reviewer_1_Email`,`rh`.`Reviewer_2_ID` AS `Reviewer_2_ID`,`rhrr2`.`Staff_Name_First` AS `Reviewer_2_Name_First`,`rhrr2`.`Staff_Name_Last` AS `Reviewer_2_Name_Last`,nullif(concat(`rhrr2`.`Staff_Name_Last`,_cp1251', ',`rhrr2`.`Staff_Name_First`),_cp1251', ') AS `Reviewer_2_Name_Full`,`rhrr2`.`Initials` AS `Reviewer_2_Initials`,`rhrr2`.`Login_ID` AS `Reviewer_2_Login_ID`,`rhrr2`.`Email` AS `Reviewer_2_Email`,`rh`.`Last_Edit_Date` AS `Last_Edit_Date`,`rh`.`Last_Edit_User` AS `Last_Edit_User`,`rheu`.`Staff_Name_First` AS `Last_Edit_User_Name_First`,`rheu`.`Staff_Name_Last` AS `Last_Edit_User_Name_Last`,nullif(concat(`rheu`.`Staff_Name_Last`,_cp1251', ',`rheu`.`Staff_Name_First`),_cp1251', ') AS `Last_Edit_User_Name_Full`,`rheu`.`Email` AS `Last_Edit_User_Email`,`rheu`.`Initials` AS `Last_Edit_User_Initials`,`rheu`.`Login_ID` AS `Last_Edit_User_Login_ID`,`r`.`Is_Locked` AS `Is_Locked` from ((((((`tax_return_status` `trs` left join `tax_return_status_history` `rh` on((`trs`.`Tax_Return_Status_ID` = `rh`.`Tax_Return_Status_ID`))) left join `tax_return` `r` on((`r`.`Tax_Return_ID` = `rh`.`Tax_Return_ID`))) left join `staff` `rhs` on((`rh`.`Staff_ID` = `rhs`.`Staff_ID`))) left join `staff` `rheu` on((`rh`.`Last_Edit_User` = `rheu`.`Staff_ID`))) left join `staff` `rhrr1` on((`rh`.`Reviewer_1_ID` = `rhrr1`.`Staff_ID`))) left join `staff` `rhrr2` on((`rhrr2`.`Staff_ID` = `rh`.`Reviewer_2_ID`)));

/*View structure for view tax_return_info */

/*!50001 DROP TABLE IF EXISTS `tax_return_info` */;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tax_return_info` AS select `tr`.`Tax_Return_ID` AS `Tax_Return_ID`,`tr`.`Client_ID` AS `Client_ID`,nullif(if((trim(`c`.`Company_Name`) = _cp1251''),concat(trim(`c`.`Client_Name_Last`),_cp1251', ',trim(`c`.`Client_Name_First`)),trim(`c`.`Company_Name`)),_cp1251', ') AS `Client_Name`,nullif(`cs`.`Client_Status_ID`,0) AS `Client_Status_ID`,nullif(`cs`.`Client_Status_Label`,_utf8'') AS `Client_Status_Label`,group_concat(distinct `csl`.`State_ID` order by `csl`.`State_ID` ASC separator ', ') AS `Client_States`,nullif(`tf`.`Tax_Form_ID`,0) AS `Tax_Form_ID`,nullif(`tf`.`Tax_Form_Name`,_cp1251'') AS `Tax_Form_Name`,nullif(`tr`.`Partner_ID`,0) AS `Partner_ID`,nullif(trim(`p`.`Staff_Name_Last`),_cp1251'') AS `Partner_Name_Last`,nullif(trim(`p`.`Staff_Name_First`),_cp1251'') AS `Partner_Name_First`,nullif(concat(trim(`p`.`Staff_Name_Last`),_cp1251', ',trim(`p`.`Staff_Name_First`)),_cp1251', ') AS `Partner_Name_Full`,nullif(trim(`p`.`Initials`),_cp1251'') AS `Partner_Initials`,nullif(trim(`p`.`Email`),_cp1251'') AS `Partner_Email`,nullif(trim(`p`.`Login_ID`),_cp1251'') AS `Partner_Login`,nullif(nullif(`tr`.`Date_Sent`,0),-(1)) AS `Date_Sent`,nullif(nullif(`tr`.`Tax_Year`,0),-(1)) AS `Tax_Year`,nullif(`tr`.`Tax_Quarter`,0) AS `Tax_Quarter_ID`,if((`tr`.`Tax_Quarter` = -(1)),'Not Applicable',if((`tr`.`Tax_Quarter` = 1),'1st Quarter',if((`tr`.`Tax_Quarter` = 2),'2nd Quarter',if((`tr`.`Tax_Quarter` = 3),'3rd Quarter',if((`tr`.`Tax_Quarter` = 4),'4th Quarter',NULL))))) AS `Tax_Quarter`,nullif(nullif(`tr`.`Tax_Month`,0),-(1)) AS `Tax_Month_ID`,if((`tr`.`Tax_Month` > 0),date_format(concat(_utf8'2012/',`tr`.`Tax_Month`,_utf8'/01'),_utf8'%M'),NULL) AS `Tax_Month`,`tr`.`Is_Locked` AS `Is_Locked`,nullif(nullif(`tr`.`Instructions_ID`,0),-(1)) AS `Instructions_ID`,nullif(`i`.`Instructions_Label`,_cp1251'') AS `Instructions`,nullif(nullif(`tr`.`Tax_Return_Status_ID`,0),-(1)) AS `Tax_Return_Status_ID`,nullif(trim(`trs`.`Tax_Return_Status_Label`),_cp1251'') AS `Tax_Return_Status`,nullif(`tr`.`990PF_Date`,0) AS `990PF_Date`,nullif(nullif(`tr`.`Date_Released`,0),-(1)) AS `Date_Released`,nullif(trim(`tr`.`Tax_Return_Notes`),_cp1251'') AS `Tax_Return_Notes`,nullif(`tr`.`Staff_ID`,0) AS `Staff_ID`,nullif(trim(`s`.`Staff_Name_Last`),_cp1251'') AS `Staff_Name_Last`,nullif(trim(`s`.`Staff_Name_First`),_cp1251'') AS `Staff_Name_First`,nullif(concat(trim(`s`.`Staff_Name_Last`),_cp1251', ',trim(`s`.`Staff_Name_First`)),_cp1251', ') AS `Staff_Name_Full`,nullif(trim(`s`.`Initials`),_cp1251'') AS `Staff_Initials`,nullif(trim(`s`.`Email`),_cp1251'') AS `Staff_Email`,nullif(trim(`s`.`Login_ID`),_cp1251'') AS `Staff_Login`,nullif(`tr`.`Reviewer_1_ID`,0) AS `Reviewer_1_ID`,nullif(trim(`r1`.`Staff_Name_Last`),_cp1251'') AS `Reviewer_1_Name_Last`,nullif(trim(`r1`.`Staff_Name_First`),_cp1251'') AS `Reviewer_1_Name_First`,nullif(concat(trim(`r1`.`Staff_Name_Last`),_cp1251', ',trim(`r1`.`Staff_Name_First`)),_cp1251', ') AS `Reviewer_1_Name_Full`,nullif(trim(`r1`.`Initials`),_cp1251'') AS `Reviewer_1_Initials`,nullif(trim(`r1`.`Email`),_cp1251'') AS `Reviewer_1_Email`,nullif(trim(`r1`.`Login_ID`),_cp1251'') AS `Reviewer_1_Login`,nullif(`tr`.`Reviewer_2_ID`,0) AS `Reviewer_2_ID`,nullif(trim(`r2`.`Staff_Name_Last`),_cp1251'') AS `Reviewer_2_Name_Last`,nullif(trim(`r2`.`Staff_Name_First`),_cp1251'') AS `Reviewer_2_Name_First`,nullif(concat(trim(`r2`.`Staff_Name_Last`),_cp1251', ',trim(`r2`.`Staff_Name_First`)),_cp1251', ') AS `Reviewer_2_Name_Full`,nullif(trim(`r2`.`Initials`),_cp1251'') AS `Reviewer_2_Initials`,nullif(trim(`r2`.`Email`),_cp1251'') AS `Reviewer_2_Email`,nullif(trim(`r2`.`Login_ID`),_cp1251'') AS `Reviewer_2_Login`,nullif(`tr`.`Stamp_Date`,0) AS `Stamp_Date`,nullif(nullif(`tr`.`Stamp_User`,0),-(1)) AS `Stamp_User`,nullif(trim(`su`.`Staff_Name_Last`),_cp1251'') AS `Stamp_User_Name_Last`,nullif(trim(`su`.`Staff_Name_First`),_cp1251'') AS `Stamp_User_Name_First`,nullif(concat(trim(`su`.`Staff_Name_Last`),_cp1251', ',trim(`su`.`Staff_Name_First`)),_cp1251', ') AS `Stamp_User_Name_Full`,nullif(trim(`su`.`Initials`),_cp1251'') AS `Stamp_User_Initials`,nullif(trim(`su`.`Email`),_cp1251'') AS `Stamp_User_Email`,nullif(trim(`su`.`Login_ID`),_cp1251'') AS `Stamp_User_Login`,nullif(`tr`.`Last_Edit_Date`,0) AS `Last_Edit_Date`,nullif(nullif(`tr`.`Last_Edit_User`,0),-(1)) AS `Last_Edit_User`,nullif(trim(`eu`.`Staff_Name_Last`),_cp1251'') AS `Last_Edit_User_Name_Last`,nullif(trim(`eu`.`Staff_Name_First`),_cp1251'') AS `Last_Edit_User_Name_First`,nullif(concat(trim(`eu`.`Staff_Name_Last`),_cp1251', ',trim(`eu`.`Staff_Name_First`)),_cp1251', ') AS `Last_Edit_User_Name_Full`,nullif(trim(`eu`.`Initials`),_cp1251'') AS `Last_Edit_User_Initials`,nullif(trim(`eu`.`Email`),_cp1251'') AS `Last_Edit_User_Email`,nullif(trim(`eu`.`Login_ID`),_cp1251'') AS `Last_Edit_User_Login` from ((((((((((((`tax_return` `tr` left join `client` `c` on((`tr`.`Client_ID` = `c`.`Client_ID`))) left join `tax_return_status` `trs` on((`tr`.`Tax_Return_Status_ID` = `trs`.`Tax_Return_Status_ID`))) left join `tax_form` `tf` on((`tr`.`Tax_Form_ID` = `tf`.`Tax_Form_ID`))) left join `staff` `p` on((`tr`.`Partner_ID` = `p`.`Staff_ID`))) left join `staff` `s` on((`tr`.`Staff_ID` = `s`.`Staff_ID`))) left join `staff` `r1` on((`tr`.`Reviewer_1_ID` = `r1`.`Staff_ID`))) left join `staff` `r2` on((`tr`.`Reviewer_2_ID` = `r2`.`Staff_ID`))) left join `staff` `su` on((`tr`.`Stamp_User` = `su`.`Staff_ID`))) left join `staff` `eu` on((`tr`.`Last_Edit_User` = `eu`.`Staff_ID`))) left join `instructions` `i` on((`tr`.`Instructions_ID` = `i`.`Instructions_ID`))) left join `client_status` `cs` on((`c`.`Client_Status_ID` = `cs`.`Client_Status_ID`))) left join `client_state_link` `csl` on((`c`.`Client_ID` = `csl`.`Client_ID`))) group by `tr`.`Tax_Return_ID`;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
