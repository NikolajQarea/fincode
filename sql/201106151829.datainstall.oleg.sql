ALTER TABLE `return_status_history` CHANGE `status_id` `id` INT( 11 ) NOT NULL AUTO_INCREMENT;
ALTER TABLE `return_status_history` ADD `return_tax_id` INT( 11 ) NOT NULL AFTER `id`;
ALTER TABLE `return_status_history` CHANGE `status_name` `status_id` INT( 11 ) NOT NULL;
ALTER TABLE `return_status_history` DROP `transmitted` ,
DROP `review_status` ;

CREATE TABLE `ggg`.`status` (
`status_id` INT( 11 ) NOT NULL ,
`field_required` VARCHAR( 50 ) NOT NULL ,
`values` TEXT NULL
) ENGINE = MYISAM ;
