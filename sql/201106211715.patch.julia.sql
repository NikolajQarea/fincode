CREATE TABLE IF NOT EXISTS `status_name` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(100) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=15 ;

INSERT INTO `status_name` (`status_id`, `status_name`) VALUES
(1, 'active'),
(2, 'inactive'),
(3, 'info received'),
(4, 'in doc'),
(5, 'bookmarked'),
(6, 'date assigned'),
(7, 'staff'),
(8, 'missing info'),
(9, 'ready for review'),
(10, 'returned for revision'),
(11, 'reviewed'),
(12, 'review status'),
(13, 'collated'),
(14, 'transmitted');
