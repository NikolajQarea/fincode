CREATE TABLE  `occupation` (
`id_occupation` INT( 11 ) NOT NULL AUTO_INCREMENT ,
`name_occupation` VARCHAR( 100 ) NOT NULL ,
PRIMARY KEY (  `id_occupation` )
) ENGINE = MYISAM ;

INSERT INTO  `occupation` (
`id_occupation` ,
`name_occupation`
)
VALUES (
'1',  'Teacher'
), (
'2',  'Contractor/Construction'
);

INSERT INTO  `occupation` (
`id_occupation` ,
`name_occupation`
)
VALUES (
'3',  'Dan Coughlan'
), (
'4',  'Indobox'
);

INSERT INTO  `occupation` (
`id_occupation` ,
`name_occupation`
)
VALUES (
'5',  'Information services'
), (
'6',  'MKM Capital Advisors'
);

INSERT INTO  `occupation` (
`id_occupation` ,
`name_occupation`
)
VALUES (
'7',  'Trust'
);
