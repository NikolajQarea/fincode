CREATE TABLE `ggg`.`return_status_history` (
`status_id` INT( 11 ) NOT NULL AUTO_INCREMENT ,
`status_name` VARCHAR( 50 ) NOT NULL ,
`create_date` INT( 11 ) NOT NULL ,
`transmitted` TEXT NOT NULL ,
`review_status` TEXT NOT NULL ,
`change_user_id` INT( 11 ) NOT NULL ,
`change_date` INT NOT NULL ,
PRIMARY KEY ( `status_id` )
) ENGINE = MYISAM ;
