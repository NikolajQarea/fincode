ALTER TABLE `return_status_history` CHANGE `return_tax_id` `tax_return_id` INT( 11 ) NOT NULL;
ALTER TABLE `return_status_history` DROP `status_data`;
ALTER TABLE `return_status_history` CHANGE `status_id` `status_name` VARCHAR( 100 ) NOT NULL;
ALTER TABLE `return_status_history` DROP `create_date`;
ALTER TABLE `return_status_history` CHANGE `change_user_id` `modified_by` INT( 11 ) NOT NULL;
ALTER TABLE `return_status_history` CHANGE `change_date` `modified_date` INT( 11 ) NOT NULL;
ALTER TABLE `return_status_history` ADD `date` INT( 11 ) NULL ,
ADD `staff` VARCHAR( 10 ) NULL ,
ADD `transmitted` VARCHAR( 10 ) NULL ,
ADD `review_status` VARCHAR( 100 ) NULL;
ALTER TABLE `tax_return` CHANGE `status` `status` VARCHAR( 255 ) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL;