ALTER TABLE  `client` CHANGE  `name`  `company_name` VARCHAR( 255 ) CHARACTER SET cp1251 COLLATE cp1251_general_ci NOT NULL;
ALTER TABLE  `client` ADD  `last_name` VARCHAR( 255 ) NOT NULL AFTER  `company_name`;
ALTER TABLE  `client` ADD  `first_name` VARCHAR( 255 ) NOT NULL AFTER  `last_name`
