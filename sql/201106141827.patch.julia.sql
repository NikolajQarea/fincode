-- phpMyAdmin SQL Dump
-- version 3.2.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 14, 2011 at 06:26 PM
-- Server version: 5.1.40
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `ggg`
--

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `id_client` int(11) NOT NULL AUTO_INCREMENT,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `input_date` int(11) NOT NULL,
  `edited_date` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `edited_by` int(11) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_spouse` varchar(100) NOT NULL,
  `first_spouse` varchar(100) NOT NULL,
  `form` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `staff` varchar(100) NOT NULL,
  `entity` varchar(100) NOT NULL,
  `partner` enum('MG','RG','MJG') NOT NULL,
  `billing_client` varchar(100) NOT NULL,
  `doc_id` int(20) NOT NULL,
  `month` int(2) NOT NULL,
  `initial_year` int(4) NOT NULL,
  `retainer_year` int(4) NOT NULL,
  `outlook` tinyint(1) NOT NULL,
  `retainer` varchar(100) NOT NULL,
  `accounting` varchar(100) NOT NULL,
  `principal` varchar(100) NOT NULL,
  `ssn_ein` int(20) NOT NULL,
  `ssn_spouse` int(20) NOT NULL,
  `referred_by` varchar(100) NOT NULL,
  `occupation` varchar(100) NOT NULL,
  `occupation_spouse` varchar(100) NOT NULL,
  `inactive_date` int(11) NOT NULL,
  `notes` text NOT NULL,
  PRIMARY KEY (`id_client`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id_client`, `status`, `input_date`, `edited_date`, `added_by`, `edited_by`, `last_name`, `first_name`, `last_spouse`, `first_spouse`, `form`, `state`, `staff`, `entity`, `partner`, `billing_client`, `doc_id`, `month`, `initial_year`, `retainer_year`, `outlook`, `retainer`, `accounting`, `principal`, `ssn_ein`, `ssn_spouse`, `referred_by`, `occupation`, `occupation_spouse`, `inactive_date`, `notes`) VALUES
(4, 'active', 1308036565, 1308063382, 8, 8, 'q', 'q', '', '', '', '', 'ET', '1065', 'RG', '', 0, 4, 2000, 2000, 0, 'individual', 'Quicbooks', 'q', 11, 0, '', 'Contractor/Construction', '', 0, ' '),
(7, 'active', 1308043176, 1308049958, 8, 8, 'r', 'rsss', '', '', '1040', 'AZ,LA', 'ET', '1065', 'RG', '', 0, 1, 2000, 2000, 0, 'individual', 'Quicbooks', 'r', 3, 0, '', 'Contractor/Construction', '', 0, ' '),
(10, 'active', 1308043804, 0, 8, 0, 'sss', 'sss', '', '', '', 'CT,', 'ET', '1065', 'RG', '', 0, 1, 2000, 2000, 0, 'individual', 'Quickbooks', 'ss', 33333, 0, '', 'Contractor/Construction', '', 0, ' '),
(11, 'active', 1308044050, 0, 8, 0, 'dd', 'ddd', '', '', '1041,', '', 'ET', '1065', 'RG', '', 0, 1, 2000, 2000, 0, 'individual', 'Quickbooks', 'dddd', 23, 0, '', 'Contractor/Construction', '', 0, ' '),
(15, 'active', 1308045021, 0, 8, 0, 'gg', 'gg', '', '', '', '', 'ET', '1065', 'RG', '', 0, 1, 2000, 2000, 0, 'individual', 'Quickbooks', 'we', 3, 0, '', 'Contractor/Construction', '', 0, ' '),
(13, 'active', 1308044409, 0, 8, 0, 'fdg', 'ffg', '', '', '1040,1041,', '', 'ET', '1065', 'RG', '', 0, 1, 2000, 2000, 0, 'individual', 'Quickbooks', 'ee', 23, 0, '', 'Contractor/Construction', '', 0, ' '),
(16, 'active', 1308045088, 0, 8, 0, 'dd', 'd', '', '', '', '', 'ET', '1065', 'RG', '', 0, 1, 2000, 2000, 0, 'individual', 'Quickbooks', 'd', 4, 0, '', 'Contractor/Construction', '', 0, ' '),
(17, 'active', 1308045200, 0, 8, 0, 'dd', 'ddd', '', '', '1041', '', 'ET', '1065', 'RG', '', 0, 1, 2000, 2000, 0, 'individual', 'Quickbooks', 'dfd', 45, 0, '', 'Contractor/Construction', '', 0, ' '),
(18, 'active', 1308045246, 0, 8, 0, 'dsd', 'sds', '', '', '1120', 'LA,NJ,NY,', 'ET', '1065', 'RG', '', 0, 1, 2000, 2000, 0, 'individual', 'Quickbooks', 'dd', 0, 0, '', 'Contractor/Construction', '', 0, ' '),
(19, 'active', 1308045887, 0, 8, 0, 'rr', 'r', '', '', '1040,1120', '', 'ET', '1065', 'RG', '', 0, 1, 2000, 2000, 0, 'individual', 'Quickbooks', 'd', 5, 0, '', 'Contractor/Construction', '', 0, ' '),
(20, 'active', 1308046345, 0, 8, 0, 'ete', 'ere', '', '', '709,1041', 'MA,PA,RI', 'ET', '1065', 'RG', '', 0, 1, 2000, 2000, 0, 'individual', 'Quickbooks', 'ffg', 34, 0, '', 'Contractor/Construction', '', 0, ' ');

-- --------------------------------------------------------

--
-- Table structure for table `tax_return`
--

CREATE TABLE IF NOT EXISTS `tax_return` (
  `id_tax_return` int(11) NOT NULL AUTO_INCREMENT,
  `id_client` int(11) NOT NULL,
  `partner` varchar(100) NOT NULL,
  `form` varchar(50) NOT NULL,
  `tax_year` int(4) NOT NULL,
  `tax_month` int(2) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `notes` text NOT NULL,
  `instructions` text NOT NULL,
  `sent_date` int(11) NOT NULL,
  `released_date` int(11) NOT NULL,
  PRIMARY KEY (`id_tax_return`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tax_return`
--


-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(100) NOT NULL,
  `pwd` varchar(100) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `type` enum('admin','editor','reader') NOT NULL,
  `date_registered` int(11) NOT NULL,
  `removed` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `login`, `pwd`, `mail`, `type`, `date_registered`, `removed`) VALUES
(8, 'Julia', 'qwerty', 'qwe@maaa.maa', 'admin', 1308036155, 0),
(9, 'Anna', '111111', 'anna@mail.bb', 'editor', 1307718088, 0),
(17, 'qw', 'sss', 'ss@w.s', 'editor', 1307708667, 0);
