-- phpMyAdmin SQL Dump
-- version 3.2.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 14, 2011 at 12:02 PM
-- Server version: 5.1.40
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `ggg`
--

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `id_c` int(11) NOT NULL AUTO_INCREMENT,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `input_date` int(11) NOT NULL,
  `id_u` int(11) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_spouse` varchar(100) NOT NULL,
  `first_spouse` varchar(100) NOT NULL,
  `form` enum('990','1041','1040','1065','1120','1120S','941','709','940','942','1098','1099','1040NR','5500','8027','990PF','IT-201','IT-204LL','IT-205','NYCCRA','NYS45','ST100','ST101','ST809','ST810') NOT NULL,
  `state` varchar(100) NOT NULL,
  `staff` varchar(100) NOT NULL,
  `entity` varchar(100) NOT NULL,
  `partner` enum('MG','RG','MJG') NOT NULL,
  `billing_client` varchar(100) NOT NULL,
  `doc_id` int(20) NOT NULL,
  `month` int(2) NOT NULL,
  `initial_year` int(4) NOT NULL,
  `retainer_year` int(4) NOT NULL,
  `outlook` tinyint(1) NOT NULL,
  `retainer` varchar(100) NOT NULL,
  `accounting` varchar(100) NOT NULL,
  `principal` varchar(100) NOT NULL,
  `ssn_ein` int(20) NOT NULL,
  `ssn_spouse` int(20) NOT NULL,
  `referred_by` varchar(100) NOT NULL,
  `occupation` varchar(100) NOT NULL,
  `occupation_spouse` varchar(100) NOT NULL,
  `inactive_date` int(11) NOT NULL,
  `notes` text NOT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id_c`, `status`, `input_date`, `id_u`, `last_name`, `first_name`, `last_spouse`, `first_spouse`, `form`, `state`, `staff`, `entity`, `partner`, `billing_client`, `doc_id`, `month`, `initial_year`, `retainer_year`, `outlook`, `retainer`, `accounting`, `principal`, `ssn_ein`, `ssn_spouse`, `referred_by`, `occupation`, `occupation_spouse`, `inactive_date`, `notes`) VALUES
(3, 'active', 1308036524, 8, 'aa', 'aa', '', '', '', '', 'ET', '1065', 'RG', '', 0, 1, 2000, 2000, 0, 'individual', 'Quickbooks', 'q', 1, 0, '', 'Contractor/Construction', '', 0, ' '),
(4, 'active', 1308036565, 8, 'q', 'q', '', '', '', '', 'ET', '1065', 'RG', '', 0, 4, 2000, 2000, 0, 'individual', 'Quickbooks', 'q', 11, 0, '', 'Contractor/Construction', '', 0, ' ');

-- --------------------------------------------------------

--
-- Table structure for table `tax_return`
--

DROP TABLE IF EXISTS `tax_return`;
CREATE TABLE IF NOT EXISTS `tax_return` (
  `id_t` int(11) NOT NULL AUTO_INCREMENT,
  `id_c` int(11) NOT NULL,
  `partner` varchar(100) NOT NULL,
  `form` varchar(50) NOT NULL,
  `tax_year` int(4) NOT NULL,
  `tax_month` int(2) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `notes` text NOT NULL,
  `instructions` text NOT NULL,
  `sent_date` int(11) NOT NULL,
  `released_date` int(11) NOT NULL,
  PRIMARY KEY (`id_t`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tax_return`
--


-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id_u` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(100) NOT NULL,
  `pwd` varchar(100) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `type` enum('admin','editor','reader') NOT NULL,
  `date_registered` int(11) NOT NULL,
  `removed` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_u`)
) ENGINE=MyISAM  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_u`, `login`, `pwd`, `mail`, `type`, `date_registered`, `removed`) VALUES
(8, 'Julia', 'qwerty', 'qwe@maaa.maa', 'admin', 1308036155, 0),
(9, 'Anna', '111111', 'anna@mail.bb', 'editor', 1307718088, 0),
(17, 'qw', 'sss', 'ss@w.s', 'editor', 1307708667, 0);
